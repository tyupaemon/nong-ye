#pragma once
#include "../DX12/d3dx12.h"
#include<DirectXMath.h>
#include<vector>

#include <dxgi1_4.h>
#include <windows.h>

#include <string>
#include <wrl.h>
#include <process.h>
#include <shellapi.h>

using Microsoft::WRL::ComPtr;
using namespace Microsoft::WRL;

class SimpleObjectData;

struct ID3D12PipelineState;
struct ID3D12RootSignature;

struct TimeData
{
	float time;
};

class CreateSimpleObject
{
private:
	ID3D12PipelineState * _pipelineStateONAlpha = nullptr;//アルファあり
	ID3D12PipelineState * _pipelineStateOFFAlpha = nullptr;//アルファなし

	//shadowmap描画用///////////////////////////////////////////

	//shadowmap用PipelineState
	ComPtr<ID3D12PipelineState> _pipelineStateSM = nullptr;
	//shadowmap用VS
	ID3DBlob* _smvs;
	//shadowmap用PS
	ID3DBlob* _smps;

	//~shadowmap描画用//////////////////////////////////////////


	std::vector<ID3DBlob*> _vs;
	std::vector<ID3DBlob*> _ps;

	//リソース
	std::vector<ID3D12Resource*> _textureResource;//テクスチャリソース
	std::vector<ID3D12DescriptorHeap*> _descriptorHeapSRV;

	UINT _descriptorSize;

	void LoadTexture(const wchar_t* path, ID3D12Resource*& texture);

public:
	CreateSimpleObject();
	~CreateSimpleObject();

	//パイプラインステートの初期化
	void Init(ID3D12RootSignature*& rootSignature);

	//パイプラインステートのセット
	void SetDrawMode();

	//平面のオブジェクト作成
	SimpleObjectData* CreateBoard(DirectX::XMFLOAT3 center,float width, float depth,float r, float g, float b);

	//水面用の平面オブジェクト作成
	SimpleObjectData* CreateWaterSurface(DirectX::XMFLOAT3 center, float width, float depth, float r, float g, float b);

	//PipelineStateのゲッター

	ID3D12PipelineState*& GetPipelineStateONAlpha() { return _pipelineStateONAlpha;}
	ID3D12PipelineState*& GetPipelineStateOFFAlpha() { return _pipelineStateOFFAlpha; }
	//shadowmap用
	ComPtr<ID3D12PipelineState>& GetPipelineStateSM() { return _pipelineStateSM; }

	ID3D12DescriptorHeap*& GetDescriptorHeap(int type) { return _descriptorHeapSRV[type]; }
};

