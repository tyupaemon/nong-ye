#include "WaterSurface.h"
#include"../DX12/DirectX12Device.h"
#include <Windows.h>
#include<vector>



WaterSurface::WaterSurface(DirectX::XMFLOAT3 center, float width, float depth, float r, float g, float b,
	ID3D12PipelineState*& pipelineState, ID3D12DescriptorHeap*& descriptorHeapSRV)
	:_descriptorHeapSRV(descriptorHeapSRV)
{
	_pipelineState = pipelineState;
	_center = center;

	//矩形構造体に値を入れる
	_rect.x = _center.x;
	_rect.y = _center.z;
	_rect.w = width;
	_rect.h = depth;

	std::vector<SimpleObjectVertex> vertices;
	//pos,normal,rgb,tangent
	vertices.push_back(SimpleObjectVertex(
		- width / 2, 0, depth / 2,
		0, 1, 0,
		0, 0,
		r, g, b,
		0,1,1));

	vertices.push_back(SimpleObjectVertex(
		width / 2, 0,depth / 2,
		0, 1, 0,
		1, 0,
		r, g, b,
		0, 1, 1));

	vertices.push_back(SimpleObjectVertex(
		- width / 2, 0, - depth / 2,
		0, 1, 0,
		0, 1,
		r, g, b,
		0, 1, 1));

	vertices.push_back(SimpleObjectVertex(
		width / 2,0, - depth / 2,
		0, 1, 0,
		1, 1,
		r, g, b,
		0, 1, 1));

	

	HRESULT result = S_OK;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	//リソース生成
	result = device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertices.size() * sizeof(SimpleObjectVertex)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vb)
	);

	SimpleObjectVertex* vertexData = nullptr;
	result = _vb->Map(0, nullptr, (void**)&vertexData);
	std::copy(vertices.begin(), vertices.end(), vertexData);
	_vb->Unmap(0, nullptr);

	_vbView = {};
	_vbView.BufferLocation = _vb->GetGPUVirtualAddress();
	_vbView.StrideInBytes = sizeof(SimpleObjectVertex);
	_vbView.SizeInBytes = vertices.size() * sizeof(SimpleObjectVertex);

	//座標X,Y,Z
	pos.m128_f32[0] += _center.x;
	pos.m128_f32[1] += _center.y;
	pos.m128_f32[2] += _center.z;

	//定数バッファ
	D3D12_HEAP_PROPERTIES properties = {};
	D3D12_DESCRIPTOR_HEAP_DESC constantBufferDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC constantBufferViewDesc = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	constantBufferDescriptorHeapDesc.NumDescriptors = 1;
	constantBufferDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	constantBufferDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	properties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	properties.VisibleNodeMask = 1;
	properties.CreationNodeMask = 1;
	properties.Type = D3D12_HEAP_TYPE_UPLOAD;
	//定数バッファの生成
	result = dx12dev.Device()->CreateCommittedResource(&properties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(TimeData) + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_constantBuffer));
	//定数バッファ用ディスクリプタヒープ生成
	result = dx12dev.Device()->CreateDescriptorHeap(&constantBufferDescriptorHeapDesc,
		IID_PPV_ARGS(&_constantBufferDescHeap));

	//タイムデータをマップしておく
	D3D12_RANGE range = {};
	result = _constantBuffer->Map(0, &range, (void**)&_timeData);
	assert(SUCCEEDED(result));
	memcpy(&_timeData->time, &_time, sizeof(float));

	constantBufferViewDesc.BufferLocation = _constantBuffer->GetGPUVirtualAddress();
	constantBufferViewDesc.SizeInBytes = (sizeof(TimeData) + 0xff)&~0xff;
	dx12dev.Device()->CreateConstantBufferView(&constantBufferViewDesc, _constantBufferDescHeap->GetCPUDescriptorHandleForHeapStart());

}

WaterSurface::~WaterSurface()
{
}
void WaterSurface::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	auto cmdList = dx12dev.CmdList();
	dx12dev.CmdList()->SetPipelineState(_pipelineState);
	//ディスクリプタヒープテーブルの設定
	auto handle = _descriptorHeapSRV->GetGPUDescriptorHandleForHeapStart();

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbMDdataDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV3, pcbMDdataDescHeap->GetGPUDescriptorHandleForHeapStart());

	_time++;
	memcpy(&_timeData->time, &_time, sizeof(float));
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_constantBufferDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV2, _constantBufferDescHeap->GetGPUDescriptorHandleForHeapStart());


	pDModelData->world = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z)*XMMatrixTranslation(pos.m128_f32[0], pos.m128_f32[1], pos.m128_f32[2]);

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(ViewBoard, handle);

	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	cmdList->IASetVertexBuffers(0, 1, &_vbView);
	cmdList->DrawInstanced(4, 1, 0, 0);
}
void WaterSurface::Update()
{
	//DirectX12Device& dx12dev = DirectX12Device::Instance();

}