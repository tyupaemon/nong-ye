#pragma once
#include"SimpleObjectData.h"
#include"../Goto/RenderableObject.h"
#include<d3d12.h>

class WaterSurface :
	public SimpleObjectData,public RenderableObject
{
private:
	ID3D12Resource * _vb = nullptr;
	D3D12_VERTEX_BUFFER_VIEW _vbView;
	ID3D12DescriptorHeap*& _descriptorHeapSRV;

	DirectX::XMFLOAT3 _center;//中心

	Rect _rect;//矩形型
	struct TimeData
	{
		float time;
	};
	TimeData* _timeData;
	float _time = 0;
	ID3D12Resource* _constantBuffer = nullptr;//定数バッファ
	ID3D12DescriptorHeap* _constantBufferDescHeap = nullptr;//定数バッファ用ディスクリプタヒープ


public:
	WaterSurface(DirectX::XMFLOAT3 center, float width, float depth, float r, float g, float b,
		ID3D12PipelineState*& pipelineState, ID3D12DescriptorHeap*& descriptorHeapSRV);
	~WaterSurface();
	void Draw();
	void Update();
	Rect GetRect() { return _rect; }
};

