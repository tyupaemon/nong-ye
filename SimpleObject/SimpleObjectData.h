#pragma once
#include<DirectXMath.h>
#include<d3d12.h>
#include"../Goto/RenderableObject.h"
#include"../Imamuu/Geometry.h"

struct SimpleObjectVertex 
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 normal;
	DirectX::XMFLOAT2 uv;
	DirectX::XMFLOAT3 color;
	DirectX::XMFLOAT3 tangent;

	SimpleObjectVertex()
	{
		pos = DirectX::XMFLOAT3(0, 0, 0);
		normal = DirectX::XMFLOAT3(0, 0, 0);
		uv = DirectX::XMFLOAT2(0, 0);
		color = DirectX::XMFLOAT3(0, 0, 0);
		tangent = DirectX::XMFLOAT3(0, 1, 1);

	}
	SimpleObjectVertex(DirectX::XMFLOAT3& p, DirectX::XMFLOAT3& n,
		DirectX::XMFLOAT2& u, DirectX::XMFLOAT3& c,DirectX::XMFLOAT3& t)
	{
		pos = p;
		normal = n;
		uv = u;
		color = c;
		tangent = t;
	}
	SimpleObjectVertex(float x,float y,float z,
		float nx,float ny,float nz,
		float u,float v,float r,float g,float b,
		float tx,float ty,float tz)
	{
		pos = DirectX::XMFLOAT3(x, y, z);
		normal = DirectX::XMFLOAT3(nx, ny, nz);
		uv = DirectX::XMFLOAT2(u, v);
		color= DirectX::XMFLOAT3(r,g,b);
		tangent = DirectX::XMFLOAT3(tx, ty, tz);
	}
};

enum SimpleObjectType
{
	SimpleGrassType,//地面
	SimpleWaterSurface,//水面
	SimpleNormalMap,//ノーマルマップ
	SimpleTypeMAX,
};

class SimpleObjectData : Geometry
{
protected:
	ID3D12PipelineState* _pipelineState;

public:
	SimpleObjectData();
	~SimpleObjectData();
	//virtual void Draw() = 0;
	//virtual void Update() = 0;
	virtual Rect GetRect() = 0;

	//PipelineStateをセット
	void SetPipelineState(ID3D12PipelineState*& ps) { _pipelineState = ps; };
};

