#pragma once
#include"SimpleObjectData.h"
#include"../Goto//RenderableObject.h"
#include<d3d12.h>

class Board :
	public SimpleObjectData, public RenderableObject
{
private:
	ID3D12Resource * _vb = nullptr;
	D3D12_VERTEX_BUFFER_VIEW _vbView;
	ID3D12DescriptorHeap*& _descriptorHeapSRV;

	DirectX::XMFLOAT3 _center;//���S

	Rect _rect;


public:
	Board(DirectX::XMFLOAT3 center,float width,float depth,float r,float g,float b,
		ID3D12PipelineState*& pipelineState, ID3D12DescriptorHeap*& descriptorHeapSRV);
	~Board();
	void Draw();
	void Update();
	Rect GetRect() { return _rect; }
};

