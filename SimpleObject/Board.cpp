#include "Board.h"
#include"../DX12/DirectX12Device.h"
#include <Windows.h>
#include<vector>


Board::Board(DirectX::XMFLOAT3 center,float width,float depth,float r,float g,float b,
	ID3D12PipelineState*& pipelineState, ID3D12DescriptorHeap*& descriptorHeapSRV)
	:_descriptorHeapSRV(descriptorHeapSRV)
{
	_pipelineState = pipelineState;
	_center = center;

	std::vector<SimpleObjectVertex> vertices;
	//pos,normal,rgb,tangent
	vertices.push_back(SimpleObjectVertex(
		-width / 2, 0, depth / 2,
		0, 1, 0,
		0, 0,
		r, g, b,
		0, 1, 1));

	vertices.push_back(SimpleObjectVertex(
		width / 2, 0, depth / 2,
		0, 1, 0,
		1, 0,
		r, g, b,
		0, 1, 1));

	vertices.push_back(SimpleObjectVertex(
		-width / 2, 0, -depth / 2,
		0, 1, 0,
		0, 1,
		r, g, b,
		0, 1, 1));

	vertices.push_back(SimpleObjectVertex(
		width / 2, 0, -depth / 2,
		0, 1, 0,
		1, 1,
		r, g, b,
		0, 1, 1));

	HRESULT result = S_OK;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	//リソース生成
	result = device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertices.size() * sizeof(SimpleObjectVertex)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vb)
	);

	SimpleObjectVertex* vertexData = nullptr;
	result = _vb->Map(0, nullptr, (void**)&vertexData);
	std::copy(vertices.begin(), vertices.end(), vertexData);
	_vb->Unmap(0,nullptr);

	_vbView = {};
	_vbView.BufferLocation = _vb->GetGPUVirtualAddress();
	_vbView.StrideInBytes = sizeof(SimpleObjectVertex);
	_vbView.SizeInBytes = vertices.size() * sizeof(SimpleObjectVertex);

	//座標X,Y,Z
	pos.m128_f32[0] += _center.x;
	pos.m128_f32[1] += _center.y;
	pos.m128_f32[2] += _center.z;

}


Board::~Board()
{
}

void Board::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	auto cmdList = dx12dev.CmdList();
	dx12dev.CmdList()->SetPipelineState(_pipelineState);
	//ディスクリプタヒープテーブルの設定
	auto handle = _descriptorHeapSRV->GetGPUDescriptorHandleForHeapStart();


	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbMDdataDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV3, pcbMDdataDescHeap->GetGPUDescriptorHandleForHeapStart());

	pDModelData->world = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z)*XMMatrixTranslation(pos.m128_f32[0], pos.m128_f32[1], pos.m128_f32[2]);

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(ViewBoard, handle);

	//shadowmap
	handle.ptr += dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(ShadowMap, handle);


	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	cmdList->IASetVertexBuffers(0, 1, &_vbView);
	cmdList->DrawInstanced(4, 1, 0, 0);
}

void Board::Update()
{
	//DirectX12Device& dx12dev = DirectX12Device::Instance();

}