#include "CreateSimpleObject.h"
#include <windows.h>
#include <tchar.h>
#include <d3dcompiler.h>


#include"../DX12/DirectX12Device.h"
#include "../DX12/common.h"
#include"Board.h"
#include"WaterSurface.h"

CreateSimpleObject::CreateSimpleObject()
{
}


CreateSimpleObject::~CreateSimpleObject()
{
}

//パイプラインステートの初期化
void CreateSimpleObject::Init(ID3D12RootSignature*& rootSignature)
{
	HRESULT result = S_OK;

	//シェーダ読み込み
	ID3DBlob* error;
	_vs.resize(SimpleTypeMAX);
	_ps.resize(SimpleTypeMAX);

	//地面用
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimpleVS", "vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_vs[SimpleGrassType], nullptr);
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr, 
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimplePS", "ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps[SimpleGrassType], nullptr);

	//水面用
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "WaterSurfaceVS", "vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_vs[SimpleWaterSurface], nullptr);
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "WaterSurfacePS", "ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps[SimpleWaterSurface], nullptr);

	//shadowmap用
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimpleSMVS", "vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_smvs, nullptr);
	result = D3DCompileFromFile(_T("../SimpleObject/SimpleObjectShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimpleSMPS", "ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_smps, nullptr);

	//D3DDECLUSAGE_TANGENT
	D3D12_INPUT_ELEMENT_DESC inputDescs[] = 
	{
		{ "POSITION" ,0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "NORMAL" ,0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TEXCOORD" ,0,DXGI_FORMAT_R32G32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "COLOR" ,0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TANGENT" ,0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	};

	//パイプラインステートを生成
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();
	//アルファブレンドなし
	{
		D3D12_GRAPHICS_PIPELINE_STATE_DESC desc = {};
		desc.VS = CD3DX12_SHADER_BYTECODE(_vs[SimpleGrassType]);
		desc.PS = CD3DX12_SHADER_BYTECODE(_ps[SimpleGrassType]);
		desc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		desc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		desc.DepthStencilState.DepthEnable = true;
		desc.DepthStencilState.StencilEnable = false;
		desc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		desc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
		desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		desc.NumRenderTargets = 1;
		desc.InputLayout.NumElements = sizeof(inputDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
		desc.InputLayout.pInputElementDescs = inputDescs;
		desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.SampleDesc.Count = 1;
		desc.pRootSignature = rootSignature;
		desc.SampleMask = 0xffffffff;
		result = device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&_pipelineStateOFFAlpha));
	}

	//パイプラインステートを生成
	//アルファブレンドあり
	{
		D3D12_GRAPHICS_PIPELINE_STATE_DESC desc = {};
		desc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		desc.BlendState.AlphaToCoverageEnable = false;
		desc.BlendState.IndependentBlendEnable = false;

		D3D12_RENDER_TARGET_BLEND_DESC rarget = {};
		ZeroMemory(&rarget, sizeof(rarget));
		rarget.BlendEnable = true;
		rarget.SrcBlend = D3D12_BLEND_SRC_ALPHA;
		rarget.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		rarget.BlendOp = D3D12_BLEND_OP_ADD;
		rarget.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
		rarget.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
		rarget.BlendOpAlpha = D3D12_BLEND_OP_ADD;
		rarget.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
		for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++) {
			desc.BlendState.RenderTarget[i] = rarget;
		}

		desc.DepthStencilState.DepthEnable = true;
		desc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		desc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		desc.DepthStencilState.StencilEnable = true;
		desc.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
		desc.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;
		
		desc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		
		desc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
		desc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		
		desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		desc.VS = CD3DX12_SHADER_BYTECODE(_vs[SimpleWaterSurface]);
		desc.PS = CD3DX12_SHADER_BYTECODE(_ps[SimpleWaterSurface]);
		desc.InputLayout.NumElements = sizeof(inputDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
		desc.InputLayout.pInputElementDescs = inputDescs;
		desc.pRootSignature = dx12dev.RootSignature();
		desc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		desc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;

		desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		desc.SampleDesc.Count = 1;
		desc.NumRenderTargets = 1;
		desc.SampleMask = 0xffffffff;

		result = device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&_pipelineStateONAlpha));
	}

	//パイプラインステートを生成
	//shadowmap用
	{
		D3D12_GRAPHICS_PIPELINE_STATE_DESC desc = {};
		desc.VS = CD3DX12_SHADER_BYTECODE(_smvs);
		desc.PS = CD3DX12_SHADER_BYTECODE(_smps);
		desc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		desc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		desc.DepthStencilState.DepthEnable = true;
		desc.DepthStencilState.StencilEnable = false;
		desc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		desc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
		desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		desc.NumRenderTargets = 1;
		desc.InputLayout.NumElements = sizeof(inputDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
		desc.InputLayout.pInputElementDescs = inputDescs;
		desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.SampleDesc.Count = 1;
		desc.pRootSignature = rootSignature;
		desc.SampleMask = 0xffffffff;
		result = device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&_pipelineStateSM));
	}


	//テクスチャ読み込み,ノーマルマップリソース生成
	_textureResource.resize(SimpleTypeMAX);
	LoadTexture(_T("Resource/今村用素材/床/地面(草).jpg"), _textureResource[SimpleGrassType]);
	LoadTexture(_T("Resource/今村用素材/床/水面.jpg"), _textureResource[SimpleWaterSurface]);
	//LoadTexture(_T("Resource/今村用素材/床/normal.bmp"), _textureResource[SimpleNormalMap]);
	LoadTexture(_T("Resource/今村用素材/床/block_normal.png"), _textureResource[SimpleNormalMap]);
	//LoadTexture(_T("Resource/今村用素材/床/水面_normal.png"), _textureResource[SimpleNormalMap]);


	//_descriptorSize = device->GetDescriptorHandleIncrementSize[D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV];

	_descriptorHeapSRV.resize(SimpleTypeMAX);
	//SRV用のディスクリプタヒープを生成
	{
		D3D12_DESCRIPTOR_HEAP_DESC desc = {};
		desc.NumDescriptors = 1 + 1;//texture + shadowmap
		desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

		for (int i = 0; i < SimpleTypeMAX; i++)
		{
			result = device->CreateDescriptorHeap
			(&desc, IID_PPV_ARGS(&_descriptorHeapSRV[i]));
		}

	}

	//SRVの設定
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC desc = {};
		desc.Format = DXGI_FORMAT_UNKNOWN;
		desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		desc.Texture2D.MipLevels = 1;
		desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		desc.Texture2D.MostDetailedMip = 0;
		desc.Texture2D.PlaneSlice = 0;
		desc.Texture2D.ResourceMinLODClamp = 0.0f;

		//SRVの生成
		D3D12_CPU_DESCRIPTOR_HANDLE handle;
		for (int i = 0; i < SimpleTypeMAX; i++)
		{
			desc.Format = DXGI_FORMAT_UNKNOWN;

			handle = _descriptorHeapSRV[i]->GetCPUDescriptorHandleForHeapStart();
			device->CreateShaderResourceView(_textureResource[i], &desc, handle);

			//shadowmap
			desc.Format = DXGI_FORMAT_R32_FLOAT;

			handle.ptr += device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
			device->CreateShaderResourceView(dx12dev.ShadowTex().Get(), &desc, handle);

		}
	}
}


void CreateSimpleObject::SetDrawMode()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	//dx12dev.CmdList()->SetPipelineState(_pipelineStateONAlpha);

	//ディスクリプタヒープテーブルの設定
	auto handle = _descriptorHeapSRV[SimpleNormalMap]->GetGPUDescriptorHandleForHeapStart();
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV[SimpleNormalMap]);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, handle);

	//handle.ptr += _descriptorSize;
}

//平面のオブジェクト作成
SimpleObjectData* CreateSimpleObject::CreateBoard(DirectX::XMFLOAT3 center, float width, float depth,float r,float g,float b)
{
	return new Board(center,width, depth,r,g,b, _pipelineStateOFFAlpha, _descriptorHeapSRV[SimpleGrassType]);
}
//水面用平面のオブジェクト作成
SimpleObjectData* CreateSimpleObject::CreateWaterSurface(DirectX::XMFLOAT3 center, float width, float depth, float r, float g, float b)
{
	return new WaterSurface(center, width, depth, r, g, b, _pipelineStateONAlpha, _descriptorHeapSRV[SimpleWaterSurface]);
}


void CreateSimpleObject::LoadTexture(const wchar_t* path,ID3D12Resource*& texture)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	std::unique_ptr<uint8_t[]> dec;
	D3D12_SUBRESOURCE_DATA sub;
	LoadWICTex(texture, sub, path, dec);
	D3D12_BOX box = {};
	box.front = 0;
	box.back = 1;
	box.top = box.left = 0;
	box.right = sub.RowPitch / 4;
	box.bottom = sub.SlicePitch / sub.RowPitch;
	HRESULT result = texture->WriteToSubresource(0, &box, &dec[0], box.right * 4, box.right*box.bottom * 4);
	assert(SUCCEEDED(result));

	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(texture,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();
}