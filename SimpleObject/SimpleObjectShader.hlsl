#include<../Project2/Shader/Header.hlsli>
Texture2D<float4> tex4:register(t4);
Texture2D<float4> normal:register(t0);

cbuffer material : register(b1) {
	float time;
}
cbuffer objData : register(b2) {
	matrix objWorld;
}
struct OutputSimple
{
	float4 svpos : SV_POSITION;
	float4 pos : POSITION;
	float3 normal : NORMAL; //法線ベクトル
	float2 uv : TEXCOORD;//テクスチャUV
	float3 color : COLOR;//色
	float3 L : TEXCOORD1;
	float3 E : TEXCOORD2;
};

//ShadowMap-------////////////////////////////////////////////////////////////////
//ShadowMap用VS
OutputSimple SimpleSMVS(float4 pos : POSITION, float3 normal : NORMAL,
	float2 uv : TEXCOORD, float3 color : COLOR, float3 tangent : TANGENT)
{

	OutputSimple o;
	o.svpos = mul(mul(LightVP, objWorld), pos);

	return o;
}

//ShadowMap用PS
float4 SimpleSMPS(OutputSimple o) : SV_Target
{
	float3 color = float3(1,0,0);

	return float4(color, 1);
}
//~ShadowMap-------////////////////////////////////////////////////////////////////


//床用
OutputSimple SimpleVS(float4 pos : POSITION, float3 normal : NORMAL,
	float2 uv : TEXCOORD, float3 color : COLOR,float3 tangent :TANGENT)
{

	OutputSimple o;
	o.svpos = mul(mul(viewproj, objWorld), pos);
	o.uv = uv;
	o.normal = normal;
	o.color = color;

	float3 N = o.normal;
	float3 T = tangent;
	float3 B = cross(N, T);


	float3 E = eye - o.svpos.xyz;
	o.E.x = dot(E, T);
	o.E.y = dot(E, B);
	o.E.z = dot(E, N);

	float4 light = float4(0.577f, -0.577f, -0.577f, 0.0f);
	float3 L = -light.xyz;
	o.L.x = dot(L, T);
	o.L.y = dot(L, B);
	o.L.z = dot(L, N);

	return o;
}

float4 SimplePS(OutputSimple o) : SV_Target
{

	float3 color = (tex4.Sample(smp2, o.uv));


	float3 N = 2.0f*normal.Sample(smp2,o.uv).rgb - 1.0;
	float3 L = normalize(o.L);
	float3 R = reflect(-normalize(o.E), N);
	float amb = 0.5;
	float3 p = pow(max(0, dot(R, L)),8);
	//return float4(color*max(0, dot(N, L) + amb) + 0.3*p, 1.0);

	//float3 color2 = pow(shadowmap.Sample(smp, o.uv),10);
	//return float4(color2, 1);

	return float4(tex4.Sample(smp2, o.uv).rgb*o.color, 0.6);
}


//水面用
OutputSimple WaterSurfaceVS(float4 pos : POSITION, float3 normal : NORMAL,
	float2 uv : TEXCOORD, float3 color : COLOR, float3 tangent : TANGENT)
{

	OutputSimple o;
	o.svpos = mul(mul(viewproj, objWorld), pos);
	o.uv = uv;
	o.normal = normal;
	o.color = color;
	o.uv.x = cos(time/360+o.uv.y)/2+1+uv.x;
	o.uv.y += time / 1000;

	float3 N = o.normal;
	float3 T = tangent;
	float3 B = cross(N, T);


	float3 E = eye - o.svpos.xyz;
	o.E.x = dot(E, T);
	o.E.y = dot(E, B);
	o.E.z = dot(E, N);

	float4 light = float4(0.577f, -0.577f, -0.577f, 0.0f);
	float3 L = -light.xyz;
	o.L.x = dot(L, T);
	o.L.y = dot(L, B);
	o.L.z = dot(L, N);


	return o;
}

float4 WaterSurfacePS(OutputSimple o) : SV_Target
{
	o.uv.x = cos(time / 1000 + o.uv.y * 3) / 2 + 1 + o.uv.x;
	o.uv.y += time / 1000;
	float3 color = (tex4.Sample(smp2, o.uv));
	return float4(color, 0.6);


	//float3 N = 2.0f*normal.Sample(smp2,o.uv).rgb-1.0;
	//float3 L = normalize(o.L);
	//float3 R = reflect(-normalize(o.E), N);
	//float amb = 0.5;
	//float3 p = pow(max(0, dot(R, L)),8);
	//return float4(color*max(0, dot(N, L) + amb)+0.3*p, 0.6);



	//return float4(tex4.Sample(smp2, o.uv).rgb*o.color, 0.6);
}