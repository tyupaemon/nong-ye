#include "FieldObject.h"
#include<DirectXMath.h>



FieldObject::FieldObject() :_house("Resource/_ê/house2.pmx"), _well("Resource/_ê/well.pmx"),
_waterTower("Resource/_ê/water_tower.pmx")
{
	DirectX::XMVECTOR pos;
	//ÀWX,Y,Z
	pos.m128_f32[0] = -30; pos.m128_f32[1] = 0; pos.m128_f32[2] = 25;
	_house.SetPos(pos);

	pos.m128_f32[0] = 0; pos.m128_f32[1] = 0; pos.m128_f32[2] = 20;
	_well.SetPos(pos);

	pos.m128_f32[0] = -30; pos.m128_f32[1] = 0; pos.m128_f32[2] = 55;
	_waterTower.SetPos(pos);
}


FieldObject::~FieldObject()
{
}

void FieldObject::Update()
{

}