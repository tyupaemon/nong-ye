#pragma once
#include "../DX12/common.h"
#include <functional>
struct Vertex {
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT2 uv;
};

class NoProj2DPolygon
{
private:
	//���_
	Vertex vertices[4] = {
	{ { -1.0f	,	-1.0f	,	0.0f },{ 0.0f,0.0f } },
	{ { 1.0f	,	-1.0f	,	0.0f },{ 1.0f,0.0f } },
	{ { -1.0f	,	1.0f	,	0.0f },{ 0.0f,1.0f } },
	{ { 1.0f	,	1.0f	,	0.0f },{ 1.0f,1.0f } },
	};
	Vertex* _pVert = nullptr;

	ID3D12Resource* _vertexBuf = nullptr;
	ID3D12DescriptorHeap* _descriptorHeapSRV = nullptr;
	ID3D12DescriptorHeap* _descriptorHeapCBV = nullptr;
	D3D12_VERTEX_BUFFER_VIEW vbView = {};
	ID3D12PipelineState* _pipelineState = nullptr;
	static ID3DBlob* vs;
	static ID3DBlob* ps;
	ID3D12Resource* _texBuf = nullptr;
	ID3D12DescriptorHeap* pcbDescHeap;

//	StrBufData* pBufData;
	ID3D12Resource* pConstBuf = nullptr;
	ID3D12DescriptorHeap* _cbDescHeap;
	CBdata* pcBuf;

	void CreateTex(std::wstring _file);
	void CreateVertBuf();
	void CreateConstBuf(D3D12_CPU_DESCRIPTOR_HANDLE& _srvHandle);

	std::vector<std::function<void()>> release;


public:
	void SetPos(float _x, float _y);
	void SetSize(float _size);
	NoProj2DPolygon();
	~NoProj2DPolygon();
	void draw();
	ID3D12Resource*& VertexBuf() { return _vertexBuf; };
	ID3D12DescriptorHeap*& DescriptorHeapSRV() { return _descriptorHeapSRV; };
	D3D12_VERTEX_BUFFER_VIEW& VertBufView() { return vbView; };
	ID3D12PipelineState*& PipelineState() { return _pipelineState; };
	ID3DBlob*& VS() { return vs; };
	ID3DBlob*& PS() { return ps; }
	ID3D12Resource*& TexBuf() { return _texBuf; };
	Vertex*& VertData() { return _pVert; };

	void CreatePipeline();

	void f()
	{
		HRESULT result = _vertexBuf->Map(0, nullptr, (void**)&_pVert);
		assert(SUCCEEDED(result));
		memcpy(_pVert, vertices, sizeof(vertices));
		_vertexBuf->Unmap(0, nullptr);

	}
};

