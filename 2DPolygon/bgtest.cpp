#include "bgtest.h"

#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <d3d12.h>
#include <tchar.h>

#include "../DX12/DirectX12Device.h"
#include "../DX12/d3dx12.h"
#include "../DX12/common.h"
#include "../DX12/TGALoader.h"
#include"../Goto/Camera.h"
#include "../Goto/FontManager.h"
#include<codecvt>
#include<string>

bgtest::bgtest()
{
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	vertices[0] = { { -1.f	, 1.f	, 0.0f },{ 0.0f,0.0f } };
	vertices[1] = { { 1.f	, 1.f	, 0.0f },{ 1.0f,0.0f } };
	vertices[2] = { { -1.f	, -1.f	, 0.0f },{ 0.0f,1.0f } };
	vertices[3] = { { 1.f	, -1.f	, 0.0f },{ 1.0f,1.0f } };

	CreateVertBuf();


	//テクスチャリソース
	CreateTex(_T("Resource/Goto/1315.png"));

	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(_texBuf,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();
	//シェーダリソースビュー
	D3D12_DESCRIPTOR_HEAP_DESC texDescripterHeapDesc = {};
	texDescripterHeapDesc.NumDescriptors = 2;
	texDescripterHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	texDescripterHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	result = _device->CreateDescriptorHeap(&texDescripterHeapDesc, IID_PPV_ARGS(&_descriptorHeapSRV));
	assert(SUCCEEDED(result));


	D3D12_CPU_DESCRIPTOR_HANDLE srvHandle = {};
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvHandle = _descriptorHeapSRV->GetCPUDescriptorHandleForHeapStart();
	_device->CreateShaderResourceView(_texBuf, &srvDesc, srvHandle);


	//コンスタントバッファ
	CreateConstBuf(srvHandle);
	CreatePipeline();
}
void bgtest::CreateVertBuf()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	//頂点バッファ
	HRESULT result = _device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(vertices)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vertexBuf));
	assert(SUCCEEDED(result));
	result = _vertexBuf->Map(0, nullptr, (void**)&_pVert);
	assert(SUCCEEDED(result));
	vbView.BufferLocation = _vertexBuf->GetGPUVirtualAddress();
	vbView.StrideInBytes = sizeof(Vertex);
	vbView.SizeInBytes = sizeof(vertices);

	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);

}
void bgtest::CreateConstBuf(D3D12_CPU_DESCRIPTOR_HANDLE& _srvHandle)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;
	ID3D12Resource* _constantBuf;
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&_cbDescHeap));
	assert(SUCCEEDED(result));
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;

	cbDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	cbDesc.Width = (sizeof(CBdata) + 0xff)&~0xff;
	cbDesc.Height = 1;
	cbDesc.DepthOrArraySize = 1;
	cbDesc.MipLevels = 1;
	cbDesc.SampleDesc.Count = 1;
	cbDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;


	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&cbDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_constantBuf));
	assert(SUCCEEDED(result));
	int size = sizeof(CBdata);
	cbViewDesc.BufferLocation = _constantBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(CBdata) + 0xff)&~0xff;
	_srvHandle.ptr += _device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	_device->CreateConstantBufferView(&cbViewDesc, _srvHandle);
	D3D12_RANGE range = {};
	result = _constantBuf->Map(0, &range, (void**)&pcBuf);
	assert(SUCCEEDED(result));

}

void bgtest::CreateTex(std::wstring _file)
{

	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;
	//std::wstring wPath = _T("Resource/Goto/img_b11.gif");
	//std::wstring wPath = _T("Resource/Goto/bg.png");
	std::wstring wPath=_file;

	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = (UINT)(1);
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbDescHeap));


	{
		std::unique_ptr<uint8_t[]> dec;
		D3D12_SUBRESOURCE_DATA sub;
		LoadWICTex(_texBuf, sub, wPath.c_str(), dec);
		void* pt = &dec[0];
		D3D12_BOX box = {};
		box.front = 0;
		box.back = 1;
		box.top = box.left = 0;
		box.right = (UINT)sub.RowPitch / 4;
		box.bottom = (UINT)(sub.SlicePitch / sub.RowPitch);
		result = _texBuf->WriteToSubresource(0, &box, &dec[0], box.right * 4, box.right*box.bottom * 4);
		assert(SUCCEEDED(result));

		dx12dev.CmdList()->ResourceBarrier(1,
			&CD3DX12_RESOURCE_BARRIER::Transition(_texBuf,
				D3D12_RESOURCE_STATE_COPY_DEST,
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

		dx12dev.CmdList()->Close();
		dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
		dx12dev.fence();

	}
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;

	CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle(pcbDescHeap->GetCPUDescriptorHandleForHeapStart(),
		_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
		0);
	srvDesc.Format = _texBuf->GetDesc().Format;
	_device->CreateShaderResourceView(_texBuf, &srvDesc, cbvHandle);


}

bgtest::~bgtest()
{
	_vertexBuf->Unmap(0, nullptr);
	_vertexBuf->Release();
	_descriptorHeapSRV->Release();
	_pipelineState->Release();
	vs->Release();
	ps->Release();
	_texBuf->Release();
	pcbDescHeap->Release();

	_cbDescHeap->Release();

}


void bgtest::draw()
{
	//ラジアン角(0〜2PI)をuv(0〜1)に変換
	float f = Camera::Instance().GetYaw() / (2.f*3.1415f);
	vertices[0].uv.x = vertices[2].uv.x = f - (1.f / 4.f / 2.f);
	vertices[1].uv.x = vertices[3].uv.x = f + (1.f / 4.f / 2.f);

	//ラジアン角(0〜2PI)をuv(0〜1)に変換
	float f2 = Camera::Instance().GetPitch() / (2.f*3.1415f);
	f2 += 0.5f;
	vertices[0].uv.y = vertices[1].uv.y = f2 - (1.f / 4.f / 2.f);
	vertices[2].uv.y = vertices[3].uv.y = f2 + (1.f / 4.f / 2.f);
	memcpy(_pVert, vertices, sizeof(vertices));


	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	dx12dev.CmdList()->SetPipelineState(_pipelineState);
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, _descriptorHeapSRV->GetGPUDescriptorHandleForHeapStart());
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);
	dx12dev.CmdList()->DrawInstanced(4, 1, 0, 0);
}


void bgtest::SetPos(float _x, float _y)
{
	float p[2] = { _x,_y };
	memcpy(pBufData->pos, p, sizeof(float) * 2);

}
void bgtest::SetSize(float _size)
{
	memcpy(&pBufData->size, &_size, sizeof(float) * 1);
}

void bgtest::CreatePipeline()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();
	HRESULT result;
	D3D12_INPUT_ELEMENT_DESC elementDescs[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		,{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	result = D3DCompileFromFile(_T("Shader/String.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "testVS", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vs, nullptr);
	assert(SUCCEEDED(result));
	result = D3DCompileFromFile(_T("Shader/String.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "testPS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &ps, nullptr);
	assert(SUCCEEDED(result));

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc = {};
	pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = false;
	pipelineStateDesc.BlendState.IndependentBlendEnable = false;

	D3D12_RENDER_TARGET_BLEND_DESC Target = {};
	ZeroMemory(&Target, sizeof(Target));
	Target.BlendEnable = true;
	Target.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOp = D3D12_BLEND_OP_ADD;
	Target.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	Target.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++) {
		pipelineStateDesc.BlendState.RenderTarget[i] = Target;
	}


	pipelineStateDesc.DepthStencilState.DepthEnable = false;
	pipelineStateDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	pipelineStateDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	pipelineStateDesc.DepthStencilState.StencilEnable = true;
	pipelineStateDesc.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
	pipelineStateDesc.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;

	pipelineStateDesc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(vs);
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(ps);

	pipelineStateDesc.InputLayout.NumElements = sizeof(elementDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	pipelineStateDesc.InputLayout.pInputElementDescs = elementDescs;
	pipelineStateDesc.pRootSignature = dx12dev.RootSignature();
	pipelineStateDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;

	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.SampleMask = 0xffffffff;

	result = device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&_pipelineState));
	assert(SUCCEEDED(result));
}