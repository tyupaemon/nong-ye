#include "DrawString.h"

#include "../Goto/FontManager.h"
#include"../DX12/common.h"

using namespace DirectX;
const int INTERVAL = 0;
DrawString::DrawString()
{
	CreatePipeline();
	pos.x = 0;
	pos.y = 0;
	cSize = 40.f;
}


//文字列用パイプラインのリリース
DrawString::~DrawString()
{
	DX_SAFE_RELEASE(pPipelineState)
}
void DrawString::CreatePipeline()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();
	HRESULT result;
	D3D12_INPUT_ELEMENT_DESC elementDescs[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		,{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	result = D3DCompileFromFile(_T("Shader/String.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "StrVS", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &pvs, nullptr);
	assert(SUCCEEDED(result));
	result = D3DCompileFromFile(_T("Shader/String.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "StrPS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &pps, nullptr);
	assert(SUCCEEDED(result));

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc = {};
	pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = false;
	pipelineStateDesc.BlendState.IndependentBlendEnable = false;

	D3D12_RENDER_TARGET_BLEND_DESC Target = {};
	ZeroMemory(&Target, sizeof(Target));
	Target.BlendEnable = true;
	Target.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOp = D3D12_BLEND_OP_ADD;
	Target.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	Target.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++) {
		pipelineStateDesc.BlendState.RenderTarget[i] = Target;
	}

	pipelineStateDesc.DepthStencilState.DepthEnable = false;
	pipelineStateDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	pipelineStateDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	pipelineStateDesc.DepthStencilState.StencilEnable = true;
	pipelineStateDesc.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
	pipelineStateDesc.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;

	pipelineStateDesc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(pvs);
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(pps);

	pipelineStateDesc.InputLayout.NumElements = sizeof(elementDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	pipelineStateDesc.InputLayout.pInputElementDescs = elementDescs;
	pipelineStateDesc.pRootSignature = dx12dev.RootSignature();
	pipelineStateDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;

	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.SampleMask = 0xffffffff;

	result = device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pPipelineState));
	assert(SUCCEEDED(result));

	DX_SAFE_RELEASE(pvs)
	DX_SAFE_RELEASE(pps)
}

void DrawString::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	dx12dev.CmdList()->SetPipelineState(pPipelineState);
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	for (int i = 0; i < letters.size(); i++)
	{
		letters[i]->draw();
	}
}
//文字は

void DrawString::Alignment()
{
	float def = 0;
	int cnt = 0;
	int line = 0;
	for (auto& l:letters)
	{
		l->SetSize(cSize);
		if (wstr[cnt] == _T('\n'))
		{
			cnt++;
			line++;
			def = 0;
			continue;
		}
//		l->SetPos(def / FONT_SIZEF / WINDOW_WIDF + (pos.x - 1.0f) / cSize,
//			(-pos.y + 1.0f) / cSize - ((.5f + (float)line) / WINDOW_HEIF));
		l->SetPos(pos.x+def, pos.y-(float)line/WINDOW_HEIF*cSize*2);
		//		l->SetCenter(pos.x, pos.y);
		def += (l->width * cSize/FONT_SIZEF*2) / WINDOW_WIDF;
		int w= l->width;
		int w2= l->width2;
		int o= l->orig;
		cnt++;
//		letters.push_back(l);
	}

}
void DrawString::StringSizeUpdate()
{
	float wid, hei;
	wid = hei = 0;
	float def = 0;
	sSize.x = sSize.y = 0;
	for (auto& w : wstr)
	{
		if (w == _T('\n'))
		{
			wid = 0; 
			sSize.y += 1/WINDOW_HEIF * cSize * 2;
			continue;
		}
		void* p;
		def += (FontManager::Instance().GetFontTex(w, (ID3D12Resource**)&p).wid + cSize) / WINDOW_WIDF / 2;
		sSize.x = max(def, sSize.x);
		//		letters.push_back(l);
	}
	sSize.y += 1/WINDOW_HEIF * cSize * 2;
}

void DrawString::init(std::wstring _wstr)
{
	for (auto& l : letters)
	{
		delete(l);
	}
	letters.resize(0);
	wstr = _wstr;
	//	letters.resize(wstr.length());
	for (auto& s:wstr)
	{
		letters.push_back(new letter((short)s));
	}
	StringSizeUpdate();
	Alignment();
}

//左上を(0,0)右下を(WINDOW_WID,WINDOW_HEI)として座標指定
void DrawString::SetPos(float _x, float _y)
{
	pos.x = _x * 2 / WINDOW_WIDF - 1;
	pos.y = _y * -2 / WINDOW_HEIF + 1;
	Alignment();
}
void DrawString::SetCenter(float _x, float _y)
{
	pos.x = _x * 2 / WINDOW_WIDF - 1;
	pos.y = _y * -2 / WINDOW_HEIF + 1;
	pos.x -= sSize.x/2;
	pos.y += sSize.y/2;
	Alignment();
}

void DrawString::SetSize(float _size)
{
	cSize = _size;
	StringSizeUpdate();
	Alignment();
}
void DrawString::SetColor(float _r, float _g, float _b, float _a)
{
	color[0] = _r;
	color[1] = _g;
	color[2] = _b;
	color[3] = _a;
	for (auto& l : letters)
	{
		l->SetColor(color[0], color[1], color[2], color[3]);
	}
}
