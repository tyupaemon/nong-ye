#pragma once
#include "../DX12/d3dx12.h"
#include"../DX12/common.h"
#include<functional>
#include"NoProj2DPolygon.h"
struct StrBufData
{
	float color[4];
	float pos[2];
	float raito;
	float size;
};
//描画用文字クラス
class letter
{
private:
	Vertex vertices[4] = {
		{ { -1.0f	,	-1.0f	,	0.0f },{ 0.0f,0.0f } },
		{ { 1.0f	,	-1.0f	,	0.0f },{ 1.0f,0.0f } },
		{ { -1.0f	,	1.0f	,	0.0f },{ 0.0f,1.0f } },
		{ { 1.0f	,	1.0f	,	0.0f },{ 1.0f,1.0f } },
	};
	char* _vdata = nullptr;
	ID3D12Resource* _vertexBuf = nullptr;
	ID3D12DescriptorHeap* _descriptorHeapSRV = nullptr;
	ID3D12DescriptorHeap* _descriptorHeapCBV = nullptr;
	D3D12_VERTEX_BUFFER_VIEW vbView = {};
//	ID3D12PipelineState* _pipelineState = nullptr;
//	ID3DBlob* _vs = nullptr;
//	ID3DBlob* _ps = nullptr;
	ID3D12Resource* _texBuf = nullptr;

	ID3D12DescriptorHeap* pcbDescHeap;
	ID3D12Resource* pConstBuf = nullptr;
	StrBufData* pBufData;

	void CreateFontTex(short code);
	void CreateVertBuf();
	void CreateConstBuf(D3D12_CPU_DESCRIPTOR_HANDLE& _srvHandle);

	std::vector<std::function<void()>> release;

public:
	Vec2 origPos;
	Vec2 texSize;
	float size;
	int width;
	int width2;
	int orig;
	void SetCenter(float _x, float _y);
	void SetPos(float _x, float _y);
	void SetSize(float _size);
	void SetColor(float _r, float _g, float _b, float _a = 1.0f);
//	letter() {};
	letter(float _hx = -1, float _hy = -1, float _tx = 1, float _ty = 1, float _z = 0);
	letter(short _code,float _xSize=1,float _ySize=1);
	~letter();
	void draw();

};

