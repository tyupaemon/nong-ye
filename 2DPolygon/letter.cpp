#include "letter.h"

#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <d3d12.h>
#include <tchar.h>

#include "../DX12/DirectX12Device.h"
#include "../DX12/d3dx12.h"
#include "../DX12/common.h"
#include "../DX12/TGALoader.h"
#include "../Goto/FontManager.h"
#include<codecvt>
#include<string>

CBdata* pcBuf;
ID3D12DescriptorHeap* _cbDescHeap;
int animMax = 1;
letter::letter(float _hx, float _hy, float _tx, float _ty, float _z)
{
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	vertices[0] = { { _hx	,	_hy	,0.0f	},{ 0.0f,0.0f } };
	vertices[1] = { { _tx	,	_hy	,0.0f	},{ 1.0f,0.0f } };
	vertices[2] = { { _hx	,	_ty	,0.0f	},{ 0.0f,1.0f } };
	vertices[3] = { { _tx	,	_ty	,0.0f	},{ 1.0f,1.0f } };

	CreateVertBuf();


	//テクスチャリソース
	CreateFontTex(_T("あ")[0]);

	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(_texBuf,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();
	//シェーダリソースビュー
	D3D12_DESCRIPTOR_HEAP_DESC texDescripterHeapDesc = {};
	texDescripterHeapDesc.NumDescriptors = 2;
	texDescripterHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	texDescripterHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	result = _device->CreateDescriptorHeap(&texDescripterHeapDesc, IID_PPV_ARGS(&_descriptorHeapSRV));
	assert(SUCCEEDED(result));


	D3D12_CPU_DESCRIPTOR_HANDLE srvHandle = {};
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvHandle = _descriptorHeapSRV->GetCPUDescriptorHandleForHeapStart();
	_device->CreateShaderResourceView(_texBuf, &srvDesc, srvHandle);


	//コンスタントバッファ
	CreateConstBuf(srvHandle);

}
letter::letter(short _code,float _xSize,float _ySize)
{
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	vertices[0] = { { -_xSize	/ WINDOW_WID , _ySize		/ WINDOW_HEI ,0.0f },{ 0.0f,0.0f } };
	vertices[1] = { { _xSize	/ WINDOW_WID , _ySize		/ WINDOW_HEI ,0.0f },{ 1.0f,0.0f } };
	vertices[2] = { { -_xSize	/ WINDOW_WID , -_ySize	/ WINDOW_HEI ,0.0f },{ 0.0f,1.0f } };
	vertices[3] = { { _xSize	/ WINDOW_WID , -_ySize	/ WINDOW_HEI ,0.0f },{ 1.0f,1.0f } };


	CreateVertBuf();


	//テクスチャリソース
	//CreateFontTex(_code);
	FontData f=FontManager::Instance().GetFontTex(_code, &_texBuf);
	width = f.wid;
	origPos.x = (float)f.x;
	origPos.y = (float)f.y;

	//シェーダリソースビュー
	D3D12_DESCRIPTOR_HEAP_DESC texDescripterHeapDesc = {};
	texDescripterHeapDesc.NumDescriptors = 2;
	texDescripterHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	texDescripterHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	result = _device->CreateDescriptorHeap(&texDescripterHeapDesc, IID_PPV_ARGS(&_descriptorHeapSRV));
	release.push_back([&]() {DX_SAFE_RELEASE(_descriptorHeapSRV); });
	assert(SUCCEEDED(result));


	D3D12_CPU_DESCRIPTOR_HANDLE srvHandle = {};
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvHandle = _descriptorHeapSRV->GetCPUDescriptorHandleForHeapStart();
	_device->CreateShaderResourceView(_texBuf, &srvDesc, srvHandle);

	//コンスタントバッファ
	//CreateConstBuf(srvHandle);

	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = DirectX12Device::Instance().Device()->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(CBCommonData) + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&pConstBuf));
	release.push_back([&]() {
		DX_RESOURCE_RELEASE(pConstBuf);
	});
	assert(SUCCEEDED(result));

	assert(SUCCEEDED(result));
	result = DirectX12Device::Instance().Device()->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbDescHeap));

	assert(SUCCEEDED(result));

	D3D12_RANGE range = {};
	result = pConstBuf->Map(0, &range, (void**)&pBufData);
	assert(SUCCEEDED(result));
	float c[4] = { 1,0,0,0.6f };
	memcpy(pBufData->color, c, sizeof(float) * 4);
	pConstBuf->Unmap(0, nullptr);
	cbViewDesc.BufferLocation = pConstBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(CBCommonData) + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, pcbDescHeap->GetCPUDescriptorHandleForHeapStart());
	release.push_back([&]() {DX_SAFE_RELEASE(pcbDescHeap); });

}
void letter::CreateVertBuf()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	//頂点バッファ
	HRESULT result = _device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(vertices)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vertexBuf));
	release.push_back([&]() {DX_RESOURCE_RELEASE(_vertexBuf); });

	assert(SUCCEEDED(result));
	result = _vertexBuf->Map(0, nullptr, (void**)&_vdata);
	assert(SUCCEEDED(result));
	memcpy(_vdata, vertices, sizeof(vertices));
	_vertexBuf->Unmap(0, nullptr);
	vbView.BufferLocation = _vertexBuf->GetGPUVirtualAddress();
	vbView.StrideInBytes = sizeof(Vertex);
	vbView.SizeInBytes = sizeof(vertices);

	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);

}
void letter::CreateConstBuf(D3D12_CPU_DESCRIPTOR_HANDLE& _srvHandle)
{
	return;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;
	ID3D12Resource* _constantBuf;
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;

	cbDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	cbDesc.Width = (sizeof(CBdata) + 0xff)&~0xff;
	cbDesc.Height = 1;
	cbDesc.DepthOrArraySize = 1;
	cbDesc.MipLevels = 1;
	cbDesc.SampleDesc.Count = 1;
	cbDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&cbDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_constantBuf));
	assert(SUCCEEDED(result));
	int size = sizeof(CBdata);
	cbViewDesc.BufferLocation = _constantBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(CBdata) + 0xff)&~0xff;
	_srvHandle.ptr += _device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	_device->CreateConstantBufferView(&cbViewDesc, _srvHandle);
	D3D12_RANGE range = {};
	result = _constantBuf->Map(0, &range, (void**)&pcBuf);
	assert(SUCCEEDED(result));

}

void letter::CreateFontTex(short code)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	D3D12_BOX box = {};
	box.front = 0;
	box.back = 1;
	box.top = box.left = 0;
	FontManager& fManager = FontManager::Instance();
	HFONT& hFont = fManager.GetFont(0);
	if (!(hFont))
	{
		return;
	}
	HDC hdc = GetDC(NULL);
	HFONT oldFont = (HFONT)SelectObject(hdc, hFont);
	// フォントビットマップ取得
	TEXTMETRIC TM;
	GetTextMetrics(hdc, &TM);
	GLYPHMETRICS GM;
	MAT2 Mat = { { 0,1 },{ 0,0 },{ 0,0 },{ 0,1 } };
	DWORD f_size = GetGlyphOutline(hdc, code, GGO_GRAY4_BITMAP, &GM, 0, NULL, &Mat);
	f_size = GetGlyphOutline(hdc, code, GGO_GRAY4_BITMAP, &GM, 0, NULL, &Mat);
	BYTE* ptr = new BYTE[f_size];
	GetGlyphOutline(hdc, code, GGO_GRAY4_BITMAP, &GM, f_size, ptr, &Mat);

	// デバイスコンテキストとフォントハンドルの開放
	SelectObject(hdc, oldFont);
	ReleaseDC(NULL, hdc);

	// Create texture
	D3D12_RESOURCE_DESC desc = {};
//	desc.Width = GM.gmBlackBoxX;
//	desc.Height = TM.tmHeight;
	desc.Width = desc.Height = FONT_SIZE;

	desc.MipLevels = 1;
	desc.DepthOrArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Flags = D3D12_RESOURCE_FLAG_NONE;
	desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

	//	desc.MipLevels = (uint16_t)mipCount;
	//	desc.Format = format;
	//	desc.Flags = resFlags;
	//	D3D12_RESOURCE_FLAG_NONE;
	//	WIC_LOADER_DEFAULT;


	// CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_CPU_PAGE_PROPERTY_WRITE_BACK, D3D12_MEMORY_POOL_L0);
	//	ID3D12Resource* tex = nullptr;
	HRESULT hr = _device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_texBuf));
	if (FAILED(hr))
	{
		return;
	}

	width = GM.gmCellIncX;
	origPos.x = (float)GM.gmptGlyphOrigin.x;
	origPos.y = (float)GM.gmptGlyphOrigin.y;
	D3D12_SUBRESOURCE_DATA cSub;
	std::vector<char> decodedData;
	int iOfs_x = (int)GM.gmptGlyphOrigin.x;
	int iOfs_y = (int)TM.tmAscent - GM.gmptGlyphOrigin.y;
	int iBmp_w = (int)GM.gmBlackBoxX + (4 - (GM.gmBlackBoxX % 4)) % 4;
	int iBmp_h = (int)GM.gmBlackBoxY;

	decodedData.resize(desc.Width*desc.Height * 4);

	int Level = 17;
	int x, y;
	DWORD Color;
	char Alpha;
	BYTE* pBits = (BYTE*)&decodedData[0];
	std::fill(decodedData.begin(), decodedData.end(), 0);
	for (y = iOfs_y; y < iOfs_y + iBmp_h; y++)
	{
		for (x = iOfs_x; x < iOfs_x + iBmp_w; x++)
		{
			Alpha = (255 * ptr[x - iOfs_x + iBmp_w * (y - iOfs_y)]) / (Level - 1);
			Color = 0x00ffffff | (Alpha << 24);
			decodedData[(x) * 4 + (desc.Width * 4)*y + 0] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 1] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 2] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 3] = Alpha;
			//memcpy((BYTE*)pBits + hMappedResource.RowPitch * y + 4 * x, &Color, sizeof(DWORD));
		}
	}
	cSub.pData = &decodedData[0];
	cSub.RowPitch = desc.Width * 4;
	cSub.SlicePitch = desc.Width*desc.Height * 4;

	box.right = (UINT)cSub.RowPitch / 4;
	box.bottom = (UINT)(cSub.SlicePitch / cSub.RowPitch);

	HRESULT result = _texBuf->WriteToSubresource(0, &box, &decodedData[0], (UINT)desc.Width * 4, (UINT)decodedData.size());
	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(_texBuf,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();

}


letter::~letter()
{
	for (auto& r : release)
	{
		r();
	}
}

void letter::draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();

	//文字データセット
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV2, pcbDescHeap->GetGPUDescriptorHandleForHeapStart());
	//テクスチャセット
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, _descriptorHeapSRV->GetGPUDescriptorHandleForHeapStart());
	
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);
	dx12dev.CmdList()->DrawInstanced(4, 1, 0, 0);
}

void letter::SetCenter(float _x, float _y)
{
	float p[2] = { _x,_y };
	memcpy(pBufData->pos, p, sizeof(float) * 2);

}
void letter::SetPos(float _x, float _y)
{
//def / FONT_SIZEF / WINDOW_WIDF + (pos.x - 1.0f) /
//	((.5f + (float)line) / WINDOW_HEIF)
	
	float p[2] = {
		_x - vertices[0].pos.x*size,
		_y - vertices[0].pos.y*size };
	memcpy(pBufData->pos, p, sizeof(float) * 2);

}
void letter::SetSize(float _size)
{
	size = _size;
	memcpy(&pBufData->size, &_size, sizeof(float) * 1);
}

void letter::SetColor(float _r, float _g, float _b,float _a)
{
	D3D12_RANGE range = {};
	float c[4] = { _r,_g,_b,_a };
	memcpy(pBufData->color, c, sizeof(float) * 4);
}