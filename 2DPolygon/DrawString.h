#pragma once

#include<string>
#include<vector>

#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <d3d12.h>

#include "../DX12/DirectX12Device.h"
#include "../DX12/d3dx12.h"

#include"letter.h"
#include"../DX12/common.h"

//文字列描画クラス
//画面左上を（0,0）右下を（WINDOW_WID,WINDOW_HEI）として座標をとる
//
class DrawString
{
private:
	//文字サイズ
	float cSize;
	//文字列サイズ
	Vec2 sSize;
	Vec2 pos;
	
	float color[4];
	std::wstring wstr;
	unsigned int drawLen;
	std::vector<int> incX;
	std::vector<letter*> letters;
	ID3D12PipelineState* pPipelineState = nullptr;
	ID3DBlob* pvs = nullptr;
	ID3DBlob* pps = nullptr;
	//文字列位置に文字を整列させる
	void Alignment();
	void StringSizeUpdate();
public:
	void Draw();
	void SetPos(float _x, float _y);
	void SetCenter(float _x, float _y);
	void SetSize(float _size);
	void SetColor(float _r,float _g,float _b,float _a=1.0f);
	DrawString();
	//任意の文字列で初期化
	void init(std::wstring _wstr);
	void CreatePipeline();
	~DrawString();
};

