#include "RenderableObject.h"

#include "../DX12/DirectX12Device.h"
#include"../Objects/ObjectController.h"
#include<d3d12.h>
#include<algorithm>
#include"../DX12/common.h"

RenderableObject::RenderableObject(char _type)
{
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };
	pcbMDdataBuf = nullptr;
	if (_type&(1 << O_DHEnable))
	{
		D3D12_HEAP_PROPERTIES cbHeapProperties = {};
		D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
		D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
		cbDescriptorHeapDesc.NumDescriptors = 1;
		cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		cbHeapProperties.VisibleNodeMask = 1;
		cbHeapProperties.CreationNodeMask = 1;
		cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

		result = _device->CreateCommittedResource(&cbHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer((sizeof(DrawModelData) + 0xff)&~0xff),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&pcbMDdataBuf));
		assert(SUCCEEDED(result));

		result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbMDdataDescHeap));
		assert(SUCCEEDED(result));

		cbViewDesc.BufferLocation = pcbMDdataBuf->GetGPUVirtualAddress();
		cbViewDesc.SizeInBytes = (sizeof(DrawModelData) + 0xff)&~0xff;
		_device->CreateConstantBufferView(&cbViewDesc, pcbMDdataDescHeap->GetCPUDescriptorHandleForHeapStart());

		D3D12_RANGE range = {};
		result = pcbMDdataBuf->Map(0, &range, (void**)&pDModelData);
	}
	rot.x = rot.y = rot.z = 0;
	pos.m128_f32[0] = pos.m128_f32[1] = pos.m128_f32[2] = pos.m128_f32[3] = 0;
	wid = hei = 0;
	ObjectController::Instance().Register(this);
}

RenderableObject::~RenderableObject()
{
	ObjectController::Instance().Deregistration(this);
	DX_RESOURCE_RELEASE(pcbMDdataBuf)
	DX_SAFE_RELEASE(pcbMDdataDescHeap)
}
bool RenderableObject::SortComp(const RenderableObject* o1, const RenderableObject* o2)
{
	float d1, d2;
	d1 = pow(pos.m128_f32[0] - o1->pos.m128_f32[0], 2) +
		 pow(pos.m128_f32[1] - o1->pos.m128_f32[1], 2) +
		 pow(pos.m128_f32[2] - o1->pos.m128_f32[2], 2);

	d2 = pow(pos.m128_f32[0] - o2->pos.m128_f32[0], 2) +
		 pow(pos.m128_f32[1] - o2->pos.m128_f32[1], 2) +
		 pow(pos.m128_f32[2] - o2->pos.m128_f32[2], 2);

	return d1 > d2;
}

void RenderableObject::SetHitObject(RenderableObject*& _obj)
{
	hittingObjs.push_back(_obj);

}
void RenderableObject::FlushHitObject()
{
	hittingObjs.clear();
}
void RenderableObject::SortHitObject()
{
	std::sort(hittingObjs.begin(), hittingObjs.end(),
		[&](const RenderableObject* o1, const RenderableObject* o2) {return SortComp(o1, o2); }
	);

}

