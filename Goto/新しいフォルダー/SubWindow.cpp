﻿#include "SubWindow.h"

#include<codecvt>
#include<sstream>
#include<iomanip>
#include<CommCtrl.h>
#pragma comment(lib,"comctl32.lib")
LRESULT SubWindow::WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static HWND hTab;
	static TCITEM tc_item;
	if (msg == WM_DESTROY) {
		return 0;
	}
	if (msg == WM_SIZE)
	{
		MoveWindow(hTab, 0, 0, LOWORD(lpal), 30, TRUE);
	}
	if (msg == WM_NOTIFY)
	{
		switch (((NMHDR *)lpal)->code)
		{
		case TCN_SELCHANGE:
			if (TabCtrl_GetCurSel(hTab) == 0)
			{
				PlayerMenuDraw(SW_SHOW);
				ObjectMenuDraw(SW_HIDE);
			}
			else
			{
				PlayerMenuDraw(SW_HIDE);
				ObjectMenuDraw(SW_SHOW);
			}
			break;
		}
		InvalidateRect(hwnd, NULL, TRUE);
	}
	if (msg == WM_CREATE)
	{
		InitCommonControls();
		hTab = CreateWindowEx(0, WC_TABCONTROL, NULL,
			WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
			0, 0, 10, 10, hwnd, (HMENU)0x10,
			((LPCREATESTRUCT)lpal)->hInstance, NULL
		);
		tc_item.mask = TCIF_TEXT;
		tc_item.pszText = _T("TAB1");
		TabCtrl_InsertItem(hTab, 0, &tc_item);

		tc_item.pszText = _T("TAB2");
		TabCtrl_InsertItem(hTab, 1, &tc_item);
	}
	if (msg == WM_COMMAND)
	{
		switch (LOWORD(wp))
		{
		case BUTTON_ID1:
			PlCommandUpdate();
			DispInputs();
			DispInput();
			DispCommand();
			break;
		case BUTTON_ID2:
	//		operationCommand.inputs.re[cIdx3]
			break;
		case ID_DIRCHECK1:
		case ID_DIRCHECK2:
		case ID_DIRCHECK3:
		case ID_DIRCHECK4:
		case ID_BUTTONCHECK1:
		case ID_BUTTONCHECK2:
		case ID_BUTTONCHECK3:
		case ID_BUTTONCHECK4:
		case ID_BUTTONCHECK5:
		case ID_BUTTONCHECK6:
		case ID_BUTTONCHECK7:
			OpCommandUpdate();
			DispInputs();
			DispInput();
			break;

		case ID_BUTTON1:
			break;
		case ID_LIST1:
			if (player != nullptr)
			{
				cIdx1 = min(player->commands.size() - 1, SendMessage(list1, LB_GETCURSEL, NULL, NULL));
				if (cIdx1 < 0) { break; }
//				operationCommand = player->commands[cIdx1][cIdx2];
				DispCommand();
			}
			break;
		case ID_LIST2:
			SendMessage(list3, LB_RESETCONTENT, NULL, NULL);
			if (player != nullptr)
			{
				cIdx2 = min(player->commands[cIdx1].size() - 1, SendMessage(list2, LB_GETCURSEL, NULL, NULL));
				if (cIdx2 < 0) { break; }
				operationCommand = player->commands[cIdx1][cIdx2];
				DispInputs();
			}
			break;
		case ID_LIST3:
			if (player != nullptr)
			{
				cIdx3 = min(player->commands[cIdx1][cIdx2].inputs.size()-1, SendMessage(list3, LB_GETCURSEL, NULL, NULL));
				if (cIdx3 < 0) { break; }
				//operationCommand = player->commands[cIdx1][cIdx2];
				DispInput();
			}
			break;
		case ID_TEXT1:
			switch (HIWORD(wp))
			{
			case EN_UPDATE:
				break;
			}
		}
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

LRESULT SubWindow::WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	SubWindow* thisPtr = (SubWindow*)GetWindowLong(hwnd, GWL_USERDATA);
	if (!thisPtr)
	{
		if (msg == WM_CREATE)
		{
			int i = 0;
			thisPtr = (SubWindow*)(((LPCREATESTRUCT)lpal)->lpCreateParams);
			if (thisPtr)
			{
				SetWindowLong(hwnd,GWL_USERDATA,(LONG)thisPtr);
			}
		}
	}
	LRESULT ret = thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
	return ret;
}

SubWindow::SubWindow()
{
}


SubWindow::~SubWindow()
{
}

HWND SubWindow::Init(HINSTANCE hInst)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX w = {};
	w.lpfnWndProc = (WNDPROC)WindowProcWrapper;
	w.lpszClassName = SUB_WND_NAME;
	w.hInstance = hInst;
	w.cbSize = sizeof(WNDCLASSEX);

	w.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);

	RegisterClassEx(&w);

	RECT wrc = { 0, 0, WINDOW_WID, WINDOW_HEI };
	::AdjustWindowRectEx(&wrc, WS_OVERLAPPEDWINDOW, false, 0);

	hwnd = CreateWindowEx(0, w.lpszClassName,
		_T("サブウィンドウ"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		wrc.right - wrc.left,
		wrc.bottom - wrc.top,
		nullptr,
		nullptr,
		w.hInstance,
		this);

	list1 = CreateWindowA("listbox", NULL, WS_VISIBLE | WS_CHILD | LBS_NOTIFY | WS_VSCROLL,
		20, 35, 100, 200, hwnd, (HMENU)ID_LIST1, hInst, NULL);
	list2 = CreateWindowA("listbox", NULL, WS_VISIBLE | WS_CHILD | LBS_NOTIFY | WS_VSCROLL,
		130, 35, 80, 200, hwnd, (HMENU)ID_LIST2, hInst, NULL);
	list3 = CreateWindowA("listbox", NULL, WS_VISIBLE | WS_CHILD | LBS_NOTIFY | WS_VSCROLL,
		220, 35, 80, 200, hwnd, (HMENU)ID_LIST3, hInst, NULL);
	dirCheckBoxes[0] = CreateWindowA("button", "UP", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		331, 35, 60, 15, hwnd, (HMENU)ID_DIRCHECK1, hInst, NULL);
	dirCheckBoxes[1] = CreateWindowA("button", "DOWN", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		331, 71, 60, 15, hwnd, (HMENU)ID_DIRCHECK2, hInst, NULL);
	dirCheckBoxes[2] = CreateWindowA("button", "LEFT", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		300, 53, 60, 15, hwnd, (HMENU)ID_DIRCHECK3, hInst, NULL);
	dirCheckBoxes[3] = CreateWindowA("button", "RIGHT", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		363, 53, 60, 15, hwnd, (HMENU)ID_DIRCHECK4, hInst, NULL);

	buttonCheckBoxes[0] = CreateWindowA("button", "1", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		425, 35, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK1, hInst, NULL);
	buttonCheckBoxes[1] = CreateWindowA("button", "2", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		425, 50, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK2, hInst, NULL);
	buttonCheckBoxes[2] = CreateWindowA("button", "3", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		425, 65, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK3, hInst, NULL);
	buttonCheckBoxes[3] = CreateWindowA("button", "4", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		425, 80, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK4, hInst, NULL);
	buttonCheckBoxes[4] = CreateWindowA("button", "5", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		460, 35, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK5, hInst, NULL);
	buttonCheckBoxes[5] = CreateWindowA("button", "6", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		460, 50, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK6, hInst, NULL);
	buttonCheckBoxes[6] = CreateWindowA("button", "7", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		460, 65, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK7, hInst, NULL);

	maskCheckBox = CreateWindowA("button", "m", WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
		460, 80, 30, 15, hwnd, (HMENU)ID_BUTTONCHECK7, hInst, NULL);

	numText = CreateWindowA("edit", "num", WS_VISIBLE | WS_CHILD | ES_LEFT | ES_NUMBER,
		500, 40, 50, 15, hwnd, (HMENU)ID_TEXT1, hInst, NULL);
	intervalText = CreateWindowA("edit", "interval", WS_VISIBLE | WS_CHILD | ES_LEFT | ES_NUMBER ,
		500, 65, 50, 15, hwnd, (HMENU)ID_TEXT2, hInst, NULL);


	CreateWindowA(
		"button", "適用",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		50, 250, 40, 20,
		hwnd, (HMENU)BUTTON_ID1, hInst, NULL
	);
	CreateWindowA(
		"button", "入力削除",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		0, 250, 40, 20,
		hwnd, (HMENU)BUTTON_ID2, hInst, NULL
	);
	CreateWindowA(
		"button", "コマンド削除",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		100, 250, 40, 20,
		hwnd, (HMENU)BUTTON_ID3, hInst, NULL
	);
	CreateWindowA(
		"button", "入力追加",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		150, 250, 40, 20,
		hwnd, (HMENU)BUTTON_ID2, hInst, NULL
	);
	CreateWindowA(
		"button", "コマンド追加",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		200, 250, 40, 20,
		hwnd, (HMENU)BUTTON_ID2, hInst, NULL
	);
	return hwnd;
}
void SubWindow::Show()
{
	ShowWindow(hwnd, SW_SHOW);
}
void SubWindow::SetPlayer(Player* _p)
{
	player = _p;
	DispCommands();
}
void SubWindow::SetCommand()
{
	//player->commands[cIdx1][cIdx2] = operationCommand;
}
void SubWindow::DispCommand()
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	std::wstring pre = _T("to");
	SendMessage(list2, LB_RESETCONTENT, NULL, NULL);
	for (auto& command : player->commands[cIdx1])
	{
		std::wstring wStr;
		std::ostringstream sout;
		sout << std::setfill('0') << std::setw(5) << command.num;
		std::string s = sout.str();
		wStr = pre + converter.from_bytes(s);
		//		wStr=pre+(std::to_wstring(cnt));
		SendMessage(list2, LB_ADDSTRING, NULL, (LPARAM)wStr.c_str());

	}
}
void SubWindow::DispCommands()
{
	std::wstring wStr;
	std::wstring pre = _T("state");
	int cnt = 0;
	for (auto& command : player->commands)
	{
		std::ostringstream sout;
		sout << std::setfill('0') << std::setw(5) << cnt;
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
		std::string s = sout.str();
		wStr = pre + converter.from_bytes(s);
		//		wStr=pre+(std::to_wstring(cnt));
		SendMessage(list1, LB_ADDSTRING, NULL, (LPARAM)wStr.c_str());
		cnt++;
	}
}
void SubWindow::DispInputs()
{
	SendMessage(list3, LB_RESETCONTENT, NULL, NULL);
	SetWindowText(numText, std::to_wstring(operationCommand.num).c_str());
	SetWindowText(intervalText, std::to_wstring(operationCommand.interval).c_str());

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	std::wstring pre = _T("to");
	for (auto& input : operationCommand.inputs)
	{
		std::wstring wStr = _T("null");
		std::ostringstream sout;
		sout << std::setfill('0') << std::setw(11) << (input & 0x07ff);
		std::string s = sout.str();
		if (input & 0x0f)
		{
			switch (input & 0x0f)
			{
			case(1 << dirDown):
				wStr = _T("↓");
				break;
			case(1 << dirUp):
				wStr = _T("↑");
				break;
			case(1 << dirRight):
				wStr = _T("→");
				break;
			case(1 << dirLeft):
				wStr = _T("←");
				break;
			case((1 << dirDown) | (1 << dirRight)):
				wStr = _T("┘");
				break;
			case((1 << dirDown) | (1 << dirLeft)):
				wStr = _T("└");
				break;
			case((1 << dirUp) | (1 << dirRight)):
				wStr = _T("┐");
				break;
			case((1 << dirUp) | (1 << dirLeft)):
				wStr = _T("┌");
				break;
			default:
				wStr = _T("?");
				break;
			}
		}
		if (input & 0x07f0)
		{
			unsigned short s = 0x0010;
			for (int i = 0; i < 7; i++)
			{
				if ((s << i)&input)
				{
					if (wStr == _T("null"))
					{
						wStr = std::to_wstring(i);
					}
					else
					{
						wStr += _T("+") + std::to_wstring(i);
					}
				}
			}
		}
		SendMessage(list3, LB_ADDSTRING, NULL, (LPARAM)wStr.c_str());

	}
}
void SubWindow::DispInput()
{
	for (int i = 0; i < 4; i++)
	{
		if (operationCommand.inputs[cIdx3] & (1 << i))
		{
			SendMessage(dirCheckBoxes[i], BM_SETCHECK, BST_CHECKED, 0);
		}
		else
		{
			SendMessage(dirCheckBoxes[i], BM_SETCHECK, BST_UNCHECKED, 0);
		}
	}
	for (int i = 0; i < 7; i++)
	{
		if (operationCommand.inputs[cIdx3] & (1 << (i+4)))
		{
			SendMessage(buttonCheckBoxes[i], BM_SETCHECK, BST_CHECKED, 0);
		}
		else
		{
			SendMessage(buttonCheckBoxes[i], BM_SETCHECK, BST_UNCHECKED, 0);
		}
	}

}

void SubWindow::OpCommandUpdate()
{
	int num;
	int interval;
	std::vector<unsigned short> inputs;
	unsigned char option;
	unsigned short mask = 0xffff;
	
	unsigned short input = 0;
	for (int i = 0; i < 4; i++)
	{

		if (SendMessage(dirCheckBoxes[i], BM_GETCHECK, 0, 0) == BST_CHECKED)
		{
			input |= (1 << i);
		}
	}
	for (int i = 0; i < 7; i++)
	{

		if (SendMessage(buttonCheckBoxes[i], BM_GETCHECK, 0, 0) == BST_CHECKED)
		{
			input |= (1 << (i+4));
		}
	}
	operationCommand.inputs[cIdx3]=input;
}
void SubWindow::PlCommandUpdate()
{
	player->commands[cIdx1][cIdx2] = operationCommand;
}

void SubWindow::ObjectMenuDraw(int _visible)
{}
void SubWindow::PlayerMenuDraw(int _visible)
{
	ShowWindow(list1, _visible);
	ShowWindow(list2, _visible);
	ShowWindow(list3, _visible);
	for (auto& a : dirCheckBoxes)
	{
		ShowWindow(a, _visible);
	}
	for (auto& a : buttonCheckBoxes)
	{
		ShowWindow(a, _visible);
	}
}

void SubWindow::AddInput() {}
void SubWindow::DelInput() {}
void SubWindow::AddCommand() {}
void SubWindow::DelCommand() {}
