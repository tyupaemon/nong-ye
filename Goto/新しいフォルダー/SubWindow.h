#pragma once
#include <windows.h>
#include<tchar.h>

#include"common.h"
#include"Player.h"
#include"Object.h"
const TCHAR SUB_WND_NAME[] = _T("SubWindow");
const int ID_BUTTON1 = 1;
//const int ID_TEXT1 = 2;
const int ID_LIST1 = 3;
const int ID_LIST2 = 4;
const int ID_LIST3 = 5;
const int ID_DIRCHECK1 = 6;
const int ID_DIRCHECK2 = 7;
const int ID_DIRCHECK3 = 8;
const int ID_DIRCHECK4 = 9;

const int ID_BUTTONCHECK1 = 10;
const int ID_BUTTONCHECK2 = 11;
const int ID_BUTTONCHECK3 = 12;
const int ID_BUTTONCHECK4 = 13;
const int ID_BUTTONCHECK5 = 14;
const int ID_BUTTONCHECK6 = 15;
const int ID_BUTTONCHECK7 = 16;

const int ID_CHECKBOX1 = 17;

const int ID_TEXT1 = 18;
const int ID_TEXT2 = 19;
const int ID_TEXT3 = 20;

const int ID_OPTIONCHECK1 = 21;
const int ID_OPTIONCHECK2 = 22;
const int ID_OPTIONCHECK3 = 23;
const int ID_OPTIONCHECK4 = 24;
const int ID_OPTIONCHECK5 = 25;
const int ID_OPTIONCHECK6 = 26;
const int ID_OPTIONCHECK7 = 27;
const int ID_OPTIONCHECK8 = 28;

const int TAB1 = 0;
const int TAB2 = 1;
const int BUTTON_ID1 = 29;
const int BUTTON_ID2 = 30;
const int BUTTON_ID3 = 31;
const int BUTTON_ID4 = 32;
const int BUTTON_ID5 = 33;
const int BUTTON_ID6 = 34;

class SubWindow
{
private:
	HWND g_button1;
	HWND g_text1;
	HWND list1;
	HWND list2;
	HWND list3;
	HWND dirCheckBoxes[4];
	HWND buttonCheckBoxes[7];
	HWND optionCheckBoxes[8];
	HWND maskCheckBox;
	HWND numText;
	HWND intervalText;
	HWND inputLenText;
	HWND button1;
	HWND button2;
	HWND button3;
	HWND button4;
	HWND hwnd;
	Object* obj;
	Player* player;

	static LRESULT CALLBACK WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	int cIdx1 = 0;
	int cIdx2 = 0;
	int cIdx3 = 0;
	Command operationCommand;

	void DispCommand();
	void DispCommands();
	void DispInput();
	void DispInputs();

	void AddInput();
	void DelInput();
	void AddCommand();
	void DelCommand();

	void OpCommandUpdate();
	void PlCommandUpdate();

	void ObjectMenuDraw(int _visible);
	void PlayerMenuDraw(int _visible);
public:
	HWND Init(HINSTANCE hInst);
	HWND GetWnd() { return hwnd; }
	void Show();
	void SetPlayer(Player* _p);
	void SetObject(Object* _o);
	void SetCommand();
	SubWindow();
	~SubWindow();
};

