#include "VInput.h"
#include <windows.h>

int commandMax = 32;

VInput::VInput()
{
	input = 0;
	commandDate.resize(commandMax);
	for (auto& a : adaptKeys)
	{
		a = 0;
	}
	for (auto& a : commandDate)
	{
		a = 0;
	}
	adaptKeys[dirUp] = 'W';
	adaptKeys[dirDown] = 'S';
	adaptKeys[dirLeft] = 'A';
	adaptKeys[dirRight] = 'D';

	adaptKeys[button01] = 'J';
	adaptKeys[button02] = 'K';
	adaptKeys[button03] = 'L';
	adaptKeys[button04] = 'U';
	adaptKeys[button05] = 'I';
	adaptKeys[button06] = 'O';
	adaptKeys[button07] = VK_SPACE;
}


VInput::~VInput()
{
}

bool VInput::SearchInputHistry(std::vector<unsigned short>& _tCommand, int _interval, bool _rev, unsigned char _option,unsigned short _checkMask)
{
	short interval = 0;
	int cIdx = _tCommand.size() - 1;
	unsigned short len = 0;
	unsigned short checkMask = 0x07ff;
	checkMask &= _checkMask;
	if ((_option&(1 << Release)))
	{
	}
	if (!(bool)(_option&(1 << Inadvance)))
	{
		unsigned short input = commandDate[0];
		if (_rev) {
			input = (input&(0xfff3)) + ((input & (1 << dirRight)) >> 1) + ((input & (1 << dirLeft)) << 1);
		}

		if ((input & checkMask) != (_tCommand[cIdx] & checkMask))
		{
			return false;
		}
	}
	//	for (auto input : commandDate)
	for (int i=0;i<commandDate.size();i++)
	{
		unsigned short input = commandDate[i];
		if (_rev) {
			input = (input&(0xfff3)) + ((input & (1 << dirRight)) >> 1) + ((input & (1 << dirLeft)) << 1);
		}
		unsigned short test1 = (input & checkMask);
		unsigned short test2 = (_tCommand[cIdx] & checkMask);
		if ((input & checkMask) == (_tCommand[cIdx] & checkMask))
		{
			len += (input & 0xf800) >> 0x0b;
			unsigned short s = (_tCommand[cIdx] & 0xf800) >> 0x0b;
			if (len >= s)
			{
				cIdx--;
				interval += len - s;
				len = 0;
			}
		}
		else
			interval += (input & 0xf800) >> 0x0b;
		if (cIdx < 0)
		{
			if ((_option&(1 << Flash)))
			{
				for (auto& c : commandDate)
					c = 0;
			}

			return true;
		}
		if (interval >= _interval)
			break;

	}
	return false;
}
bool VInput::SearchMoveInputHistry(std::vector<short> _target, int _interval, bool _rev)
{
	int interval = 0;
	int i = _target.size() - 1;
	int len = 0;
	if (_rev) {
		for (auto input : commandDate)
		{
			if ((input & _target[i] & 0x000f) == _target[i] & 0x000f)
			{
				len += input & 0xf800;
				if (len > _target[i] & 0xf800)
				{
					i--;
					interval += (len - (_target[i] & 0xf800)) >> 0x0800;
				}
			}
			else
				interval += (input & 0xf800) >> 0x0800;
			if (i < 0)
			{
				return true;
			}
			if (interval > _interval)
				break;
		}
	}
	else
	{
		for (auto input : commandDate)
		{
			short rInput = (input&(0x7f3)) + ((input & (1 << dirRight)) >> 1) + ((input & (1 << dirLeft)) << 1);
			if ((rInput & _target[i]&0x00f) == _target[i] & 0x000f)
			{
				len += input & 0xf800;
				if (len > _target[i] & 0xf800)
				{
					i--;
					interval += (len - (_target[i] & 0xf800)) >> 0x0800;
				}
			}
			else
				interval += (input & 0xf800) >> 0x0800;
			if (i < 0)
			{
				return true;
			}
			if (interval > _interval)
				break;

		}
	}
	return false;
}

void VInput::Update(unsigned char _keyState[256])
{
	prevInput = input;
	input = 0;
	for (int i = 0; i < inputMax; i++)
	{
		if (adaptKeys[i] == 0)
			continue;

		if (_keyState[adaptKeys[i]] & 0x80)
		{
			input += 1 << i;
		}
	}
	unsigned short newCommand = input & 0x07ff;
	if ((newCommand & 0x07ff) == (commandDate[0] & 0x07ff))
	{
		if ((commandDate[0] & 0xf800) == 0xf800)
		{
			for (int i = commandMax - 1; i > 0; i--)
			{
				commandDate[i] = commandDate[i - 1];
			}
			commandDate[0] = newCommand;
		}
	}
	else
	{
		for (int i = commandMax - 1; i > 0; i--)
		{
			commandDate[i] = commandDate[i - 1];
		}
		commandDate[0] = newCommand;
	}
	commandDate[0] += 0x0800;
	return;
}
