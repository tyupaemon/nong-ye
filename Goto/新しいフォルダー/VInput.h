#pragma once
#include"../DX12/common.h"
#include<vector>
#include<array>
enum CommandOption {
	Strict,
	Reverse,
	Flash,
	Inadvance,
	Release,

};
const unsigned short DirOnly=0x000f;
const unsigned short ButtonOnly=0x07f0;


enum InputName {
	dirUp,
	dirDown,
	dirLeft,
	dirRight,
	button01,
	button02,
	button03,
	button04,
	button05,
	button06,
	button07,
	button08,
	button09,
	button10,
	button11,
	button12,
	button13,
	button14,
	button15,
	button16,
	button17,
	button18,
	button19,
	button20,
	button21,
	button22,
	button23,
	button24,
	button25,
	button26,
	button27,
	button28,
	inputMax
};
class VInput
{
private:
	VInput();
	VInput(const VInput&);
	VInput& operator=(const VInput&) {};

	//4方向+6ボタン+挑発
	//残り5bitに同一入力の連続した時間
	std::vector<unsigned short> commandDate;
	int input;
	int prevInput;
	unsigned char adaptKeys[inputMax];
	float angle1;
	float angle2;
public:
	static VInput& Instance()
	{
		static VInput instance;
		return instance;
	}

	bool SearchInputHistry(std::vector<unsigned short>& target, int allowInterval,bool _rev, unsigned char _option,unsigned short _checkMask=0x07ff);
	bool SearchMoveInputHistry(std::vector<short> _target, int _interval, bool _dir);
	void Update(unsigned char _keyStake[256]);
	~VInput();
};

