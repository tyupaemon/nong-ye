Texture2D<float4> tex:register(t0);
Texture2D<float> shadowTex:register(t3);
Texture2D<float4> tex2:register(t1);
Texture2D<float4> tex3:register(t2);
SamplerState smp : register(s0);
SamplerState smp2 : register(s1);
#define MATMAX (1024)
cbuffer strData : register(b1) {
	float4 sColor;
	float2 sPos;
	float sRaito;
	float sSize;
}
cbuffer cBuf : register(b2)
{
}

struct Output {
	float4 svpos : SV_POSITION;
	float4 lvPos : POSITION;
	float2 uv : TEXCOORD;
	float4 normal : NORMAL;
	float edge : EDGE;
	float size : PSIZE;
	//	int bone;
};

Output BoardVS(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	Output o;
	//o.svpos = mul(mul(viewproj, world), pos);
	o.svpos = pos;
	o.uv = uv;
	return o;
}

float4 BoardPS(Output o) : SV_Target
{

	float4 color = tex.Sample(smp, o.uv);
	//return float4(0, 0, 0, 1);
	if (color.a > 0)
	{
//		return float4(o.uv,0,1);
		return sColor;
		return float4(0, 0, 0, 1);
		//discard;
	}

	return color;

}
Output StrVS(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	Output o;
	//o.svpos = mul(mul(viewproj, world), pos);
	o.svpos = pos;
	o.svpos.xy *= sSize;
	o.svpos.xy += sPos*sSize;
	o.uv = uv;
	return o;
}

float4 StrPS(Output o) : SV_Target
{
	float4 color = tex.Sample(smp, o.uv);
	//return float4(0, 0, 0, 1);
	if (color.a > 0)
	{
		//		return float4(o.uv,0,1);
		return sColor;
		return float4(0, 0, 0, 1);
		//discard;
	}

	return color;

}
