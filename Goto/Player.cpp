#include "Player.h"
#include "../PMX/PMXObject.h"


#define BOUNDARYTILT 11000
#define GamePadNumber 0

using namespace DirectX;
Player::Player()
{
	target = nullptr;
	stateFunction[NeutralState] = [&]() {return Neutral(); };
	stateFunction[MoveState] = [&]() {return Move(); };
	stateFunction[FishCatchState] = [&]() {return FishCatch(); };
	stateFunction[VegetableHarvestState] = [&]() {return VegetableHarvest(); };
	stateFunction[PlantState] = [&]() {return Plant(); };

	nowState = stateFunction[NeutralState];



	//char str[256];
	//int idx;
	//std::function<void(char)> f = [&](char c) {
	//	idx = 0;
	//	str[idx] = c;
	//	f = [&](char c) {
	//		idx++;
	//		str[idx] = c;

	//	};
	//};
	//f('A');
	//f('B');
}



Player::~Player()
{
}

XMVECTOR Player::GetPos()
{
	XMVECTOR ret = {0,0,0,0};
	if (target != nullptr) {
		ret = target->pos;
	}
	return ret;
}
XMFLOAT3 Player::GetRot()
{
	XMFLOAT3 ret;
	if (target != nullptr) {
		ret = target->rot;
	}
	return ret;
}

void Player::SetPos(XMVECTOR _pos)
{
	if (target != nullptr) {
		target->pos = _pos;
	}
}
void Player::SetRot(XMFLOAT3 _rot)
{
	if (target != nullptr) {
		target->rot = _rot;
	}
}

void Player::Update()
{
		

	nowState();

}

int Player::GetID()
{
	if (target != nullptr)
	{
		return target->objId; 
	}
	return -1;
}

void Player::Neutral()
{
	//スティックが傾いているなら移動状態にする
	if (_input.CheckTilted(GamePadNumber, ThumbLX, BOUNDARYTILT) || _input.CheckTilted(GamePadNumber, ThumbLX, -BOUNDARYTILT)
		|| _input.CheckTilted(GamePadNumber, ThumbLY, BOUNDARYTILT) || _input.CheckTilted(GamePadNumber, ThumbLY, -BOUNDARYTILT))
	{
		float x_ratio = _input.GetTilted(GamePadNumber, ThumbLX) / 32767.0f;
		float y_ratio = _input.GetTilted(GamePadNumber, ThumbLY) / 32767.0f;

		if (abs(x_ratio) < 0.2f)
		{
			x_ratio = 0.0f;
		}
		if (abs(y_ratio) < 0.2f)
		{
			y_ratio = 0.0f;
		}

		if (x_ratio != 0.0f || y_ratio != 0.0f)
		{
		
			nowState = stateFunction[MoveState];
			target->LoadMotion("Resource/モーション/walk.vmd");
			target->MotionPlayback();
			target->SetPostMotionLoop(0);

			return;
		}
	}

	//野菜を植える
	if (_input.PushButton(GamePadNumber, XINPUT_GAMEPAD_A))
	{
		nowState = stateFunction[PlantState];
		target->LoadMotion("Resource/モーション/FishCatch.vmd");
		target->MotionPlayback();
		target->SetPostMotionLoop(0);
		_waitTime = 60;
	}
}
void Player::Move()
{

	XMVECTOR pos = GetPos();
	XMFLOAT3 rot;
	//target->rot.y += 0.01;



	if (_input.CheckTilted(GamePadNumber, ThumbLX, BOUNDARYTILT) || _input.CheckTilted(GamePadNumber, ThumbLX, -BOUNDARYTILT)
		|| _input.CheckTilted(GamePadNumber, ThumbLY, BOUNDARYTILT) || _input.CheckTilted(GamePadNumber, ThumbLY, -BOUNDARYTILT))
	{
		float x_ratio = _input.GetTilted(GamePadNumber, ThumbLX) / 32767.0f;
		float y_ratio = _input.GetTilted(GamePadNumber, ThumbLY) / 32767.0f;

		if (abs(x_ratio) < 0.2f)
		{
			x_ratio = 0.0f;
		}
		if (abs(y_ratio) < 0.2f)
		{
			y_ratio = 0.0f;
		}

		//移動
		pos.m128_f32[0] += _moveSpeed * x_ratio;
		pos.m128_f32[2] += _moveSpeed * y_ratio;
		SetPos(pos);


		//回転
		float rad = atan2f(-x_ratio, -y_ratio);

		rot.y = rad;
		SetRot(rot);

		return;
	}

	nowState = stateFunction[NeutralState];
	target->LoadMotion("Resource/モーション/Neutral.vmd");
	target->MotionPlayback();
	target->SetPostMotionLoop(0);

}
void Player::FishCatch()
{

}
void Player::VegetableHarvest()
{

}
void Player::Plant()
{
	_waitTime--;
	if (_waitTime == 0)
	{
		nowState = stateFunction[NeutralState];
		target->LoadMotion("Resource/モーション/Neutral.vmd");
		target->MotionPlayback();
		target->SetPostMotionLoop(0);
	}
		
}