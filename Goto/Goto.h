#pragma once
#include"Camera.h"
#include"RenderableObject.h"
#include"../PMX/PMXObject.h"
#include"Player.h"
#include"../Instancing/Instancing.h"
#include"../Instancing/InstancingObject.h"
#include"FieldConfig.h"
#include"../2DPolygon/DrawString.h"

#include<time.h>
#include<DirectXMath.h>
#include<memory>
#define SAVE_DATA SaveData1
class Goto
{
private:
	PMXObject obj;
	Player& p;
	std::vector<std::shared_ptr<InstancingObject>> inses;
	InstancingObject* o;
	int insID[INST_MAX];
	FieldConfig& fConf;
	DrawString dStr;

	struct SaveData1
	{
		XMVECTOR pos;
		XMFLOAT3 rot;
		int mapID;
		int level;
		int equip;
		char item[128];
		time_t time;
	};
	SAVE_DATA s;
public:
	void Init();
	void Update();
	//RenderableObjectを継承しているオブジェクトの更新、プレイヤーの更新、オブジェクトの生成を行う

	void Draw();
	//RenderableObjectを継承しているオブジェクトの描画を行う

	void Load(std::string& _path);
	void Save();
	Goto();
	~Goto();

	Player& GetPlayer() { return p; };
};

