#pragma once
#include <string>
#include <vector>
#include <map>

#include"FieldStructs.h"

//農作業用データ読み込みクラス
class FieldConfig
{
private:
	FieldConfig();
	FieldConfig(const FieldConfig&);
	FieldConfig& operator=(const FieldConfig&) {};

	std::vector<Growth> growthes;
	std::vector<Havest> havests;
	std::vector<Vegetable> vegetables;
	std::map <std::string, int> nameIDs;
	void VegetableLoad(std::string _name);
	void ConfigLoad();
public:
	static FieldConfig& Instance()
	{
		static FieldConfig instance;
		return instance;
	}

	const std::vector<Growth>& GetGrowthData() {return growthes;}
	const std::vector<Havest>& GetHavestData() { return havests; }
	const std::vector<Vegetable>& GetVegetableData() { return vegetables; }
	int GetID(std::string _str) { return nameIDs[_str]; }
	std::map <std::string, int>& GetIDMap() { return nameIDs; }

	~FieldConfig();
};

