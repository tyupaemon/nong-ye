#include "FieldConfig.h"
#include <functional>
const char FILE_NAME[] = { "fConf.csv" };
const char FILE_PATH[] = {"Resource/Goto/"};

FieldConfig::FieldConfig()
{
	ConfigLoad();
}

FieldConfig::~FieldConfig()
{
}
#include <fstream>
#include <iostream>
#include <string>
#include<sstream>
void FieldConfig::ConfigLoad()
{
	{
		std::string fName = FILE_PATH;
		fName += FILE_NAME;
		std::ifstream ifs(fName);
		std::string line;
		int num = 0;
		char buf[64] = {};
		unsigned int i;
		while (getline(ifs, line)) {
			if ((line[0] == '#'))
				continue;
			std::istringstream stream(line);
			std::string field;
			while (getline(stream, field, ','))
			{
				if ((field[0] == '#'))
				{
					break;
				}
				sscanf(field.c_str(), "%s", buf);
				VegetableLoad(buf);
			}
		}
		ifs.close();
	}

}

void FieldConfig::VegetableLoad(std::string _name)
{
	Vegetable v;
	v.havestID = (int)vegetables.size();
	v.name = _name;

	Growth g;
	Havest h;
	int num = 0;
	char buf[64] = {};

	std::function<void()> StrSet = [&]()
	{
		v.dispName = buf;
		StrSet = [&]() {
			v.modelName[num - 1] = buf;
		};
	};
	std::function<void()> ValSet = [&]()
	{
		v.value = buf[0];
		ValSet = [&]() {
			g.state[num - (VegeStatNum - 2)] = buf[0];
			if (num - (VegeStatNum - 2) == GSMax-1)
			{
				ValSet = [&]() {
					h.state[num - (VegeStatNum - 2) - GSMax] = buf[0];
				};
			}
		};
	};
	{
		std::ifstream ifs(FILE_PATH+_name + ".csv");
		std::string line;
		while (getline(ifs, line)) {
			if ((line[0] == '#'))
				continue;
			std::istringstream stream(line);
			std::string field;
			while (getline(stream, field, ','))
			{
				if ((field[0] == '#'))
				{
					break;
				}
				if (num<StrStatNum - 1)
				{
					sscanf(field.c_str(), "%s", buf);
					StrSet();
				}
				else
				{
					sscanf(field.c_str(), "%d", buf);
					ValSet();
				}
				num++;
			}
		}
		ifs.close();
	}
	vegetables.push_back(v);
	havests.push_back(h);
	growthes.push_back(g);
	nameIDs[_name] = nameIDs.size();
	return;
}