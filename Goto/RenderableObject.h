#pragma once
#include<DirectXMath.h>
#include<memory>
#include<vector>
#include<functional>
class Camera;
class Player;
class CollisionObject;
struct ID3D12DescriptorHeap;
struct ID3D12Resource;
using namespace DirectX;

enum ObjectOption{
	O_DHEnable,//デスクリプタヒープの有効化（デフォルト引数で有効）
};
//当たり判定の種類
//1<<種類名のビットをフラグとして使う
enum CollisionType
{
	C_Enable,		//当たり判定の有効化
	C_PushBack,		//押し戻しを行う
	C_Static,		//固定オブジェクト
	
};

//衝突中オブジェクトの種類判別
//リアクションを決定する
enum ObjectLabel
{
	L_Character,	//人
	L_Field,		//畑
	L_Produce,		//野菜
	L_Fish,			//魚
	
};

//3Dモデルを描画するオブジェクトの継承用クラス
//座標と回転と変換行列バッファビューを持つ
//作成時に管理クラスに登録され、自動でUpdateとDrawを実行される
class RenderableObject
{
friend Camera;
friend Player;
friend CollisionObject;
//モデル描画用のバッファデータ（プロジェクション、ライトなどの全体で使うデータ以外）
struct DrawModelData {
	XMMATRIX world;
};
protected:
	XMVECTOR pos;
	XMFLOAT3 rot;
	float wid,hei;

	ID3D12DescriptorHeap* pcbMDdataDescHeap = nullptr;
	ID3D12Resource* pcbMDdataBuf = nullptr;
	DrawModelData* pDModelData;
	int objId;
	
	//当たり判定ステータス
	ObjectLabel label=L_Fish;
	char colType = 1<<C_Enable;
	std::vector<RenderableObject*> hittingObjs;
	ObjectLabel hitLabel;

	//衝突中オブジェクトのソート用比較関数
	bool SortComp(const RenderableObject* o1, const RenderableObject* o2);
	std::vector<std::function<void()>> release;
public:
	RenderableObject(char _type=(1<<O_DHEnable));
	~RenderableObject();
	virtual void Draw() {};
	virtual void Update() {};

	const XMVECTOR& GetPos() { return pos; }
	const XMFLOAT3& GetRot() { return rot; }
	virtual void SetPos(XMVECTOR _pos) { pos = _pos; }
	virtual void SetRot(XMFLOAT3 _rot) { rot = _rot; }
	void SetID(int _id) { objId = _id; }
	
	float GetWidth() { return wid; }
	float Gethei() { return hei; }
	char GetCollisionType() { return colType; }

	//衝突判定補助関数
	//衝突中オブジェクトの追加
	void SetHitObject(RenderableObject*& _obj);
	//衝突中オブジェクトのリセット
	void FlushHitObject();
	//衝突中オブジェクトのソート
	void SortHitObject();
	std::vector<RenderableObject*>& GetHittingObjects() { return hittingObjs; };

};

