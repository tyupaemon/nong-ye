#pragma once

#include<DirectXMath.h>
#include<d3d12.h>
#include"RenderableObject.h"
#include"../2DPolygon/bgtest.h"

using namespace DirectX;
struct Situation {
	XMMATRIX world;
	XMMATRIX camera;
	XMFLOAT3 eye;
	XMMATRIX lightVP;
	XMFLOAT3 light;
	XMFLOAT3 lightDiffuse;
	XMFLOAT3 lightAmbient;
	XMFLOAT3 lightSpecular;
};
//シングルトンクラス
//注視点とカメラ位置に関する値を取得できる
class Camera
{
private:
	Camera();
	Camera(const Camera&);
	Camera& operator=(const Camera&) {};

	XMMATRIX view;
	XMMATRIX proj;

	XMVECTOR front = { 0,4,-4 };

	XMVECTOR pos;
	XMVECTOR relative;
	XMVECTOR light;
	XMVECTOR lookPos = { 0,0,-5,1 };
	float lightDif[3];
	float lightAmb[3];
	float lightSpe[3];
	float nearLim;
	float farLim;
	float rot[2];
	float dist;
	float ratio;
	bool flg;
	RenderableObject* target;
	ID3D12DescriptorHeap* pcbSituationDescHeap = nullptr;
	ID3D12Resource* pcbSituationBuf = nullptr;
	Situation* pSituation;
	void UpdateSituation();

	void FreeCamera();
	void ChaseCamera();
	bgtest bg;

public:
	~Camera();
	static Camera& Instance()
	{
		static Camera instance;
		return instance;
	}

	XMMATRIX & GetView() { return view; };
	XMMATRIX& GetProj() { return proj; };
	void SetTarget(RenderableObject* _target) { target = _target; };
	void SetRelativePos(float x, float y, float z) {
		relative.m128_f32[0] = x;
		relative.m128_f32[1] = y;
		relative.m128_f32[2] = z;
	};

	void SetLightPos(float x, float y, float z) {
		light.m128_f32[0] = x;
		light.m128_f32[1] = y;
		light.m128_f32[2] = z;
	};

	void SetLightDif(float _r, float _g, float _b)
	{
		lightDif[0] = _r;
		lightDif[1] = _g;
		lightDif[2] = _b;
	}
	void SetLightSpe(float _r, float _g, float _b)
	{
		lightSpe[0] = _r;
		lightSpe[1] = _g;
		lightSpe[2] = _b;
	}
	void SetLightAmb(float _r, float _g, float _b)
	{
		lightAmb[0] = _r;
		lightAmb[1] = _g;
		lightAmb[2] = _b;
	}
	void SetNearLimit(float _near)
	{
		nearLim = _near;
	}
	void SetFarLimit(float _far)
	{
		farLim = _far;
	}
	void Update();
	//変換行列、光源情報をCBV1に設定
	void SituationSet();
	//Y軸回転
	float GetYaw() { return rot[1]; }

	//X軸回転
	float GetPitch() { return rot[0]; }

	float GetDistance() { return dist; }
	XMVECTOR GetPos() { return pos; }
	XMVECTOR GetTargetPos();
};

