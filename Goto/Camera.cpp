#include "Camera.h"

#include "../DX12/DirectX12Device.h"
#include "../DX12/common.h"
#include <math.h>
#define toRAD(a) a/180.0f*3.141592f
#define fromRAD(a) a*180.0f/3.141592f

const XMVECTOR relativeInit = { 0,10,-15,1 };

Camera::Camera()
{

	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };

	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(Situation) + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&pcbSituationBuf));
	assert(SUCCEEDED(result));

	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbSituationDescHeap));
	assert(SUCCEEDED(result));

	cbViewDesc.BufferLocation = pcbSituationBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(Situation) + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, pcbSituationDescHeap->GetCPUDescriptorHandleForHeapStart());

	D3D12_RANGE range = {};
	result = pcbSituationBuf->Map(0, &range, (void**)&pSituation);
	
	
	SetRelativePos(0, 10, 15);
	SetLightPos(-50, 50, -50);
	SetLightDif(0.5f,0.5f,0.5f);
	SetLightSpe(0.5f,0.5f,0.5f);
	SetLightAmb(0.5f,0.5f,0.5f);

	nearLim = 0.1f;
	farLim = 500.0f;
	rot[0] = rot[1] = 0;
	ratio = 0.5f;
	flg = true;
}


Camera::~Camera()
{
	pcbSituationBuf->Unmap(0, nullptr);
	if (pcbSituationDescHeap != nullptr)
	{
		pcbSituationDescHeap->Release();
	}
	if (pcbSituationBuf != nullptr)
	{
		pcbSituationBuf->Release();
	}
}

void Camera::UpdateSituation()
{

}

void Camera::Update()
{
	static bool pre = 0;
	bool now = GetKeyState(VK_SHIFT) & 0x8000;
	if (!flg)
	{
		FreeCamera();
		if ((!pre)&now) {
			flg = !flg;
			relative.m128_f32[1] = pos.m128_f32[1] - target->pos.m128_f32[1];
			relative.m128_f32[2]= sqrt(
				pow(pos.m128_f32[0] - target->pos.m128_f32[0], 2)+ 
				pow(pos.m128_f32[2] - target->pos.m128_f32[2], 2));
		//	relative = XMVector4Transform(relative, XMMatrixRotationRollPitchYaw(0, -rot[1], 0));

			front = { 0,4,-4 };

		}
	}
	else
	{
		ChaseCamera();
		if ((!pre)&now) {

			XMVECTOR temp1, temp2;
			temp1 = front = lookPos - pos;
			front = XMVector4Transform(front, XMMatrixRotationY(-rot[1]));
			front = XMVector4Transform(front, XMMatrixRotationX(+rot[0]));
			;
			flg = !flg;
		}
	}
	pre = now;
}

void Camera::FreeCamera()
{
	if (GetKeyState('1') & 0x8000)
	{
		ratio = 0.5f;
	}
	if (GetKeyState('2') & 0x8000)
	{
		ratio = 1.0f;
	}
	if (GetKeyState('3') & 0x8000)
	{
		ratio = 1.5f;
	}
	if (GetKeyState('4') & 0x8000)
	{
		ratio = 3.0f;
	}
	if (GetKeyState('5') & 0x8000)
	{
		ratio = 10.0f;
	}

#pragma region move
	if (!(bool)(GetKeyState(VK_CONTROL) & 0x80))
	{
		if (GetKeyState('A') & 0x8000)
		{
			pos.m128_f32[2] -= sin((rot[1]))*ratio;
			pos.m128_f32[0] += cos((rot[1]))*ratio;
			lookPos.m128_f32[2] -= sin((rot[1]))*ratio;
			lookPos.m128_f32[0] += cos((rot[1]))*ratio;
		}
		if (GetKeyState('D') & 0x8000)
		{
			pos.m128_f32[2] += sin((rot[1]))*ratio;
			pos.m128_f32[0] -= cos((rot[1]))*ratio;
			lookPos.m128_f32[2] += sin((rot[1]))*ratio;
			lookPos.m128_f32[0] -= cos((rot[1]))*ratio;

		}
		if (GetKeyState('W') & 0x8000)
		{
			pos.m128_f32[0] -= sin((rot[1]))*ratio;
			pos.m128_f32[2] -= cos((rot[1]))*ratio;
			lookPos.m128_f32[0] -= sin((rot[1]))*ratio;
			lookPos.m128_f32[2] -= cos((rot[1]))*ratio;

		}
		if (GetKeyState('S') & 0x8000)
		{
			pos.m128_f32[0] += sin((rot[1]))*ratio;
			pos.m128_f32[2] += cos((rot[1]))*ratio;
			lookPos.m128_f32[0] += sin((rot[1]))*ratio;
			lookPos.m128_f32[2] += cos((rot[1]))*ratio;

		}
		if (GetKeyState(VK_SPACE) & 0x8000)
		{
			if (GetKeyState('R') & 0x8000)
			{
				rot[0] += toRAD(1.f)*ratio;
			}
			if (GetKeyState('F') & 0x8000)
			{
				rot[0] -= toRAD(1.f)*ratio;
			}
		}
		else
		{
			if (GetKeyState('R') & 0x8000)
			{
				pos.m128_f32[1] += .2f*ratio;
				lookPos.m128_f32[1] += .2f*ratio;
			}
			if (GetKeyState('F') & 0x8000)
			{
				pos.m128_f32[1] -= .2f*ratio;
				lookPos.m128_f32[1] -= .2f*ratio;
			}
		}
		if (GetKeyState('Q') & 0x8000)
		{
			rot[1] -= toRAD(1.f)*ratio;
			XMVECTOR temp = lookPos - pos;
			//	temp = XMVector4Transform(temp, XMMatrixRotationRollPitchYaw(rot[0], rot[1], 0));
			temp = XMVector4Transform(temp, XMMatrixRotationRollPitchYaw(0, (rot[1]), 0));
			temp += pos;
			lookPos = temp;

		}
		if (GetKeyState('E') & 0x8000)
		{
			rot[1] += toRAD(1.f)*ratio;
			XMVECTOR temp = lookPos - pos;
			//	temp = XMVector4Transform(temp, XMMatrixRotationRollPitchYaw(rot[0], rot[1], 0));
			temp = XMVector4Transform(temp, XMMatrixRotationRollPitchYaw(0, (rot[1]), 0));
			temp += pos;
			lookPos = temp;

		}
	}

	auto temp2 = XMVector4Transform(XMVector4Transform(front, XMMatrixRotationX(-rot[0])), XMMatrixRotationY(rot[1]));

	lookPos = pos + temp2;

#pragma endregion
	XMVECTOR up = { 0,1,0 };

	XMVECTOR o = { 0,0,0 };

	view = XMMatrixLookAtLH(pos, lookPos, up);
	//画角,アス比,表示限界（近）,表示限界（遠）
	proj = XMMatrixPerspectiveFovLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	XMMATRIX viewproj = view * proj;

	//XMMATRIX lVP = XMMatrixLookAtLH(light, target->pos, up)*XMMatrixOrthographicLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	XMMATRIX lVP = XMMatrixLookAtLH(light, o, up)*XMMatrixPerspectiveFovLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	memcpy(&pSituation->eye, &pos, sizeof(float) * 3);
	memcpy(&pSituation->camera, &viewproj, sizeof(DirectX::XMMATRIX));
	memcpy(&pSituation->light, &light, sizeof(float) * 3);
	memcpy(&pSituation->lightVP, &lVP, sizeof(DirectX::XMMATRIX));


	memcpy(&pSituation->lightDiffuse, &lightDif, sizeof(float) * 3);
	memcpy(&pSituation->lightAmbient, &lightAmb, sizeof(float) * 3);
	memcpy(&pSituation->lightSpecular, &lightSpe, sizeof(float) * 3);

}

//プレイヤーに追従するカメラ
void Camera::ChaseCamera()
{
	XMVECTOR vec = -pos;
	vec.m128_f32[1] = 0;
	vec = XMVector3Normalize(vec);
	if (GetKeyState('A') & 0x8000)
	{
		rot[1] += 1.0f * 3.1415f / 180.0f;
	}
	if (GetKeyState('D') & 0x8000)
	{
		rot[1] -= 1.0f * 3.1415f / 180.0f;
	}
	if (GetKeyState('W') & 0x8000)
	{
		relative.m128_f32[2] -= 0.2f;
	}
	if (GetKeyState('S') & 0x8000)
	{
		relative.m128_f32[2] += 0.2f;
	}
	if (GetKeyState('R') & 0x8000)
	{
		relative.m128_f32[1] += 0.2f;
	}
	if (GetKeyState('F') & 0x8000)
	{
		relative.m128_f32[1] -= 0.2f;
	}
	/*
	if (GetKeyState('Q') & 0x8000)
	{
		target->rot.y -= 1.f / 180.f*3.1415f;
	}
	if (GetKeyState('E') & 0x8000)
	{
		target->rot.y += 1.f / 180.f*3.1415f;
	}
	*/
	if (GetKeyState('T') & 0x8000)
	{
		rot[1] = target->rot.y;
	}

	XMVECTOR up = { 0,1,0 };
	pos = target->pos + XMVector4Transform(relative, XMMatrixRotationRollPitchYaw(0, rot[1], 0));
	rot[0] = atan2(pos.m128_f32[1] - lookPos.m128_f32[1],
		sqrt(pow(pos.m128_f32[2] - lookPos.m128_f32[2],2)+ pow(pos.m128_f32[0] - lookPos.m128_f32[0], 2))
	);
	lookPos = target->pos + XMVector4Transform(front, XMMatrixRotationRollPitchYaw(rot[0], rot[1], 0));
	XMVECTOR o = { 0,0,0 };
//	rot[0] = atan2(pos.m128_f32[1] - target->pos.m128_f32[1], pos.m128_f32[2] - target->pos.m128_f32[2]);
	//	view = XMMatrixLookAtLH(target->pos + pos, target->pos, up);
	view = XMMatrixLookAtLH(pos, lookPos, up);
	//画角,アス比,表示限界（近）,表示限界（遠）
	proj = XMMatrixPerspectiveFovLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	XMMATRIX viewproj = view * proj;

	//XMMATRIX lVP = XMMatrixLookAtLH(light, target->pos, up)*XMMatrixOrthographicLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	XMMATRIX lVP = XMMatrixLookAtLH(light, o, up)*XMMatrixPerspectiveFovLH(PI / 4, (float)(WINDOW_WID) / (float)(WINDOW_HEI), nearLim, farLim);
	memcpy(&pSituation->eye, &(target->pos + pos), sizeof(float) * 3);
	memcpy(&pSituation->camera, &viewproj, sizeof(DirectX::XMMATRIX));
	memcpy(&pSituation->light, &light, sizeof(float) * 3);
	memcpy(&pSituation->lightVP, &lVP, sizeof(DirectX::XMMATRIX));


	memcpy(&pSituation->lightDiffuse, &lightDif, sizeof(float) * 3);
	memcpy(&pSituation->lightAmbient, &lightAmb, sizeof(float) * 3);
	memcpy(&pSituation->lightSpecular, &lightSpe, sizeof(float) * 3);

}
void Camera::SituationSet()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbSituationDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV1, pcbSituationDescHeap->GetGPUDescriptorHandleForHeapStart());
	bg.draw();
}

XMVECTOR Camera::GetTargetPos()
{
	if (flg)
	{
		return(pos + lookPos);
	}
	else
	{
		return target->pos;
	}
}
