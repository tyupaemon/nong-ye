#include "FontManager.h"
#include <fstream>
#include<sstream>
#include<string>
#include<codecvt>
#include<tchar.h>
#include "../DX12/DirectX12Device.h"
#include "../DX12/d3dx12.h"
#include"../DX12/common.h"
struct Font {
	std::string fileName;
	std::wstring fontName;
};
//const std::string FONT_PATH = "../font/";
const std::wstring WFONT_PATH = _T("font/");
FontManager::FontManager()
{
	//DESIGNVECTOR design;
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;

	std::ifstream ifs(WFONT_PATH+_T("font.conf"));

	std::wstring buf = {};
	
	std::string line;
	while (getline(ifs, line)) {
		if ((line[0] == '#'))
			continue;
		std::istringstream stream(line);
		std::string field;
		getline(stream, field, ',');
//		sscanf(field.c_str(), "%s", buf);
		std::wstring wstr;

		wstr = WFONT_PATH + converter.from_bytes(field);

		bool b = AddFontResourceEx(
			&wstr[0],
			FR_PRIVATE,
			nullptr
		);
		getline(stream, field, ',');
		buf=converter.from_bytes(field.c_str());
		LOGFONT lf = {
			FONT_SIZE,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			SHIFTJIS_CHARSET,
			OUT_TT_ONLY_PRECIS,
			CLIP_DEFAULT_PRECIS,
			PROOF_QUALITY,
			FIXED_PITCH | FF_MODERN
		};
		memcpy(lf.lfFaceName, &buf[0], buf.length() * sizeof(WCHAR));
		HFONT hFont = CreateFontIndirect(&lf);
		fonts.push_back(hFont);
		pathes.push_back(wstr);
	}
	ifs.close();

}


FontManager::~FontManager()
{
	for (auto&path : pathes)
	{
		bool b = RemoveFontResourceEx(
			path.c_str(),
			FR_PRIVATE,
			nullptr
		);
	}
	for (auto& font : fonts)
	{
		DeleteObject(font);
	}
	for (auto& it : fontDatas)
	{
		DX_RESOURCE_RELEASE(it.second.prs);
	}
}


FontData& FontManager::GetFontTex(short _code, ID3D12Resource** _prs)
{
	if (fontDatas.find(_code) != fontDatas.end())
	{
		*_prs = fontDatas[_code].prs;
		return fontDatas[_code];
	}
	fontDatas[_code] = {};
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	D3D12_BOX box = {};
	box.front = 0;
	box.back = 1;
	box.top = box.left = 0;
	FontManager& fManager = FontManager::Instance();
	HFONT& hFont = fManager.GetFont(0);
	if (!(hFont))
	{
		_prs = nullptr;
		return fontDatas[_code];
	}
	HDC hdc = GetDC(NULL);
	HFONT oldFont = (HFONT)SelectObject(hdc, hFont);
	// フォントビットマップ取得
	TEXTMETRIC TM;
	GetTextMetrics(hdc, &TM);
	GLYPHMETRICS GM;
	MAT2 Mat = { { 0,1 },{ 0,0 },{ 0,0 },{ 0,1 } };
	DWORD f_size = GetGlyphOutline(hdc, _code, GGO_GRAY4_BITMAP, &GM, 0, NULL, &Mat);
	f_size = GetGlyphOutline(hdc, _code, GGO_GRAY4_BITMAP, &GM, 0, NULL, &Mat);
	BYTE* ptr = new BYTE[f_size];
	GetGlyphOutline(hdc, _code, GGO_GRAY4_BITMAP, &GM, f_size, ptr, &Mat);

	// デバイスコンテキストとフォントハンドルの開放
	SelectObject(hdc, oldFont);
	ReleaseDC(NULL, hdc);

	// Create texture
	D3D12_RESOURCE_DESC desc = {};
	//	desc.Width = GM.gmBlackBoxX;
	//	desc.Height = TM.tmHeight;
	desc.Width = desc.Height = FONT_SIZE;

	desc.MipLevels = 1;
	desc.DepthOrArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Flags = D3D12_RESOURCE_FLAG_NONE;
	desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;

	//	desc.MipLevels = (uint16_t)mipCount;
	//	desc.Format = format;
	//	desc.Flags = resFlags;
	//	D3D12_RESOURCE_FLAG_NONE;
	//	WIC_LOADER_DEFAULT;


	// CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_CPU_PAGE_PROPERTY_WRITE_BACK, D3D12_MEMORY_POOL_L0);
	//	ID3D12Resource* tex = nullptr;
	HRESULT hr = _device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&fontDatas[_code].prs));
	if (FAILED(hr))
	{
		*_prs = nullptr;
		return fontDatas[_code];
	}

	fontDatas[_code].wid = GM.gmCellIncX;
	fontDatas[_code].x = (int)GM.gmptGlyphOrigin.x;
	fontDatas[_code].y = (int)GM.gmptGlyphOrigin.y;
	D3D12_SUBRESOURCE_DATA cSub;
	std::vector<char> decodedData;
	int iOfs_x = (int)GM.gmptGlyphOrigin.x;
	int iOfs_y = (int)TM.tmAscent - GM.gmptGlyphOrigin.y;
	int iBmp_w = (int)GM.gmBlackBoxX + (4 - (GM.gmBlackBoxX % 4)) % 4;
	int iBmp_h = (int)GM.gmBlackBoxY;

	decodedData.resize(desc.Width*desc.Height * 4);

	int Level = 17;
	int x, y;
	DWORD Color;
	char Alpha;
	BYTE* pBits = (BYTE*)&decodedData[0];
	std::fill(decodedData.begin(), decodedData.end(), 0);
	for (y = iOfs_y; y < iOfs_y + iBmp_h; y++)
	{
		for (x = iOfs_x; x < iOfs_x + iBmp_w; x++)
		{
			Alpha = (255 * ptr[x - iOfs_x + iBmp_w * (y - iOfs_y)]) / (Level - 1);
			Color = 0x00ffffff | (Alpha << 24);
			decodedData[(x) * 4 + (desc.Width * 4)*y + 0] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 1] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 2] = Alpha;
			decodedData[(x) * 4 + (desc.Width * 4)*y + 3] = Alpha;
			//memcpy((BYTE*)pBits + hMappedResource.RowPitch * y + 4 * x, &Color, sizeof(DWORD));
		}
	}
	cSub.pData = &decodedData[0];
	cSub.RowPitch = desc.Width * 4;
	cSub.SlicePitch = desc.Width*desc.Height * 4;

	box.right = (UINT)cSub.RowPitch / 4;
	box.bottom = (UINT)(cSub.SlicePitch / cSub.RowPitch);

	HRESULT result = fontDatas[_code].prs->WriteToSubresource(0, &box, &decodedData[0], (UINT)desc.Width * 4, (UINT)decodedData.size());
	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(fontDatas[_code].prs,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();
	*_prs = fontDatas[_code].prs;
	return fontDatas[_code];
}

