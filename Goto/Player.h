#pragma once
#include <stdio.h>
#include <tchar.h>
#include "../Controller/Input.h"
#include<functional>
#include<DirectXMath.h>
enum StateFunction
{
	NeutralState,
	MoveState,
	FishCatchState,
	VegetableHarvestState,
	PlantState,
	PlayerStateFunctionMAX,
};

class PMXObject;

class Player
{
private:
	Player();
	Player(const Player&);
	Player& operator=(const Player&) {};

	PMXObject* target;

	//移動用
	float _moveSpeed = 1.0f;

	//入力用
	Input& _input = Input::Instance();

	//プレイヤーの状態
	//ニュートラル、移動、魚取り、野菜収穫、野菜植え
	void Neutral();
	void Move();
	void FishCatch();
	void VegetableHarvest();
	void Plant();

	int _waitTime = 0;

	//関数ポインタ,状態遷移に使用
	std::function<void()> stateFunction[PlayerStateFunctionMAX];
	std::function<void()> nowState;//今の状態を保存


public:
	static Player& Instance()
	{
		static Player instance;
		return instance;
	}

	DirectX::XMVECTOR GetPos();
	DirectX::XMFLOAT3 GetRot();
	int GetID();
	void SetPos(DirectX::XMVECTOR _pos);
	void SetRot(DirectX::XMFLOAT3 _rot);
	void SetTarget(PMXObject* _target) { target = _target; };
	PMXObject* GetTarget() { return target; };
	void Update();
	~Player();


};

