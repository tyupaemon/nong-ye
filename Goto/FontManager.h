#pragma once
#include <vector>
#include <map>
#include <string>
#include<windows.h>
#include<unordered_map>
const int FONT_SIZE = 128;
const float FONT_SIZEF = (float)FONT_SIZE;
struct ID3D12Resource;
struct FontData {
	int wid, x, y;
	ID3D12Resource* prs;
};
//シングルトンクラス
//フォントデータを読み込み、テクスチャを保存する
//フォントテクスチャの取得ができる
class FontManager
{
private:
	FontManager();
	FontManager(const FontManager&);
	FontManager& operator=(const FontManager&) {};
	std::map < std::wstring, std::map<int, HFONT> > fontsMap;
	std::vector <std::map<int, HFONT> > fontsVec;
	std::vector <HFONT> fonts;
	std::vector <std::wstring> pathes;
	std::unordered_map<short, FontData> fontDatas;
public:
	static FontManager& Instance()
	{
		static FontManager instance;
		return instance;
	}
	FontData& GetFontTex(short _code, ID3D12Resource** _prs);
	HFONT& GetFont(int _idx) { return fonts[_idx]; }
	~FontManager();
};

