#pragma once

#include "../2DPolygon/DrawString.h"
//文字列描画テスト用クラス
class DoDrawString
{
private:
	DrawString ds;

public:
	void Start();
	void Update();
	DoDrawString();
	~DoDrawString();
};

