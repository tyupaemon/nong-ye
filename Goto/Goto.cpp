#include "Goto.h"

#include "../PMX/PMXMotion.h"
#include "Save.h"
#include"../DX12/DirectX12Device.h"
#include <fstream>
#include <iostream>
#include<direct.h>
#include<DirectXMath.h>
#include<math.h>
#include<codecvt>
#include"../Instancing/InstancingManager.h"
#include"../Objects/ObjectController.h"
#include"../Objects/ProduceObject.h"
#include"FieldStructs.h"
#include"FieldConfig.h"
#include"FontManager.h"
#include"../StageEditor/StageEditor.h"
#include"../Objects/ObjectSaver.h"
#include<atlstr.h>
#include"../Mouse/Mouse.h"

const char g_filename[] = "PlayerSave.g";
const std::vector<std::string> exceptions =
{
	"house2.pmx",
	"scarecrow.pmx",
	"house1.pmx",
	"water_tower.pmx",
	"silo.pmx",
};
using namespace DirectX;
using namespace std;
Goto::Goto() :obj("Resource/主人公/test.pmx"),p(Player::Instance()),fConf(FieldConfig::Instance())
{
	_mkdir(g_savepath);
	Camera::Instance().SetTarget(&obj);
	p.SetTarget(&obj);
	obj.LoadMotion("Resource/モーション/FishCatch.vmd");
	PMXMotion m;
	FieldConfig::Instance();
	std::string path;
	path = g_savepath;
	path += g_filename;

	Load(path);

	o = nullptr;
	InstancingManager::Instance();
	std::vector<std::string> names = InstancingManager::Instance().GetInstancingNames();
	for (int i = 0; i < 10; i++)
	{
		short s = _T("0")[0] + i;
		ID3D12Resource* prs;
		FontManager::Instance().GetFontTex(s, &prs);
	}

}


Goto::~Goto()
{
}

void Goto::Init()
{
	StageEditor& stEdit = StageEditor::Instance();
	stEdit.Init(GetModuleHandle(NULL));

	auto h = FindWindow(_T("window"), _T("1501273後藤"));
	SetActiveWindow(h);
}
void Goto::Update()
{
	XMVECTOR vec = obj.GetPos();
	vec.m128_f32[0] = max(min(vec.m128_f32[0], 100), -100);
	vec.m128_f32[1] = max(min(vec.m128_f32[1], 10), -10);
	vec.m128_f32[2] = max(min(vec.m128_f32[2], 100), -100);
	obj.SetPos(vec);
//	obj.Update();
	p.Update();

	if (GetKeyState(VK_CONTROL) & 0x8000)
	{
		if (GetKeyState('S') & 0x8000)
		{
			Save();
		}
		{
			static bool pre = false;
			if (GetKeyState('Q') & 0x8000)
			{
				if (pre == false)
				{
					if (obj.GetHittingObjects().size())
					{
						//shared_ptr<InstancingObject> p(new InstancingObject(InstancingManager::Instance().GetInstancingNames()[0]));
						shared_ptr<InstancingObject> pProduce(new ProduceObject("tomato"));

						inses.push_back(pProduce);

						XMVECTOR pos = obj.GetPos();
						inses.back()->SetPos(obj.GetHittingObjects()[0]->GetPos());
					}
				}
				pre = true;
			}
			else
				pre = false;
		}
		{
			static bool pre = false;
			if (GetKeyState('W') & 0x8000)
			{
				if (pre == false)
				{
					//shared_ptr<InstancingObject> p(new InstancingObject(InstancingManager::Instance().GetInstancingNames()[1]));
					shared_ptr<InstancingObject> p(new ProduceObject("watermelon"));

					inses.push_back(p);

					XMVECTOR pos = obj.GetPos();
					inses.back()->SetPos(pos);
				}
				pre = true;
			}
			else
				pre = false;
		}
		{
			static bool pre = false;

			if (GetKeyState('E') & 0x8000)
			{
				if (pre == false)
				{
					if (inses.size() > 0)
						inses.pop_back();
				}
				pre = true;
			}
			else
				pre = false;
		}
		{
			static bool pre = false;
			dStr.SetPos(0, 0);

			if (Mouse::Instance().IsClick_L())
			{
				if (pre == false)
				{
					CStringW cstrw = StageEditor::Instance().GetCreateObjName().c_str();
					dStr.init(cstrw.GetString());
					auto& iObj = ObjectSaver::Instance().CreateInstancing(StageEditor::Instance().GetCreateObjName().c_str());
					iObj->SetPos(obj.GetPos());
				}
				pre = true;
			}
			else
				pre = false;
		}
	}
	ObjectController::Instance().Update();
	InstancingManager::Instance().Update();

}
void Goto::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };

	ObjectController::Instance().Draw();
	InstancingManager::Instance().Draw();
}

void Goto::Load(std::string& _path)
{

	ifstream f(_path, ios::in | ios::binary);
	if (f)
	{
		float ver;
		f.read((char*)&ver,sizeof(float));
		f.read((char*)&s, sizeof(SAVE_DATA));
		
	}
	else
	{
		s.pos.m128_f32[0] = s.pos.m128_f32[1] = s.pos.m128_f32[2] = s.pos.m128_f32[3] = 0;
		s.rot.x = s.rot.y = s.rot.z = 0;
		s.mapID = 1;
		s.level = 3;
		s.equip = 0;
		std::fill(&s.item[0], &s.item[127], 0);
	}
	p.SetPos(s.pos);
	p.SetRot(s.rot);
	f.close();
}

void Goto::Save()
{
	s.pos = p.GetPos();
	s.rot = p.GetRot();
	time(&s.time);
	std::ofstream f;
	std::string path;
	path = g_savepath;
	path += g_filename;
	f.open(path, ios::out | ios::binary | ios::trunc);
	float ver = 1;
	f.write((char*)&ver, sizeof(float));
	f.write((char*)&s, sizeof(SAVE_DATA));
	f.close();
}


