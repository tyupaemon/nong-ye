#pragma once
#include <string>

//農作業用構造体ヘッダ

const char SEASON_MASK = 0b11;

enum Seasons {
	spring,
	summer,
	autum,
	winter
};
//野菜データ
struct Vegetable
{
	std::string name;			//野菜名
	std::string dispName;		//表示用の名前
	std::string modelName[5];	//段階別モデル名
	int havestID;				//野菜番号
	int value;					//価値
};
const int VegeStatNum = 9;
const int StrStatNum = 7;
//成長データ
enum GrowthStates
{
GS_life,		
GS_step1,
GS_step2,
GS_step3,
GS_step4,
GS_unsafeNut,	
GS_dmgUnsafeNut,	
GS_dmgTyph,		
GS_dmgSnow,		
GS_season1,
GS_season2,
GS_season3,
GS_season4,
GS_increase,		
GS_enrich,		
GSMax
};
struct Growth
{
	char state[GSMax];
	/*
	unsigned char life;			//寿命
	char step[4];				//成長段階(種-（1）→芽-（2）→葉-（3）→実-（4）→収穫)
	unsigned char unsafeNut;	//必要な栄養
	unsigned char dmgUndafeNut;	//栄養不足ダメージ
	unsigned char dmgTyph;		//台風ダメージ
	unsigned char dmgSnow;		//雪ダメージ
	char seasons;				//季節適正:(float)((seasons>>Season)&SEASON_MASK)/2.0f
	unsigned char increase;		//収穫数増加に必要な値
	unsigned char enrich;		//品質上昇に必要な値
	*/
};
//収穫データ
enum HavestStates {
	HS_nutrition,	//栄養の消費
	HS_increase, 	//作物増加値
	HS_enrich,		//品質上昇値	
	HS_speed,		//成長速度
	HS_level,		//土経験値
	HS_exp,		//プレイヤー経験値
	HS_skill,		//スキル経験値
	HSMax
};
struct Havest
{
	char state[HSMax];
};


//作物データ
//土データ
struct Soil {
	unsigned char hp;	//体力(最大値はレベルで決定)
	char guard;			//耐性(最大値はレベルで決定)

	unsigned char nutrition;	//栄養
	unsigned char increase;		//作物増加
	unsigned char enrich;		//品質上昇
	unsigned char speed;		//成長速度:1+(speed/64)
	char level;
	bool water;
};
