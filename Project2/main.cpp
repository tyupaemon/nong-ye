#include "../lua\lua-5.3.0\src\lua.hpp"
#pragma comment(lib,"../lua/lua-5.3.0/src/lua5.3.0-static.lib")
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <windows.h>

#include <wrl.h>
#include <tchar.h>
#include <string>

#include "../DX12/common.h"
#include "../DX12/DirectX12Device.h"
#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

//谷口
#include "../DirectSoundtest/Sound.h"
#include "../Collision/CollisionObject.h"
#include "../Menu//Menu.h"

//後藤
#include"../Goto/Goto.h"
#include"../Goto/DoDrawString.h"
#include"../Objects/ObjectController.h"
#include<typeinfo>

//今村
#include"../imamuu/imamuu.h"

//金原
#include"../Controller/Input.h"
#include"../Mouse/Mouse.h"
#include"../MouseClick/MouseClick.h"
#include"../Time/Time.h"

#define  WIDTH 50

//using DirectX12Device;
using namespace std;
using namespace DirectX;
using Microsoft::WRL::ComPtr;

const TCHAR WND_CLASS_NAME[] = _T("window");
const int BONE_MAX = 1024;

struct CBMatrix {
	DirectX::XMMATRIX world;
	DirectX::XMMATRIX camera;
	DirectX::XMFLOAT3 eye;

};

LRESULT __stdcall WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	if (msg == WM_DESTROY) {
		PostQuitMessage(0);
		return 0;
	}
	if (msg == WM_LBUTTONDOWN)
	{
		EnableWindow(hwnd,true);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

HWND initWindow(HINSTANCE hInst)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX w = {};
	w.lpfnWndProc = (WNDPROC)WindowProcedure;
	w.lpszClassName = WND_CLASS_NAME;
	//w.hInstance = GetModuleHandle(NULL);
	w.hInstance = hInst;
	w.cbSize = sizeof(WNDCLASSEX);
	RegisterClassEx(&w);

	RECT wrc = { 0, 0, WINDOW_WID, WINDOW_HEI };
	::AdjustWindowRectEx(&wrc, WS_OVERLAPPEDWINDOW, false, 0);

	HWND hwnd = CreateWindowEx(0, w.lpszClassName,
		_T("1501273後藤"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		wrc.right - wrc.left,
		wrc.bottom - wrc.top,
		nullptr,
		nullptr,
		w.hInstance,
		nullptr);

	return hwnd;
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR cmdLine, int cmdShow)
{
	HRESULT result;
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	freopen("CONIN$", "r", stdin);

	HWND hwnd = initWindow(hInst);
	DragAcceptFiles(hwnd, true);
	ShowWindow(hwnd, SW_SHOW);

	HRESULT hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

	DirectX12Device& dx12dev = DirectX12Device::Instance();
	dx12dev.init(hwnd);
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };
	float color[] = { 0.5f,0.8f,0.5f,0.5f };
	D3D12_VIEWPORT vp;
	vp.Width = WINDOW_WID;
	vp.Height = WINDOW_HEI;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	D3D12_RECT rect;
	rect.left = 0;
	rect.top = 0;
	rect.right = WINDOW_WID;
	rect.bottom = WINDOW_HEI;
	MSG msg = MSG();
	
	//入力用
	Input& input = Input::Instance();

	//マウス用
	Mouse& mouse = Mouse::Instance();
	mouse.Init(hInst,hwnd);

	//カメラ
	Camera& camera = Camera::Instance();


	ObjectController& object = ObjectController::Instance();

	//時間
	Time GameTime;
	GameTime.StartCount();

	//サウンドデータ
	Sound sound;
	sound.StartUp(hwnd);

	CollisionObject collsion;

	Menu menu;

	imamuu ima;
	ima.Init();

	Goto g;
	g.Init();
	DoDrawString dStr;
	dStr.Start();

#pragma endregion
	int i = 2;
	float f = 0;
	bool Menuflag = false;
	static int push = 0;
	while (true)
	{
		//BGM
		sound.Update();

		//更新（パッド、マウス）
		input.Update();
		mouse.Update();

		XMVECTOR out;
		CalcScreenToXZ(&out, mouse.GetPoint().x, mouse.GetPoint().y, WINDOW_WID, WINDOW_HEI, camera.GetView(), camera.GetProj());

		//printf("マウス座標:x = %f,y = %f\n", mouse.GetPos().x, mouse.GetPos().y);
		//ゲーム開始からの計測時間（ｍ秒）
		printf("%ld秒\n", GameTime.GetCount());

		if (!Menuflag) 
		{
			if (mouse.IsClick_L())
			{
				g.GetPlayer().SetPos(out);
				printf("マウス座標:x = %f,z = %f\n", out.m128_f32[0], out.m128_f32[2]);

			}
		}

		f += 0.005;


		//前準備
		int backBufID = dx12dev.SwapChain()->GetCurrentBackBufferIndex();
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle = dx12dev.RTVHandle();
		dx12dev.ComAlloc()->Reset();
#pragma region shadowmap描画
#pragma region PreDraw
		result = dx12dev.CmdList()->Reset(dx12dev.ComAlloc(), NULL);

		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.RenderTargets()[backBufID], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));
		dx12dev.CmdList()->ClearDepthStencilView(dx12dev.ShadowDSVHeap()->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

		dx12dev.CmdList()->OMSetRenderTargets(0, nullptr, false, &dx12dev.ShadowDSVHeap()->GetCPUDescriptorHandleForHeapStart());

		//dx12dev.CmdList()->ClearRenderTargetView(rtvHandle, color, 0, nullptr);
		//dx12dev.CmdList()->OMSetRenderTargets(1, &rtvHandle, false, &dx12dev.ShadowDSVHeap()->GetCPUDescriptorHandleForHeapStart());

		dx12dev.CmdList()->RSSetViewports(1, &vp);
		dx12dev.CmdList()->RSSetScissorRects(1, &rect);
		dx12dev.CmdList()->SetGraphicsRootSignature(dx12dev.RootSignature());
#pragma endregion

		//オブジェクトの生成（シェーダリソースの書き込み）を含むためResetからPresentまでの間でアップデートを行う
		g.Update();
		camera.Update();

		//モデル表示
		//g.Draw();
		camera.SituationSet();
		g.Draw();
		ima.Update();

		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.RenderTargets()[backBufID], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

		dx12dev.CmdList()->Close();
		dx12dev.CmdQueue()->ExecuteCommandLists(1, _cmdLists);
#pragma endregion
#pragma region 通常描画
#pragma region PreDraw
		result = dx12dev.CmdList()->Reset(dx12dev.ComAlloc(), NULL);

		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.RenderTargets()[backBufID], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));
		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.ShadowTex().Get(), D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_GENERIC_READ));

		dx12dev.CmdList()->ClearDepthStencilView(dx12dev.DepHeap()->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		//dx12dev.CmdList()->ClearDepthStencilView(_shadowDSV->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		dx12dev.CmdList()->ClearRenderTargetView(rtvHandle, color, 0, nullptr);
		dx12dev.CmdList()->OMSetRenderTargets(1, &rtvHandle, false, &dx12dev.DepHeap()->GetCPUDescriptorHandleForHeapStart());
		dx12dev.CmdList()->RSSetViewports(1, &vp);
		dx12dev.CmdList()->RSSetScissorRects(1, &rect);
		dx12dev.CmdList()->SetGraphicsRootSignature(dx12dev.RootSignature());

#pragma endregion


		if (input.GetKey('Z') & 0x80)
		{
			sound.selectUpdate(Plow);
		}

		if (input.GetKey('X') & 0x80)
		{
			sound.selectUpdate(Watering);
		}

		if (input.GetKey('C') & 0x80)
		{
			sound.selectUpdate(Fish);
		}

		if (input.GetKey(VK_ESCAPE))
		{
			if (push == 0)
			{
				Menuflag = !Menuflag;
			}
			push = 1;
		}
		else {
			push = 0;
		}
		camera.SituationSet();

		//モデル表示
		//g.Update();
		g.Draw();

		//床表示
		ima.Update();
		dStr.Update();

		if (Menuflag)
		{
			menu.Update();
			if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				if (msg.message == WM_QUIT)
				{
					break;
				}
			}
		}

		else {
			//当たり判定チェック
			for (auto& id : object.GetObjects())
			{
				if (id == object.GetObjects()[g.GetPlayer().GetID()]) { continue; }
				if (id->GetCollisionType() == (1 << C_Enable))
				{
					if (collsion.hitcheck(g.GetPlayer().GetPos(), id->GetPos(), object.GetObjects()[g.GetPlayer().GetID()]->GetWidth(), id->GetWidth()))
					{
						object.GetObjects()[g.GetPlayer().GetID()]->SetHitObject(id);
						printf("hit\n");
						break;
					}
					else {
						printf("nohit\n");
					}
				}
			}

		}
		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.RenderTargets()[backBufID], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
		dx12dev.CmdList()->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(dx12dev.ShadowTex().Get(), D3D12_RESOURCE_STATE_GENERIC_READ,D3D12_RESOURCE_STATE_DEPTH_WRITE));

		dx12dev.CmdList()->Close();
		dx12dev.CmdQueue()->ExecuteCommandLists(1, _cmdLists);
#pragma endregion

		dx12dev.fence();
		dx12dev.SwapChain()->Present(1, 0);
		//if (GetKeyState(VK_ESCAPE)&0x80) { break; }
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				break;
			}
		}
	}
	dx12dev.SwapChain()->Release();
	dx12dev.Device()->Release();
	CoUninitialize();
	UnregisterClass(WND_CLASS_NAME, hInst);

	FreeConsole();
	return 0;

}

