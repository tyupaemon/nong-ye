//#include<Header.hlsli>

Texture2D<float4> tex:register(t0);
Texture2D<float4> tex2:register(t1);
Texture2D<float4> tex3:register(t2);
SamplerState smp : register(s0);
SamplerState smp2 : register(s1);
#define MATMAX (1024)
cbuffer wvp : register(b0) {
	float4x4 world;
	float4x4 viewproj;
	float3 eye;
}
cbuffer material : register(b1) {
	float4 diffuse;
	float3 specular;
	float specularity;
	float3 ambient;
	float edgeSize;
	float4 edgeColor;
	int flag;
}
#define existTex (1 << 0)
#define existSphere (1 << 1)
#define existToon (1 << 2)
cbuffer cBuf : register(b2)
{
	matrix boneMat[MATMAX];
}
cbuffer edge : register(b3) {

}

struct VSin {
	float4 pos : POSITION;
	float4 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 exuv1 : TEXCOORD1;
	float4 exuv2 : TEXCOORD2;
	float4 exuv3 : TEXCOORD3;
	float4 exuv4 : TEXCOORD4;
	float edge : EDGE;
	uint boneid1 : BLENDINDICES0;
	uint boneid2 : BLENDINDICES1;
	uint boneid3 : BLENDINDICES2;
	uint boneid4 : BLENDINDICES3;
	float weight1 : BLENDWEIGHT0;
	float weight2 : BLENDWEIGHT1;
	float weight3 : BLENDWEIGHT2;
	float weight4 : BLENDWEIGHT3;
	int type : WEIGHTTYPE;
	float3 sdef1 : SDEFVEC0;
	float3 sdef2 : SDEFVEC1;
	float3 sdef3 : SDEFVEC2;
};

struct Output {
	float4 svpos : SV_POSITION;
	float4 pos : POSITION;
	float2 uv : TEXCOORD;
	float4 normal : NORMAL;
	float edge : EDGE;
	float size : PSIZE;
	//	int bone;
};


matrix DisableMove(matrix m)
{
	m._m03 = 0;
	m._m13 = 0;
	m._m23 = 0;
	//m._m33 = 0;
	return m;
}
matrix DisableRot(matrix m)
{
	m._m00 = 1;
	m._m01 = 0;
	m._m02 = 0;
	m._m10 = 0;
	m._m11 = 1;
	m._m12 = 0;
	m._m20 = 0;
	m._m21 = 0;
	m._m22 = 1;
	return m;
}


Output main(VSin data, uint InstanceId : SV_InstanceID)
{
	data.pos.z += InstanceId * 4;
	Output o;
	matrix bone =
		data.weight1 * boneMat[data.boneid1] +
		data.weight2 * boneMat[data.boneid2] +
		data.weight3 * boneMat[data.boneid3] +
		data.weight4 * boneMat[data.boneid4];
	matrix m = mul((viewproj), (world));
	o.pos = mul(mul(m, bone), data.pos);
	//o.svpos = mul(mul(m, bone), data.pos);
	o.svpos = mul(m, mul(bone, data.pos));
	o.uv = data.uv;
	o.normal = mul(DisableMove(world), data.normal);
	o.edge = data.edge;
	return o;
}

