Texture2D<float4> tex:register(t0);
Texture2D<float4> tex2:register(t1);
Texture2D<float4> tex3:register(t2);

Texture2D<float> shadowmap:register(t5);

SamplerState smp : register(s0);
SamplerState smp2 : register(s1);
#define MATMAX (1024)
cbuffer situation : register(b0) {
	float4x4 world;
	float4x4 viewproj;
	float3 eye;
	float4x4 LightVP;
	float3 LightPos;
	float3 LightDiffuse;
	float3 LightAmbient;
	float3 LightSpecular;
}
cbuffer material : register(b1) {
	float4 diffuse;
	float3 specular;
	float specularity;
	float3 ambient;
	float edgeSize;
	float4 edgeColor;
	int flag;
}
#define existTex (1 << 0)
#define existSphere (1 << 1)
#define existToon (1 << 2)
cbuffer cBuf : register(b4)
{
	matrix boneMat[1024];
}

struct VSin {
	float4 pos : POSITION;
	float4 normal : NORMAL;
	float2 uv : TEXCOORD0;
	float4 exuv1 : TEXCOORD1;
	float4 exuv2 : TEXCOORD2;
	float4 exuv3 : TEXCOORD3;
	float4 exuv4 : TEXCOORD4;
	float edge : EDGE;
	uint boneid1 : BLENDINDICES0;
	uint boneid2 : BLENDINDICES1;
	uint boneid3 : BLENDINDICES2;
	uint boneid4 : BLENDINDICES3;
	float weight1 : BLENDWEIGHT0;
	float weight2 : BLENDWEIGHT1;
	float weight3 : BLENDWEIGHT2;
	float weight4 : BLENDWEIGHT3;
	int type : WEIGHTTYPE;
	float3 sdef1 : SDEFVEC0;
	float3 sdef2 : SDEFVEC1;
	float3 sdef3 : SDEFVEC2;
};

struct Output {
	float4 svpos : SV_POSITION;
	float4 pos : POSITION0;
	float3 relativePos : POSITION1;
	float2 uv : TEXCOORD0;
	float2 exuv1 : TEXCOORD1;
	float4 exuv2 : TEXCOORD2;
	float4 normal : NORMAL;
	float edge : EDGE;
	int id : InstanceID;
	//float size : PSIZE;
	//	int bone;
};
