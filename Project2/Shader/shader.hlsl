#include<Header.hlsli>
cbuffer ID : register(b1) {
	float4 IDdiffuse[1024];
}

cbuffer objData: register(b2) {
	matrix objWorld[1024];
}
matrix DisableMove(matrix m)
{
	m._m03 = 0;
	m._m13 = 0;
	m._m23 = 0;
	//m._m33 = 0;
	return m;
}
matrix DisableRot(matrix m)
{
	m._m00 = 1;
	m._m01 = 0;
	m._m02 = 0;
	m._m10 = 0;
	m._m11 = 1;
	m._m12 = 0;
	m._m20 = 0;
	m._m21 = 0;
	m._m22 = 1;
	return m;
}

//ShadowMap-------////////////////////////////////////////////////////////////////
//ShadowMap用VS
Output SMVS(VSin data, uint InstanceId : SV_InstanceID)
{

	Output o;
	matrix bone =
		data.weight1 * boneMat[data.boneid1] +
		data.weight2 * boneMat[data.boneid2] +
		data.weight3 * boneMat[data.boneid3] +
		data.weight4 * boneMat[data.boneid4];
	matrix m = mul((LightVP), (objWorld[InstanceId]));
	o.pos = mul(mul(m, bone), data.pos);
	o.svpos = mul(m, mul(bone, data.pos));
	o.relativePos = eye - mul(data.pos, objWorld[InstanceId]).xyz;
	o.uv = data.uv;
	o.exuv1 = data.exuv1.xy;
	o.exuv2.xy = data.exuv1.xy;

	o.normal = mul(DisableMove(objWorld[InstanceId]), data.normal);
	o.edge = data.edge;
	o.id = InstanceId;
	return o;
}

//ShadowMap用PS
float4 SMPS(Output o): SV_Target
{
	float3 color = float3(1,0,0);

	return float4(color, 1);
}
//~ShadowMap-------////////////////////////////////////////////////////////////////

Output BasicVS(float4 pos : POSITION)
{
	Output o;
	matrix m = mul(viewproj,world);
	o.pos = mul(m, pos);
	o.svpos = mul(m, pos);
	return o;
}

float4 BasicPS(Output o) : SV_Target
{
	return tex.Sample(smp, o.uv);
}
float4 EdgePS(Output o) : SV_Target
{
	//return float4(specularity,specularity,specularity,1);
	return float4(edgeColor);
	//return float4(diffuse.rgb,1);
}

Output VS(VSin data, uint InstanceId : SV_InstanceID)
{
	Output o;
	matrix bone =
		data.weight1 * boneMat[data.boneid1] +
		data.weight2 * boneMat[data.boneid2] +
		data.weight3 * boneMat[data.boneid3] +
		data.weight4 * boneMat[data.boneid4];
	matrix m = mul((viewproj), (objWorld[InstanceId]));
	//matrix m = (viewproj);
	o.pos = mul(mul(m, bone), data.pos);
	//o.svpos = mul(mul(m, bone), data.pos);
	o.svpos = mul(m, mul(bone, data.pos));
	//o.svpos += float4(InstanceId * 0.1,0,0,1);
	o.relativePos = eye - mul(data.pos, objWorld[InstanceId]).xyz;
	o.uv = data.uv;
	o.exuv1 = data.exuv1.xy;
	o.exuv2.xy = data.exuv1.xy;
	
	o.normal = mul(DisableMove(objWorld[InstanceId]), data.normal);
//	o.normal = mul(DisableMove(objWorld), data.normal);
	o.edge = data.edge;
	o.id = InstanceId;
	return o;
}

Output OutlineVS(VSin data, uint InstanceId : SV_InstanceID)
{
	Output o;
	matrix bone =
		data.weight1 * boneMat[data.boneid1] +
		data.weight2 * boneMat[data.boneid2] +
		data.weight3 * boneMat[data.boneid3] +
		data.weight4 * boneMat[data.boneid4];
	matrix m = mul((viewproj), (world));
	o.pos = mul(world, mul(bone,data.pos));
	//o.svpos = mul(mul(m, bone), data.pos);
	o.svpos = mul(world,mul(bone,data.pos));
	o.uv = data.uv;
	o.normal = mul((world), data.normal);
	o.edge = data.edge;
	o.svpos.xyz = o.svpos.xyz + o.normal.xyz*edgeSize*0.01*data.edge;
	o.svpos = mul(viewproj, o.svpos);
	return o;
}
float4 Black(Output o) : SV_Target
{
	return float4(0,0,0,1);
}

#define LIGHT float3(0.4,-0.2,0.2)

float4 PS(Output o) : SV_Target
{
	float4 color = float4(1,1,1,1);
	color.rgb = ambient + diffuse.rgb;
	float3 light = LIGHT;
	float b = max(0,dot(o.normal.xyz, -light));
	float4 bright;
	if ((flag&existToon) == existToon)
	{
		color.rgb += (diffuse).rgb*b;
	}
	color.a = diffuse.a;
	color = saturate(color);

	float3 HalfVector = normalize(normalize(o.relativePos) + -light);
	float3 sp = pow(max(0, dot(HalfVector, o.normal.xyz)), specularity) * specular;
	sp *= b;

	if ((flag&existTex) != existTex)
	{
		color *= tex.Sample(smp, o.uv);
		if (color.a < 1)
		{
			discard;
		}
	}
	if ((flag&existToon) != existToon)
	{
		bright = float4(b, b, b, 1);
		float LightNormal = dot(o.normal.xyz, -light);
		color *= tex3.Sample(smp, float2(0, 0.5 - LightNormal * 0.5));
	}

	color.rgb += sp * b;
	return color;
}
float4 PSID(Output o) : SV_Target
{
	return IDdiffuse[o.id];
}

float4 SubTexPS(Output o) : SV_Target
{
	float4 color=float4(1,1,1,1);
	color.rgb = ambient + diffuse.rgb;

	float3 light = LIGHT;
	float b = dot(o.normal.xyz, -light);
	float4 bright;
	if ((flag&existToon) == existToon)
	{
		color.rgb+=diffuse.rgb*b;
	}
	else
	{
		bright = tex3.Sample(smp, float2(0, 1 - b));
	}
	color.a = diffuse.a;
	color = saturate(color);

	float3 HalfVector = normalize(normalize(o.relativePos) + -light);
	float3 sp = pow(max(0, dot(HalfVector, o.normal.xyz)), specularity) * specular;
	sp *= b;
	if ((flag&existTex) != existTex)
	{
		color *= tex.Sample(smp, o.exuv1);
		if (color.a < 0.1)
		{
			discard;
		}
		else
		{
			color.a = 1;
		}
	}
	if ((flag&existToon) != existToon)
	{
		bright = float4(b, b, b, 1);
		float LightNormal = dot(o.normal.xyz, -light);
		color *= tex3.Sample(smp, float2(0, 0.5 - LightNormal * 0.5));
		//color *= tex3.Sample(smp, float2(0, 1 - pow(LightNormal,2)));
	}

	color.rgb += sp*b;

	return color;
}

float4 AddPS(Output o) : SV_Target
{
	//	return tex.Sample(smp, o.uv);
	float4 color = float4(1,1,1,1);
	color = (flag&existTex) ? float4(1, 1, 1, 1) : tex.Sample(smp, o.uv);
	float4 sph;
	if ((flag&existSphere) == existSphere)
	{
		sph = float4(0, 0, 0, 1);
	}
	else
	{
		sph = tex2.Sample(smp, float2(o.normal.x*0.5 + 0.5, o.normal.y*-0.5 + 0.5));
	}
	//	return sph;
	float3 light = normalize(float3(-3, 5, -6));
	float b = dot(o.normal.xyz,light);
	float4 bright;
	if ((flag&existToon))
	{
		bright = float4(b, b, b, 1);
	}
	else
	{
		bright = tex3.Sample(smp, float2(0, 1 - b));
	}
	//	bright = float4(b, b, b, 1);
	if ((color.a < 1))
	{
		//			discard;
	}
	//return float4((saturate((bright*diffuse) + ambient)*color + specularity * specular + sph).rgb, color.a);
	return float4((saturate((bright.rgb*diffuse.rgb) + ambient)*color.rgb + specularity * specular + sph.rgb).rgb, color.a);
}


float4 MulPS(Output o) : SV_Target
{
//	return float4(0,0,0,1);
	float4 color;
if (flag&existTex)
{
	color = float4(1, 1, 1, 1);
}
else
{
	color = tex.Sample(smp, o.uv);
}
float4 sph;
if (flag&existSphere)
{
		sph = float4(1, 1, 1, 1);
	}
	else
	{
		sph = tex2.Sample(smp, o.normal.xy*float2(0.5, -0.5) + float2(0.5, 0.5));
	}
	float3 light = normalize(float3(-3,5,-6));
	float b = dot(o.normal.xyz, light);
	float4 bright;
	if (flag&existToon)
	{
		bright = tex3.Sample(smp, float2(0, 1 - b));
	}
	else
	{
		bright = float4(b, b, b, 1);
	}
	return bright;
	return float4((saturate((bright.rgb*diffuse.rgb) + ambient)*color.rgb + specularity*specular).rgb*sph.rgb, color.a);
}
float4 PSClear(Output o) : SV_Target
{
	float4 color;
if ((flag&existTex) == existTex)
{
	color = float4(1, 1, 1, 1);
}
else
{
	color = tex.Sample(smp, o.uv);
}
	float3 light = normalize(float3(-3,5,-6));
	float b = dot(o.normal.xyz, light);
	float4 bright;
	if ((flag&existToon) == existToon)
	{
		bright = tex3.Sample(smp, float2(0, 1 - b));
	}
	else
	{
		bright = float4(b, b, b, 1);
	}
	return float4(saturate((bright.rgb*ambient.rgb) + diffuse.rgb)*color.rgb*bright.rgb + bright.rgb*specularity*specular, color.a/2);
}
float4 PSNoAlpha(Output o) : SV_Target
{
	float4 color;
if ((flag&existTex) == existTex)
{
	color = float4(1, 1, 1, 1);
}
else
{
	color = tex.Sample(smp, o.uv);
}

	float3 light = normalize(float3(-3,5,-6));
	float b = dot(o.normal.xyz, light);
	float4 bright;
	if ((flag&existToon) == existToon)
	{
		bright = tex3.Sample(smp, float2(0, 1 - b));
	}
	else
	{
		bright = float4(b, b, b, 1);
	}
	if ((color.a < 1)||(diffuse.a<1))
	{
		discard;
	}
	//	return float4(bright, bright, bright, 1.0f);
	return float4(saturate((bright.rgb*ambient) + diffuse.rgb)*color.rgb*bright.rgb + bright.rgb*specularity*specular, color.a / 2);

	//	return o.normal;
	//	return  float4(1,1,1,1);
}
float4 PSAlphaOnly(Output o) : SV_Target
{
	float4 color;
if ((flag&existTex) == existTex)
{
	color = float4(1, 1, 1, 1);
}
else
{
	color = tex.Sample(smp, o.uv);
}

	float3 light = normalize(float3(-3,5,-6));
	float bright = dot(o.normal.xyz,light);
	bright = bright;
	if (!(color.a < 1)||!(diffuse.a<1))
	{
			discard;
	}
//		return float4(bright, bright, bright, 1.0f);
	return float4(saturate((bright*ambient) + diffuse.rgb)*color.rgb*bright + bright*specularity*specular, color.a);
}
float4 PSDepth(Output o) : SV_Target
{
	float4 color = tex.Sample(smp, o.uv);

	float3 light = normalize(float3(-3, 10, -5));
	//if (color.a < 1)
	//tda式ミク
	if (color.a < .65)
	{
		discard;
		//return float4(saturate((bright*ambient) + diffuse.rgb)*color.rgb*bright + bright*specularity*specular, color.a / 2);
	}
	return float4(0,0,0,0);
}

Output BoardVS(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	Output o;
	matrix m = mul(viewproj, world);
	o.pos = mul(m, pos);
	o.svpos = mul(m, pos);
	o.uv = uv;
	return o;
}

float4 BoardPS(Output o) : SV_Target
{
	float4 color = tex.Sample(smp, o.uv);
	if (color.a < 0.1f)
	{
		discard;
	}
	return color;
}

Output PMXVS(float4 pos : POSITION, float4 normal : NORMAL,float2 uv : TEXCOORD//,
//	float2 uvE1 : TEXCOORDE1, float2 uvE2 : TEXCOORDE2, float2 uvE3 : TEXCOORDE3, float2 uvE4 : TEXCOORDE4,
//	bool type: WEIGHTTYPE,
//	uint boneID1 : BONE1, uint boneID2 : BONE2, uint boneID3 : BONE3, uint boneID4 : BONE4,
//	float boneWeight1 : WEIGHT1, float boneWeight2 : WEIGHT2, float boneWeight3 : WEIGHT3, float boneWeight4 : WEIGHT4,
//	float4 sdefVec1 : SDEFVEC1, float4 sdefVec2 : SDEFVEC2, float4 sdefVec3 : SDEFVEC3,
//	float edgeSize: EDGE
)
{
	Output o;
	matrix m = mul(viewproj, world);
	o.pos = mul(m, pos);
	o.svpos = mul(m, pos);
	return o;
}

Output BillBoardVS(float4 pos : POSITION, float2 uv : TEXCOORD)
{
	Output o;
	matrix m = mul(viewproj, DisableRot(world));
	o.pos = mul(m, pos);
	o.svpos = mul(m, pos);
	o.uv = uv;
	return o;
}
