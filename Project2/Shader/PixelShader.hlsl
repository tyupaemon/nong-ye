#include<Header.hlsli>

float4 PS(Output o) : SV_Target
{
	float4 color;
	float3 light = normalize(float3(-3, 5, -6));
	//color.rgb = ambient;
	color = float4(1, 1, 1, 1);
	if (flag&existToon)
	{
		color.rgb += max(0, dot(o.normal, light))*diffuse.rgb;
	}
	color.a = diffuse.a;
	color = saturate(color);

	float3 e = eye - o.pos;
	float3 HalfVector = normalize(normalize(e) + light);
	float3 spec= pow(
		max(0, dot(HalfVector, o.normal))
		, specularity) * specular;
	if ((flag&existTex) != existTex)
	{
		color *= tex.Sample(smp, o.uv);
	}
	if ((flag&existToon) != existToon)
	{
		float b = (dot(o.normal, light));
		color *= tex3.Sample(smp, float2(0, 0.5 - b*0.5));
	}
	color.rgb += spec;
	return o.normal;
	/*
	if ((flag&existTex) == existTex)
	{
		color = float4(1, 1, 1, 1);
	}

	else
	{
		color = tex.Sample(smp, o.uv);
	}

	float3 light = normalize(float3(-3,5,-6));
	float b = saturate(dot(o.normal,light));
	float4 bright;
	if (flag&existToon)
	{
		bright = float4(b, b, b, 1);
	}
	else
	{
		bright = tex3.Sample(smp, float2(0, 1-b));
	}
//	bright = float4(b, b, b, 1);
	if ((color.a < 1))
	{
					discard;
	}
	//return float4(bright.rgb, 1);
	//return float4((saturate((bright*diffuse) + ambient)*color + specularity*specular*bright.rgb).rgb, color.a);
	//return float4((saturate((bright*diffuse) + ambient)*color).rgb, color.a);
	//return float4(ambient, color.a);
	return float4(((bright*diffuse) + ambient)*color,color.a);
*/
}

