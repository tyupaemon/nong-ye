#pragma once
#include <DirectXMath.h>

using namespace DirectX;

class CollisionObject
{
private:

public:
	CollisionObject();
	~CollisionObject();

	//当たっているかどうかの判定
	///今回はX軸とZ軸で行うため高さはいらない
	////apos・・・当たり判定を行うオブジェクトのA側
	////bpos・・・当たり判定を行うオブジェクトのB側
	////awidth・・・当たり判定を行うオブジェクトのAの幅
	////bwidth・・・当たり判定を行うオブジェクトのBの幅
	bool hitcheck(XMVECTOR apos, XMVECTOR bpos, int awidth, int bwidth);

};
