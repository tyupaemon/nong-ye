#include "Fish.h"
#include"../DX12/DirectX12Device.h"
#include <Windows.h>
#include<vector>
#include"../Goto/Camera.h"
#include<cstdlib>  


Fish::Fish(DirectX::XMFLOAT3 center, float width, float depth,
	ID3D12PipelineState*& pipelineState, ID3D12DescriptorHeap*& descriptorHeapSRVShadow,
	ID3D12DescriptorHeap*& descriptorHeapSRV, Rect waterRect)
	:_pipelineState(pipelineState),_descriptorHeapSRVShadow(descriptorHeapSRVShadow),
	_descriptorHeapSRV(descriptorHeapSRV)
{
	_center = center;
	_waterRect = waterRect;

	//矩形構造体に値を入れる
	_rect.x = _center.x;
	_rect.y = _center.z;
	_rect.w = width;
	_rect.h = depth;

	wid = width;
	hei = depth;

	std::vector<FishVertex> vertices;
	//pos,uv
	vertices.push_back(FishVertex(
		- width / 2, 0, depth / 2,
		0, 0));

	vertices.push_back(FishVertex(
		width / 2, 0,depth / 2,
		1, 0));

	vertices.push_back(FishVertex(
		- width / 2, 0, - depth / 2,
		0, 1));

	vertices.push_back(FishVertex(
		width / 2, 0,  - depth / 2,
		1, 1));

	HRESULT result = S_OK;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	//リソース生成
	result = device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertices.size() * sizeof(FishVertex)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vb)
	);

	result = _vb->Map(0, nullptr, (void**)&_vertexData);
	std::copy(vertices.begin(), vertices.end(), _vertexData);
	_vb->Unmap(0, nullptr);

	_vbView = {};
	_vbView.BufferLocation = _vb->GetGPUVirtualAddress();
	_vbView.StrideInBytes = sizeof(FishVertex);
	_vbView.SizeInBytes = vertices.size() * sizeof(FishVertex);

	//座標Y
	pos.m128_f32[1] += _center.y;

	_func = &Fish::Active;
}


Fish::~Fish()
{
}

void Fish::Draw()
{

	_time++;
	(this->*_func)();
	DirectX12Device& dx12dev = DirectX12Device::Instance();

	dx12dev.CmdList()->SetPipelineState(_pipelineState);

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbMDdataDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV3, pcbMDdataDescHeap->GetGPUDescriptorHandleForHeapStart());

	pDModelData->world = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z)*
		//座標X,Y,Z
		XMMatrixTranslation(_rect.x, pos.m128_f32[1], _rect.y);

	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &_vbView);
	dx12dev.CmdList()->DrawInstanced(4, 1, 0, 0);
}

void Fish::Update()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();

}
const int movementLevel = 50;
const int randomArea = 10;
void Fish::Acceleration()
{
	int i = rand() % randomArea;
	if (i<randomArea/2+1)
	{
		_moveSpeed.x = 0;
		_moveSpeed.y = 0;
		return;
	}
	//rand() % randomArea /2-2、たまにマイナスになるように-2する
	_moveSpeed.x += rand() % randomArea /2-2;
	_moveSpeed.x /= movementLevel;

	_moveSpeed.y += rand() % randomArea/2 - 2;
	_moveSpeed.y /= movementLevel;
}

const int accelerationfrequency = 100;//加速頻度
void Fish::Active()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	auto handle = _descriptorHeapSRVShadow->GetGPUDescriptorHandleForHeapStart();
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRVShadow);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(ViewBoard, handle);

	if (_time % accelerationfrequency == 0)
		Acceleration();

	_rect.x += _moveSpeed.x;
	_rect.y += _moveSpeed.y;
	if (_rect.IsHitWide(_waterRect))
	{
	}
	else
	{
		_rect.x -= _moveSpeed.x;
	}
	if (_rect.IsHitVertical(_waterRect))
	{
	}
	else
	{
		_rect.y -= _moveSpeed.y;
	}
}

void Fish::Capture()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	auto handle = _descriptorHeapSRV->GetGPUDescriptorHandleForHeapStart();
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_descriptorHeapSRV);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(ViewBoard, handle);
}