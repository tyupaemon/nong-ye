#include "CreateFish.h"
#include <windows.h>
#include <tchar.h>
#include <d3dcompiler.h>


#include"../DX12/DirectX12Device.h"
#include "../DX12/common.h"
#include"Fish.h"



CreateFish::CreateFish()
{
}


CreateFish::~CreateFish()
{
}

//パイプラインステートの初期化
void CreateFish::Init(ID3D12RootSignature*& rootSignature)
{
	HRESULT result = S_OK;

	//シェーダ読み込み
	ID3DBlob* error;
	result = D3DCompileFromFile(_T("../Fish/FishShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "FishVS", "vs_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_vs, nullptr);
	result = D3DCompileFromFile(_T("../Fish/FishShader.hlsl"), nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, "FishPS", "ps_5_0",
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);

	//D3DDECLUSAGE_TANGENT
	D3D12_INPUT_ELEMENT_DESC inputDescs[] =
	{
		{ "POSITION" ,0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
		{ "TEXCOORD" ,0,DXGI_FORMAT_R32G32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0 },
	};

	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	//パイプラインステートを生成
	D3D12_GRAPHICS_PIPELINE_STATE_DESC desc = {};
	desc.VS = CD3DX12_SHADER_BYTECODE(_vs);
	desc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	desc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	desc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	desc.DepthStencilState.DepthEnable = true;
	desc.DepthStencilState.StencilEnable = false;
	desc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	desc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	desc.NumRenderTargets = 1;
	desc.InputLayout.NumElements = sizeof(inputDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	desc.InputLayout.pInputElementDescs = inputDescs;
	desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.pRootSignature = rootSignature;
	desc.SampleMask = 0xffffffff;
	result = device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&_pipelineState));

	//テクスチャ読み込み,リソース生成
	_textureResource.resize(FishTypeMAX);
	LoadTexture(_T("Resource/今村用素材/魚/魚影っぽい.png"), _textureResource[FishShadows]);
	LoadTexture(_T("Resource/今村用素材/魚/真鯛.jpg"), _textureResource[Redsnapper]);


	//SRV用のディスクリプタヒープを生成
	_descriptorHeapSRV.resize(FishTypeMAX);
	{
		D3D12_DESCRIPTOR_HEAP_DESC desc = {};
		desc.NumDescriptors = 1;
		desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		result = device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&_descriptorHeapSRV[FishShadows]));
		result = device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&_descriptorHeapSRV[Redsnapper]));
	}
	//SRVの設定
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC desc = {};
		desc.Format = DXGI_FORMAT_UNKNOWN;
		desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		desc.Texture2D.MipLevels = 1;
		desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		desc.Texture2D.MostDetailedMip = 0;
		desc.Texture2D.PlaneSlice = 0;
		desc.Texture2D.ResourceMinLODClamp = 0.0f;

		//SRVの生成
		D3D12_CPU_DESCRIPTOR_HANDLE handle = _descriptorHeapSRV[FishShadows]->GetCPUDescriptorHandleForHeapStart();
		device->CreateShaderResourceView(_textureResource[FishShadows], &desc, handle);


		handle = _descriptorHeapSRV[Redsnapper]->GetCPUDescriptorHandleForHeapStart();
		device->CreateShaderResourceView(_textureResource[Redsnapper], &desc, handle);
	}
}

//パイプラインステートのセット
void CreateFish::SetDrawMode()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	dx12dev.CmdList()->SetPipelineState(_pipelineState);

}

FishData* CreateFish::CreateFishs(DirectX::XMFLOAT3 center, float width, float depth, Rect rect,int type)
{
	return new Fish(center, width, depth, _pipelineState, 
		_descriptorHeapSRV[FishShadows],_descriptorHeapSRV[type],rect);
}

void CreateFish::LoadTexture(const wchar_t* path, ID3D12Resource*& texture)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* device = dx12dev.Device();

	std::unique_ptr<uint8_t[]> dec;
	D3D12_SUBRESOURCE_DATA sub;
	LoadWICTex(texture, sub, path, dec);
	D3D12_BOX box = {};
	box.front = 0;
	box.back = 1;
	box.top = box.left = 0;
	box.right = sub.RowPitch / 4;
	box.bottom = sub.SlicePitch / sub.RowPitch;
	HRESULT result = texture->WriteToSubresource(0, &box, &dec[0], box.right * 4, box.right*box.bottom * 4);
	assert(SUCCEEDED(result));

	dx12dev.CmdList()->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(texture,
			D3D12_RESOURCE_STATE_COPY_DEST,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	dx12dev.CmdList()->Close();
	dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
	dx12dev.fence();
}