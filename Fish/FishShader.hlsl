#include<../Project2/Shader/Header.hlsli>
Texture2D<float4> tex4:register(t4);

cbuffer material : register(b1) {
	float time;
}

struct OutputFish
{
	float4 svpos : SV_POSITION;
	float4 pos : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
	float3 color : COLOR;
};

cbuffer objData: register(b2) {
	matrix objWorld;
}

OutputFish FishVS(float4 pos : POSITION, float2 uv : TEXCOORD)
{

	OutputFish o;
	o.svpos = mul(mul(viewproj, objWorld), pos);
	o.uv = uv;
	return o;
}

float4 FishPS(OutputFish o) : SV_Target
{
	return float4(1,0,0,1);
	float4 color = tex4.Sample(smp2, o.uv);
	if (color.r == 1&&color.g==1&&color.b==1)
	{
		discard;
	}
	return color;
}