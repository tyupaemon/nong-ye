#pragma once
#include<DirectXMath.h>
#include<d3d12.h>
#include"../Imamuu/Geometry.h"

struct FishVertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT2 uv;

	FishVertex()
	{
		pos = DirectX::XMFLOAT3(0, 0, 0);
		uv = DirectX::XMFLOAT2(0, 0);
	}
	FishVertex(DirectX::XMFLOAT3& p,DirectX::XMFLOAT2& u)
	{
		pos = p;
		uv = u;
	}
	FishVertex(float x, float y, float z,float u, float v)
	{
		pos = DirectX::XMFLOAT3(x, y, z);
		uv = DirectX::XMFLOAT2(u, v);
	}
};

enum FishType
{
	FishShadows,//���e
	Redsnapper,//�^��
	FishTypeMAX
};

class FishData : Geometry
{
public:
	FishData();
	~FishData();
	//virtual void Draw() = 0;
	//virtual void Update() = 0;
	virtual Rect GetRect() = 0;
};

