#pragma once
#include"FishData.h"
#include"../Goto//RenderableObject.h"
#include<d3d12.h>


class Fish:
	public FishData, public RenderableObject
{
private:
	ID3D12Resource * _vb = nullptr;
	D3D12_VERTEX_BUFFER_VIEW _vbView;
	ID3D12PipelineState*& _pipelineState;

	//テクスチャリソース
	ID3D12DescriptorHeap*& _descriptorHeapSRVShadow;//魚影用
	ID3D12DescriptorHeap*& _descriptorHeapSRV;//魚のテクスチャ

	DirectX::XMFLOAT3 _center;
	FishVertex* _vertexData = nullptr;

	Rect _rect;//自分の矩形
	Rect _waterRect;//水面の矩形
	Vector2 _moveSpeed;//移動速度

	int _time=0;//経過時間

	void(Fish::*_func)();
	void Acceleration();//速度加速
	void Capture();//捕獲
	void Active();

public:
	Fish(DirectX::XMFLOAT3 center, float width, float depth,
		ID3D12PipelineState*& pipelineState, 
		ID3D12DescriptorHeap*& descriptorHeapSRVShadow,
		ID3D12DescriptorHeap*& descriptorHeapSRV,
		Rect waterRect);
	~Fish();
	void Draw();
	void Update();
	Rect GetRect() { return _rect; }
};

