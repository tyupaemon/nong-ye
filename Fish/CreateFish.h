#pragma once
#include "../DX12/d3dx12.h"
#include<DirectXMath.h>
#include"../Imamuu/Geometry.h"
#include<vector>

class FishData;

struct ID3D12PipelineState;
struct ID3D12RootSignature;
struct WaterSurface;

class CreateFish
{
private:
	ID3D12PipelineState * _pipelineState = nullptr;
	ID3DBlob* _vs = nullptr;
	ID3DBlob* _ps = nullptr;

	//リソース
	std::vector<ID3D12Resource*> _textureResource;//テクスチャリソース
	std::vector<ID3D12DescriptorHeap*> _descriptorHeapSRV;

	void LoadTexture(const wchar_t* path, ID3D12Resource*& texture);

public:
	CreateFish();
	~CreateFish();

	//パイプラインステートの初期化
	void Init(ID3D12RootSignature*& rootSignature);

	//パイプラインステートのセット
	void SetDrawMode();

	//魚作成
	FishData* CreateFishs(DirectX::XMFLOAT3 center, float width, float depth,Rect rect,int type);
};

