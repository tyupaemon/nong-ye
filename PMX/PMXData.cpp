#include "PMXData.h"

#include<windows.h>

#include<fstream>
#include<tchar.h>
#include<string>
#include<vector>
#include<array>
#include<algorithm>
#include<codecvt>
#include<atlstr.h>
#include <d3dcompiler.h>

#include"PMXCommon.h"

#include "../DX12/DirectX12Device.h"
#include "../DX12/d3dx12.h"
#include "../DX12/common.h"

#include"../DX12/TGALoader.h"

const int DRAW_CNT = 1;

enum Exists {
	existTex = 0,
	existSphere,
	existToon,
};


PMXData::PMXData(std::string name)
{
	InitPipeline();
	Load(name);

}
PMXData::PMXData()
{
	InitPipeline();
}


union PMXTextBuf {
	char c[64];
	wchar_t w[32];
};
union CompsitChar {
	char c[2];
	wchar_t w;
};

PMXData::~PMXData()
{
}
const int pathLength = 256;
void PMXData::Load(std::string _fileName)
{
	DirectX12Device& dev = DirectX12Device::Instance();

	fileName =_fileName;
	wid = hei = 0;
	HRESULT result;

	TCHAR wPath[64];
	CString path = fileName.c_str();
	lstrcpy(wPath, path);
	HANDLE hFile = CreateFile(wPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if (hFile == INVALID_HANDLE_VALUE) { assert(false); }
	int size = GetFileSize(hFile, NULL);
	std::vector<BYTE> data(size);
	DWORD d;
	//ReadFile(hFile, &data[0], size, &d, NULL);
	int idx = 0;
	PMXHeader head;

	char c[4] = {};
	//ヘッダ
	ReadFile(hFile, head.sign, sizeof(head.sign), &d, NULL);
	ReadFile(hFile, &head.ver, sizeof(head.ver), &d, NULL);
	if (head.ver == 2.0f)
	{
		ReadFile(hFile, &head.size, sizeof(head.size), &d, NULL);
		ReadFile(hFile, head.data, sizeof(BYTE)*head.size, &d, NULL);
		SetStrRead(head.data[0]);
		SetReadUnsignedVariable(ReadVertIdx, head.data[2]);
		SetReadVariable(ReadTexIdx, head.data[3]);
		SetReadVariable(ReadMaterialIdx, head.data[4]);
		SetReadVariable(ReadBoneIdx, head.data[5]);
		SetReadVariable(ReadMorpfIdx, head.data[6]);
		SetReadUnsignedVariable(ReadRigIdx, head.data[7]);
		//モデル情報
		unsigned int length;
		ReadString(hFile, name);
		ReadString(hFile, name_eng);
		ReadString(hFile, comment);
		ReadString(hFile, comment_eng);

		//頂点数 [int]
		//頂点 * 頂点数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);

		vertices.resize(length);
		for (int i = 0; i < vertices.size(); i++)
		{
			ReadFile(hFile, &vertices[i].pos, sizeof(float) * 3, &d, NULL);

			wid = max(wid, pow(vertices[i].pos.x, 2) + pow(vertices[i].pos.z, 2));
			hei = max(hei, vertices[i].pos.y);
			//			vertices[i].pos.x *= 5;
			//			vertices[i].pos.y *= 5;
			//			vertices[i].pos.z *= 5;
			ReadFile(hFile, &vertices[i].normal, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &vertices[i].uv, sizeof(float) * 2, &d, NULL);
			ReadFile(hFile, &vertices[i].ext_uv, sizeof(float) * 4 * head.data[1], &d, NULL);
			vertices[i].ext_uv[0].x = vertices[i].uv.x;
			vertices[i].ext_uv[0].y = vertices[i].uv.y;
			BYTE b;
			ReadFile(hFile, &b, sizeof(BYTE), &d, NULL);
			vertices[i].weightType = b;
			switch (vertices[i].weightType)
			{
			case(0):
				ReadBoneIdx(hFile, vertices[i].boneID[0]);
				vertices[i].boneWeight[0] = 1;
				break;
			case(1):
				ReadBoneIdx(hFile, vertices[i].boneID[0]);
				ReadBoneIdx(hFile, vertices[i].boneID[1]);
				ReadFile(hFile, &vertices[i].boneWeight[0], sizeof(float), &d, NULL);
				vertices[i].boneWeight[1] = 1.0f - vertices[i].boneWeight[0];
				break;
			case(2):
				for (int j = 0; j < 4; j++)
				{
					ReadBoneIdx(hFile, vertices[i].boneID[j]);
				}
				for (int j = 0; j < 4; j++)
				{
					ReadFile(hFile, &vertices[i].boneWeight[j], sizeof(float), &d, NULL);
				}
				break;
			case(3):
				ReadBoneIdx(hFile, vertices[i].boneID[0]);
				ReadBoneIdx(hFile, vertices[i].boneID[1]);
				ReadFile(hFile, &vertices[i].boneWeight[0], sizeof(float), &d, NULL);
				vertices[i].boneWeight[1] = 1.0f - vertices[i].boneWeight[0];
				ReadFile(hFile, &vertices[i].sdefVector[0], sizeof(float) * 3, &d, NULL);
				ReadFile(hFile, &vertices[i].sdefVector[1], sizeof(float) * 3, &d, NULL);
				ReadFile(hFile, &vertices[i].sdefVector[2], sizeof(float) * 3, &d, NULL);
				break;
			}
			for (int j = 0; j < 4; j++)
			{
				if (vertices[i].boneID[j] == -1)
				{
					vertices[i].boneID[j] = 0;
					vertices[i].boneWeight[j] = 0;
				}
			}
			ReadFile(hFile, &vertices[i].edge, sizeof(float), &d, NULL);
		}
		wid = sqrt(wid)*2;
		//面数[int]
		//参照頂点Index * 面数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		unsigned int surfaceCnt = length / 3;
		indexes.resize(length);
		for (int i = 0; i < indexes.size(); i++)
		{
			ReadVertIdx(hFile, indexes[i]);
		}
		//テクスチャ数[int]
		//テクスチャパス * テクスチャ数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		texPathes.resize(length);
		for (int i = 0; i < texPathes.size(); i++)
		{
			ReadString(hFile, texPathes[i]);
		}
		{
			std::string dir;
			int pathIdx1 = (int)_fileName.rfind('/');
			int pathIdx2 = (int)_fileName.rfind('\\');
			int pathIdx = max(pathIdx1, pathIdx2);
			if (pathIdx != _fileName.npos)
				dir = _fileName.substr(0, pathIdx) + '/';


			TCHAR wPath[64];
			CString path = dir.c_str();
			lstrcpy(wPath, path);
			std::wstring texPath = wPath;
			texPath += texPathes[0];
			HANDLE tex = CreateFile(texPath.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
			CloseHandle(tex);

		}
		//材質数[int]
		//材質 * 材質数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		materials.resize(length);
		for (int i = 0; i < materials.size(); i++)
		{
			ReadString(hFile, materials[i].name);
			ReadString(hFile, materials[i].name_eng);
			ReadFile(hFile, &materials[i].diffuse, sizeof(float) * 4, &d, NULL);
			ReadFile(hFile, &materials[i].specular, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &materials[i].specularity, sizeof(float), &d, NULL);
			ReadFile(hFile, &materials[i].ambient, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &materials[i].drawFlag, sizeof(BYTE), &d, NULL);

			ReadFile(hFile, &materials[i].edgeColor, sizeof(float) * 4, &d, NULL);
			ReadFile(hFile, &materials[i].edgeSize, sizeof(float), &d, NULL);
			ReadTexIdx(hFile, materials[i].texIdx);
			ReadTexIdx(hFile, materials[i].sphIdx);
			ReadFile(hFile, &materials[i].sphMode, sizeof(BYTE), &d, NULL);

			ReadFile(hFile, &materials[i].toonFlag, sizeof(BYTE), &d, NULL);
			if (materials[i].toonFlag == 0)
			{
				ReadTexIdx(hFile, materials[i].toonIdx);
			}
			else
			{
				ReadByteValue(hFile, materials[i].toonIdx);
			}
			ReadString(hFile, materials[i].text);
			ReadFile(hFile, &materials[i].vertCnt, sizeof(int), &d, NULL);
		}

		//ボーン数[int]
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		bones.resize(length);
		//ボーン * ボーン数
		for (int i = 0; i < bones.size(); i++)
		{
			ReadString(hFile, bones[i].name);
			ReadString(hFile, bones[i].name_eng);
			boneMap[bones[i].name] = i;
			boneMapEng[bones[i].name_eng] = i;
			ReadFile(hFile, &bones[i].pos, sizeof(float) * 3, &d, NULL);
			ReadBoneIdx(hFile, bones[i].parentIdx);
			ReadFile(hFile, &bones[i].layer, sizeof(int), &d, NULL);
			ReadFile(hFile, &bones[i].flag, sizeof(short), &d, NULL);
			//0x0001  : 接続先(PMD子ボーン指定)表示方法 -> 0 : 座標オフセットで指定 1 : ボーンで指定
			if (bones[i].flag & 0x0001)
			{
				ReadBoneIdx(hFile, bones[i].childIdx);
			}
			else
			{
				ReadFile(hFile, &bones[i].childPos, sizeof(float) * 3, &d, NULL);
			}
			//0x0100 : 回転付与
			//0x0200 : 移動付与
			if ((bones[i].flag & 0x0100) || (bones[i].flag & 0x0200))
			{
				ReadBoneIdx(hFile, bones[i].followIdx);
				ReadFile(hFile, &bones[i].followWeight, sizeof(float), &d, NULL);
			}
			//0x0400 : 軸固定
			if (bones[i].flag & 0x0400)
			{
				ReadFile(hFile, &bones[i].fixedAxis, sizeof(float) * 3, &d, NULL);
			}
			//0x0800 : ローカル軸
			if (bones[i].flag & 0x0800)
			{
				ReadFile(hFile, &bones[i].localAxisX, sizeof(float) * 3, &d, NULL);
				ReadFile(hFile, &bones[i].localAxisZ, sizeof(float) * 3, &d, NULL);
			}
			//0x2000 : 外部親変形
			if (bones[i].flag & 0x2000)
			{
				ReadFile(hFile, &bones[i].otherParentKey, sizeof(int), &d, NULL);
			}

			//0x0020 : IK
			if (bones[i].flag & 0x0020)
			{
				ReadBoneIdx(hFile, bones[i].ikIdx);
				ReadFile(hFile, &bones[i].ikCnt, sizeof(int), &d, NULL);
				ReadFile(hFile, &bones[i].ikAngle, sizeof(float), &d, NULL);
				ReadFile(hFile, &length, sizeof(int), &d, NULL);
				bones[i].ikLinks.resize(length);
				for (int j = 0; j < bones[i].ikLinks.size(); j++)
				{
					ReadBoneIdx(hFile, bones[i].ikLinks[j].idx);
					ReadFile(hFile, &bones[i].ikLinks[j].limit, sizeof(byte), &d, NULL);
					if (bones[i].ikLinks[j].limit)
					{
						ReadFile(hFile, &bones[i].ikLinks[j].lower, sizeof(float) * 3, &d, NULL);
						ReadFile(hFile, &bones[i].ikLinks[j].upper, sizeof(float) * 3, &d, NULL);
					}
				}

			}
		}
		//モーフ数[int]
		//モーフ * モーフ数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		morphs.resize(length);
		for (int i = 0; i < morphs.size(); i++)
		{
			ReadString(hFile, morphs[i].name);
			ReadString(hFile, morphs[i].name_eng);
			ReadFile(hFile, &morphs[i].category, sizeof(BYTE), &d, NULL);
			ReadFile(hFile, &morphs[i].morphType, sizeof(BYTE), &d, NULL);
			ReadFile(hFile, &morphs[i].morphCnt, sizeof(int), &d, NULL);
			switch (morphs[i].morphType)
			{
				//グループ
			case 0:
				morphs[i].morphIdx = (int)groupMorphs.size();
				groupMorphs.resize(morphs[i].morphIdx + morphs[i].morphCnt);
				for (int j = 0; j < morphs[i].morphCnt; j++)
				{
					PMXGroupMorph morph;
					ReadMorpfIdx(hFile, morph.idx);
					ReadFile(hFile, &morph.weight, sizeof(float), &d, NULL);
					groupMorphs[morphs[i].morphIdx + j] = (morph);
				}
				break;
				//頂点
			case 1:
				morphs[i].morphIdx = (int)vertMorphs.size();
				vertMorphs.resize(morphs[i].morphIdx + morphs[i].morphCnt);
				for (int j = 0; j < morphs[i].morphCnt; j++)
				{
					PMXVertMorph morph;
					ReadVertIdx(hFile, morph.idx);
					ReadFile(hFile, &morph.offset, sizeof(float) * 3, &d, NULL);
					vertMorphs[morphs[i].morphIdx + j] = (morph);
				}
				break;
				//ボーン
			case 2:
				morphs[i].morphIdx = (int)boneMorphs.size();
				boneMorphs.resize(morphs[i].morphIdx + morphs[i].morphCnt);
				for (int j = 0; j < morphs[i].morphCnt; j++)
				{
					PMXBoneMorph morph;
					ReadBoneIdx(hFile, morph.idx);
					ReadFile(hFile, &morph.move, sizeof(float) * 3, &d, NULL);
					ReadFile(hFile, &morph.rotQuaternion, sizeof(float) * 4, &d, NULL);
					boneMorphs[morphs[i].morphIdx + j] = (morph);
				}
				break;
				//uv
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				morphs[i].morphIdx = (int)uvMorphs.size();
				uvMorphs.resize(morphs[i].morphIdx + morphs[i].morphCnt);
				for (int j = 0; j < morphs[i].morphCnt; j++)
				{
					PMXUVMorph morph;
					ReadVertIdx(hFile, morph.idx);
					ReadFile(hFile, &morph.offset, sizeof(float) * 4, &d, NULL);
					uvMorphs[morphs[i].morphIdx + j] = (morph);
				}
				break;
				//マテリアル
			case 8:
				morphs[i].morphIdx = (int)materialMorphs.size();
				materialMorphs.resize(morphs[i].morphIdx + morphs[i].morphCnt);
				for (int j = 0; j < morphs[i].morphCnt; j++)
				{
					PMXMaterialMorph morph;
					ReadMaterialIdx(hFile, morph.idx);
					ReadFile(hFile, &morph.calcType, sizeof(BYTE), &d, NULL);
					ReadFile(hFile, &morph.diffuse, sizeof(float) * 4, &d, NULL);
					ReadFile(hFile, &morph.specular, sizeof(float) * 3, &d, NULL);
					ReadFile(hFile, &morph.specularity, sizeof(float), &d, NULL);
					ReadFile(hFile, &morph.ambient, sizeof(float) * 3, &d, NULL);

					ReadFile(hFile, &morph.edgeColor, sizeof(float) * 4, &d, NULL);
					ReadFile(hFile, &morph.edgeSize, sizeof(float), &d, NULL);

					ReadFile(hFile, &morph.texCoefficient, sizeof(float) * 4, &d, NULL);
					ReadFile(hFile, &morph.sphCoefficient, sizeof(float) * 4, &d, NULL);
					ReadFile(hFile, &morph.toonCoefficient, sizeof(float) * 4, &d, NULL);

					materialMorphs[morphs[i].morphIdx + j] = (morph);
				}
				break;
			}
		}
		//表示枠数[int]
		//表示枠 * 表示枠数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		group.resize(length);
		for (int i = 0; i < group.size(); i++)
		{
			ReadString(hFile, group[i].name);
			ReadString(hFile, group[i].name_eng);
			ReadFile(hFile, &group[i].exFlag, sizeof(BYTE), &d, NULL);
			ReadFile(hFile, &length, sizeof(int), &d, NULL);
			for (unsigned int j = 0; j < length; j++)
			{
				BYTE flag;
				ReadFile(hFile, &flag, sizeof(BYTE), &d, NULL);
				if (flag)
				{
					int id;
					ReadMorpfIdx(hFile, id);
					group[i].bones.push_back(id);
				}
				else
				{
					int id;
					ReadBoneIdx(hFile, id);
					group[i].morphs.push_back(id);
				}

			}

		}
		//剛体数[int]
		//剛体 * 剛体数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		rigidBodys.resize(length);
		for (auto& rig : rigidBodys)
		{
			ReadString(hFile, rig.name);
			ReadString(hFile, rig.name_eng);
			ReadBoneIdx(hFile, rig.boneIdx);
			ReadFile(hFile, &rig.group, sizeof(BYTE), &d, NULL);
			ReadFile(hFile, &rig.colFlg, sizeof(short), &d, NULL);

			ReadFile(hFile, &rig.form, sizeof(BYTE), &d, NULL);
			ReadFile(hFile, &rig.size, sizeof(float) * 3, &d, NULL);

			ReadFile(hFile, &rig.pos, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &rig.rot, sizeof(float) * 3, &d, NULL);

			ReadFile(hFile, &rig.mass, sizeof(float), &d, NULL);
			ReadFile(hFile, &rig.moveDec, sizeof(float), &d, NULL);
			ReadFile(hFile, &rig.rotDec, sizeof(float), &d, NULL);
			ReadFile(hFile, &rig.repulsion, sizeof(float), &d, NULL);
			ReadFile(hFile, &rig.friction, sizeof(float), &d, NULL);

			ReadFile(hFile, &rig.calcType, sizeof(BYTE), &d, NULL);
		}
		//Joint数[int]
		//Joint * Joint数
		ReadFile(hFile, &length, sizeof(int), &d, NULL);
		joints.resize(length);
		for (auto& joint : joints)
		{
			ReadString(hFile, joint.name);
			ReadString(hFile, joint.name_eng);
			ReadFile(hFile, &joint.type, sizeof(BYTE), &d, NULL);

			ReadRigIdx(hFile, joint.rigsAIdx);
			ReadRigIdx(hFile, joint.rigsBIdx);

			ReadFile(hFile, &joint.pos, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &joint.rot, sizeof(float) * 3, &d, NULL);

			ReadFile(hFile, &joint.posLimitL, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &joint.posLimitH, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &joint.rotLimitL, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &joint.rotLimitH, sizeof(float) * 3, &d, NULL);

			ReadFile(hFile, &joint.moveSpring, sizeof(float) * 3, &d, NULL);
			ReadFile(hFile, &joint.rotSpring, sizeof(float) * 3, &d, NULL);
		}
	}

	///boneテスト

	for (int i = 0; i < bones.size(); i++)
	{
		SendBone s;
		s.tail = bones[i].pos;
		if (bones[i].parentIdx < 0)
		{
			s.head = bones[i].pos;
		}
		else
		{
			s.head = bones[bones[i].parentIdx].pos;
		}
		test.push_back(s);
	}


	//頂点バッファ
	result = dev.Device()->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(vertices.size() * sizeof(PMXVertex)),
		//		&CD3DX12_RESOURCE_DESC::Buffer(test.size()*sizeof(SendBone)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vb));
	assert(SUCCEEDED(result));
	void* _vp = nullptr;
	result = _vb->Map(0, nullptr, (void**)&_vp);
	assert(SUCCEEDED(result));
	memcpy(_vp, &vertices[0], vertices.size() * sizeof(PMXVertex));
	//	memcpy(_vp, &test[0], test.size()*sizeof(SendBone), &d, NULL);
	_vb->Unmap(0, nullptr);

	vbView.BufferLocation = _vb->GetGPUVirtualAddress();
	vbView.StrideInBytes = sizeof(PMXVertex);
	vbView.SizeInBytes = (int)vertices.size() * sizeof(PMXVertex);
	//	vbView.StrideInBytes = sizeof(SendBone)/2;
	//	vbView.SizeInBytes = test.size()*sizeof(SendBone);
	dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);



	//インデックスバッファ
	result = dev.Device()->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(int)*indexes.size()),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_ib));
	assert(SUCCEEDED(result));

	indexBufView.BufferLocation = _ib->GetGPUVirtualAddress();
	indexBufView.SizeInBytes = sizeof(int)*(int)indexes.size();
	indexBufView.Format = DXGI_FORMAT_R32_UINT;

	{
		void* p;
		result = _ib->Map(0, nullptr, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(p, &indexes[0], sizeof(int)*indexes.size());
		_ib->Unmap(0, nullptr);
	}

	//ボーン
	boneNodes.resize(bones.size());
	for (int i = 0; i < bones.size(); i++)
	{
		if (bones[i].parentIdx != -1)
			boneNodes[bones[i].parentIdx].push_back(i);
	}
	CloseHandle(hFile);

	CreateConstBuf();

	return;
}


void PMXData::SetStrRead(BYTE encode)
{
	switch (encode)
	{
	case (1):
		ReadString = [&](HANDLE hFile, std::wstring& ret) {ReadU8(hFile, ret); };

		break;
	case(0):
		ReadString = [&](HANDLE hFile, std::wstring& ret) {ReadU16(hFile, ret); };

		break;
	}
}

void PMXData::ReadU16(HANDLE hFile, std::wstring& ret)
{
	int len;
	DWORD d;
	ReadFile(hFile, &len, sizeof(int), &d, NULL);
	ret.resize(len / 2);
	ReadFile(hFile, &ret[0], sizeof(char)*len, &d, NULL);
}

void PMXData::ReadU8(HANDLE hFile, std::wstring& ret)
{
	int len;
	DWORD d;
	ReadFile(hFile, &len, sizeof(int), &d, NULL);
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	ret.resize(len / 2);
	std::string s;
	s.resize(len);
	ReadFile(hFile, &s[0], sizeof(char)*len, &d, NULL);
	ret = converter.from_bytes(s);
}



void PMXData::InitPipeline()
{
	PMXCommon& common = PMXCommon::Instance();

	pipelineState=common.PipelineState();
	pipelineStateSubTex=common.PipelineStateSubTex();
	pipelineStateAdd=common.PipelineStateAdd();
	pipelineStateMul=common.PipelineStateMul();
	pipelineStateDep=common.PipelineStateDep();
	pipelineStateClear=common.PipelineStateClear();
	pipelineStateEdge=common.PipelineStateEdge();

}

void PMXData::draw(int _cnt)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();

	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);
	dx12dev.CmdList()->IASetIndexBuffer(&indexBufView);
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	int offset = 0;
	{
		void* p;
		HRESULT result = _ib->Map(0, nullptr, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(p, &indexes[0], sizeof(int)*indexes.size());
		_ib->Unmap(0, nullptr);
	}
	dx12dev.CmdList()->SetPipelineState(pipelineState);
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_cbDescriptorHeap);
	for (int i = 0; i < materials.size(); i++)
	{

		CD3DX12_GPU_DESCRIPTOR_HANDLE h(
			_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
			dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV), i);
		dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV2, h);
		if (materials[i].texIdx >= 0)
		{
			CD3DX12_GPU_DESCRIPTOR_HANDLE h2(
				_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
				dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
				(int)materials.size() + materials[i].texIdx);
			dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, h2);
		}
		if (materials[i].sphIdx >= 0)
		{
			CD3DX12_GPU_DESCRIPTOR_HANDLE h3(
				_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
				dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
				(int)materials.size() + materials[i].sphIdx);
			dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV2, h3);
		}
		if (materials[i].toonIdx >= 0)
		{
			if (materials[i].toonFlag)
			{
				PMXCommon::Instance().CommonToonSet(materials[i].toonIdx);
				dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_cbDescriptorHeap);
			}
			else
			{
				CD3DX12_GPU_DESCRIPTOR_HANDLE h4(
					_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
					dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
					(int)materials.size() + materials[i].toonIdx);
				dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV3, h4);

			}
		}
		switch (materials[i].sphMode)
		{
		case(0):
			dx12dev.CmdList()->SetPipelineState(pipelineState);
			break;
		case(1):
			dx12dev.CmdList()->SetPipelineState(pipelineStateMul);
			break;
		case(2):
			dx12dev.CmdList()->SetPipelineState(pipelineStateAdd);
			break;
		case(3):
			dx12dev.CmdList()->SetPipelineState(pipelineStateSubTex);
			break;
		}
		dx12dev.CmdList()->DrawIndexedInstanced(materials[i].vertCnt, _cnt, offset, 0, 0);

		offset += materials[i].vertCnt;
	}
	//bonetest
}
void PMXData::drawID(int _cnt)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();

	dx12dev.CmdList()->IASetVertexBuffers(0, 1, &vbView);
	dx12dev.CmdList()->IASetIndexBuffer(&indexBufView);
	dx12dev.CmdList()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	int offset = 0;
	{
		void* p;
		HRESULT result = _ib->Map(0, nullptr, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(p, &indexes[0], sizeof(int)*indexes.size());
		_ib->Unmap(0, nullptr);
	}
	dx12dev.CmdList()->SetPipelineState(PMXCommon::Instance().PipelineStateID());
	for (int i = 0; i < materials.size(); i++)
	{

		dx12dev.CmdList()->DrawIndexedInstanced(materials[i].vertCnt, _cnt, offset, 0, 0);

		offset += materials[i].vertCnt;
	}
}

void PMXData::WriteDepth()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	int offset = 0;
	{
		void* p;
		HRESULT result = _ib->Map(0, nullptr, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(p, &indexes[0], sizeof(int)*indexes.size());
		_ib->Unmap(0, nullptr);
	}
	dx12dev.CmdList()->SetPipelineState(pipelineStateDep);
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_cbDescriptorHeap);
	for (int i = 0; i < materials.size(); i++)
	{

		CD3DX12_GPU_DESCRIPTOR_HANDLE h(
			_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
			dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV), i);
		dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV2, h);
		if (materials[i].texIdx >= 0)
		{
			CD3DX12_GPU_DESCRIPTOR_HANDLE h2(
				_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
				dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
				(int)materials.size() + materials[i].texIdx);
			dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, h2);
		}
		dx12dev.CmdList()->DrawIndexedInstanced(materials[i].vertCnt, DRAW_CNT, offset, 0, 0);
		offset += materials[i].vertCnt;

	}

}

void PMXData::drawClear()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	int offset = 0;
	{
		void* p;
		HRESULT result = _ib->Map(0, nullptr, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(p, &indexes[0], sizeof(int)*indexes.size());
		_ib->Unmap(0, nullptr);
	}
	dx12dev.CmdList()->SetPipelineState(pipelineStateClear);
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_cbDescriptorHeap);
	offset = 0;
	for (int i = 0; i < materials.size(); i++)
	{
		CD3DX12_GPU_DESCRIPTOR_HANDLE h(
			_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
			dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV), i);
		dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV2, h);
		if (materials[i].texIdx >= 0)
		{
			CD3DX12_GPU_DESCRIPTOR_HANDLE h2(
				_cbDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
				dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
				(int)materials.size() + materials[i].texIdx);
			dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV1, h2);
		}
		dx12dev.CmdList()->DrawIndexedInstanced(materials[i].vertCnt, DRAW_CNT, offset, 0, 0);
		offset += materials[i].vertCnt;
	}

}

//

void PMXData::CreateConstBuf()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;
	//コンスタントバッファ
	std::vector<ID3D12Resource*> _constantBuf;
	_constantBuf.resize(texPathes.size() + materials.size());
	std::vector<D3D12_CONSTANT_BUFFER_VIEW_DESC> cbViewDesc = {};
	cbViewDesc.resize(materials.size());

	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = (UINT)(cbViewDesc.size() + texPathes.size()) + 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&_cbDescriptorHeap));
	assert(SUCCEEDED(result));
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;

	cbDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	cbDesc.Width = (sizeof(CBdata) + 0xff)&~0xff;
	cbDesc.Height = 1;
	cbDesc.DepthOrArraySize = 1;
	cbDesc.MipLevels = 1;
	cbDesc.SampleDesc.Count = 1;
	cbDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

	for (int i = 0; i < materials.size(); i++)
	{

		result = _device->CreateCommittedResource(&cbHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&cbDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&_constantBuf[i]));
		assert(SUCCEEDED(result));
	}
	int size = sizeof(CBdata);
	CBdata* p;
	for (int i = 0; i < materials.size(); i++)
	{
		auto a = _constantBuf[i]->GetGPUVirtualAddress();

		cbViewDesc[i].BufferLocation = _constantBuf[i]->GetGPUVirtualAddress();
		cbViewDesc[i].SizeInBytes = (sizeof(CBdata) + 0xff)&~0xff;
		CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle(_cbDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), _device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV), i);
		_device->CreateConstantBufferView(&cbViewDesc[i], cbvHandle);

		D3D12_RANGE range = {};
		result = _constantBuf[i]->Map(0, &range, (void**)&p);
		assert(SUCCEEDED(result));
		memcpy(&p->diffuse, &materials[i].diffuse, sizeof(float) * 4);
		memcpy(&p->specular, &materials[i].specular, sizeof(float) * 3);
		memcpy(&p->specularity, &materials[i].specularity, sizeof(float));
		memcpy(&p->ambient, &materials[i].ambient, sizeof(float) * 3);
		memcpy(&p->edgeColor, &materials[i].edgeColor, sizeof(float) * 4);
		memcpy(&p->edgeSize, &materials[i].edgeSize, sizeof(float));

		int flg = 0;
		flg |= !(bool)(materials[i].texIdx >= 0) << existTex;
		flg |= !(bool)(materials[i].sphIdx >= 0) << existSphere;
		flg |= !(materials[i].toonFlag) << existToon;
		memcpy(&p->flag, &flg, sizeof(int));
		_constantBuf[i]->Unmap(0, nullptr);
	}
	std::string path;
	int pathIdx1 = (int)fileName.rfind('/');
	int pathIdx2 = (int)fileName.rfind('\\');
	int pathIdx = max(pathIdx1, pathIdx2);
	if (pathIdx != fileName.npos)
		path = fileName.substr(0, pathIdx) + '/';
	else
		path = fileName;

	std::vector<wchar_t> wFilePath(path.size());
	MultiByteToWideChar(CP_ACP, 0, path.c_str(), -1, &wFilePath[0], (int)path.size());
	wchar_t wc[256];
	MultiByteToWideChar(CP_ACP, 0, path.c_str(), -1, wc, (int)path.size());

	for (int i = 0; i < texPathes.size(); i++) {
		_constantBuf[i + materials.size()];

		std::wstring wPath;
		wPath += &wFilePath[0];
		wPath = wPath.substr(0, wFilePath.size());
		wPath += texPathes[i].c_str();
		D3D12_BOX box = {};
		std::wstring ext;
		int extIdx;
		extIdx = (int)texPathes[i].rfind('.');
		ext = texPathes[i].substr(extIdx + 1, texPathes[i].length() - extIdx);
		if (ext == _T("tga"))
		{
			DirectX::TexMetadata metadata;
			DirectX::ScratchImage image;
			DirectX::LoadFromTGAFile(wPath.c_str(), &metadata, image);


			// CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
			CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_CPU_PAGE_PROPERTY_WRITE_BACK, D3D12_MEMORY_POOL_L0);
			ID3D12Resource* tex = nullptr;
			result = dx12dev.Device()->CreateCommittedResource(
				&defaultHeapProperties,
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R8G8B8A8_UNORM, (UINT)metadata.width, (UINT)metadata.height),
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&_constantBuf[i + materials.size()]));
			assert(SUCCEEDED(result));

			box.front = 0;
			box.back = 1;
			box.top = box.left = 0;
			box.right =		(UINT)metadata.width;
			box.bottom =	(UINT)metadata.height;
			result = _constantBuf[i + materials.size()]->WriteToSubresource(0, &box, image.GetPixels(), box.right * 4, box.right*box.bottom * 4);
			assert(SUCCEEDED(result));
		}
		else
		{
			std::unique_ptr<uint8_t[]> dec;
			D3D12_SUBRESOURCE_DATA sub;
			LoadWICTex(_constantBuf[i + materials.size()], sub, wPath.c_str(), dec);
			void* pt = &dec[0];
			D3D12_BOX box = {};
			box.front = 0;
			box.back = 1;
			box.top = box.left = 0;
			box.right = (UINT)sub.RowPitch / 4;
			box.bottom = (UINT)(sub.SlicePitch / sub.RowPitch);
			result = _constantBuf[i + materials.size()]->WriteToSubresource(0, &box, &dec[0], box.right * 4, box.right*box.bottom * 4);
			assert(SUCCEEDED(result));
		}

		dx12dev.CmdList()->ResourceBarrier(1,
			&CD3DX12_RESOURCE_BARRIER::Transition(_constantBuf[i + materials.size()],
				D3D12_RESOURCE_STATE_COPY_DEST,
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

		dx12dev.CmdList()->Close();
		dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
		dx12dev.fence();
	}

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;

	for (int i = 0; i < texPathes.size(); i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle(_cbDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
			_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
			i + (int)materials.size());
		srvDesc.Format = _constantBuf[i + materials.size()]->GetDesc().Format;
		_device->CreateShaderResourceView(_constantBuf[i + materials.size()], &srvDesc, cbvHandle);

	}

	//shadowmapのハンドル
	CD3DX12_CPU_DESCRIPTOR_HANDLE smHandle(_cbDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
		_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
		(int)texPathes.size() + (int)materials.size());

	//formatの変更(shadowmap用に)
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;

	//shadwomaptextureを関連付ける
	_device->CreateShaderResourceView(dx12dev.ShadowTex().Get(), &srvDesc, smHandle);

	D3D12_RANGE range = {};
	result = _constantBuf[0]->Map(0, &range, (void**)&pcBuf);
	result = _constantBuf[1]->Map(0, &range, (void**)&pcBuf2);

}