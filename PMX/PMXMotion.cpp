#include "PMXMotion.h"

#include<algorithm>
#include<codecvt>
#include "../DX12/DirectX12Device.h"
#include<d3dcompiler.h>
#include"../Goto/Player.h"

const int BONE_MAX = 1024;

using namespace DirectX;

bool operator < (const PMXMotion::MotionData& left, const PMXMotion::MotionData& right)
{
	return left.frameNo < right.frameNo;
}
bool operator > (const PMXMotion::MotionData& left, const PMXMotion::MotionData& right)
{
	return left.frameNo > right.frameNo;
}

PMXMotion::PMXMotion()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;

	boneMatrixes.resize(BONE_MAX);
	//コンスタントバッファ
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(XMMATRIX)*BONE_MAX + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_constBoneBuf));

	assert(SUCCEEDED(result));
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&boneDescHeap));
	assert(SUCCEEDED(result));

	int size = sizeof(CBdata);

	cbViewDesc.BufferLocation = _constBoneBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(XMMATRIX) * BONE_MAX + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, boneDescHeap->GetCPUDescriptorHandleForHeapStart());

	D3D12_RANGE range = {};
	result = _constBoneBuf->Map(0, &range, (void**)&pBoneBuf);
	assert(SUCCEEDED(result));
	Reset();

}


PMXMotion::~PMXMotion()
{
	_constBoneBuf->Unmap(0, nullptr);
}

void PMXMotion::ApplyBoneMatrix(std::vector<std::vector<int>>& nodes, int idx=0)
{
	XMMATRIX matrix = pBoneBuf[idx];
	for (auto& cIdx : nodes[idx])
	{
		XMMATRIX child = pBoneBuf[cIdx];
		child = child * matrix;
		pBoneBuf[cIdx] = child;
		ApplyBoneMatrix(nodes, cIdx);
	}
}

void PMXMotion::BoneDeform(PMXData& pmx, XMMATRIX mat, int idx)
{
	XMVECTOR headPos = XMLoadFloat3(&pmx.bones[idx].pos);
	XMMATRIX rot;
	rot = XMMatrixTranslationFromVector(-headPos);
	rot = rot * mat;
	rot = rot * XMMatrixTranslationFromVector(headPos);
//	boneMatrixes[idx] = rot;
	memcpy((pBoneBuf + idx),&rot,sizeof(XMMATRIX));

}

void PMXMotion::Load(const char* path)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;

#pragma pack(1)
	struct Motion
	{
		char name[15];
		unsigned int frame;
		XMFLOAT3 location;
		XMFLOAT4 rotation;
		unsigned char interporation[64];
	};
#pragma pack()

	//kf.clear();
	
	for (auto& it : kf)
	{
		it.second.resize(0);
	}
	
	FILE* fp = nullptr;
	fopen_s(&fp, path, "rb");
	char head[50];
	fread(head, 50, 1, fp);
	DWORD cnt;
	int len = sizeof(int);
	fread(&cnt, sizeof(DWORD), 1, fp);
	std::vector	<Motion> vmdMotion(cnt);
	fread(&vmdMotion[0], sizeof(Motion), cnt, fp);
	fclose(fp);
//	MotionData0 _motion0;
	MotionData _motion = {};
	for (auto& motion : vmdMotion)
	{
//		_motion0.name = motion.name;
//		_motion0.quaternion = XMLoadFloat4(&motion.rotation);
//		md[motion.frame].push_back(_motion0);

		_motion.frameNo = motion.frame;
		//XMMatrixTranslationFromVector(motion.location);
		_motion.quaternion = XMLoadFloat4(&motion.rotation);
		_motion.location.m128_f32[0] = motion.location.x;
		_motion.location.m128_f32[1] = motion.location.y;
		_motion.location.m128_f32[2] = motion.location.z;
		_motion.location.m128_f32[3] = 1;
#include<locale.h>
		wchar_t dest[16];
		size_t size;
		setlocale(LC_ALL, "japanese");
		std::string s;
		s.resize(15);
		memcpy(&s[0], motion.name, 15);

		mbstowcs_s(&size, dest, s.c_str(), 16);
		std::wstring ws = dest;
		kf[dest].push_back(_motion);
		
		maxFrame = max((unsigned int)maxFrame, motion.frame);
	}
	for (auto& keyFrames : kf)
	{
		std::sort(keyFrames.second.begin(), keyFrames.second.end());
	}
	return;
}

void PMXMotion::BoneDeform(PMXData& mesh, float nowFrame)
{
	for (auto& keyFrames : kf)
	{
		auto revit = std::find_if(keyFrames.second.rbegin(), keyFrames.second.rend(),
			[nowFrame](const MotionData& md) {return md.frameNo <= nowFrame; });
		auto it = revit.base();
		//if (it == keyFrames.second.end())
		int idx = mesh.BoneMap()[keyFrames.first];
		XMVECTOR headPos = XMLoadFloat3(&mesh.Bones()[idx].pos);
		XMMATRIX rot;
		rot = XMMatrixTranslationFromVector(-headPos);
		if (it == keyFrames.second.end())
		{
			rot = rot * XMMatrixRotationQuaternion(revit->quaternion);
		}
		else
		{
			float t = (nowFrame - (float)revit->frameNo) / (float)(revit.base()->frameNo - revit->frameNo);
			rot = rot * XMMatrixRotationQuaternion(XMQuaternionSlerp(revit->quaternion, revit.base()->quaternion, t));
		}
		rot = rot * XMMatrixTranslationFromVector(headPos);
//		boneMatrixes[idx] = rot;
		memcpy(pBoneBuf+idx,&rot,sizeof(XMMATRIX));
	}

	return;
}
void PMXMotion::MatrixSet()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&boneDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV5, boneDescHeap->GetGPUDescriptorHandleForHeapStart());

}

void PMXMotion::Reset() {
	//std::fill(&boneMatrixes[0], &boneMatrixes[BONE_MAX - 1], XMMatrixIdentity());
	std::fill(pBoneBuf, pBoneBuf+BONE_MAX, XMMatrixIdentity());
};
