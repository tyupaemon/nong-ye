#pragma once
#include "../DX12/common.h"
#include "../DX12/d3dx12.h"
#include <string>
#include <array>
const std::array<std::wstring, 10> COMMON_TOON = {
	{
		_T("Resource/toon01.bmp"),
		_T("Resource/toon02.bmp"),
		_T("Resource/toon03.bmp"),
		_T("Resource/toon04.bmp"),
		_T("Resource/toon05.bmp"),
		_T("Resource/toon06.bmp"),
		_T("Resource/toon07.bmp"),
		_T("Resource/toon08.bmp"),
		_T("Resource/toon09.bmp"),
		_T("Resource/toon10.bmp") }
};

class PMXCommon
{
private:
	PMXCommon();
	PMXCommon(const PMXCommon&);
	PMXCommon& operator=(const PMXCommon&) {};


	ID3D12PipelineState* pipelineState			= nullptr;
	ID3D12PipelineState* pipelineStateSubTex	= nullptr;
	ID3D12PipelineState* pipelineStateAdd		= nullptr;
	ID3D12PipelineState* pipelineStateMul		= nullptr;

	ID3D12PipelineState* pipelineStateDep		= nullptr;
	ID3D12PipelineState* pipelineStateClear		= nullptr;
	ID3D12PipelineState* pipelineStateEdge		= nullptr;
	ID3D12PipelineState* pipelineStateID		= nullptr;

	ID3DBlob* _vs = nullptr;
	ID3DBlob* _ps = nullptr;
	std::vector<ID3D12Resource*> commonToon;
	ID3D12DescriptorHeap* cToonDescHeap = nullptr;
	void CreateCommonToon();
	void CreatePipeline();
public:
	void CommonToonSet(char _idx);
	ID3D12PipelineState* PipelineState			(){return pipelineState;}
	ID3D12PipelineState* PipelineStateSubTex	(){return pipelineStateSubTex;}
	ID3D12PipelineState* PipelineStateAdd 		(){return pipelineStateAdd;	}
	ID3D12PipelineState* PipelineStateMul 		(){return pipelineStateMul;	}
												   
	ID3D12PipelineState* PipelineStateDep  		(){return pipelineStateDep;	}
	ID3D12PipelineState* PipelineStateClear 	(){return pipelineStateClear;	}
	ID3D12PipelineState* PipelineStateEdge() { return pipelineStateEdge; }
	ID3D12PipelineState* PipelineStateID() { return pipelineStateID; }

	static PMXCommon& Instance()
	{
		static PMXCommon instance;
		return instance;
	}
	~PMXCommon();
};

