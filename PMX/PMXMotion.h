#pragma once
#include"PMXData.h"

#include <DirectXMath.h>
#include <vector>

#include <DirectXMath.h>

class PMXMotion
{
public:
	struct MotionData0
	{
		std::string name;
		DirectX::XMVECTOR quaternion;
	};
	typedef std::map<int, std::vector<MotionData0>> MotionDatas;
	struct MotionData
	{
		MotionData() {};
		unsigned int frameNo;
		DirectX::XMVECTOR quaternion;
		DirectX::XMVECTOR location;
	};
	typedef std::map<std::wstring, std::vector<MotionData>> KeyFrames;
	std::map<std::wstring, std::vector<MotionData>> & GetKF() { return kf; }
	//MotionDatas& GetMotion() { return md; }
	int GetMaxFrame() { return maxFrame; }

	void ApplyBoneMatrix(std::vector<std::vector<int>>& nodes, int idx);
	void BoneDeform(PMXData& pmx, DirectX::XMMATRIX mat, int idx);

	PMXMotion();
	~PMXMotion();
	void Load(const char* path);
	void BoneDeform(PMXData& mesh, float nowFrame);
	void MatrixSet();
	void Reset();
	std::vector<DirectX::XMMATRIX>& BoneMatrixes() { return boneMatrixes; }
private:
	DirectX::XMMATRIX * pBoneBuf;
	ID3D12DescriptorHeap* boneDescHeap = nullptr;
	std::vector<DirectX::XMMATRIX> boneMatrixes;
	ID3D12Resource* _constBoneBuf = nullptr;
	std::map<std::wstring, std::vector<MotionData>> kf;
//	MotionDatas md;
	int maxFrame = 0;

};

