#include "PMXObject.h"

#include"../DX12/DirectX12Device.h"

const int BONE_MAX = 1024;

//1:カメラ,2;マテリアル,3:ワールド行列,5:ボーン
void ApplyBoneMatrix(std::vector<std::vector<int>>& nodes, std::vector<XMMATRIX>& matrixes, int idx)
{
	XMMATRIX matrix = matrixes[idx];
	for (auto& cIdx : nodes[idx])
	{
		XMMATRIX child = matrixes[cIdx];
		child = child * matrix;
		matrixes[cIdx] = child;
		ApplyBoneMatrix(nodes, matrixes, cIdx);
	}
}

void BoneDeform(PMXData& pmx, XMMATRIX mat, int idx, XMMATRIX& res)
{
	XMVECTOR headPos = XMLoadFloat3(&pmx.bones[idx].pos);
	XMMATRIX rot;
	rot = XMMatrixTranslationFromVector(-headPos);
	rot = rot * mat;
	rot = rot * XMMatrixTranslationFromVector(headPos);
	res = rot;

}

PMXObject::PMXObject(std::string _name) :RenderableObject(), model(_name)
{
	motionCnt = 0;
	motionSpd = 1;

	motionable = false;
	motionenable = false;
	SetPostMotionStop();
	modelName = _name;
	wid = model.GetWidth();
	hei = model.GetHeight();
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;

	boneMatrixes.resize(BONE_MAX);
	//コンスタントバッファ
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(XMMATRIX)*BONE_MAX + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_constBoneBuf));

	assert(SUCCEEDED(result));
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&_boneDescHeap));
	assert(SUCCEEDED(result));

	cbViewDesc.BufferLocation = _constBoneBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(XMMATRIX) * BONE_MAX + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, _boneDescHeap->GetCPUDescriptorHandleForHeapStart());

	D3D12_RANGE range = {};
	result = _constBoneBuf->Map(0, &range, (void**)&_pBoneBuf);
	assert(SUCCEEDED(result));

	std::fill(boneMatrixes.begin(), boneMatrixes.end(), XMMatrixRotationZ(0));
	memcpy(_pBoneBuf, &boneMatrixes[0], sizeof(DirectX::XMMATRIX)*BONE_MAX);

}


PMXObject::~PMXObject()
{
	if (_constBoneBuf != nullptr)
	{
		_constBoneBuf->Unmap(0, nullptr);
//		_constBoneBuf->Release();
	}
	if (_boneDescHeap != nullptr)
	{
		ID3D12DescriptorHeap* _boneDescHeap = nullptr;
	}
}

void PMXObject::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbMDdataDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV3, pcbMDdataDescHeap->GetGPUDescriptorHandleForHeapStart());
	/*
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&_boneDescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV5, _boneDescHeap->GetGPUDescriptorHandleForHeapStart());
	*/
	motion.MatrixSet();
	model.draw();
}
void PMXObject::InstancingDraw(int _cnt)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };

	model.draw(_cnt);

}

void PMXObject::Update()
{
	pDModelData->world = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z)*XMMatrixTranslation(pos.m128_f32[0],pos.m128_f32[1],pos.m128_f32[2]);
	if (motionenable)
	{
		//motion.BoneDeform(&model, 0);
		motion.Reset();
		motionCnt += motionSpd;
		fPostMotion();
		motion.BoneDeform(model, motionCnt);
		motion.ApplyBoneMatrix(model.boneNodes, 0);
	}

}

void PMXObject::LoadMotion(const char* path)
{
	motion.Load(path);
	motionable = true;
}
