#include "PMXCommon.h"

#include "../DX12/DirectX12Device.h"
#include<d3dcompiler.h>

PMXCommon::PMXCommon()
{
	CreatePipeline();
	CreateCommonToon();
}


PMXCommon::~PMXCommon()
{
}

void PMXCommon::CreatePipeline()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	D3D12_INPUT_ELEMENT_DESC elementDescs[] = {
	{ "POSITION"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "NORMAL"		, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD"	, 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD"	, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD"	, 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD"	, 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD"	, 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "EDGE"		, 0, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDINDICES", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDINDICES", 1, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDINDICES", 2, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDINDICES", 3, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDWEIGHT", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDWEIGHT", 1, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDWEIGHT", 2, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "BLENDWEIGHT", 3, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "WEIGHTTYPE"	, 0, DXGI_FORMAT_R8_SINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "SDEFVEC"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "SDEFVEC"	, 1, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "SDEFVEC"	, 2, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};
	HRESULT result;
	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", "vs_5_0", D3DCOMPILE_SKIP_VALIDATION | D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_vs, nullptr);
	//#include "../Project2/Shader/Test.h";
	//	result = D3DReadFileToBlob(_T("Shader/VS.cso"), &_vs);
	result = D3DReadFileToBlob(_T("Shader/vs.cso"), &_vs);
	//	std::string str = (char*)e->GetBufferPointer();
	//	assert(SUCCEEDED(result));
	//	_vs = g_main;

	result = D3DReadFileToBlob(_T("Shader/ps.cso"), &_ps);

	assert(SUCCEEDED(result));


	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc = {};
	pipelineStateDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	pipelineStateDesc.BlendState.AlphaToCoverageEnable = false;
	pipelineStateDesc.BlendState.IndependentBlendEnable = false;

	D3D12_RENDER_TARGET_BLEND_DESC Target = {};
	ZeroMemory(&Target, sizeof(Target));
	Target.BlendEnable = true;
	Target.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOp = D3D12_BLEND_OP_ADD;
	Target.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
	Target.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
	Target.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	Target.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++) {
		pipelineStateDesc.BlendState.RenderTarget[i] = Target;
	}
	pipelineStateDesc.DepthStencilState.DepthEnable = true;
	pipelineStateDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	pipelineStateDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	pipelineStateDesc.DepthStencilState.StencilEnable = true;
	pipelineStateDesc.DepthStencilState.StencilReadMask = D3D12_DEFAULT_STENCIL_READ_MASK;
	pipelineStateDesc.DepthStencilState.StencilWriteMask = D3D12_DEFAULT_STENCIL_WRITE_MASK;

	pipelineStateDesc.DepthStencilState.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DepthStencilState.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	pipelineStateDesc.DepthStencilState.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;

	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(_vs);
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);

	pipelineStateDesc.InputLayout.NumElements = sizeof(elementDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	pipelineStateDesc.InputLayout.pInputElementDescs = elementDescs;
	pipelineStateDesc.pRootSignature = dx12dev.RootSignature();
	pipelineStateDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;

	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.SampleMask = 0xffffffff;

	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineState));
	assert(SUCCEEDED(result));

	result = D3DReadFileToBlob(_T("Shader/psid.cso"), &_ps);
	assert(SUCCEEDED(result));
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateID));
	assert(SUCCEEDED(result));

	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "AddPS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	result = D3DReadFileToBlob(_T("Shader/addps.cso"), &_ps);
	assert(SUCCEEDED(result));
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateAdd));
	assert(SUCCEEDED(result));

	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "MulPS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	result = D3DReadFileToBlob(_T("Shader/mulps.cso"), &_ps);
	assert(SUCCEEDED(result));

	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateMul));
	assert(SUCCEEDED(result));

	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "SubTexPS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	result = D3DReadFileToBlob(_T("Shader/subtexps.cso"), &_ps);
	assert(SUCCEEDED(result));
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateSubTex));
	assert(SUCCEEDED(result));


	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PSClear", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	result = D3DReadFileToBlob(_T("Shader/clearps.cso"), &_ps);
	assert(SUCCEEDED(result));

	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateClear));
	assert(SUCCEEDED(result));

	//	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PSDepth", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	result = D3DReadFileToBlob(_T("Shader/depthps.cso"), &_ps);
	assert(SUCCEEDED(result));
	pipelineStateDesc.PS = CD3DX12_SHADER_BYTECODE(_ps);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineStateDep));
	assert(SUCCEEDED(result));

	/*
	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
	pipelineStateDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	pipelineStateDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "EdgePS", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_ps, nullptr);
	assert(SUCCEEDED(result));
	result = D3DCompileFromFile(_T("Shader/shader.hlsl"), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "EdgeVS", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &_vs, nullptr);
	assert(SUCCEEDED(result));
	pipelineStateDesc.VS = CD3DX12_SHADER_BYTECODE(_vs);
	result = _device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&_pipelineStateEdge));
	*/
	assert(SUCCEEDED(result));


}

void PMXCommon::CreateCommonToon()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	HRESULT result;

	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_RESOURCE_DESC cbDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = (UINT)COMMON_TOON.size();
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&cToonDescHeap));

	//コンスタントバッファ
	std::vector<ID3D12Resource*> _constantBuf;
	_constantBuf.resize(COMMON_TOON.size());

	for (int i = 0; i < COMMON_TOON.size(); i++) {
		_constantBuf[i];
		std::unique_ptr<uint8_t[]> dec;
		D3D12_SUBRESOURCE_DATA sub;
		LoadWICTex(_constantBuf[i], sub, COMMON_TOON[i].c_str(), dec);
		D3D12_BOX box = {};
		box.front = 0;
		box.back = 1;
		box.top = box.left = 0;
		box.right = (UINT)sub.RowPitch / 4;
		box.bottom = (UINT)(sub.SlicePitch / sub.RowPitch);
		result = _constantBuf[i]->WriteToSubresource(0, &box, &dec[0], box.right * 4, box.right*box.bottom * 4);
		assert(SUCCEEDED(result));

		dx12dev.CmdList()->ResourceBarrier(1,
			&CD3DX12_RESOURCE_BARRIER::Transition(_constantBuf[i],
				D3D12_RESOURCE_STATE_COPY_DEST,
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

		dx12dev.CmdList()->Close();
		dx12dev.CmdQueue()->ExecuteCommandLists(1, (ID3D12CommandList* const*)&(dx12dev.CmdList()));
		dx12dev.fence();
	}
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;

	for (int i = 0; i < COMMON_TOON.size(); i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle(cToonDescHeap->GetCPUDescriptorHandleForHeapStart(),
			_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),i);
		srvDesc.Format = _constantBuf[i]->GetDesc().Format;
		_device->CreateShaderResourceView(_constantBuf[i], &srvDesc, cbvHandle);

	}

}

void PMXCommon::CommonToonSet(char _idx)
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();

	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&cToonDescHeap);

	CD3DX12_GPU_DESCRIPTOR_HANDLE h(
		cToonDescHeap->GetGPUDescriptorHandleForHeapStart(),
		dx12dev.Device()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV),
		_idx);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(SRV3, h);

}
