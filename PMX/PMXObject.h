#pragma once
#include"../Goto/RenderableObject.h"
#include"PMXData.h"
#include"PMXMotion.h"

#include<string>
#include<functional>
class PMXObject:public RenderableObject
{
private: 
	PMXData model;
	std::string modelName;
	std::vector<XMMATRIX> boneMatrixes;
	ID3D12Resource* _constBoneBuf = nullptr;
	ID3D12DescriptorHeap* _boneDescHeap = nullptr;
	void* _pBoneBuf;

	PMXMotion motion;
	float motionCnt;
	float motionSpd;
	//モーション再生可能状態
	bool motionable;
	//モーションの有効化
	bool motionenable;
	std::function<void()> fPostMotion;
public:
	void LoadMotion(const char* path);
	void MotionPlayback() {
		motionenable = motionable;
		motionCnt=0; }
	void MotionStop() { motionenable = false; }
	void SetMotionCount(float _f) { motionCnt = _f; }
	void SetMotionSpeed(float _f) { motionSpd = _f; }
	//モーション再生後にループさせる
	void SetPostMotionLoop(float _wait) { fPostMotion = [=]() {if (motionCnt > motion.GetMaxFrame() + _wait) { motionCnt = 0; }}; }
	//モーション再生後に停止させる
	void SetPostMotionStop() { fPostMotion = [&]() {motionCnt = min(motionCnt, motion.GetMaxFrame()); }; }
	void Draw();
	void InstancingDraw(int _cnt);
	void Update();

	PMXObject(std::string _name);
	~PMXObject();
};

