#pragma once

#include<windows.h>
#include<vector>
#include<map>
#include<functional>
#include <DirectXMath.h>

#include "../DX12/common.h"
#include "../DX12/d3dx12.h"

const char MODEL_PATH[] = "Resource/";

struct PMXHeader
{
	char sign[4];
	float ver;
	BYTE size;
	//[0] - エンコード方式			| 0:UTF16 1 : UTF8
	//[1] - 追加UV数				| 0〜4 
	//[2] - 頂点Indexサイズ			| 1, 2, 4 のいずれか
	//[3] - テクスチャIndexサイズ	| 1, 2, 4 のいずれか
	//[4] - 材質Indexサイズ			| 1, 2, 4 のいずれか
	//[5] - ボーンIndexサイズ		| 1, 2, 4 のいずれか
	//[6] - モーフIndexサイズ		| 1, 2, 4 のいずれか
	//[7] - 剛体Indexサイズ			| 1, 2, 4 のいずれか
	BYTE data[8];
};

struct PMXVertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 normal;
	DirectX::XMFLOAT2 uv;
	DirectX::XMFLOAT4 ext_uv[4];
	float edge;
	int boneID[4];
	float boneWeight[4];
	int weightType;
	DirectX::XMFLOAT3 sdefVector[3];
	PMXVertex()
	{
		boneID[0] = boneID[1] = boneID[2] = boneID[3] = 0;
		boneWeight[0] = boneWeight[1] = boneWeight[2] = boneWeight[3] = 0;
	}
};

struct PMXMaterial
{
	std::wstring name;
	std::wstring name_eng;
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT3 specular;
	float specularity;
	DirectX::XMFLOAT3 ambient;
	BYTE drawFlag;
	DirectX::XMFLOAT4 edgeColor;
	float edgeSize;
	int texIdx;
	int sphIdx;
	BYTE sphMode;
	BYTE toonFlag;
	int toonIdx;
	std::wstring text;
	int vertCnt;
};
struct PMXIKLink {
	int idx;
	BYTE limit;
	DirectX::XMFLOAT3 upper;
	DirectX::XMFLOAT3 lower;
};
struct PMXBone {
	std::wstring name;
	std::wstring name_eng;
	DirectX::XMFLOAT3 pos;
	int parentIdx;
	int layer;

	//0x0001  : 接続先(PMD子ボーン指定)表示方法 -> 0 : 座標オフセットで指定 1 : ボーンで指定
	//0x0002 : 回転可能
	//0x0004 : 移動可能
	//0x0008 : 表示
	//0x0010 : 操作可
	//0x0020 : IK
	//0x0080 : ローカル付与 | 付与対象 0 : ユーザー変形値／IKリンク／多重付与 1 : 親のローカル変形量
	//0x0100 : 回転付与
	//0x0200 : 移動付与
	//0x0400 : 軸固定
	//0x0800 : ローカル軸
	//0x1000 : 物理後変形
	//0x2000 : 外部親変形
	short flag;

	DirectX::XMFLOAT3 childPos;
	int childIdx;

	int followIdx;
	float followWeight;


	DirectX::XMFLOAT3 fixedAxis;

	DirectX::XMFLOAT3 localAxisX;
	DirectX::XMFLOAT3 localAxisZ;

	int otherParentKey;

	int ikIdx;
	int ikCnt;
	float ikAngle;
	std::vector<PMXIKLink> ikLinks;
};

struct PMXVertMorph {
	int idx;
	DirectX::XMFLOAT3 offset;
};
struct PMXUVMorph {
	int idx;
	DirectX::XMFLOAT4 offset;
};
struct PMXBoneMorph {
	int idx;
	DirectX::XMFLOAT3 move;
	DirectX::XMFLOAT4 rotQuaternion;
};
struct PMXMaterialMorph {
	int idx;
	BYTE calcType;
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT3 specular;
	float specularity;
	DirectX::XMFLOAT3 ambient;
	DirectX::XMFLOAT4 edgeColor;
	float edgeSize;
	DirectX::XMFLOAT4 texCoefficient;
	DirectX::XMFLOAT4 sphCoefficient;
	DirectX::XMFLOAT4 toonCoefficient;

};
struct PMXGroupMorph {
	int idx;
	float weight;
};
struct PMXMorph {
	std::wstring name;
	std::wstring name_eng;
	BYTE category;
	BYTE morphType;
	int morphCnt;
	int morphIdx;
};

struct PMXGroup {
	std::wstring name;
	std::wstring name_eng;
	BYTE exFlag;
	int groupNum;
	std::vector<int> bones;
	std::vector<int> morphs;
};
struct PMXRigidbody {
	std::wstring name;
	std::wstring name_eng;
	int boneIdx;
	BYTE group;
	unsigned short colFlg;
	BYTE form;
	DirectX::XMFLOAT3 size;
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 rot;

	float mass;
	float moveDec;
	float rotDec;
	float repulsion;
	float friction;
	BYTE calcType;
};

struct PMXJoint {
	std::wstring name;
	std::wstring name_eng;

	BYTE type;
	int rigsAIdx;
	int rigsBIdx;

	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 rot;

	DirectX::XMFLOAT3 posLimitH;
	DirectX::XMFLOAT3 posLimitL;
	DirectX::XMFLOAT3 rotLimitH;
	DirectX::XMFLOAT3 rotLimitL;

	DirectX::XMFLOAT3 moveSpring;
	DirectX::XMFLOAT3 rotSpring;

};


struct CBdata;


struct SendBone {
	DirectX::XMFLOAT3 head;
	DirectX::XMFLOAT3 tail;
};

class PMXData
{
private:
	std::wstring name;
	std::wstring name_eng;
	std::wstring comment;
	std::wstring comment_eng;
	std::vector<std::wstring> texPathes;
	std::vector<std::wstring> w_texPathes;
	std::vector<PMXMaterial> materials;
	std::vector<PMXMorph> morphs;

	std::vector<PMXVertMorph> vertMorphs;
	std::vector<PMXUVMorph> uvMorphs;
	std::vector<PMXBoneMorph> boneMorphs;
	std::vector<PMXMaterialMorph> materialMorphs;
	std::vector<PMXGroupMorph> groupMorphs;
	std::vector<PMXGroup> group;
	std::vector<PMXRigidbody> rigidBodys;
	std::vector<PMXJoint> joints;
	std::vector<PMXVertex> vertices;
	std::vector<int> indexes;
	void ReadByteValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		char _byte;
		ReadFile(hFile, &_byte, sizeof(BYTE), &d, NULL);
		ret = _byte;
		return;
	}
	void ReadShortValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		short _short;
		ReadFile(hFile, &_short, sizeof(short), &d, NULL);
		ret = _short;
		return;
	}
	void ReadIntValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		int _int;
		ReadFile(hFile, &_int, sizeof(int), &d, NULL);
		ret = _int;
		return;
	}
	void ReadUByteValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		unsigned char _byte;
		ReadFile(hFile, &_byte, sizeof(BYTE), &d, NULL);
		ret = _byte;
		return;
	}
	void ReadUShortValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		unsigned short _short;
		ReadFile(hFile, &_short, sizeof(short), &d, NULL);
		ret = _short;
		return;
	}
	void ReadUIntValue(HANDLE hFile, int& ret)
	{
		DWORD d;
		unsigned int _int;
		ReadFile(hFile, &_int, sizeof(int), &d, NULL);
		ret = _int;
		return;
	}
	void ReadU16(HANDLE hFile, std::wstring& ret);
	void ReadU8(HANDLE hFile, std::wstring& ret);

	std::function<void(HANDLE hFile, std::wstring& ret)> ReadString;
	void InitPipeline();
	unsigned int _vertexStride;

	ID3D12PipelineState* pipelineState = nullptr;
	ID3D12PipelineState* pipelineStateSubTex = nullptr;
	ID3D12PipelineState* pipelineStateAdd = nullptr;
	ID3D12PipelineState* pipelineStateMul = nullptr;

	ID3D12PipelineState* pipelineStateDep = nullptr;
	ID3D12PipelineState* pipelineStateClear = nullptr;
	ID3D12PipelineState* pipelineStateEdge = nullptr;

	//shadowmap描画用
	ID3D12PipelineState* pipelineStateSM = nullptr;

	ID3DBlob* _vs = nullptr;
	ID3DBlob* _ps = nullptr;
	CBdata* pcBuf;
	CBdata* pcBuf2;
	ID3D12DescriptorHeap* _cbDescriptorHeap = nullptr;
	ID3D12Resource* _vb = nullptr;
	ID3D12Resource* _ib = nullptr;
	D3D12_VERTEX_BUFFER_VIEW vbView = {};
	D3D12_INDEX_BUFFER_VIEW indexBufView = {};
	std::vector<ID3D12Resource*> _texBufs;
	std::string fileName;

	std::vector<DirectX::XMMATRIX> _boneMatrixes;

	std::vector<SendBone> test;
	float wid,hei;
public:
	std::map<std::wstring, int> boneMap;
	std::map<std::wstring, int> boneMapEng;
	std::vector<std::vector<int>> boneNodes;
	std::vector<PMXBone> bones;
	PMXData(std::string name);
	PMXData();
	~PMXData();

	void Load(std::string filePath);
	std::function<void(HANDLE hFile, int& ret)> ReadVertIdx;
	std::function<void(HANDLE hFile, int& ret)> ReadTexIdx;
	std::function<void(HANDLE hFile, int& ret)> ReadMaterialIdx;
	std::function<void(HANDLE hFile, int& ret)> ReadBoneIdx;
	std::function<void(HANDLE hFile, int& ret)> ReadMorpfIdx;
	std::function<void(HANDLE hFile, int& ret)> ReadRigIdx;

	std::function<void(HANDLE hFile, PMXMaterial& ret)> ReadMaterial;
	std::function<void(HANDLE hFile, int& ret)> GetRigIdx;
	void SetReadVariable(std::function<void(HANDLE, int&)>& func, BYTE length)
	{
		switch (length)
		{
		case(1):
			func = [&](HANDLE hFile, int& ret) {ReadByteValue(hFile, ret); };
			break;
		case(2):
			func = [&](HANDLE hFile, int& ret) {ReadShortValue(hFile, ret); };
			break;
		case(4):
			func = [&](HANDLE hFile, int& ret) {ReadIntValue(hFile, ret); };
			break;
		}
	}
	void SetReadUnsignedVariable(std::function<void(HANDLE, int&)>& func, BYTE length)
	{
		switch (length)
		{
		case(1):
			func = [&](HANDLE hFile, int& ret) {ReadUByteValue(hFile, ret); };
			break;
		case(2):
			func = [&](HANDLE hFile, int& ret) {ReadUShortValue(hFile, ret); };
			break;
		case(4):
			func = [&](HANDLE hFile, int& ret) {ReadUIntValue(hFile, ret); };
			break;
		}
	}

	void SetStrRead(BYTE encode);
	void draw(int _cnt = 1);
	void drawID(int _cnt = 1);
	void WriteDepth();
	void drawClear();
	float GetWidth() { return wid; }
	float GetHeight() { return hei; }
//	void draw(int idx);
//	void DrawInstancing(int _cnt);
	D3D12_VERTEX_BUFFER_VIEW& VertBufView() { return vbView; };
	D3D12_INDEX_BUFFER_VIEW& IndexBufView() { return indexBufView; };
	ID3D12PipelineState*& PipelineState() { return pipelineState; }
	void CreateConstBuf();
	int GetMaterialCnt() { return (int)materials.size(); }
//	std::vector<DirectX::XMMATRIX>& BoneMatrixes() { return _boneMatrixes; }
	std::map<std::wstring, int>& BoneMap() { return boneMap; }
	std::vector<PMXBone>& Bones() { return bones; }
};

