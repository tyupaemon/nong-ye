#pragma once
#include <Windows.h>
#include <Xinput.h>
#include <vector>

#define MAX_CONTROLLERS 4 //XInputが認識できるのは４つまで
#define Threshold 65535/4 //しきい値

#define ThumbLX 0
#define ThumbLY 1
#define ThumbRX 2
#define ThumbRY 3

#define RightTrigger 0
#define LeftTrigger 1

struct ControllerState
{
	XINPUT_STATE state;
	bool bConnected;
};

class Input
{
private:

	//パッド関連
	std::vector<ControllerState> _gamepads;
	std::vector<ControllerState> _beforgamepads;

	//キーボード関連
	BYTE _keystate[256];
	BYTE _beforkeystate[256];


	SHORT GetTilt(int padNum, int stick);
	WORD GetTrigger(int padNum, int trigger);

	Input();
	Input(const Input&);
	Input& operator = (const Input&);

public:
	static Input& Instance() {
		static Input instance;
		return instance;
	};
	~Input();

	//更新関数
	void Update();
	void UpdateKeyboardState();
	HRESULT UpdateControllerState();
	std::vector<ControllerState>& GetControllerState();

	//ボタンを押した瞬間かを調べる
	//第一引数　ゲームパッドの番号
	//第二引数　調べたいボタン
	bool PushButton(int padNum,WORD button);

	//対象のスティックが任意の数以上傾いているか調べる
	//第一引数　ゲームパッドの番号
	//第二引数　調べたいスティック
	//第三引数　角度
	bool CheckTilted(int padNum , int stick , SHORT tilt);

	//対象のスティックの傾きを調べる
	//第一引数　ゲームパッドの番号
	//第二引数　調べたいスティック
	SHORT GetTilted(int padNum, int stick);

	//対象のトリガー任意の数以上押し込まれているか調べる
	//第一引数　ゲームパッドの番号
	//第二引数　調べたいトリガー
	//第三引数　コントロール値
	bool CheckTrigger(int padNum, int trigger, BYTE control);

	//対象のキーが押されているかを調べる
	//第一引数　調べたいキー
	bool GetKey(short key);

};

