#include "Input.h"
#include <iostream>
#include <vector>

#pragma comment(lib,"xinput.lib")

#define XINPUT_GAMEPAD_NEUTRAL 0x0000

SHORT Input::GetTilt(int padNum, int stick)
{
	switch (stick)
	{
	case ThumbLX:
		return _gamepads[padNum].state.Gamepad.sThumbLX;
		break;

	case ThumbLY:
		return _gamepads[padNum].state.Gamepad.sThumbLY;
		break;

	case ThumbRX:
		return _gamepads[padNum].state.Gamepad.sThumbRX;
		break;

	case ThumbRY:
		return _gamepads[padNum].state.Gamepad.sThumbRY;
		break;

	default:
		return 0;
		break;
	}
}

WORD Input::GetTrigger(int padNum, int trigger)
{
	switch (trigger)
	{
	case RightTrigger:
		return _gamepads[padNum].state.Gamepad.bRightTrigger;
		break;

	case LeftTrigger:
		return _gamepads[padNum].state.Gamepad.bLeftTrigger;
		break;

	default:
		return 0;
		break;
	}
}

Input::Input() : _beforgamepads(MAX_CONTROLLERS) , _gamepads(MAX_CONTROLLERS)
{
	DWORD dwResult;

	for (DWORD i = 0; i < MAX_CONTROLLERS; i++)
	{
		dwResult = XInputGetState(i, &_gamepads[i].state);
		dwResult = XInputGetState(i, &_beforgamepads[i].state);

		if (dwResult == ERROR_SUCCESS)
		{
			_gamepads[i].bConnected = true;
		}
		else
		{
			_gamepads[i].bConnected = false;
		}
	}
}


Input::~Input()
{
}

void Input::Update()
{
	UpdateKeyboardState();
	UpdateControllerState();
}

void Input::UpdateKeyboardState()
{
	for (int i = 0; i < 256; i++)
	{
		_beforkeystate[i] = _keystate[i];
	}

	GetKeyboardState(_keystate);
}

HRESULT Input::UpdateControllerState()
{
	DWORD dwResult;

	_beforgamepads = _gamepads;

	for (DWORD i = 0; i < MAX_CONTROLLERS; i++)
	{
		dwResult = XInputGetState(i, &_gamepads[i].state);

		if (dwResult == ERROR_SUCCESS)
		{
			_gamepads[i].bConnected = true;
		}
		else
		{
			_gamepads[i].bConnected = false;
		}
	}

	return S_OK;
}

std::vector<ControllerState>& Input::GetControllerState()
{
	return _gamepads;
}

bool Input::PushButton(int padNum,WORD button)
{
	if (_beforgamepads[padNum].state.Gamepad.wButtons == XINPUT_GAMEPAD_NEUTRAL
			&& _gamepads[padNum].state.Gamepad.wButtons == button)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Input::CheckTilted(int padNum, int stick, SHORT tilt)
{
	if (tilt > 0)
	{
		if (tilt < GetTilt(padNum,stick))
		{
			return true;
		}
	}
	else if (tilt <0)
	{
		if (tilt > GetTilt(padNum, stick))
		{
			return true;
		}
	}

	return false;
}

SHORT Input::GetTilted(int padNum, int stick)
{
	return GetTilt(padNum, stick);
}

bool Input::CheckTrigger(int padNum, int trigger, BYTE control)
{
	if (GetTrigger(padNum,trigger) > control)
	{
		return true;
	}

	return false;
}

bool Input::GetKey(short key)
{

	if (_keystate[key] & 0x80)
	{
		return true;
	}

	return false;
}
