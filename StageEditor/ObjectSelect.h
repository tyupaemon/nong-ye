#pragma once
#include <windows.h>
#include<tchar.h>

#include<vector>

enum ObjSelect {
	INST_NAME_LIST,
	OS_MAX
};

//編集用ウィンドウの設置オブジェクト選択タブクラス
class ObjectSelect
{
private:
	static LRESULT CALLBACK WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);

	HWND root;

	//描画領域
	RECT tabRect;

	HWND list;

	//野菜データ選択
	int idx;
	std::vector<std::string> vegeNames;
	std::vector<std::string> instNames;

public:
	void Show(int _visible);

	ObjectSelect();
	~ObjectSelect();
	//タブの初期化
	void InitTab(HINSTANCE _hInst,HWND _hwnd,RECT _tabRect);
	//野菜オブジェクト名の取得
	void InitVegetable();
	//インスタンシングオブジェクト名の取得
	void InitInstancing();
	//野菜データ名をリストに表示
	void DispVegeatable();
	//モデル名をリストに表示
	void DispInstancing();
	const std::string& GetSelectVegetable() { return vegeNames[max(0, idx)]; }
	const std::string& GetSelectInstancing() { return instNames[max(0, idx)]; }
};

