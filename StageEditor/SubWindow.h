#pragma once
#include <windows.h>
#include<tchar.h>

#include"./ObjectSelect.h"
#include<string>
#include<vector>
#include<functional>

const TCHAR SUB_WND_NAME[] = _T("SubWindow");

class SubWindow
{
private:
	//Operation op;
	ObjectSelect os;
	RECT rect;
	HWND g_button1;
	HWND g_text1;

	HWND button1;
	HWND button2;
	HWND button3;
	HWND button4;



	HWND hwnd;

	static LRESULT CALLBACK WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	
//	int fIdx = 0;
	int stateMax;

	void Tab2Draw(int _visible);
	void Tab1Draw(int _visible);
	void InitTab1(HINSTANCE _hInst);
	void InitTab2(HINSTANCE _hInst);
	void InitStateNames();
public:
	HWND Init(HINSTANCE hInst);
	HWND GetWnd() { return hwnd; }
	RECT GetTabRect();
	void Show();
	void DrawOpText();
	void SetCommand();
	SubWindow();
	~SubWindow();
};

