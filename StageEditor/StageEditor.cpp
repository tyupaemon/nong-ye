#include "StageEditor.h"
#include<string>
#include<CommCtrl.h>
#include<functional>
#include<Windows.h>
#include<tchar.h>
#pragma comment(lib,"comctl32.lib")

using namespace std;

const TCHAR E_WIN_NAME[] = _T("EditWindow");
WCHAR TAB_NAME1[] = _T("tab1");
WCHAR TAB_NAME2[] = _T("tab2");

LRESULT StageEditor::WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static std::function<void()> DispText = []() {};

	if (msg == WM_DESTROY) {
		return 0;
	}
	if (msg == WM_SIZE)
	{
		MoveWindow(hTab, 0, 0, LOWORD(lpal), 30, TRUE);
	}
	if (msg == WM_NOTIFY)
	{
		switch (((NMHDR *)lpal)->code)
		{
		case TCN_SELCHANGE:
			if (TabCtrl_GetCurSel(hTab) == 0)
			{
				operateTab.Show(SW_HIDE);
				selectTab.Show(SW_SHOW);
			}
			else
			{
				operateTab.Show(SW_SHOW);
				selectTab.Show(SW_HIDE);
			}
			break;
		}
		InvalidateRect(hwnd, NULL, TRUE);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

LRESULT StageEditor::WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static TCITEM tc_item;
	static StageEditor* thisPtr = nullptr;
	if (!thisPtr)
	{
		thisPtr = (StageEditor*)GetWindowLong(hwnd, GWLP_USERDATA);
		if (msg == WM_CREATE)
		{
			int i = 0;
			thisPtr = (StageEditor*)(((LPCREATESTRUCT)lpal)->lpCreateParams);
			if (thisPtr)
			{
				InitCommonControls();
				thisPtr->hTab = CreateWindowEx(0, WC_TABCONTROL, NULL,
					WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
					0, 0, 10, 10, hwnd, (HMENU)0x10,
					((LPCREATESTRUCT)lpal)->hInstance, NULL);
				tc_item.mask = TCIF_TEXT;
				tc_item.pszText = _T("TAB1");
				TabCtrl_InsertItem(thisPtr->hTab, 0, &tc_item);
				tc_item.pszText = _T("TAB2");
				TabCtrl_InsertItem(thisPtr->hTab, 1, &tc_item);

				return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
			}
		}
	}
	else
	{
		return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

StageEditor::StageEditor()
{
}

StageEditor::~StageEditor()
{
}

HWND StageEditor::Init(HINSTANCE _hInst)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX w = {};
	w.lpfnWndProc = (WNDPROC)WindowProcWrapper;
	w.lpszClassName = E_WIN_NAME;
	w.hInstance = _hInst;
	w.cbSize = sizeof(WNDCLASSEX);

	w.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);

	w.style = CS_HREDRAW | CS_VREDRAW;
	w.cbClsExtra = w.cbWndExtra = 0;
	w.hCursor = w.hIcon = NULL;
	w.lpszMenuName = NULL;


	RegisterClassEx(&w);

	RECT wrc = { 0, 0, E_WIN_WID, E_WIN_HEI };
	::AdjustWindowRectEx(&wrc, WS_OVERLAPPEDWINDOW, false, 0);
	hwnd = CreateWindowEx(0, w.lpszClassName,
		_T("サブウィンドウ"),
		WS_OVERLAPPEDWINDOW | WS_CAPTION,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		wrc.right - wrc.left,
		wrc.bottom - wrc.top,
		nullptr,
		nullptr,
		w.hInstance,
		this);

	ShowWindow(hwnd, SW_SHOW);

	RECT r;
	GetClientRect(hwnd, &r);
	TabCtrl_AdjustRect(hTab, FALSE, &r);

	operateTab.InitTab(_hInst, hwnd, r);
	selectTab.InitTab(_hInst, hwnd, r);
	selectTab.InitVegetable();
	selectTab.InitInstancing();
	selectTab.DispInstancing();
	return hwnd;
	operateTab.Show(SW_HIDE);
	selectTab.Show(SW_SHOW);

}
