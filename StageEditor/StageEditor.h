#pragma once
#include <windows.h>
#include<tchar.h>

#include"ObjectSelect.h"
#include"ObjectOperate.h"


const int E_WIN_WID = 640;
const int E_WIN_HEI = 480;
enum CreateObjectType
{
	instancing
};
//シングルトンクラス
//ステージ編集用データの選択などを行うウィンドウを作成する
class StageEditor
{
private:
	StageEditor();
	StageEditor(const StageEditor&);
	StageEditor& operator=(const StageEditor&) {};

	static LRESULT CALLBACK WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);

	//
	ObjectSelect selectTab;
	ObjectOperate operateTab;
	HWND hwnd;
	HWND hTab;

	CreateObjectType cObjType = instancing;

public:
	static StageEditor& Instance()
	{
		static StageEditor instance;
		return instance;
	}
	CreateObjectType GetCreateObjType() { return cObjType; }
	std::string GetCreateObjName() { return selectTab.GetSelectInstancing(); }
	HWND Init(HINSTANCE _hInst);
	~StageEditor();
};

