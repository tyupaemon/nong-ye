#include "ObjectSelect.h"
#include"StageEditor.h"

#include"../Goto/FieldConfig.h"

#include<tchar.h>
#include<codecvt>
#include<functional>
const WCHAR TAB_NAME[] = _T("選択");


LRESULT ObjectSelect::WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	if (msg == WM_COMMAND)
	{
		switch (LOWORD(wp))
		{
		case INST_NAME_LIST:
			idx = SendMessage(list, LB_GETCURSEL, NULL, NULL);
			break;
		}
	}

	return DefWindowProc(hwnd, msg, wp, lpal);
}

LRESULT ObjectSelect::WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static ObjectSelect* thisPtr = nullptr;
	static bool flg = false;
	if (!thisPtr)
	{
		thisPtr = (ObjectSelect*)GetWindowLong(hwnd, GWLP_USERDATA);
		if (msg == WM_CREATE)
		{
			int i = 0;
			thisPtr = (ObjectSelect*)(((LPCREATESTRUCT)lpal)->lpCreateParams);
			if (thisPtr)
			{
				return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
			}
		}
	}
	else
	{
		return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}
ObjectSelect::ObjectSelect()
{
}


ObjectSelect::~ObjectSelect()
{
}

void ObjectSelect::InitTab(HINSTANCE _hInst, HWND _hwnd,RECT _tabRect)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX w = {};
	w.lpfnWndProc = (WNDPROC)WindowProcWrapper;
	w.lpszClassName = TAB_NAME;
	w.hInstance = _hInst;
	w.cbSize = sizeof(WNDCLASSEX);

	w.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);

	RegisterClassEx(&w);
	tabRect = _tabRect;
	root = CreateWindowEx(WS_EX_COMPOSITED, TAB_NAME, NULL, WS_VISIBLE | WS_CHILD,
		0, tabRect.top,
		E_WIN_WID, tabRect.bottom,
		_hwnd, NULL, _hInst, this);
	list = CreateWindow(_T("listbox"), NULL, WS_VISIBLE | WS_CHILD | LBS_NOTIFY | WS_VSCROLL,
		0, 0,
		100, 300,
		root, (HMENU)INST_NAME_LIST, _hInst, NULL);
	ShowWindow(root, SW_SHOW);

}
#include"../Instancing/InstancingManager.h"
//インスタンシングオブジェクト名の取得
void ObjectSelect::InitInstancing()
{
	instNames.resize(0);
	for(auto& name:InstancingManager::Instance().GetInstancingNames())
	{
		instNames.push_back(name);
	}

}
//野菜オブジェクト名の取得
void ObjectSelect::InitVegetable()
{
	auto& map = FieldConfig::Instance().GetIDMap();
	vegeNames.resize(0);
	for (auto& it : map)
	{
		vegeNames.push_back(it.first);
	}

}
//モデル名をリストに表示
void ObjectSelect::DispInstancing()
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	SendMessage(list, LB_RESETCONTENT, NULL, NULL);

	for (int i = 0; i < instNames.size(); i++)
	{
		std::string fileName;
		int partIdx1 = (int)instNames[i].rfind('/');
		int partIdx2 = (int)instNames[i].rfind('\\');
		int partIdx = max(partIdx1, partIdx2);
		if (partIdx != instNames[i].npos)
			fileName = instNames[i].substr(partIdx+1, instNames[i].size());
		else
			fileName = instNames[i];

		std::wstring wstr = std::to_wstring(i) + _T(":");
		wstr += converter.from_bytes(fileName);
		SendMessage(list, LB_ADDSTRING, NULL, (LPARAM)wstr.c_str());
	}
}
//野菜データ名をリストに表示
void ObjectSelect::DispVegeatable()
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	for (int i = 0; i < vegeNames.size(); i++)
	{
		std::wstring wstr = std::to_wstring(i) + _T(":");
		wstr += converter.from_bytes(vegeNames[i]);
		SendMessage(list, LB_ADDSTRING, NULL, (LPARAM)wstr.c_str());
	}
}

void ObjectSelect::Show(int _visible)
{
	InitInstancing();
	DispInstancing();

	ShowWindow(root, _visible);
	return;

}
