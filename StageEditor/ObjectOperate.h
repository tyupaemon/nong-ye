#pragma once
#include <windows.h>
#include "ObjectSelect.h"
enum ObjOperate{
	INSTANCINGS = OS_MAX,
	DEL_INST,
	ROTX_TEXT,
	ROTY_TEXT,
	ROTZ_TEXT,
	POSX_TEXT,
	POSY_TEXT,
	POSZ_TEXT,
};

class ObjectOperate
{
	static LRESULT CALLBACK WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);
	LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal);

	HWND root;
	HWND list;
	HWND save;
	HWND posText[3];
	HWND rotText[3];
	//�`��̈�
	RECT tabRect;

	int idx;
public:
	void Show(int _visible);
	//�^�u�̏�����
	void InitTab(HINSTANCE _hInst, HWND _hwnd, RECT _tabRect);
	void DispInstancings();
	ObjectOperate();
	~ObjectOperate();
};

