#include "ObjectOperate.h"
#include "StageEditor.h"

#include<tchar.h>
#include<codecvt>
#include"../Objects/ObjectSaver.h"
#include<atlstr.h>
#include<Windows.h>
#include<algorithm>
const WCHAR TAB_NAME[] = _T("操作");

#define toRad(r) r/180.0f*3.141592f
#define fromRad(r) r/3.141592f*180.f
LRESULT WPNumEdit(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static HWND edit;
	static WNDPROC DefStaticProc;
	static bool gettedDefProc = false;
	if (msg == WM_CHAR)
	{
		if (wp == 'A')
		{
			return -1;
		}
	}
	if (!gettedDefProc)
	{
		if (msg == WM_CREATE)
		{
			/*
			edit = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT | ES_NUMBER,
				0, 0,
				0, 0,
				root, (HMENU)POSX_TEXT, _hInst, NULL);
			*/
			(WNDPROC)GetWindowLong(edit, GWLP_WNDPROC);
		}
	}
	if (gettedDefProc)
	{
		return CallWindowProc(DefStaticProc, hwnd, msg, wp, lpal);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}
LRESULT ObjectOperate::WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	static ObjectSaver& oSave = ObjectSaver::Instance();
	char buf[16];
	std::wstring wBuf;

	if (msg == WM_COMMAND)
	{
		switch (LOWORD(wp))
		{
		case INSTANCINGS :
			idx = SendMessage(list, LB_GETCURSEL, NULL, NULL);
			if (idx < 0) { break; }
			if (oSave.GetInstancings().size() <= idx) { break; }
			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetPos().m128_f32[0]);
			SetWindowText(posText[0], converter.from_bytes(buf).c_str());
			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetPos().m128_f32[1]);
			SetWindowText(posText[1], converter.from_bytes(buf).c_str());
			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetPos().m128_f32[2]);
			SetWindowText(posText[2], converter.from_bytes(buf).c_str());

			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetRot().x);
			SetWindowText(rotText[0], converter.from_bytes(buf).c_str());
			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetRot().y);
			SetWindowText(rotText[1], converter.from_bytes(buf).c_str());
			snprintf(buf, 16, "%f", oSave.GetInstancings()[idx]->GetRot().z);
			SetWindowText(rotText[2], converter.from_bytes(buf).c_str());

			break;
			case ROTX_TEXT:
				switch (HIWORD(wp))
				{
				case EN_KILLFOCUS:
					snprintf(buf, 16, "%f", fromRad(oSave.GetInstancings()[idx]->GetRot().x));
					SetWindowText(rotText[0], converter.from_bytes(buf).c_str());
					break;
				case EN_UPDATE:
					wBuf.resize(GetWindowTextLength(rotText[0]));
					GetWindowText(rotText[0], &wBuf[0],32);
					DirectX::XMFLOAT3 r = oSave.GetInstancings()[idx]->GetRot();
					if ((float)_wtof(wBuf.c_str()) == 0.0f)
					{
							for (wchar_t wc : wBuf)
							{
								if (('a' <= wc && wc <= 'z') ||
									('A' <= wc && wc <= 'Z'))
								{
									snprintf(buf, 16, "%f", fromRad(oSave.GetInstancings()[idx]->GetRot().x));
									SetWindowText(rotText[0], converter.from_bytes(buf).c_str());
								}
							}
					}
					r.x = toRad((float)_wtof(wBuf.c_str()));
					oSave.GetInstancings()[idx]->SetRot(r);
					break;
				default:
					break;
				}
				break;
			case ROTY_TEXT:
			case ROTZ_TEXT:
			case POSX_TEXT:
			case POSY_TEXT:
			case POSZ_TEXT:

		default:
			break;
		}
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

LRESULT ObjectOperate::WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static ObjectOperate* thisPtr = nullptr;
	static bool flg = false;
	if (!thisPtr)
	{
		thisPtr = (ObjectOperate*)GetWindowLong(hwnd, GWLP_USERDATA);
		if (msg == WM_CREATE)
		{
			int i = 0;
			thisPtr = (ObjectOperate*)(((LPCREATESTRUCT)lpal)->lpCreateParams);
			if (thisPtr)
			{
				return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
			}
		}
	}
	else
	{
		return thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
	}
	return DefWindowProc(hwnd, msg, wp, lpal);
}

ObjectOperate::ObjectOperate()
{
}


ObjectOperate::~ObjectOperate()
{
}

const int GAP = 10;			//間隔
const int LIST_HEI = 300;	//リスト高さ
const int LIST_WID = 100;	//リスト幅
const int TEXT_HEI = 20;	//テキスト高さ
const int TEXT_WID = 100;	//テキスト幅
WNDPROC DefStaticProc;

void ObjectOperate::InitTab(HINSTANCE _hInst, HWND _hwnd, RECT _tabRect)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX wEx = {};
	wEx.lpfnWndProc = (WNDPROC)WindowProcWrapper;
	wEx.lpszClassName = TAB_NAME;
	wEx.hInstance = _hInst;
	wEx.cbSize = sizeof(WNDCLASSEX);

	wEx.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	RegisterClassEx(&wEx);

	tabRect = _tabRect;
	int x, y;
	x = y = 0;
	root = CreateWindowEx(WS_EX_COMPOSITED, TAB_NAME, NULL, WS_VISIBLE | WS_CHILD,
		0, tabRect.top,
		E_WIN_WID, tabRect.bottom,
		_hwnd, NULL, _hInst, this);
	list = CreateWindow(_T("listbox"), NULL, WS_VISIBLE | WS_CHILD | LBS_NOTIFY | WS_VSCROLL,
		x, y,
		LIST_WID, LIST_HEI,
		root, (HMENU)INSTANCINGS, _hInst, NULL);
	x += LIST_WID + GAP/2;
	posText[0] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT ,
		x, y,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)POSX_TEXT, _hInst, NULL);
	rotText[0] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT,
		x, y + TEXT_HEI + GAP,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)ROTX_TEXT, _hInst, NULL);

	x += LIST_WID + GAP/2;
	posText[1] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT ,
		x, y,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)POSY_TEXT, _hInst, NULL);
	rotText[1] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT ,
		x, y + TEXT_HEI + GAP,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)ROTY_TEXT, _hInst, NULL);

	x += LIST_WID + GAP/2;
	posText[2] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT ,
		x, y,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)POSZ_TEXT, _hInst, NULL);
	rotText[2] = CreateWindow(_T("edit"), _T(""), WS_VISIBLE | WS_CHILD | ES_LEFT ,
		x, y + TEXT_HEI + GAP,
		TEXT_WID, TEXT_HEI,
		root, (HMENU)ROTZ_TEXT, _hInst, NULL);


	ShowWindow(root, SW_SHOW);

}
void ObjectOperate::DispInstancings()
{
	SendMessage(list, LB_RESETCONTENT, NULL, NULL);

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	int cnt = 0;
	for (auto& inst : ObjectSaver::Instance().GetInstancings())
	{
		std::string fileName;
		int partIdx1 = (int)inst->GetName().rfind('/');
		int partIdx2 = (int)inst->GetName().rfind('\\');
		int partIdx = max(partIdx1, partIdx2);
		if (partIdx != inst->GetName().npos)
			fileName = inst->GetName().substr(partIdx + 1, inst->GetName().size());
		else
			fileName = inst->GetName();

		std::wstring wstr = std::to_wstring(cnt++) + _T(":");
		wstr += converter.from_bytes(fileName);
		SendMessage(list, LB_ADDSTRING, NULL, (LPARAM)wstr.c_str());
	}
}

void ObjectOperate::Show(int _visible)
{
	ShowWindow(root, _visible);
	DispInstancings();
}

