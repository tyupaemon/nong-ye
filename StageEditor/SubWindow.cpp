﻿#include "SubWindow.h"

#include<codecvt>
#include<sstream>
#include<iomanip>
#include<CommCtrl.h>

#include <fstream>
#include <iostream>

#pragma comment(lib,"comctl32.lib")

LRESULT SubWindow::WindowProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	static TCITEM tc_item;
	static std::function<void()> DispText = []() {};
	static HWND hTab;
	if (msg == WM_COMMAND)
	{
		switch (LOWORD(wp))
		{
		case 1:
			int i = os.WindowProcedure(hwnd, msg, wp, lpal);
			break;
		}
	}

	if (msg == WM_DESTROY) {
		return 0;
	}
	if (msg == WM_SIZE)
	{
		MoveWindow(hTab, 0, 0, LOWORD(lpal), 30, TRUE);
	}
	if (msg == WM_NOTIFY)
	{
		switch (((NMHDR *)lpal)->code)
		{
		case TCN_SELCHANGE:
			if (TabCtrl_GetCurSel(hTab) == 0)
			{
				os.Show(SW_SHOW);
				Tab2Draw(SW_HIDE);
			}
			else
			{
				os.Show(SW_HIDE);
				Tab2Draw(SW_SHOW);
			}
			break;
		}
		InvalidateRect(hwnd, NULL, TRUE);
	}
	if (msg == WM_CREATE)
	{
		InitCommonControls();
		hTab = CreateWindowEx(0, WC_TABCONTROL, NULL,
			WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
			0, 0, 10, 10, hwnd, (HMENU)0x10,
			((LPCREATESTRUCT)lpal)->hInstance, NULL
		);
		tc_item.mask = TCIF_TEXT;
		tc_item.pszText = _T("TAB1");
		TabCtrl_InsertItem(hTab, 0, &tc_item);
		tc_item.pszText = _T("TAB2");
		TabCtrl_InsertItem(hTab, 1, &tc_item);
		GetClientRect(hwnd, &rect);
		TabCtrl_AdjustRect(hTab, FALSE, &rect);

		DispText = [&]() {DrawOpText(); };
	}
	DispText();
	return DefWindowProc(hwnd, msg, wp, lpal);
}

LRESULT SubWindow::WindowProcWrapper(HWND hwnd, UINT msg, WPARAM wp, LPARAM lpal)
{
	SubWindow* thisPtr = (SubWindow*)GetWindowLong(hwnd, GWLP_USERDATA);
	if (msg == WM_CREATE)
	{
		if (!thisPtr)
		{
			int i = 0;
			thisPtr = (SubWindow*)(((LPCREATESTRUCT)lpal)->lpCreateParams);
			if (thisPtr)
			{
				SetWindowLong(hwnd, GWLP_USERDATA, (LONG)thisPtr);
				LRESULT ret = thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
				return ret;
			}
		}
	}
	LRESULT ret = thisPtr->WindowProcedure(hwnd, msg, wp, lpal);
	return ret;
}

SubWindow::SubWindow()
{
}


SubWindow::~SubWindow()
{
}

HWND SubWindow::Init(HINSTANCE hInst)
{
	//ウィンドウクラスの作成、登録
	WNDCLASSEX w = {};
	w.lpfnWndProc = (WNDPROC)WindowProcWrapper;
	w.lpszClassName = SUB_WND_NAME;
	hInst = GetModuleHandle(NULL);

	w.hInstance = hInst;
	w.cbSize = sizeof(WNDCLASSEX);

	w.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	w.style = CS_HREDRAW | CS_VREDRAW;
	w.cbClsExtra = w.cbWndExtra = 0;
	w.hCursor = w.hIcon = NULL;
	w.lpszMenuName = NULL;


	RegisterClassEx(&w);

	RECT wrc = { 0, 0, 640, 480 };
	::AdjustWindowRectEx(&wrc, WS_OVERLAPPEDWINDOW, false, 0);
	hwnd = CreateWindowEx(0, w.lpszClassName,
		_T("サブウィンドウ"),
		WS_OVERLAPPEDWINDOW|WS_CAPTION,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		wrc.right - wrc.left,
		wrc.bottom - wrc.top,
		nullptr,
		nullptr,
		w.hInstance,
		this);
	ShowWindow(hwnd, SW_SHOW);

	//タブの初期化
	os.InitTab(hInst,hwnd,GetTabRect());
	os.InitVegetable();
	os.DispVegeatable();
	InitTab2(hInst);
	Tab2Draw(SW_HIDE);
	os.Show(SW_SHOW);
	DrawOpText();
	return hwnd;
}


void SubWindow::Show()
{
	ShowWindow(hwnd, SW_SHOW);
}

void SubWindow::InitTab1(HINSTANCE _hInst)
{
}

void SubWindow::InitTab2(HINSTANCE _hInst)
{
}

void SubWindow::Tab2Draw(int _visible)
{
//	ShowWindow(funcList, _visible);
//	ShowWindow(nFuncNameText, _visible);
//	ShowWindow(sFuncNameText, _visible);
//	ShowWindow(funcApply, _visible);
}

void SubWindow::DrawOpText()
{
	/*
	RECT r;
	LPCWSTR s = _T("入力時間");
	HDC hdc;
	hdc = GetDC(hwnd);
	SetBkMode(hdc, TRANSPARENT);
	//SetTextColor(hdc, RGB(128, 230, 230));
	s = _T("入力時間");
	r = op.GetGuidRect(inputLen);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("呼出し番号");
	r = op.GetGuidRect(stateNum);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("受付時間");
	r = op.GetGuidRect(intervalLen);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("状態名");
	r = op.GetGuidRect(stateName);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("luaファイル名");
	r = op.GetGuidRect(scriptName);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("更新関数");
	r = op.GetGuidRect(updateFunc);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	s = _T("呼出し時関数");
	r = op.GetGuidRect(callFunc);
	DrawText(hdc, s, lstrlen(s), &r, 0);

	ReleaseDC(hwnd, hdc);
	*/
}

RECT SubWindow::GetTabRect()
{
	return rect;
}
