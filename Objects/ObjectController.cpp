#include "ObjectController.h"

#include<algorithm>
#include"../Goto/Player.h"
ObjectController::ObjectController()
{
}


ObjectController::~ObjectController()
{
}

void ObjectController::Update()
{
	for (auto& object : objects)
	{
		object->SortHitObject();
		object->Update();
		object->FlushHitObject();
	}
}
void ObjectController::Register(RenderableObject* _obj)
{
	_obj->SetID((int)objects.size());
	objects.push_back(_obj);
}

void ObjectController::Deregistration(int _id)
{
	objects[_id] = objects.back();
	objects[_id]->SetID(_id);
	objects.pop_back();
}

void ObjectController::Deregistration(RenderableObject* _obj)
{
	auto result = std::find(objects.begin(), objects.end(), _obj);
	if (objects.end() != result)
	{
		int idx = (int)std::distance(objects.begin(), result);
		objects[idx] = objects.back();
		objects[idx]->SetID(idx);
		objects.pop_back();
	}
}

void ObjectController::Draw()
{
	for (auto& object : objects)
	{
		object->Draw();
	}
}

