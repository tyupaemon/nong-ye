#include "ProduceObject.h"
#include"../Goto/FieldConfig.h"
#include "../Instancing/InstancingManager.h"

const int DEY_PER_SEC = 2;

ProduceObject::ProduceObject(std::string _name)
{
	time.StartCount();
	label = L_Produce;
	colType = (1 << C_Enable) | (1 << C_Static);
	elapsedDays = 0;	//経過日数
	growNum=0;		//成長値
	hp=128;	//体力
	inc=0;			//累計の収穫数増加値
	enr=0;			//累計の品質上昇値
	id = FieldConfig::Instance().GetIDMap()[_name];
	auto n = FieldConfig::Instance().GetVegetableData()[id].modelName[elapsedDays];
	InstancingManager::Instance().AddInstance(n, this);
	uFunc = [&]() {
		int cnt = (int)time.GetCount();
		if (cnt > DEY_PER_SEC)
		{
			{
				auto n = FieldConfig::Instance().GetVegetableData()[id].modelName[elapsedDays];

				InstancingManager::Instance().DelInstance(n, instId);
			}
			if (++elapsedDays == 5)
			{
				uFunc = []() {};
				dFunc = []() {};
				return;
			}
			time.StartCount();
			auto n = FieldConfig::Instance().GetVegetableData()[id].modelName[elapsedDays];
			InstancingManager::Instance().AddInstance(n, this);
			SetRot(rot);
		}
	};
	dFunc = [&]() {
		auto n = FieldConfig::Instance().GetVegetableData()[id].modelName[elapsedDays];
		InstancingManager::Instance().DelInstance(n, instId);
	};
}


ProduceObject::~ProduceObject()
{
	dFunc();
}

void ProduceObject::Update()
{
	uFunc();
}