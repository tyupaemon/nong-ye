#pragma once
#include "ProduceObject.h"
#include "../PMX/PMXObject.h"
#include "../Instancing/InstancingObject.h"

#include<vector>

//シングルトンクラス
//ゲーム終了時に保存されるオブジェクトを作成する
class ObjectSaver
{
	ObjectSaver();
	ObjectSaver(const ObjectSaver&);
	ObjectSaver& operator=(const ObjectSaver&) {};


	std::vector<ProduceObject> produces;
	std::vector<PMXObject> pmxs;
	std::vector<InstancingObject*> instancings;
public:
	static ObjectSaver& Instance()
	{
		static ObjectSaver instance;
		return instance;
	}
	~ObjectSaver();

	void Save();
	void Load();
	ProduceObject& CreateProduce();
	PMXObject& CreatePMX();
	InstancingObject*& CreateInstancing(std::string _name);
	void DeleteProduce();
	void DeletePMX();
	void DeleteInstancing(int _i);

	std::vector<ProduceObject*>& GetProduce();
	std::vector<PMXObject*>& GetPMX();
	std::vector<InstancingObject*>& GetInstancings() { return instancings; }

};

