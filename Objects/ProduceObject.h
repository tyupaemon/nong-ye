#pragma once

#include"../Instancing/InstancingObject.h"
#include"../Time/Time.h"

#include<string>
#include<functional>
#include"../Goto/Field.h"
class ProduceObject:public InstancingObject
{
private:
	char elapsedDays;	//経過日数
	float growNum;		//成長値
	unsigned char hp;	//体力
	short inc;			//累計の収穫数増加値
	short enr;			//累計の品質上昇値
	Time time;
	int id = 0;

	Field* pField;
	int soilID;
	std::function<void()> uFunc;
	std::function<void()> dFunc;
public:
	void Update();
	ProduceObject(std::string _name);
	~ProduceObject();
};

