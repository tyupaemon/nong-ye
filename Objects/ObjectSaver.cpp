#include "ObjectSaver.h"

#include"../Instancing/InstancingManager.h"

#include <fstream>
#include <iostream>
using namespace std;

const std::string FILE_NAME = "savetest.g";
const std::string FILE_PATH = "Save/";
ObjectSaver::ObjectSaver()
{
	Load();
}


ObjectSaver::~ObjectSaver()
{
	Save();
}

InstancingObject*& ObjectSaver::CreateInstancing(std::string _name)
{
	instancings.push_back(new InstancingObject(_name));
	return instancings.back();
}
void ObjectSaver::DeleteInstancing(int _i)
{
	instancings.erase(instancings.begin() + _i);


}

void ObjectSaver::Save()
{
	std::ofstream f;
	std::string name = FILE_PATH + FILE_NAME;

	f.open(name, ios::out | ios::binary | ios::trunc);
	int size = instancings.size();
	f.write((char*)&size, sizeof(int));
	for (auto& instancing : instancings)
	{
		size = instancing->GetName().size();
		f.write((char*)&size, sizeof(int));
		f.write((char*)&instancing->GetName()[0], sizeof(char)*size);
		f.write((char*)&instancing->GetPos(), sizeof(XMVECTOR));
		f.write((char*)&instancing->GetRot(), sizeof(XMFLOAT3));
	}
	f.close();

}
void ObjectSaver::Load()
{
	std::string name = FILE_PATH + FILE_NAME;
	ifstream f(name, ios::in | ios::binary);
	if (f)
	{
		int size;
		f.read((char*)&size, sizeof(int));
		instancings.resize(size);

		for (auto& instancing : instancings)
		{
			std::string buf;
			XMVECTOR v;
			XMFLOAT3 f3;
			f.read((char*)&size, sizeof(int));
			buf.resize(size);
			f.read((char*)&buf[0], sizeof(char)*size);
			instancing = new InstancingObject(buf);
			f.read((char*)&v, sizeof(XMVECTOR));
			instancing->SetPos(v);
			f.read((char*)&f3, sizeof(XMFLOAT3));
			instancing->SetRot(f3);
		}

	}

}

