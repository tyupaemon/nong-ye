#pragma once
#include<memory>
#include<vector>
#include"../Goto/RenderableObject.h"

//登録されたオブジェクトの一括管理を行う
//
class ObjectController
{
private:
	ObjectController();
	ObjectController(const ObjectController&);
	ObjectController& operator=(const ObjectController&) {};


	std::vector<RenderableObject*> objects;
//	std::vector<RenderableObject&> testobjects;
public:
	static ObjectController& Instance()
	{
		static ObjectController instance;
		return instance;
	}
	~ObjectController();
	void Update();
	void Draw();
	void Register(RenderableObject* _obj);
	void Deregistration(int _id);
	void Deregistration(RenderableObject* _obj);
	RenderableObject*& GetObject(int _id) { return objects[_id]; }
	std::vector<RenderableObject*>& GetObjects() {return objects;}
};

