#include"TGALoader.h"

#include <assert.h>
#include <memory>
#include <malloc.h>

using namespace DirectX;

#define TEX_FILTER_MASK 0xF00000

#define XBOX_DXGI_FORMAT_R10G10B10_7E3_A2_FLOAT DXGI_FORMAT(116)
#define XBOX_DXGI_FORMAT_R10G10B10_6E4_A2_FLOAT DXGI_FORMAT(117)
#define XBOX_DXGI_FORMAT_D16_UNORM_S8_UINT DXGI_FORMAT(118)
#define XBOX_DXGI_FORMAT_R16_UNORM_X8_TYPELESS DXGI_FORMAT(119)
#define XBOX_DXGI_FORMAT_X16_TYPELESS_G8_UINT DXGI_FORMAT(120)

#define WIN10_DXGI_FORMAT_P208 DXGI_FORMAT(130)
#define WIN10_DXGI_FORMAT_V208 DXGI_FORMAT(131)
#define WIN10_DXGI_FORMAT_V408 DXGI_FORMAT(132)

#ifndef XBOX_DXGI_FORMAT_R10G10B10_SNORM_A2_UNORM
#define XBOX_DXGI_FORMAT_R10G10B10_SNORM_A2_UNORM DXGI_FORMAT(189)
#endif

#define XBOX_DXGI_FORMAT_R4G4_UNORM DXGI_FORMAT(190)

enum TEXP_SCANLINE_FLAGS
{
	TEXP_SCANLINE_NONE = 0,
	TEXP_SCANLINE_SETALPHA = 0x1,  // Set alpha channel to known opaque value
	TEXP_SCANLINE_LEGACY = 0x2,  // Enables specific legacy format conversion cases
};

struct handle_closer { void operator()(HANDLE h) { assert(h != INVALID_HANDLE_VALUE); if (h) CloseHandle(h); } };

typedef std::unique_ptr<void, handle_closer> ScopedHandle;

inline HANDLE safe_handle(HANDLE h) { return (h == INVALID_HANDLE_VALUE) ? 0 : h; }

enum CONVERSION_FLAGS
{
	CONV_FLAGS_NONE = 0x0,
	CONV_FLAGS_EXPAND = 0x1,      // Conversion requires expanded pixel size
	CONV_FLAGS_INVERTX = 0x2,      // If set, scanlines are right-to-left
	CONV_FLAGS_INVERTY = 0x4,      // If set, scanlines are top-to-bottom
	CONV_FLAGS_RLE = 0x8,      // Source data is RLE compressed

	CONV_FLAGS_SWIZZLE = 0x10000,  // Swizzle BGR<->RGB data
	CONV_FLAGS_888 = 0x20000,  // 24bpp format
};

enum TGAImageType
{
	TGA_NO_IMAGE = 0,
	TGA_COLOR_MAPPED = 1,
	TGA_TRUECOLOR = 2,
	TGA_BLACK_AND_WHITE = 3,
	TGA_COLOR_MAPPED_RLE = 9,
	TGA_TRUECOLOR_RLE = 10,
	TGA_BLACK_AND_WHITE_RLE = 11,
};

enum TGADescriptorFlags
{
	TGA_FLAGS_INVERTX = 0x10,
	TGA_FLAGS_INVERTY = 0x20,
	TGA_FLAGS_INTERLEAVED_2WAY = 0x40, // Deprecated
	TGA_FLAGS_INTERLEAVED_4WAY = 0x80, // Deprecated
};

const char* g_TGA20_Signature = "TRUEVISION-XFILE.";


#pragma pack(push,1)
struct TGA_HEADER
{
	uint8_t     bIDLength;
	uint8_t     bColorMapType;
	uint8_t     bImageType;
	uint16_t    wColorMapFirst;
	uint16_t    wColorMapLength;
	uint8_t     bColorMapSize;
	uint16_t    wXOrigin;
	uint16_t    wYOrigin;
	uint16_t    wWidth;
	uint16_t    wHeight;
	uint8_t     bBitsPerPixel;
	uint8_t     bDescriptor;
};

struct TGA_FOOTER
{
	uint16_t    dwExtensionOffset;
	uint16_t    dwDeveloperOffset;
	char        Signature[18];
};

struct TGA_EXTENSION
{
	uint16_t    wSize;
	char        szAuthorName[41];
	char        szAuthorComment[324];
	uint16_t    wStampMonth;
	uint16_t    wStampDay;
	uint16_t    wStampYear;
	uint16_t    wStampHour;
	uint16_t    wStampMinute;
	uint16_t    wStampSecond;
	char        szJobName[41];
	uint16_t    wJobHour;
	uint16_t    wJobMinute;
	uint16_t    wJobSecond;
	char        szSoftwareId[41];
	uint16_t    wVersionNumber;
	uint8_t     bVersionLetter;
	uint32_t    dwKeyColor;
	uint16_t    wPixelNumerator;
	uint16_t    wPixelDenominator;
	uint16_t    wGammaNumerator;
	uint16_t    wGammaDenominator;
	uint32_t    dwColorOffset;
	uint32_t    dwStampOffset;
	uint32_t    dwScanOffset;
	uint8_t     bAttributesType;
};
#pragma pack(pop)
_Use_decl_annotations_
inline bool __cdecl IsCompressed(DXGI_FORMAT fmt)
{
	switch (fmt)
	{
	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
	case DXGI_FORMAT_BC6H_TYPELESS:
	case DXGI_FORMAT_BC6H_UF16:
	case DXGI_FORMAT_BC6H_SF16:
	case DXGI_FORMAT_BC7_TYPELESS:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		return true;

	default:
		return false;
	}
}
_Use_decl_annotations_
bool IsPacked(DXGI_FORMAT fmt)
{
	switch (static_cast<int>(fmt))
	{
	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case DXGI_FORMAT_YUY2: // 4:2:2 8-bit
	case DXGI_FORMAT_Y210: // 4:2:2 10-bit
	case DXGI_FORMAT_Y216: // 4:2:2 16-bit
		return true;

	default:
		return false;
	}
}


_Use_decl_annotations_
inline bool __cdecl IsValid(DXGI_FORMAT fmt)
{
	return (static_cast<size_t>(fmt) >= 1 && static_cast<size_t>(fmt) <= 190);
}
_Use_decl_annotations_
bool IsPlanar(DXGI_FORMAT fmt)
{
	switch (static_cast<int>(fmt))
	{
	case DXGI_FORMAT_NV12:      // 4:2:0 8-bit
	case DXGI_FORMAT_P010:      // 4:2:0 10-bit
	case DXGI_FORMAT_P016:      // 4:2:0 16-bit
	case DXGI_FORMAT_420_OPAQUE:// 4:2:0 8-bit
	case DXGI_FORMAT_NV11:      // 4:1:1 8-bit

	case WIN10_DXGI_FORMAT_P208: // 4:2:2 8-bit
	case WIN10_DXGI_FORMAT_V208: // 4:4:0 8-bit
	case WIN10_DXGI_FORMAT_V408: // 4:4:4 8-bit
								 // These are JPEG Hardware decode formats (DXGI 1.4)

	case XBOX_DXGI_FORMAT_D16_UNORM_S8_UINT:
	case XBOX_DXGI_FORMAT_R16_UNORM_X8_TYPELESS:
	case XBOX_DXGI_FORMAT_X16_TYPELESS_G8_UINT:
		// These are Xbox One platform specific types
		return true;

	default:
		return false;
	}
}
_Use_decl_annotations_
inline bool __cdecl IsPalettized(DXGI_FORMAT fmt)
{
	switch (fmt)
	{
	case DXGI_FORMAT_AI44:
	case DXGI_FORMAT_IA44:
	case DXGI_FORMAT_P8:
	case DXGI_FORMAT_A8P8:
		return true;

	default:
		return false;
	}
}

void _SwizzleScanline(
	void* pDestination,
	size_t outSize,
	const void* pSource,
	size_t inSize,
	DXGI_FORMAT format,
	DWORD flags)
{
	assert(pDestination && outSize > 0);
	assert(pSource && inSize > 0);
	assert(IsValid(format) && !IsPlanar(format) && !IsPalettized(format));

	switch (static_cast<int>(format))
	{
		//---------------------------------------------------------------------------------
	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
	case DXGI_FORMAT_R10G10B10A2_UNORM:
	case DXGI_FORMAT_R10G10B10A2_UINT:
	case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
	case XBOX_DXGI_FORMAT_R10G10B10_SNORM_A2_UNORM:
		if (inSize >= 4 && outSize >= 4)
		{
			if (flags & TEXP_SCANLINE_LEGACY)
			{
				// Swap Red (R) and Blue (B) channel (used for D3DFMT_A2R10G10B10 legacy sources)
				if (pDestination == pSource)
				{
					uint32_t *dPtr = reinterpret_cast<uint32_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 3); count += 4)
					{
						uint32_t t = *dPtr;

						uint32_t t1 = (t & 0x3ff00000) >> 20;
						uint32_t t2 = (t & 0x000003ff) << 20;
						uint32_t t3 = (t & 0x000ffc00);
						uint32_t ta = (flags & TEXP_SCANLINE_SETALPHA) ? 0xC0000000 : (t & 0xC0000000);

						*(dPtr++) = t1 | t2 | t3 | ta;
					}
				}
				else
				{
					const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
					uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 3); count += 4)
					{
						uint32_t t = *(sPtr++);

						uint32_t t1 = (t & 0x3ff00000) >> 20;
						uint32_t t2 = (t & 0x000003ff) << 20;
						uint32_t t3 = (t & 0x000ffc00);
						uint32_t ta = (flags & TEXP_SCANLINE_SETALPHA) ? 0xC0000000 : (t & 0xC0000000);

						*(dPtr++) = t1 | t2 | t3 | ta;
					}
				}
				return;
			}
		}
		break;

		//---------------------------------------------------------------------------------
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
	case DXGI_FORMAT_B8G8R8A8_UNORM:
	case DXGI_FORMAT_B8G8R8X8_UNORM:
	case DXGI_FORMAT_B8G8R8A8_TYPELESS:
	case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
	case DXGI_FORMAT_B8G8R8X8_TYPELESS:
	case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
		if (inSize >= 4 && outSize >= 4)
		{
			// Swap Red (R) and Blue (B) channels (used to convert from DXGI 1.1 BGR formats to DXGI 1.0 RGB)
			if (pDestination == pSource)
			{
				uint32_t *dPtr = reinterpret_cast<uint32_t*>(pDestination);
				for (size_t count = 0; count < (outSize - 3); count += 4)
				{
					uint32_t t = *dPtr;

					uint32_t t1 = (t & 0x00ff0000) >> 16;
					uint32_t t2 = (t & 0x000000ff) << 16;
					uint32_t t3 = (t & 0x0000ff00);
					uint32_t ta = (flags & TEXP_SCANLINE_SETALPHA) ? 0xff000000 : (t & 0xFF000000);

					*(dPtr++) = t1 | t2 | t3 | ta;
				}
			}
			else
			{
				const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
				uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
				size_t size = std::min<size_t>(outSize, inSize);
				for (size_t count = 0; count < (size - 3); count += 4)
				{
					uint32_t t = *(sPtr++);

					uint32_t t1 = (t & 0x00ff0000) >> 16;
					uint32_t t2 = (t & 0x000000ff) << 16;
					uint32_t t3 = (t & 0x0000ff00);
					uint32_t ta = (flags & TEXP_SCANLINE_SETALPHA) ? 0xff000000 : (t & 0xFF000000);

					*(dPtr++) = t1 | t2 | t3 | ta;
				}
			}
			return;
		}
		break;

		//---------------------------------------------------------------------------------
	case DXGI_FORMAT_YUY2:
		if (inSize >= 4 && outSize >= 4)
		{
			if (flags & TEXP_SCANLINE_LEGACY)
			{
				// Reorder YUV components (used to convert legacy UYVY -> YUY2)
				if (pDestination == pSource)
				{
					uint32_t *dPtr = reinterpret_cast<uint32_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 3); count += 4)
					{
						uint32_t t = *dPtr;

						uint32_t t1 = (t & 0x000000ff) << 8;
						uint32_t t2 = (t & 0x0000ff00) >> 8;
						uint32_t t3 = (t & 0x00ff0000) << 8;
						uint32_t t4 = (t & 0xff000000) >> 8;

						*(dPtr++) = t1 | t2 | t3 | t4;
					}
				}
				else
				{
					const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
					uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 3); count += 4)
					{
						uint32_t t = *(sPtr++);

						uint32_t t1 = (t & 0x000000ff) << 8;
						uint32_t t2 = (t & 0x0000ff00) >> 8;
						uint32_t t3 = (t & 0x00ff0000) << 8;
						uint32_t t4 = (t & 0xff000000) >> 8;

						*(dPtr++) = t1 | t2 | t3 | t4;
					}
				}
				return;
			}
		}
		break;
	}

	// Fall-through case is to just use memcpy (assuming this is not an in-place operation)
	if (pDestination == pSource)
		return;

	size_t size = std::min<size_t>(outSize, inSize);
	memcpy_s(pDestination, outSize, pSource, size);
}

void _CopyScanline(
	void* pDestination,
	size_t outSize,
	const void* pSource,
	size_t inSize,
	DXGI_FORMAT format,
	DWORD flags)
{
	assert(pDestination && outSize > 0);
	assert(pSource && inSize > 0);
	assert(IsValid(format) && !IsPalettized(format));

	if (flags & TEXP_SCANLINE_SETALPHA)
	{
		switch (static_cast<int>(format))
		{
			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_R32G32B32A32_TYPELESS:
		case DXGI_FORMAT_R32G32B32A32_FLOAT:
		case DXGI_FORMAT_R32G32B32A32_UINT:
		case DXGI_FORMAT_R32G32B32A32_SINT:
			if (inSize >= 16 && outSize >= 16)
			{
				uint32_t alpha;
				if (format == DXGI_FORMAT_R32G32B32A32_FLOAT)
					alpha = 0x3f800000;
				else if (format == DXGI_FORMAT_R32G32B32A32_SINT)
					alpha = 0x7fffffff;
				else
					alpha = 0xffffffff;

				if (pDestination == pSource)
				{
					uint32_t *dPtr = reinterpret_cast<uint32_t*> (pDestination);
					for (size_t count = 0; count < (outSize - 15); count += 16)
					{
						dPtr += 3;
						*(dPtr++) = alpha;
					}
				}
				else
				{
					const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
					uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 15); count += 16)
					{
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = alpha;
						++sPtr;
					}
				}
			}
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_R16G16B16A16_TYPELESS:
		case DXGI_FORMAT_R16G16B16A16_FLOAT:
		case DXGI_FORMAT_R16G16B16A16_UNORM:
		case DXGI_FORMAT_R16G16B16A16_UINT:
		case DXGI_FORMAT_R16G16B16A16_SNORM:
		case DXGI_FORMAT_R16G16B16A16_SINT:
		case DXGI_FORMAT_Y416:
			if (inSize >= 8 && outSize >= 8)
			{
				uint16_t alpha;
				if (format == DXGI_FORMAT_R16G16B16A16_FLOAT)
					alpha = 0x3c00;
				else if (format == DXGI_FORMAT_R16G16B16A16_SNORM || format == DXGI_FORMAT_R16G16B16A16_SINT)
					alpha = 0x7fff;
				else
					alpha = 0xffff;

				if (pDestination == pSource)
				{
					uint16_t *dPtr = reinterpret_cast<uint16_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 7); count += 8)
					{
						dPtr += 3;
						*(dPtr++) = alpha;
					}
				}
				else
				{
					const uint16_t * __restrict sPtr = reinterpret_cast<const uint16_t*>(pSource);
					uint16_t * __restrict dPtr = reinterpret_cast<uint16_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 7); count += 8)
					{
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = *(sPtr++);
						*(dPtr++) = alpha;
						++sPtr;
					}
				}
			}
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_R10G10B10A2_TYPELESS:
		case DXGI_FORMAT_R10G10B10A2_UNORM:
		case DXGI_FORMAT_R10G10B10A2_UINT:
		case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
		case DXGI_FORMAT_Y410:
		case XBOX_DXGI_FORMAT_R10G10B10_7E3_A2_FLOAT:
		case XBOX_DXGI_FORMAT_R10G10B10_6E4_A2_FLOAT:
		case XBOX_DXGI_FORMAT_R10G10B10_SNORM_A2_UNORM:
			if (inSize >= 4 && outSize >= 4)
			{
				if (pDestination == pSource)
				{
					uint32_t *dPtr = reinterpret_cast<uint32_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 3); count += 4)
					{
						*dPtr |= 0xC0000000;
						++dPtr;
					}
				}
				else
				{
					const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
					uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 3); count += 4)
					{
						*(dPtr++) = *(sPtr++) | 0xC0000000;
					}
				}
			}
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_R8G8B8A8_TYPELESS:
		case DXGI_FORMAT_R8G8B8A8_UNORM:
		case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
		case DXGI_FORMAT_R8G8B8A8_UINT:
		case DXGI_FORMAT_R8G8B8A8_SNORM:
		case DXGI_FORMAT_R8G8B8A8_SINT:
		case DXGI_FORMAT_B8G8R8A8_UNORM:
		case DXGI_FORMAT_B8G8R8A8_TYPELESS:
		case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
		case DXGI_FORMAT_AYUV:
			if (inSize >= 4 && outSize >= 4)
			{
				const uint32_t alpha = (format == DXGI_FORMAT_R8G8B8A8_SNORM || format == DXGI_FORMAT_R8G8B8A8_SINT) ? 0x7f000000 : 0xff000000;

				if (pDestination == pSource)
				{
					uint32_t *dPtr = reinterpret_cast<uint32_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 3); count += 4)
					{
						uint32_t t = *dPtr & 0xFFFFFF;
						t |= alpha;
						*(dPtr++) = t;
					}
				}
				else
				{
					const uint32_t * __restrict sPtr = reinterpret_cast<const uint32_t*>(pSource);
					uint32_t * __restrict dPtr = reinterpret_cast<uint32_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 3); count += 4)
					{
						uint32_t t = *(sPtr++) & 0xFFFFFF;
						t |= alpha;
						*(dPtr++) = t;
					}
				}
			}
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_B5G5R5A1_UNORM:
			if (inSize >= 2 && outSize >= 2)
			{
				if (pDestination == pSource)
				{
					uint16_t *dPtr = reinterpret_cast<uint16_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 1); count += 2)
					{
						*(dPtr++) |= 0x8000;
					}
				}
				else
				{
					const uint16_t * __restrict sPtr = reinterpret_cast<const uint16_t*>(pSource);
					uint16_t * __restrict dPtr = reinterpret_cast<uint16_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 1); count += 2)
					{
						*(dPtr++) = *(sPtr++) | 0x8000;
					}
				}
			}
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_A8_UNORM:
			memset(pDestination, 0xff, outSize);
			return;

			//-----------------------------------------------------------------------------
		case DXGI_FORMAT_B4G4R4A4_UNORM:
			if (inSize >= 2 && outSize >= 2)
			{
				if (pDestination == pSource)
				{
					uint16_t *dPtr = reinterpret_cast<uint16_t*>(pDestination);
					for (size_t count = 0; count < (outSize - 1); count += 2)
					{
						*(dPtr++) |= 0xF000;
					}
				}
				else
				{
					const uint16_t * __restrict sPtr = reinterpret_cast<const uint16_t*>(pSource);
					uint16_t * __restrict dPtr = reinterpret_cast<uint16_t*>(pDestination);
					size_t size = std::min<size_t>(outSize, inSize);
					for (size_t count = 0; count < (size - 1); count += 2)
					{
						*(dPtr++) = *(sPtr++) | 0xF000;
					}
				}
			}
			return;
		}
	}

	// Fall-through case is to just use memcpy (assuming this is not an in-place operation)
	if (pDestination == pSource)
		return;

	size_t size = std::min<size_t>(outSize, inSize);
	memcpy_s(pDestination, outSize, pSource, size);
}

HRESULT SetAlphaChannelToOpaque(_In_ const Image* image)
{
	assert(image);

	auto pPixels = reinterpret_cast<uint8_t*>(image->pixels);
	if (!pPixels)
		return E_POINTER;

	for (size_t y = 0; y < image->height; ++y)
	{
		_CopyScanline(pPixels, image->rowPitch, pPixels, image->rowPitch, image->format, TEXP_SCANLINE_SETALPHA);
		pPixels += image->rowPitch;
	}

	return S_OK;
}

_Use_decl_annotations_
size_t BitsPerPixel(DXGI_FORMAT fmt)
{
	switch (static_cast<int>(fmt))
	{
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:
	case DXGI_FORMAT_R32G32B32A32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_UINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:
		return 128;

	case DXGI_FORMAT_R32G32B32_TYPELESS:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32_SINT:
		return 96;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS:
	case DXGI_FORMAT_R16G16B16A16_FLOAT:
	case DXGI_FORMAT_R16G16B16A16_UNORM:
	case DXGI_FORMAT_R16G16B16A16_UINT:
	case DXGI_FORMAT_R16G16B16A16_SNORM:
	case DXGI_FORMAT_R16G16B16A16_SINT:
	case DXGI_FORMAT_R32G32_TYPELESS:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32_SINT:
	case DXGI_FORMAT_R32G8X24_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
	case DXGI_FORMAT_Y416:
	case DXGI_FORMAT_Y210:
	case DXGI_FORMAT_Y216:
		return 64;

	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
	case DXGI_FORMAT_R10G10B10A2_UNORM:
	case DXGI_FORMAT_R10G10B10A2_UINT:
	case DXGI_FORMAT_R11G11B10_FLOAT:
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
	case DXGI_FORMAT_R8G8B8A8_UINT:
	case DXGI_FORMAT_R8G8B8A8_SNORM:
	case DXGI_FORMAT_R8G8B8A8_SINT:
	case DXGI_FORMAT_R16G16_TYPELESS:
	case DXGI_FORMAT_R16G16_FLOAT:
	case DXGI_FORMAT_R16G16_UNORM:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16_SNORM:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R32_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R24G8_TYPELESS:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
	case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case DXGI_FORMAT_B8G8R8A8_UNORM:
	case DXGI_FORMAT_B8G8R8X8_UNORM:
	case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
	case DXGI_FORMAT_B8G8R8A8_TYPELESS:
	case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
	case DXGI_FORMAT_B8G8R8X8_TYPELESS:
	case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
	case DXGI_FORMAT_AYUV:
	case DXGI_FORMAT_Y410:
	case DXGI_FORMAT_YUY2:
	case XBOX_DXGI_FORMAT_R10G10B10_7E3_A2_FLOAT:
	case XBOX_DXGI_FORMAT_R10G10B10_6E4_A2_FLOAT:
	case XBOX_DXGI_FORMAT_R10G10B10_SNORM_A2_UNORM:
		return 32;

	case DXGI_FORMAT_P010:
	case DXGI_FORMAT_P016:
	case XBOX_DXGI_FORMAT_D16_UNORM_S8_UINT:
	case XBOX_DXGI_FORMAT_R16_UNORM_X8_TYPELESS:
	case XBOX_DXGI_FORMAT_X16_TYPELESS_G8_UINT:
	case WIN10_DXGI_FORMAT_V408:
		return 24;

	case DXGI_FORMAT_R8G8_TYPELESS:
	case DXGI_FORMAT_R8G8_UNORM:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8_SNORM:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R16_TYPELESS:
	case DXGI_FORMAT_R16_FLOAT:
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_R16_UNORM:
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16_SNORM:
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_B5G6R5_UNORM:
	case DXGI_FORMAT_B5G5R5A1_UNORM:
	case DXGI_FORMAT_A8P8:
	case DXGI_FORMAT_B4G4R4A4_UNORM:
	case WIN10_DXGI_FORMAT_P208:
	case WIN10_DXGI_FORMAT_V208:
		return 16;

	case DXGI_FORMAT_NV12:
	case DXGI_FORMAT_420_OPAQUE:
	case DXGI_FORMAT_NV11:
		return 12;

	case DXGI_FORMAT_R8_TYPELESS:
	case DXGI_FORMAT_R8_UNORM:
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8_SNORM:
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_A8_UNORM:
	case DXGI_FORMAT_AI44:
	case DXGI_FORMAT_IA44:
	case DXGI_FORMAT_P8:
	case XBOX_DXGI_FORMAT_R4G4_UNORM:
		return 8;

	case DXGI_FORMAT_R1_UNORM:
		return 1;

	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
		return 4;

	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
	case DXGI_FORMAT_BC6H_TYPELESS:
	case DXGI_FORMAT_BC6H_UF16:
	case DXGI_FORMAT_BC6H_SF16:
	case DXGI_FORMAT_BC7_TYPELESS:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		return 8;

	default:
		return 0;
	}
}

//-------------------------------------------------------------------------------------
// Computes the image row pitch in bytes, and the slice ptich (size in bytes of the image)
// based on DXGI format, width, and height
//-------------------------------------------------------------------------------------
_Use_decl_annotations_
void ComputePitch(DXGI_FORMAT fmt, size_t width, size_t height,
	size_t& rowPitch, size_t& slicePitch, DWORD flags)
{
	switch (static_cast<int>(fmt))
	{
	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
		assert(IsCompressed(fmt));
		{
			if (flags & CP_FLAGS_BAD_DXTN_TAILS)
			{
				size_t nbw = width >> 2;
				size_t nbh = height >> 2;
				rowPitch = std::max<size_t>(1, nbw * 8);
				slicePitch = std::max<size_t>(1, rowPitch * nbh);
			}
			else
			{
				size_t nbw = std::max<size_t>(1, (width + 3) / 4);
				size_t nbh = std::max<size_t>(1, (height + 3) / 4);
				rowPitch = nbw * 8;
				slicePitch = rowPitch * nbh;
			}
		}
		break;

	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
	case DXGI_FORMAT_BC6H_TYPELESS:
	case DXGI_FORMAT_BC6H_UF16:
	case DXGI_FORMAT_BC6H_SF16:
	case DXGI_FORMAT_BC7_TYPELESS:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		assert(IsCompressed(fmt));
		{
			if (flags & CP_FLAGS_BAD_DXTN_TAILS)
			{
				size_t nbw = width >> 2;
				size_t nbh = height >> 2;
				rowPitch = std::max<size_t>(1, nbw * 16);
				slicePitch = std::max<size_t>(1, rowPitch * nbh);
			}
			else
			{
				size_t nbw = std::max<size_t>(1, (width + 3) / 4);
				size_t nbh = std::max<size_t>(1, (height + 3) / 4);
				rowPitch = nbw * 16;
				slicePitch = rowPitch * nbh;
			}
		}
		break;

	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case DXGI_FORMAT_YUY2:
		assert(IsPacked(fmt));
		rowPitch = ((width + 1) >> 1) * 4;
		slicePitch = rowPitch * height;
		break;

	case DXGI_FORMAT_Y210:
	case DXGI_FORMAT_Y216:
		assert(IsPacked(fmt));
		rowPitch = ((width + 1) >> 1) * 8;
		slicePitch = rowPitch * height;
		break;

	case DXGI_FORMAT_NV12:
	case DXGI_FORMAT_420_OPAQUE:
		assert(IsPlanar(fmt));
		rowPitch = ((width + 1) >> 1) * 2;
		slicePitch = rowPitch * (height + ((height + 1) >> 1));
		break;

	case DXGI_FORMAT_P010:
	case DXGI_FORMAT_P016:
	case XBOX_DXGI_FORMAT_D16_UNORM_S8_UINT:
	case XBOX_DXGI_FORMAT_R16_UNORM_X8_TYPELESS:
	case XBOX_DXGI_FORMAT_X16_TYPELESS_G8_UINT:
		assert(IsPlanar(fmt));
		rowPitch = ((width + 1) >> 1) * 4;
		slicePitch = rowPitch * (height + ((height + 1) >> 1));
		break;

	case DXGI_FORMAT_NV11:
		assert(IsPlanar(fmt));
		rowPitch = ((width + 3) >> 2) * 4;
		slicePitch = rowPitch * height * 2;
		break;

	case WIN10_DXGI_FORMAT_P208:
		assert(IsPlanar(fmt));
		rowPitch = ((width + 1) >> 1) * 2;
		slicePitch = rowPitch * height * 2;
		break;

	case WIN10_DXGI_FORMAT_V208:
		assert(IsPlanar(fmt));
		rowPitch = width;
		slicePitch = rowPitch * (height + (((height + 1) >> 1) * 2));
		break;

	case WIN10_DXGI_FORMAT_V408:
		assert(IsPlanar(fmt));
		rowPitch = width;
		slicePitch = rowPitch * (height + ((height >> 1) * 4));
		break;

	default:
		assert(IsValid(fmt));
		assert(!IsCompressed(fmt) && !IsPacked(fmt) && !IsPlanar(fmt));
		{

			size_t bpp;

			if (flags & CP_FLAGS_24BPP)
				bpp = 24;
			else if (flags & CP_FLAGS_16BPP)
				bpp = 16;
			else if (flags & CP_FLAGS_8BPP)
				bpp = 8;
			else
				bpp = BitsPerPixel(fmt);

			if (flags & (CP_FLAGS_LEGACY_DWORD | CP_FLAGS_PARAGRAPH | CP_FLAGS_YMM | CP_FLAGS_ZMM | CP_FLAGS_PAGE4K))
			{
				if (flags & CP_FLAGS_PAGE4K)
				{
					rowPitch = ((width * bpp + 32767) / 32768) * 4096;
					slicePitch = rowPitch * height;
				}
				else if (flags & CP_FLAGS_ZMM)
				{
					rowPitch = ((width * bpp + 511) / 512) * 64;
					slicePitch = rowPitch * height;
				}
				else if (flags & CP_FLAGS_YMM)
				{
					rowPitch = ((width * bpp + 255) / 256) * 32;
					slicePitch = rowPitch * height;
				}
				else if (flags & CP_FLAGS_PARAGRAPH)
				{
					rowPitch = ((width * bpp + 127) / 128) * 16;
					slicePitch = rowPitch * height;
				}
				else // DWORD alignment
				{
					// Special computation for some incorrectly created DDS files based on
					// legacy DirectDraw assumptions about pitch alignment
					rowPitch = ((width * bpp + 31) / 32) * sizeof(uint32_t);
					slicePitch = rowPitch * height;
				}
			}
			else
			{
				// Default byte alignment
				rowPitch = (width * bpp + 7) / 8;
				slicePitch = rowPitch * height;
			}
		}
		break;
	}
}

//-------------------------------------------------------------------------------------
// Uncompress pixel data from a TGA into the target image
//-------------------------------------------------------------------------------------
HRESULT UncompressPixels(
	_In_reads_bytes_(size) const void* pSource,
	size_t size,
	_In_ const Image* image,
	_In_ DWORD convFlags)
{
	assert(pSource && size > 0);

	if (!image || !image->pixels)
		return E_POINTER;

	// Compute TGA image data pitch
	size_t rowPitch;
	if (convFlags & CONV_FLAGS_EXPAND)
	{
		rowPitch = image->width * 3;
	}
	else
	{
		size_t slicePitch;
		ComputePitch(image->format, image->width, image->height, rowPitch, slicePitch, CP_FLAGS_NONE);
	}

	auto sPtr = reinterpret_cast<const uint8_t*>(pSource);
	const uint8_t* endPtr = sPtr + size;

	switch (image->format)
	{
		//--------------------------------------------------------------------------- 8-bit
	case DXGI_FORMAT_R8_UNORM:
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);
			assert(offset < rowPitch);

			uint8_t* dPtr = reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1)))
				+ offset;

			for (size_t x = 0; x < image->width; )
			{
				if (sPtr >= endPtr)
					return E_FAIL;

				if (*sPtr & 0x80)
				{
					// Repeat
					size_t j = (*sPtr & 0x7F) + 1;
					if (++sPtr >= endPtr)
						return E_FAIL;

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						*dPtr = *sPtr;

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}

					++sPtr;
				}
				else
				{
					// Literal
					size_t j = (*sPtr & 0x7F) + 1;
					++sPtr;

					if (sPtr + j > endPtr)
						return E_FAIL;

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						*dPtr = *(sPtr++);

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}
				}
			}
		}
		break;

		//-------------------------------------------------------------------------- 16-bit
	case DXGI_FORMAT_B5G5R5A1_UNORM:
	{
		bool nonzeroa = false;
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);
			assert(offset * 2 < rowPitch);

			uint16_t* dPtr = reinterpret_cast<uint16_t*>(reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1))))
				+ offset;

			for (size_t x = 0; x < image->width; )
			{
				if (sPtr >= endPtr)
					return E_FAIL;

				if (*sPtr & 0x80)
				{
					// Repeat
					size_t j = (*sPtr & 0x7F) + 1;
					++sPtr;

					if (sPtr + 1 >= endPtr)
						return E_FAIL;

					uint16_t t = *sPtr | (*(sPtr + 1) << 8);
					if (t & 0x8000)
						nonzeroa = true;
					sPtr += 2;

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						*dPtr = t;

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}
				}
				else
				{
					// Literal
					size_t j = (*sPtr & 0x7F) + 1;
					++sPtr;

					if (sPtr + (j * 2) > endPtr)
						return E_FAIL;

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						uint16_t t = *sPtr | (*(sPtr + 1) << 8);
						if (t & 0x8000)
							nonzeroa = true;
						sPtr += 2;
						*dPtr = t;

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}
				}
			}
		}

		// If there are no non-zero alpha channel entries, we'll assume alpha is not used and force it to opaque
		if (!nonzeroa)
		{
			HRESULT hr = SetAlphaChannelToOpaque(image);
			if (FAILED(hr))
				return hr;
		}
	}
	break;

	//----------------------------------------------------------------------- 24/32-bit
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	{
		bool nonzeroa = false;
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);

			uint32_t* dPtr = reinterpret_cast<uint32_t*>(reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1))))
				+ offset;

			for (size_t x = 0; x < image->width; )
			{
				if (sPtr >= endPtr)
					return E_FAIL;

				if (*sPtr & 0x80)
				{
					// Repeat
					size_t j = (*sPtr & 0x7F) + 1;
					++sPtr;

					DWORD t;
					if (convFlags & CONV_FLAGS_EXPAND)
					{
						assert(offset * 3 < rowPitch);

						if (sPtr + 2 >= endPtr)
							return E_FAIL;

						// BGR -> RGBA
						t = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | 0xFF000000;
						sPtr += 3;

						nonzeroa = true;
					}
					else
					{
						assert(offset * 4 < rowPitch);

						if (sPtr + 3 >= endPtr)
							return E_FAIL;

						// BGRA -> RGBA
						t = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | (*(sPtr + 3) << 24);

						if (*(sPtr + 3) > 0)
							nonzeroa = true;

						sPtr += 4;
					}

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						*dPtr = t;

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}
				}
				else
				{
					// Literal
					size_t j = (*sPtr & 0x7F) + 1;
					++sPtr;

					if (convFlags & CONV_FLAGS_EXPAND)
					{
						if (sPtr + (j * 3) > endPtr)
							return E_FAIL;
					}
					else
					{
						if (sPtr + (j * 4) > endPtr)
							return E_FAIL;
					}

					for (; j > 0; --j, ++x)
					{
						if (x >= image->width)
							return E_FAIL;

						if (convFlags & CONV_FLAGS_EXPAND)
						{
							assert(offset * 3 < rowPitch);

							if (sPtr + 2 >= endPtr)
								return E_FAIL;

							// BGR -> RGBA
							*dPtr = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | 0xFF000000;
							sPtr += 3;

							nonzeroa = true;
						}
						else
						{
							assert(offset * 4 < rowPitch);

							if (sPtr + 3 >= endPtr)
								return E_FAIL;

							// BGRA -> RGBA
							*dPtr = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | (*(sPtr + 3) << 24);

							if (*(sPtr + 3) > 0)
								nonzeroa = true;

							sPtr += 4;
						}

						if (convFlags & CONV_FLAGS_INVERTX)
							--dPtr;
						else
							++dPtr;
					}
				}
			}
		}

		// If there are no non-zero alpha channel entries, we'll assume alpha is not used and force it to opaque
		if (!nonzeroa)
		{
			HRESULT hr = SetAlphaChannelToOpaque(image);
			if (FAILED(hr))
				return hr;
		}
	}
	break;

	//---------------------------------------------------------------------------------
	default:
		return E_FAIL;
	}

	return S_OK;
}


//-------------------------------------------------------------------------------------
// Copies pixel data from a TGA into the target image
//-------------------------------------------------------------------------------------
HRESULT CopyPixels(
	_In_reads_bytes_(size) const void* pSource,
	size_t size,
	_In_ const Image* image,
	_In_ DWORD convFlags)
{
	assert(pSource && size > 0);

	if (!image || !image->pixels)
		return E_POINTER;

	// Compute TGA image data pitch
	size_t rowPitch;
	if (convFlags & CONV_FLAGS_EXPAND)
	{
		rowPitch = image->width * 3;
	}
	else
	{
		size_t slicePitch;
		ComputePitch(image->format, image->width, image->height, rowPitch, slicePitch, CP_FLAGS_NONE);
	}

	const uint8_t* sPtr = reinterpret_cast<const uint8_t*>(pSource);
	const uint8_t* endPtr = sPtr + size;

	switch (image->format)
	{
		//--------------------------------------------------------------------------- 8-bit
	case DXGI_FORMAT_R8_UNORM:
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);
			assert(offset < rowPitch);

			uint8_t* dPtr = reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1)))
				+ offset;

			for (size_t x = 0; x < image->width; ++x)
			{
				if (sPtr >= endPtr)
					return E_FAIL;

				*dPtr = *(sPtr++);

				if (convFlags & CONV_FLAGS_INVERTX)
					--dPtr;
				else
					++dPtr;
			}
		}
		break;

		//-------------------------------------------------------------------------- 16-bit
	case DXGI_FORMAT_B5G5R5A1_UNORM:
	{
		bool nonzeroa = false;
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);
			assert(offset * 2 < rowPitch);

			uint16_t* dPtr = reinterpret_cast<uint16_t*>(reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1))))
				+ offset;

			for (size_t x = 0; x < image->width; ++x)
			{
				if (sPtr + 1 >= endPtr)
					return E_FAIL;

				uint16_t t = *sPtr | (*(sPtr + 1) << 8);
				sPtr += 2;
				*dPtr = t;

				if (t & 0x8000)
					nonzeroa = true;

				if (convFlags & CONV_FLAGS_INVERTX)
					--dPtr;
				else
					++dPtr;
			}
		}

		// If there are no non-zero alpha channel entries, we'll assume alpha is not used and force it to opaque
		if (!nonzeroa)
		{
			HRESULT hr = SetAlphaChannelToOpaque(image);
			if (FAILED(hr))
				return hr;
		}
	}
	break;

	//----------------------------------------------------------------------- 24/32-bit
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	{
		bool nonzeroa = false;
		for (size_t y = 0; y < image->height; ++y)
		{
			size_t offset = ((convFlags & CONV_FLAGS_INVERTX) ? (image->width - 1) : 0);

			uint32_t* dPtr = reinterpret_cast<uint32_t*>(reinterpret_cast<uint8_t*>(image->pixels)
				+ (image->rowPitch * ((convFlags & CONV_FLAGS_INVERTY) ? y : (image->height - y - 1))))
				+ offset;

			for (size_t x = 0; x < image->width; ++x)
			{
				if (convFlags & CONV_FLAGS_EXPAND)
				{
					assert(offset * 3 < rowPitch);

					if (sPtr + 2 >= endPtr)
						return E_FAIL;

					// BGR -> RGBA
					*dPtr = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | 0xFF000000;
					sPtr += 3;

					nonzeroa = true;
				}
				else
				{
					assert(offset * 4 < rowPitch);

					if (sPtr + 3 >= endPtr)
						return E_FAIL;

					// BGRA -> RGBA
					*dPtr = (*sPtr << 16) | (*(sPtr + 1) << 8) | (*(sPtr + 2)) | (*(sPtr + 3) << 24);

					if (*(sPtr + 3) > 0)
						nonzeroa = true;

					sPtr += 4;
				}

				if (convFlags & CONV_FLAGS_INVERTX)
					--dPtr;
				else
					++dPtr;
			}
		}

		// If there are no non-zero alpha channel entries, we'll assume alpha is not used and force it to opaque
		if (!nonzeroa)
		{
			HRESULT hr = SetAlphaChannelToOpaque(image);
			if (FAILED(hr))
				return hr;
		}
	}
	break;

	//---------------------------------------------------------------------------------
	default:
		return E_FAIL;
	}

	return S_OK;
}

size_t CountMips(_In_ size_t width, _In_ size_t height)
{
	size_t mipLevels = 1;

	while (height > 1 || width > 1)
	{
		if (height > 1)
			height >>= 1;

		if (width > 1)
			width >>= 1;

		++mipLevels;
	}

	return mipLevels;
}

bool _CalculateMipLevels(_In_ size_t width, _In_ size_t height, _Inout_ size_t& mipLevels)
{
	if (mipLevels > 1)
	{
		size_t maxMips = CountMips(width, height);
		if (mipLevels > maxMips)
			return false;
	}
	else if (mipLevels == 0)
	{
		mipLevels = CountMips(width, height);
	}
	else
	{
		mipLevels = 1;
	}
	return true;
}

//-------------------------------------------------------------------------------------
// Determines number of image array entries and pixel size
//-------------------------------------------------------------------------------------
_Use_decl_annotations_
void _DetermineImageArray(
	const TexMetadata& metadata,
	DWORD cpFlags,
	size_t& nImages,
	size_t& pixelSize)
{
	assert(metadata.width > 0 && metadata.height > 0 && metadata.depth > 0);
	assert(metadata.arraySize > 0);
	assert(metadata.mipLevels > 0);

	size_t _pixelSize = 0;
	size_t _nimages = 0;

	switch (metadata.dimension)
	{
	case TEX_DIMENSION_TEXTURE1D:
	case TEX_DIMENSION_TEXTURE2D:
		for (size_t item = 0; item < metadata.arraySize; ++item)
		{
			size_t w = metadata.width;
			size_t h = metadata.height;

			for (size_t level = 0; level < metadata.mipLevels; ++level)
			{
				size_t rowPitch, slicePitch;
				ComputePitch(metadata.format, w, h, rowPitch, slicePitch, cpFlags);

				_pixelSize += slicePitch;
				++_nimages;

				if (h > 1)
					h >>= 1;

				if (w > 1)
					w >>= 1;
			}
		}
		break;

	case TEX_DIMENSION_TEXTURE3D:
	{
		size_t w = metadata.width;
		size_t h = metadata.height;
		size_t d = metadata.depth;

		for (size_t level = 0; level < metadata.mipLevels; ++level)
		{
			size_t rowPitch, slicePitch;
			ComputePitch(metadata.format, w, h, rowPitch, slicePitch, cpFlags);

			for (size_t slice = 0; slice < d; ++slice)
			{
				_pixelSize += slicePitch;
				++_nimages;
			}

			if (h > 1)
				h >>= 1;

			if (w > 1)
				w >>= 1;

			if (d > 1)
				d >>= 1;
		}
	}
	break;

	default:
		assert(false);
		break;
	}

	nImages = _nimages;
	pixelSize = _pixelSize;
}

//-------------------------------------------------------------------------------------
// Fills in the image array entries
//-------------------------------------------------------------------------------------
_Use_decl_annotations_
bool _SetupImageArray(
	uint8_t *pMemory,
	size_t pixelSize,
	const TexMetadata& metadata,
	DWORD cpFlags,
	Image* images,
	size_t nImages)
{
	assert(pMemory);
	assert(pixelSize > 0);
	assert(nImages > 0);

	if (!images)
		return false;

	size_t index = 0;
	uint8_t* pixels = pMemory;
	const uint8_t* pEndBits = pMemory + pixelSize;

	switch (metadata.dimension)
	{
	case TEX_DIMENSION_TEXTURE1D:
	case TEX_DIMENSION_TEXTURE2D:
		if (metadata.arraySize == 0 || metadata.mipLevels == 0)
		{
			return false;
		}

		for (size_t item = 0; item < metadata.arraySize; ++item)
		{
			size_t w = metadata.width;
			size_t h = metadata.height;

			for (size_t level = 0; level < metadata.mipLevels; ++level)
			{
				if (index >= nImages)
				{
					return false;
				}

				size_t rowPitch, slicePitch;
				ComputePitch(metadata.format, w, h, rowPitch, slicePitch, cpFlags);

				images[index].width = w;
				images[index].height = h;
				images[index].format = metadata.format;
				images[index].rowPitch = rowPitch;
				images[index].slicePitch = slicePitch;
				images[index].pixels = pixels;
				++index;

				pixels += slicePitch;
				if (pixels > pEndBits)
				{
					return false;
				}

				if (h > 1)
					h >>= 1;

				if (w > 1)
					w >>= 1;
			}
		}
		return true;

	case TEX_DIMENSION_TEXTURE3D:
	{
		if (metadata.mipLevels == 0 || metadata.depth == 0)
		{
			return false;
		}

		size_t w = metadata.width;
		size_t h = metadata.height;
		size_t d = metadata.depth;

		for (size_t level = 0; level < metadata.mipLevels; ++level)
		{
			size_t rowPitch, slicePitch;
			ComputePitch(metadata.format, w, h, rowPitch, slicePitch, cpFlags);

			for (size_t slice = 0; slice < d; ++slice)
			{
				if (index >= nImages)
				{
					return false;
				}

				// We use the same memory organization that Direct3D 11 needs for D3D11_SUBRESOURCE_DATA
				// with all slices of a given miplevel being continuous in memory
				images[index].width = w;
				images[index].height = h;
				images[index].format = metadata.format;
				images[index].rowPitch = rowPitch;
				images[index].slicePitch = slicePitch;
				images[index].pixels = pixels;
				++index;

				pixels += slicePitch;
				if (pixels > pEndBits)
				{
					return false;
				}
			}

			if (h > 1)
				h >>= 1;

			if (w > 1)
				w >>= 1;

			if (d > 1)
				d >>= 1;
		}
	}
	return true;

	default:
		return false;
	}
}

_Use_decl_annotations_
HRESULT ScratchImage::Initialize2D(DXGI_FORMAT fmt, size_t width, size_t height, size_t arraySize, size_t mipLevels, DWORD flags)
{
	if (!IsValid(fmt) || !width || !height || !arraySize)
		return E_INVALIDARG;

	if (IsPalettized(fmt))
		return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);

	if (!_CalculateMipLevels(width, height, mipLevels))
		return E_INVALIDARG;

	Release();

	m_metadata.width = width;
	m_metadata.height = height;
	m_metadata.depth = 1;
	m_metadata.arraySize = arraySize;
	m_metadata.mipLevels = mipLevels;
	m_metadata.miscFlags = 0;
	m_metadata.miscFlags2 = 0;
	m_metadata.format = fmt;
	m_metadata.dimension = TEX_DIMENSION_TEXTURE2D;

	size_t pixelSize, nimages;
	_DetermineImageArray(m_metadata, flags, nimages, pixelSize);

	m_image = new (std::nothrow) Image[nimages];
	if (!m_image)
		return E_OUTOFMEMORY;

	m_nimages = nimages;
	memset(m_image, 0, sizeof(Image) * nimages);

	m_memory = reinterpret_cast<uint8_t*>(_aligned_malloc(pixelSize, 16));
	if (!m_memory)
	{
		Release();
		return E_OUTOFMEMORY;
	}
	m_size = pixelSize;
	if (!_SetupImageArray(m_memory, pixelSize, m_metadata, flags, m_image, nimages))
	{
		Release();
		return E_FAIL;
	}

	return S_OK;
}

_Use_decl_annotations_
const Image* ScratchImage::GetImage(size_t mip, size_t item, size_t slice) const
{
	if (mip >= m_metadata.mipLevels)
		return nullptr;

	size_t index = 0;

	switch (m_metadata.dimension)
	{
	case TEX_DIMENSION_TEXTURE1D:
	case TEX_DIMENSION_TEXTURE2D:
		if (slice > 0)
			return nullptr;

		if (item >= m_metadata.arraySize)
			return nullptr;

		index = item*(m_metadata.mipLevels) + mip;
		break;

	case TEX_DIMENSION_TEXTURE3D:
		if (item > 0)
		{
			// No support for arrays of volumes
			return nullptr;
		}
		else
		{
			size_t d = m_metadata.depth;

			for (size_t level = 0; level < mip; ++level)
			{
				index += d;
				if (d > 1)
					d >>= 1;
			}

			if (slice >= d)
				return nullptr;

			index += slice;
		}
		break;

	default:
		return nullptr;
	}

	return &m_image[index];
}

void ScratchImage::Release()
{
	m_nimages = 0;
	m_size = 0;

	if (m_image)
	{
		delete[] m_image;
		m_image = nullptr;
	}

	if (m_memory)
	{
		_aligned_free(m_memory);
		m_memory = nullptr;
	}

	memset(&m_metadata, 0, sizeof(m_metadata));
}


//-------------------------------------------------------------------------------------
// Decodes TGA header
//-------------------------------------------------------------------------------------
HRESULT DecodeTGAHeader(
	_In_reads_bytes_(size) const void* pSource,
	size_t size,
	_Out_ TexMetadata& metadata,
	size_t& offset,
	_Inout_opt_ DWORD* convFlags)
{
	if (!pSource)
		return E_INVALIDARG;

	memset(&metadata, 0, sizeof(TexMetadata));

	if (size < sizeof(TGA_HEADER))
	{
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	}

	auto pHeader = reinterpret_cast<const TGA_HEADER*>(pSource);

	if (pHeader->bColorMapType != 0
		|| pHeader->wColorMapLength != 0)
	{
		return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
	}

	if (pHeader->bDescriptor & (TGA_FLAGS_INTERLEAVED_2WAY | TGA_FLAGS_INTERLEAVED_4WAY))
	{
		return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
	}

	if (!pHeader->wWidth || !pHeader->wHeight)
	{
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	}

	switch (pHeader->bImageType)
	{
	case TGA_TRUECOLOR:
	case TGA_TRUECOLOR_RLE:
		switch (pHeader->bBitsPerPixel)
		{
		case 16:
			metadata.format = DXGI_FORMAT_B5G5R5A1_UNORM;
			break;

		case 24:
			metadata.format = DXGI_FORMAT_R8G8B8A8_UNORM;
			if (convFlags)
				*convFlags |= CONV_FLAGS_EXPAND;
			// We could use DXGI_FORMAT_B8G8R8X8_UNORM, but we prefer DXGI 1.0 formats
			break;

		case 32:
			metadata.format = DXGI_FORMAT_R8G8B8A8_UNORM;
			// We could use DXGI_FORMAT_B8G8R8A8_UNORM, but we prefer DXGI 1.0 formats
			break;
		}

		if (convFlags && (pHeader->bImageType == TGA_TRUECOLOR_RLE))
		{
			*convFlags |= CONV_FLAGS_RLE;
		}
		break;

	case TGA_BLACK_AND_WHITE:
	case TGA_BLACK_AND_WHITE_RLE:
		switch (pHeader->bBitsPerPixel)
		{
		case 8:
			metadata.format = DXGI_FORMAT_R8_UNORM;
			break;

		default:
			return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
		}

		if (convFlags && (pHeader->bImageType == TGA_BLACK_AND_WHITE_RLE))
		{
			*convFlags |= CONV_FLAGS_RLE;
		}
		break;

	case TGA_NO_IMAGE:
	case TGA_COLOR_MAPPED:
	case TGA_COLOR_MAPPED_RLE:
		return HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);

	default:
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	}

	metadata.width = pHeader->wWidth;
	metadata.height = pHeader->wHeight;
	metadata.depth = metadata.arraySize = metadata.mipLevels = 1;
	metadata.dimension = TEX_DIMENSION_TEXTURE2D;

	if (convFlags)
	{
		if (pHeader->bDescriptor & TGA_FLAGS_INVERTX)
			*convFlags |= CONV_FLAGS_INVERTX;

		if (pHeader->bDescriptor & TGA_FLAGS_INVERTY)
			*convFlags |= CONV_FLAGS_INVERTY;
	}

	offset = sizeof(TGA_HEADER);

	if (pHeader->bIDLength != 0)
	{
		offset += pHeader->bIDLength;
	}

	return S_OK;
}


HRESULT DirectX::LoadFromTGAFile(
	const wchar_t* szFile,
	TexMetadata* metadata,
	ScratchImage& image)
{
	if (!szFile)
		return E_INVALIDARG;

	image.Release();

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
	ScopedHandle hFile(safe_handle(CreateFile2(szFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, nullptr)));
#else
	ScopedHandle hFile(safe_handle(CreateFileW(szFile, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN, nullptr)));
#endif
	if (!hFile)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	// Get the file size
	FILE_STANDARD_INFO fileInfo;
	if (!GetFileInformationByHandleEx(hFile.get(), FileStandardInfo, &fileInfo, sizeof(fileInfo)))
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	// File is too big for 32-bit allocation, so reject read (4 GB should be plenty large enough for a valid TGA file)
	if (fileInfo.EndOfFile.HighPart > 0)
	{
		return HRESULT_FROM_WIN32(ERROR_FILE_TOO_LARGE);
	}

	// Need at least enough data to fill the header to be a valid TGA
	if (fileInfo.EndOfFile.LowPart < sizeof(TGA_HEADER))
	{
		return E_FAIL;
	}

	// Read the header
	uint8_t header[sizeof(TGA_HEADER)];
	DWORD bytesRead = 0;
	if (!ReadFile(hFile.get(), header, sizeof(TGA_HEADER), &bytesRead, nullptr))
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	size_t offset;
	DWORD convFlags = 0;
	TexMetadata mdata;
	HRESULT hr = DecodeTGAHeader(header, bytesRead, mdata, offset, &convFlags);
	if (FAILED(hr))
		return hr;

	// Read the pixels
	DWORD remaining = static_cast<DWORD>(fileInfo.EndOfFile.LowPart - offset);
	if (remaining == 0)
		return E_FAIL;

	if (offset > sizeof(TGA_HEADER))
	{
		// Skip past the id string
		LARGE_INTEGER filePos = { { static_cast<DWORD>(offset), 0 } };
		if (!SetFilePointerEx(hFile.get(), filePos, 0, FILE_BEGIN))
		{
			return HRESULT_FROM_WIN32(GetLastError());
		}
	}

	hr = image.Initialize2D(mdata.format, mdata.width, mdata.height, 1, 1);
	if (FAILED(hr))
		return hr;

	assert(image.GetPixels());

	if (!(convFlags & (CONV_FLAGS_RLE | CONV_FLAGS_EXPAND | CONV_FLAGS_INVERTX)) && (convFlags & CONV_FLAGS_INVERTY))
	{
		// This case we can read directly into the image buffer in place
		if (!ReadFile(hFile.get(), image.GetPixels(), static_cast<DWORD>(image.GetPixelsSize()), &bytesRead, nullptr))
		{
			image.Release();
			return HRESULT_FROM_WIN32(GetLastError());
		}

		if (bytesRead != image.GetPixelsSize())
		{
			image.Release();
			return E_FAIL;
		}

		switch (mdata.format)
		{
		case DXGI_FORMAT_R8G8B8A8_UNORM:
		{
			// TGA stores 32-bit data in BGRA form, need to swizzle to RGBA
			assert(image.GetImageCount() == 1);
			const Image* img = image.GetImage(0, 0, 0);
			if (!img)
			{
				image.Release();
				return E_POINTER;
			}

			uint8_t *pPixels = img->pixels;
			if (!pPixels)
			{
				image.Release();
				return E_POINTER;
			}

			size_t rowPitch = img->rowPitch;

			// Scan for non-zero alpha channel
			bool nonzeroa = false;

			for (size_t h = 0; h < img->height; ++h)
			{
				const uint32_t* sPtr = reinterpret_cast<const uint32_t*>(pPixels);

				for (size_t x = 0; x < img->width; ++x)
				{
					if ((*sPtr) & 0xff000000)
					{
						nonzeroa = true;
						break;
					}

					++sPtr;
				}

				if (nonzeroa)
					break;

				pPixels += rowPitch;
			}

			DWORD tflags = (!nonzeroa) ? TEXP_SCANLINE_SETALPHA : TEXP_SCANLINE_NONE;

			// Swizzle scanlines
			pPixels = img->pixels;

			for (size_t h = 0; h < img->height; ++h)
			{
				_SwizzleScanline(pPixels, rowPitch, pPixels, rowPitch, mdata.format, tflags);
				pPixels += rowPitch;
			}
		}
		break;

		// If we start using DXGI_FORMAT_B8G8R8X8_UNORM or DXGI_FORMAT_B8G8R8A8_UNORM we need to check for a fully 0 alpha channel

		case DXGI_FORMAT_B5G5R5A1_UNORM:
		{
			assert(image.GetImageCount() == 1);
			const Image* img = image.GetImage(0, 0, 0);
			if (!img)
			{
				image.Release();
				return E_POINTER;
			}

			// Scan for non-zero alpha channel
			bool nonzeroa = false;

			const uint8_t *pPixels = img->pixels;
			if (!pPixels)
			{
				image.Release();
				return E_POINTER;
			}

			size_t rowPitch = img->rowPitch;

			for (size_t h = 0; h < img->height; ++h)
			{
				const uint16_t* sPtr = reinterpret_cast<const uint16_t*>(pPixels);

				for (size_t x = 0; x < img->width; ++x)
				{
					if (*sPtr & 0x8000)
					{
						nonzeroa = true;
						break;
					}

					++sPtr;
				}

				if (nonzeroa)
					break;

				pPixels += rowPitch;
			}

			// If there are no non-zero alpha channel entries, we'll assume alpha is not used and force it to opaque
			if (!nonzeroa)
			{
				hr = SetAlphaChannelToOpaque(img);
				if (FAILED(hr))
				{
					image.Release();
					return hr;
				}
			}
		}
		break;

		default:
			break;
		}
	}
	else // RLE || EXPAND || INVERTX || !INVERTY
	{
		std::unique_ptr<uint8_t[]> temp(new (std::nothrow) uint8_t[remaining]);
		if (!temp)
		{
			image.Release();
			return E_OUTOFMEMORY;
		}

		if (!ReadFile(hFile.get(), temp.get(), remaining, &bytesRead, nullptr))
		{
			image.Release();
			return HRESULT_FROM_WIN32(GetLastError());
		}

		if (bytesRead != remaining)
		{
			image.Release();
			return E_FAIL;
		}

		if (convFlags & CONV_FLAGS_RLE)
		{
			hr = UncompressPixels(temp.get(), remaining, image.GetImage(0, 0, 0), convFlags);
		}
		else
		{
			hr = CopyPixels(temp.get(), remaining, image.GetImage(0, 0, 0), convFlags);
		}

		if (FAILED(hr))
		{
			image.Release();
			return hr;
		}
	}

	if (metadata)
		memcpy(metadata, &mdata, sizeof(TexMetadata));

	return S_OK;
}
