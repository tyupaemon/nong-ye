#pragma once


#include <stdint.h>

#include <algorithm>
#include <functional>
#include <vector>

#if !defined(__d3d11_h__) && !defined(__d3d11_x_h__) && !defined(__d3d12_h__) && !defined(__d3d12_x_h__)
#if defined(_XBOX_ONE) && defined(_TITLE)
#include <d3d11_x.h>
#define DCOMMON_H_INCLUDED
#else
#include <d3d11_1.h>
#endif
#endif

#include <directxmath.h>

#include <ocidl.h>

#define DIRECTX_TEX_VERSION 151

struct IWICImagingFactory;
struct IWICMetadataQueryReader;


namespace DirectX
{
	enum CP_FLAGS
	{
		CP_FLAGS_NONE = 0x0,      // Normal operation
		CP_FLAGS_LEGACY_DWORD = 0x1,      // Assume pitch is DWORD aligned instead of BYTE aligned
		CP_FLAGS_PARAGRAPH = 0x2,      // Assume pitch is 16-byte aligned instead of BYTE aligned
		CP_FLAGS_YMM = 0x4,      // Assume pitch is 32-byte aligned instead of BYTE aligned
		CP_FLAGS_ZMM = 0x8,      // Assume pitch is 64-byte aligned instead of BYTE aligned
		CP_FLAGS_PAGE4K = 0x200,    // Assume pitch is 4096-byte aligned instead of BYTE aligned
		CP_FLAGS_BAD_DXTN_TAILS = 0x1000,   // BC formats with malformed mipchain blocks smaller than 4x4
		CP_FLAGS_24BPP = 0x10000,  // Override with a legacy 24 bits-per-pixel format size
		CP_FLAGS_16BPP = 0x20000,  // Override with a legacy 16 bits-per-pixel format size
		CP_FLAGS_8BPP = 0x40000,  // Override with a legacy 8 bits-per-pixel format size
	};

	// Texture metadata
	enum TEX_DIMENSION
		// Subset here matches D3D10_RESOURCE_DIMENSION and D3D11_RESOURCE_DIMENSION
	{
		TEX_DIMENSION_TEXTURE1D = 2,
		TEX_DIMENSION_TEXTURE2D = 3,
		TEX_DIMENSION_TEXTURE3D = 4,
	};

	enum TEX_MISC_FLAG
		// Subset here matches D3D10_RESOURCE_MISC_FLAG and D3D11_RESOURCE_MISC_FLAG
	{
		TEX_MISC_TEXTURECUBE = 0x4L,
	};

	enum TEX_MISC_FLAG2
	{
		TEX_MISC2_ALPHA_MODE_MASK = 0x7L,
	};

	enum TEX_ALPHA_MODE
		// Matches DDS_ALPHA_MODE, encoded in MISC_FLAGS2
	{
		TEX_ALPHA_MODE_UNKNOWN = 0,
		TEX_ALPHA_MODE_STRAIGHT = 1,
		TEX_ALPHA_MODE_PREMULTIPLIED = 2,
		TEX_ALPHA_MODE_OPAQUE = 3,
		TEX_ALPHA_MODE_CUSTOM = 4,
	};

	struct TexMetadata
	{
		size_t          width;
		size_t          height;     // Should be 1 for 1D textures
		size_t          depth;      // Should be 1 for 1D or 2D textures
		size_t          arraySize;  // For cubemap, this is a multiple of 6
		size_t          mipLevels;
		uint32_t        miscFlags;
		uint32_t        miscFlags2;
		DXGI_FORMAT     format;
		TEX_DIMENSION   dimension;

		size_t __cdecl ComputeIndex(_In_ size_t mip, _In_ size_t item, _In_ size_t slice) const;
		// Returns size_t(-1) to indicate an out-of-range error

		bool __cdecl IsCubemap() const { return (miscFlags & TEX_MISC_TEXTURECUBE) != 0; }
		// Helper for miscFlags

		bool __cdecl IsPMAlpha() const { return ((miscFlags2 & TEX_MISC2_ALPHA_MODE_MASK) == TEX_ALPHA_MODE_PREMULTIPLIED) != 0; }
		void __cdecl SetAlphaMode(TEX_ALPHA_MODE mode) { miscFlags2 = (miscFlags2 & ~TEX_MISC2_ALPHA_MODE_MASK) | static_cast<uint32_t>(mode); }
		TEX_ALPHA_MODE __cdecl GetAlphaMode() const { return static_cast<TEX_ALPHA_MODE>(miscFlags2 & TEX_MISC2_ALPHA_MODE_MASK); }
		// Helpers for miscFlags2

		bool __cdecl IsVolumemap() const { return (dimension == TEX_DIMENSION_TEXTURE3D); }
		// Helper for dimension
	};
	//---------------------------------------------------------------------------------
	// Bitmap image container
	struct Image
	{
		size_t      width;
		size_t      height;
		DXGI_FORMAT format;
		size_t      rowPitch;
		size_t      slicePitch;
		uint8_t*    pixels;
	};

	class ScratchImage
	{
	public:
		ScratchImage()
			: m_nimages(0), m_size(0), m_metadata{}, m_image(nullptr), m_memory(nullptr) {}
		ScratchImage(ScratchImage&& moveFrom)
			: m_nimages(0), m_size(0), m_metadata{}, m_image(nullptr), m_memory(nullptr) { *this = std::move(moveFrom); }
		~ScratchImage() { Release(); }

		ScratchImage& __cdecl operator= (ScratchImage&& moveFrom);

		ScratchImage(const ScratchImage&) = delete;
		ScratchImage& operator=(const ScratchImage&) = delete;

		HRESULT __cdecl Initialize(_In_ const TexMetadata& mdata, _In_ DWORD flags = CP_FLAGS_NONE);

		HRESULT __cdecl Initialize1D(_In_ DXGI_FORMAT fmt, _In_ size_t length, _In_ size_t arraySize, _In_ size_t mipLevels, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl Initialize2D(_In_ DXGI_FORMAT fmt, _In_ size_t width, _In_ size_t height, _In_ size_t arraySize, _In_ size_t mipLevels, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl Initialize3D(_In_ DXGI_FORMAT fmt, _In_ size_t width, _In_ size_t height, _In_ size_t depth, _In_ size_t mipLevels, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl InitializeCube(_In_ DXGI_FORMAT fmt, _In_ size_t width, _In_ size_t height, _In_ size_t nCubes, _In_ size_t mipLevels, _In_ DWORD flags = CP_FLAGS_NONE);

		HRESULT __cdecl InitializeFromImage(_In_ const Image& srcImage, _In_ bool allow1D = false, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl InitializeArrayFromImages(_In_reads_(nImages) const Image* images, _In_ size_t nImages, _In_ bool allow1D = false, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl InitializeCubeFromImages(_In_reads_(nImages) const Image* images, _In_ size_t nImages, _In_ DWORD flags = CP_FLAGS_NONE);
		HRESULT __cdecl Initialize3DFromImages(_In_reads_(depth) const Image* images, _In_ size_t depth, _In_ DWORD flags = CP_FLAGS_NONE);

		void __cdecl Release();

		bool __cdecl OverrideFormat(_In_ DXGI_FORMAT f);

		const TexMetadata& __cdecl GetMetadata() const { return m_metadata; }
		const Image* __cdecl GetImage(_In_ size_t mip, _In_ size_t item, _In_ size_t slice) const;

		const Image* __cdecl GetImages() const { return m_image; }
		size_t __cdecl GetImageCount() const { return m_nimages; }

		uint8_t* __cdecl GetPixels() const { return m_memory; }
		size_t __cdecl GetPixelsSize() const { return m_size; }

		bool __cdecl IsAlphaAllOpaque() const;

	private:
		size_t      m_nimages;
		size_t      m_size;
		TexMetadata m_metadata;
		Image*      m_image;
		uint8_t*    m_memory;
	};

	//---------------------------------------------------------------------------------

	HRESULT __cdecl LoadFromTGAFile(_In_z_ const wchar_t* szFile,
		_Out_opt_ TexMetadata* metadata, _Out_ ScratchImage& image);
}