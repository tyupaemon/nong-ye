#include "DirectX12Device.h"
#include <assert.h>
#include <dxgi1_4.h>
#include <tchar.h>
#include <vector>
#include <d3dcompiler.h>
//#include "Vertex.h"
#include "common.h"

#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3dcompiler.lib")


const TCHAR WND_CLASS_NAME[] = _T("Name");

DirectX12Device::DirectX12Device()
{
	HRESULT result;
	D3D_FEATURE_LEVEL featureLVs[] = {
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0
	};
	for (auto lv : featureLVs)
	{
		result = D3D12CreateDevice(nullptr, lv, IID_PPV_ARGS(&_device));
		if (SUCCEEDED(result))
		{
			runLV = lv;
			break;
		}
	}
	assert(SUCCEEDED(result));
	_cmdAlloc = nullptr;
	_cmdList = nullptr;
	UINT64 fenceValue = 0;
	_fence = nullptr;
	_device->CreateFence(fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&_fence));
	_cmdQueue;
	result = _device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&_cmdAlloc));
	assert(SUCCEEDED(result));
	result = _device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, _cmdAlloc, nullptr, IID_PPV_ARGS(&_cmdList));
	assert(SUCCEEDED(result));
	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc = {};
	cmdQueueDesc.NodeMask = 0;
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cmdQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	result = _device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&_cmdQueue));
	assert(SUCCEEDED(result));

}


DirectX12Device::~DirectX12Device()
{
}

void DirectX12Device::init(HWND hwnd)
{
	HRESULT result;
	_hwnd = hwnd;
	result = CreateDXGIFactory1(IID_PPV_ARGS(&_dxgiFactory));
	assert(SUCCEEDED(result));
	InitSwapchain();
	CreateRenderTarget();
	CreateRootSignature();
	CreateDepthBuf();
	CreateShadowMapTexture();
}

void DirectX12Device::InitSwapchain()
{
	HRESULT result;
	DXGI_SWAP_CHAIN_DESC1 scDesc = {};
	scDesc.BufferCount = 1;
	scDesc.Width = (UINT)WINDOW_WID;
	scDesc.Height = (UINT)WINDOW_HEI;
	scDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scDesc.Stereo = false;
	scDesc.SampleDesc.Count = 1;
	scDesc.SampleDesc.Quality = 0;
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scDesc.BufferCount = SCREEN_BUFFER_COUNT;
	scDesc.Scaling = DXGI_SCALING_STRETCH;
	scDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	scDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	scDesc.Flags = 0;
	result = _dxgiFactory->CreateSwapChainForHwnd(_cmdQueue, _hwnd, &scDesc, nullptr, NULL, (IDXGISwapChain1**)&_swapChain);
	assert(SUCCEEDED(result));

}

void DirectX12Device::CreateRenderTarget()
{
	HRESULT result;
	//デスクリプター
	D3D12_DESCRIPTOR_HEAP_DESC dhDesc;
	dhDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	dhDesc.NumDescriptors = SCREEN_BUFFER_COUNT;
	dhDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dhDesc.NodeMask = 0;
	result = _device->CreateDescriptorHeap(&dhDesc, IID_PPV_ARGS(&_renderTargetDescriptorHeap));
	assert(SUCCEEDED(result));
	descriptorSize = _device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	CD3DX12_CPU_DESCRIPTOR_HANDLE cDescripterHandle(_renderTargetDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
	//レンダーターゲット
	renderTargets.resize(SCREEN_BUFFER_COUNT);
	for (int i = 0; i < SCREEN_BUFFER_COUNT; i++)
	{
		result = _swapChain->GetBuffer(i, IID_PPV_ARGS(&renderTargets[i]));
		assert(SUCCEEDED(result));
		_device->CreateRenderTargetView(renderTargets[i], nullptr, cDescripterHandle);
		cDescripterHandle.Offset(descriptorSize);
	}
	cDescripterHandle.InitOffsetted(_renderTargetDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), descriptorSize);

	return;
}

void DirectX12Device::CreateRootSignature()
{
	HRESULT result;
	//サンプラー
	D3D12_STATIC_SAMPLER_DESC samplerDescs[2] = { {},{} };
	samplerDescs[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
//	samplerDescs[0].Filter = D3D12_FILTER_MIN_MAG_POINT_MIP_LINEAR;
	
	samplerDescs[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDescs[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDescs[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDescs[0].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDescs[0].MinLOD = 0.0f;
	samplerDescs[0].MipLODBias = 0.0f;
	samplerDescs[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	samplerDescs[0].ShaderRegister = 0;											//レジスタ番号
	samplerDescs[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	samplerDescs[0].RegisterSpace = 0;
	samplerDescs[0].MaxAnisotropy = 0;
	samplerDescs[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;

	samplerDescs[1].Filter = D3D12_FILTER_MIN_MAG_LINEAR_MIP_POINT;
	samplerDescs[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDescs[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDescs[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDescs[1].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDescs[1].MinLOD = 0.0f;
	samplerDescs[1].MipLODBias = 0.0f;
	samplerDescs[1].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	samplerDescs[1].ShaderRegister = 1;
	samplerDescs[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	samplerDescs[1].RegisterSpace = 0;
	samplerDescs[1].MaxAnisotropy = 0;
	samplerDescs[1].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
/*
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = NONE;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
*/
	//ルートシグネチャ

	ID3DBlob* _signature = nullptr;
	ID3DBlob* _error = nullptr;

	D3D12_DESCRIPTOR_RANGE descriptorRanges[VMAX] = {};

	descriptorRanges[SRV1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[SRV1].NumDescriptors = 1;
	descriptorRanges[SRV1].BaseShaderRegister = 0;
	descriptorRanges[SRV1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[SRV2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[SRV2].NumDescriptors = 1;
	descriptorRanges[SRV2].BaseShaderRegister = 1;
	descriptorRanges[SRV2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[SRV3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[SRV3].NumDescriptors = 1;
	descriptorRanges[SRV3].BaseShaderRegister = 2;
	descriptorRanges[SRV3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[SRV4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[SRV4].NumDescriptors = 1;
	descriptorRanges[SRV4].BaseShaderRegister = 3;
	descriptorRanges[SRV4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[CBV1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[CBV1].NumDescriptors = 1;
	descriptorRanges[CBV1].BaseShaderRegister = 0;
	descriptorRanges[CBV1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[CBV2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[CBV2].NumDescriptors = 1;
	descriptorRanges[CBV2].BaseShaderRegister = 1;
	descriptorRanges[CBV2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[CBV3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[CBV3].NumDescriptors = 1;
	descriptorRanges[CBV3].BaseShaderRegister = 2;
	descriptorRanges[CBV3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[CBV4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[CBV4].NumDescriptors = 1;
	descriptorRanges[CBV4].BaseShaderRegister = 3;
	descriptorRanges[CBV4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[CBV5].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[CBV5].NumDescriptors = 1;
	descriptorRanges[CBV5].BaseShaderRegister = 4;
	descriptorRanges[CBV5].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	//水面
	descriptorRanges[ViewBoard].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[ViewBoard].NumDescriptors = 1;
	descriptorRanges[ViewBoard].BaseShaderRegister = 4;//レジスタ番号4
	descriptorRanges[ViewBoard].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	//ShadowMap
	descriptorRanges[ShadowMap].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[ShadowMap].NumDescriptors = 1;
	descriptorRanges[ShadowMap].BaseShaderRegister = 5;
	descriptorRanges[ShadowMap].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	D3D12_ROOT_PARAMETER rootParam[VMAX] = {};
	rootParam[SRV1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[SRV1].DescriptorTable.pDescriptorRanges = &descriptorRanges[SRV1];
	rootParam[SRV1].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[SRV1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParam[SRV2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[SRV2].DescriptorTable.pDescriptorRanges = &descriptorRanges[SRV2];
	rootParam[SRV2].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[SRV2].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParam[SRV3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[SRV3].DescriptorTable.pDescriptorRanges = &descriptorRanges[SRV3];
	rootParam[SRV3].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[SRV3].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParam[SRV4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[SRV4].DescriptorTable.pDescriptorRanges = &descriptorRanges[SRV4];
	rootParam[SRV4].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[SRV4].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParam[CBV1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[CBV1].DescriptorTable.pDescriptorRanges = &descriptorRanges[CBV1];
	rootParam[CBV1].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[CBV1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParam[CBV2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[CBV2].DescriptorTable.pDescriptorRanges = &descriptorRanges[CBV2];
	rootParam[CBV2].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[CBV2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParam[CBV3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[CBV3].DescriptorTable.pDescriptorRanges = &descriptorRanges[CBV3];
	rootParam[CBV3].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[CBV3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParam[CBV4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[CBV4].DescriptorTable.pDescriptorRanges = &descriptorRanges[CBV4];
	rootParam[CBV4].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[CBV4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;


	rootParam[CBV5].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[CBV5].DescriptorTable.pDescriptorRanges = &descriptorRanges[CBV5];
	rootParam[CBV5].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[CBV5].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	//水面
	rootParam[ViewBoard].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[ViewBoard].DescriptorTable.pDescriptorRanges = &descriptorRanges[ViewBoard];
	rootParam[ViewBoard].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[ViewBoard].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	//ShadowMap
	rootParam[ShadowMap].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[ShadowMap].DescriptorTable.pDescriptorRanges = &descriptorRanges[ShadowMap];
	rootParam[ShadowMap].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[ShadowMap].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	D3D12_ROOT_SIGNATURE_DESC rsDesc = {};
	rsDesc.pParameters = &rootParam[0];
	rsDesc.NumParameters = VMAX;
	rsDesc.pStaticSamplers = &samplerDescs[0];
	rsDesc.NumStaticSamplers = 2;
	rsDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	result = D3D12SerializeRootSignature(&rsDesc, D3D_ROOT_SIGNATURE_VERSION_1, &_signature, &_error);
	assert(SUCCEEDED(result));
	result = _device->CreateRootSignature(
		0,
		_signature->GetBufferPointer(),
		_signature->GetBufferSize(),
		IID_PPV_ARGS(&_rootSignature));
	assert(SUCCEEDED(result));
}

//shadowmap用textureの作成
void DirectX12Device::CreateShadowMapTexture()
{
	//heappropertiesの作成
	D3D12_HEAP_PROPERTIES heapproperties = {};
	heapproperties.Type = D3D12_HEAP_TYPE_DEFAULT;
	heapproperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapproperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapproperties.CreationNodeMask = 0;
	heapproperties.VisibleNodeMask = 0;

	//resourcedescの作成
	CD3DX12_RESOURCE_DESC shadowTexDesc(
		D3D12_RESOURCE_DIMENSION_TEXTURE2D,
		0,
		(UINT)WINDOW_WID,
		(UINT)WINDOW_HEI,
		1,
		0,
		DXGI_FORMAT_R32_TYPELESS,
		1,
		0,
		D3D12_TEXTURE_LAYOUT_UNKNOWN,
		D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL
	);
	D3D12_CLEAR_VALUE clearValue;		//リソースの作成時に必要なクリア値をランタイムに伝えます。
	clearValue.Format = DXGI_FORMAT_D32_FLOAT;
	clearValue.DepthStencil.Depth = 1.0f;
	clearValue.DepthStencil.Stencil = 0;

	//テクスチャ作成
	ThrowIfFailed(_device->CreateCommittedResource(
		&heapproperties,
		D3D12_HEAP_FLAG_NONE,
		&shadowTexDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&clearValue,
		IID_PPV_ARGS(&_shadowTexture)));

	//descriptorheapdescの作成(深度バッファ)
	D3D12_DESCRIPTOR_HEAP_DESC shadowMapHeapDesc = {};
	shadowMapHeapDesc.NumDescriptors = 1;
	shadowMapHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	shadowMapHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	shadowMapHeapDesc.NodeMask = 0;
	ThrowIfFailed(_device->CreateDescriptorHeap(&shadowMapHeapDesc, IID_PPV_ARGS(&_shadowDSVHeap)));

	//descriptorheapdescの作成(シェーダーリソース)
	D3D12_DESCRIPTOR_HEAP_DESC shadowSRVDesc = {};
	shadowSRVDesc.NumDescriptors = 1;
	shadowSRVDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	shadowSRVDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(_device->CreateDescriptorHeap(&shadowSRVDesc, IID_PPV_ARGS(&_shadowSRVHeap)));

	//デプスステンシルビューと関連付け
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.Texture2D.MipSlice = 0;
	_device->CreateDepthStencilView(_shadowTexture.Get(), &dsvDesc, _shadowDSVHeap->GetCPUDescriptorHandleForHeapStart());
	//シェーダーリソースビューと関連付け
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.PlaneSlice = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	_device->CreateShaderResourceView(_shadowTexture.Get(), &srvDesc, _shadowSRVHeap->GetCPUDescriptorHandleForHeapStart());
}

bool DirectX12Device::CreateResource()
{
	return true;
}

bool DirectX12Device::CreateDepthBuf()
{
	D3D12_RESOURCE_DESC depResDesc = {};
	depResDesc.Width = WINDOW_WID;
	depResDesc.Height = WINDOW_HEI;
	depResDesc.DepthOrArraySize = 1;
	depResDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depResDesc.SampleDesc.Count = 1;
	depResDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
	depResDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	D3D12_HEAP_PROPERTIES depHeapProp = {};
	depHeapProp.Type = D3D12_HEAP_TYPE_DEFAULT;
	depHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	depHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	D3D12_CLEAR_VALUE depClearVal = {};
	depClearVal.DepthStencil.Depth = 1.0f;
	depClearVal.DepthStencil.Stencil = 0;
	depClearVal.Format = DXGI_FORMAT_D32_FLOAT;

	HRESULT result = _device->CreateCommittedResource(
		//&CD3DX12_HEAP_PROPERTIES(D3D12_CPU_PAGE_PROPERTY_UNKNOWN, D3D12_MEMORY_POOL_UNKNOWN),
		&depHeapProp,
		D3D12_HEAP_FLAG_NONE,
		&depResDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&depClearVal,
		IID_PPV_ARGS(&_depthBuf));
	D3D12_DESCRIPTOR_HEAP_DESC depHeapDesc = {};
	depHeapDesc.NumDescriptors = 1;
	depHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	depHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	depHeapDesc.NodeMask = 0;
	result = _device->CreateDescriptorHeap(&depHeapDesc,IID_PPV_ARGS(&_depHeap));
	assert(SUCCEEDED(result));
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.Texture2D.MipSlice = 0;
	_device->CreateDepthStencilView(_depthBuf,&dsvDesc,_depHeap->GetCPUDescriptorHandleForHeapStart());
	return true;
}
