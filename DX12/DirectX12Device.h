#pragma once
#include "d3dx12.h"
#include <dxgi1_4.h>
#include <vector>
#include <windows.h>

#include <string>
#include <wrl.h>
#include <process.h>
#include <shellapi.h>

using Microsoft::WRL::ComPtr;
using namespace Microsoft::WRL;

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw std::exception();
	}
}

enum ViewNum {
	SRV1,
	SRV2,
	SRV3,
	SRV4,
	CBV1,
	CBV2,
	CBV3,
	CBV4,
	CBV5,
	ViewBoard,//床
	ShadowMap,
	VMAX
};

class DirectX12Device
{
private:
	DirectX12Device();
	DirectX12Device(const DirectX12Device&);
	DirectX12Device& operator=(const DirectX12Device&) {};
	ID3D12Device* _device = nullptr;
	D3D_FEATURE_LEVEL runLV;
	ID3D12CommandAllocator* _cmdAlloc = nullptr;
	ID3D12GraphicsCommandList* _cmdList = nullptr;
	UINT64 fenceValue = 0;
	ID3D12Fence* _fence = nullptr;
	ID3D12CommandQueue* _cmdQueue = nullptr;

	IDXGIFactory4* _dxgiFactory = nullptr;
	IDXGISwapChain3* _swapChain = nullptr;
	ID3D12DescriptorHeap* _renderTargetDescriptorHeap;
	std::vector<ID3D12Resource*>renderTargets;
	ID3D12RootSignature* _rootSignature = nullptr;
	ID3D12Resource* _depthBuf=nullptr;
	ID3D12DescriptorHeap* _depHeap = nullptr;
	D3D12_GPU_DESCRIPTOR_HANDLE _nullSrvHandle;	//範囲外の動作の場合のNull SRV。

	//shadowmap用
	ID3D12DescriptorHeap* _shadowDSVHeap = nullptr;
	ID3D12DescriptorHeap* _shadowSRVHeap = nullptr;

	ComPtr<ID3D12Resource> _shadowTexture;
	D3D12_CPU_DESCRIPTOR_HANDLE _shadowDepthView;
	D3D12_GPU_DESCRIPTOR_HANDLE _shadowDepthHandle;	//shadowmapのハンドル


	UINT descriptorSize;
	HWND _hwnd;
	void InitSwapchain();
	void CreateRenderTarget();
	void CreateRootSignature();
public:
	void CreateShadowMapTexture();	//shadowmap用textureの作成
	~DirectX12Device();
	static DirectX12Device& Instance()
	{
		static DirectX12Device instance;
		return instance;
	}

	void init(HWND hwnd);
	ID3D12Device*& Device() { return _device; }
	ID3D12CommandAllocator*& ComAlloc() { return _cmdAlloc; }
	ID3D12GraphicsCommandList*& CmdList() { return _cmdList; }
	ID3D12Fence*& Fence() { return _fence; }
	ID3D12CommandQueue*& CmdQueue() { return _cmdQueue; }
	IDXGIFactory4*& DxgiFactory() { return _dxgiFactory; }
	IDXGISwapChain3*& SwapChain() { return _swapChain; }
	ID3D12DescriptorHeap*& RenderTargetDescriptorHeap() { return _renderTargetDescriptorHeap; };
	std::vector<ID3D12Resource*>& RenderTargets() { return renderTargets; };
	ID3D12RootSignature*& RootSignature() { return _rootSignature; };

	ID3D12Resource*& DepthBuf(){ return _depthBuf; }
	ID3D12DescriptorHeap*& DepHeap() { return _depHeap; }

	//shadowmap用/////////////////////////

	//shadowmap用Texture
	ComPtr<ID3D12Resource>& ShadowTex() { return  _shadowTexture; };
	//shadowMap用デプスステンシルビュー
	ID3D12DescriptorHeap*& ShadowDSVHeap() { return _shadowDSVHeap; }
	//shadowMap用シェーダーリソースビュー
	ID3D12DescriptorHeap*& ShadowSRVHeap() { return _shadowSRVHeap; }

	//~shadowmap用/////////////////////////

//	CD3DX12_CPU_DESCRIPTOR_HANDLE r;
	CD3DX12_CPU_DESCRIPTOR_HANDLE RTVHandle() {
		int backBufID = _swapChain->GetCurrentBackBufferIndex();
		CD3DX12_CPU_DESCRIPTOR_HANDLE r(_renderTargetDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
			backBufID,
			descriptorSize);
		return r;
	}

	bool execute(int count,ID3D12CommandList* cmdLists[]) {
		_cmdQueue->ExecuteCommandLists(count, cmdLists);

		return fence();
	}
	bool fence()
	{
		++fenceValue;
		_cmdQueue->Signal(_fence, fenceValue);
		while (_fence->GetCompletedValue() != fenceValue) {}
		return true;
	}
	bool CreateDepthBuf();
	bool CreateResource();
};

