#include "common.h"

#include <Windows.h>
#include <tchar.h>
#include <memory>
#include "DirectX12Device.h"
#include"WICTextureLoader12.h"

	bool BMPLoad(const char* path, BitmapData& pData)
	{
		FILE* fp;

		fopen_s(&fp, path, "r");
		//	CreateFile(_T("texturesample.bmp"),GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,NULL);
		fread(&pData._header, sizeof(BITMAPFILEHEADER), 1, fp);
		fread(&pData._infoHeader, sizeof(BITMAPINFOHEADER), 1, fp);
		int headerSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
		fseek(fp, pData._header.bfOffBits, SEEK_SET);
		pData._data.resize(pData._infoHeader.biWidth*pData._infoHeader.biHeight * 4);

		if (pData._infoHeader.biCompression == 3 || pData._infoHeader.biCompression == 0)
		{
			//fread(&pData._data[0], pData._infoHeader.biSizeImage, 1, fp);
			fread(&pData._data[0], pData._header.bfSize - pData._header.bfOffBits, 1, fp);
			;
			if (pData._infoHeader.biBitCount == 24)
			{
				for (int i = pData._infoHeader.biHeight - 1; i >= 0; i--)
				{
					for (int j = pData._infoHeader.biWidth - 1; j >= 0; j--)
					{
						int idx = (i*pData._infoHeader.biWidth + j);
						pData._data[idx * 4 + 3] = pData._data[idx * 3 + 2];
						pData._data[idx * 4 + 2] = pData._data[idx * 3 + 1];
						pData._data[idx * 4 + 1] = pData._data[idx * 3];
						pData._data[idx * 4] = 0;

					}
				}
			}
			for (int i = 0; i < pData._infoHeader.biHeight / 2; i++)
			{
				char c;
				for (int j = 0; j < pData._infoHeader.biWidth; j++)
				{
					int idx = i*pData._infoHeader.biWidth * 4 + j * 4;
					int idx2 = (pData._infoHeader.biHeight - i - 1)*pData._infoHeader.biWidth * 4 + j * 4;
					for (int k = 0; k < 4; k++)
					{
						c = pData._data[idx + k];
						pData._data[idx + k] = pData._data[idx2 + (3 - k)];
						pData._data[idx2 + (3 - k)] = c;
					}
				}
			}
		}
		return true;
	}

	bool LoadWICTex(ID3D12Resource*& tex, D3D12_SUBRESOURCE_DATA& sub, const wchar_t* path,std::unique_ptr<uint8_t[]>& dec)
	{
		DirectX12Device& dev = DirectX12Device::Instance();
		//	assert(SUCCEEDED(DirectX::LoadWICTextureFromFile(dev.Device(), _T("texturesample.png"), &tex, dec, sub)));
		HRESULT hr = DirectX::LoadWICTextureFromFile(dev.Device(), path, &tex, dec, sub);
		
		assert(SUCCEEDED(hr));

		return true;
	}
//static HRESULT LoadWICFile(ID3D12Device* dev, ID3D12GraphicsCommandList* cmd, const wchar_t* szFileName, ID3D12Resource** texture, D3D12_SHADER_RESOURCE_VIEW_DESC* view, ID3D12Resource** upload);
