#pragma once
#include <Windows.h>
#include <vector>
#include<DirectXMath.h>
#include<d3d12.h>
#include <memory>
#include <tchar.h>
#include <string>
const int WINDOW_WID = 640;
const int WINDOW_HEI = 480;
const float WINDOW_WIDF = (float)WINDOW_WID;
const float WINDOW_HEIF = (float)WINDOW_HEI;
const int SCREEN_BUFFER_COUNT = 2;
const float PI = 3.141592653f;

#define SAFE_DELETE(x)  if(x){delete x;x=NULL;}
#define DX_SAFE_RELEASE(x)  if(x!=nullptr){x->Release();}
#define DX_RESOURCE_RELEASE(x)  if(x!=nullptr){x->Unmap(0, nullptr);x->Release();}
struct Vec2 {
	float x=0;
	float y=0;
};
struct Vec3 {
	float x=0;
	float y=0;
	float z=0;
};

struct BitmapData {
	BITMAPFILEHEADER _header;
	BITMAPINFOHEADER _infoHeader;
	std::vector<char> _data;
};
struct CBdata {
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT3 specular;
	float specularity;
	DirectX::XMFLOAT3 ambient;
	float edgeSize;
	DirectX::XMFLOAT4 edgeColor;
	int flag;

};
struct CBCommonData {
	DirectX::XMMATRIX world;
	DirectX::XMMATRIX camera;
	DirectX::XMFLOAT3 eye;
	DirectX::XMFLOAT3 lightPos;
	DirectX::XMMATRIX lvp;

};
bool BMPLoad(const char* path, BitmapData& pData);
bool LoadWICTex(ID3D12Resource*& tex, D3D12_SUBRESOURCE_DATA& sub, const wchar_t* path, std::unique_ptr<uint8_t[]>& dec);
