#pragma once
#include <dsound.h>
#include <stdio.h>
#include <tchar.h>

class DirectSound
{
private:
	LPDIRECTSOUND8		lpDS = nullptr;			//DirectSound8
	LPDIRECTSOUNDBUFFER lpPrimary = nullptr;	//プライマリサウンドバッファー
	LPDIRECTSOUNDBUFFER lpSecondary = nullptr;	//ロードしたプライマリを格納するバッファー

public:
	DirectSound();
	~DirectSound();

	//Directsound初期化
	///win・・・現在使用しているウィンドウクラス
	bool Init(HWND win);

	//プライマリサウンドバッファの作成
	bool CreatePrimaryBuffer();

	//サウンドバッファの作成
	///*dsb・・・サウンドを管理するバッファーのメモリ場所
	///*file ・・・　読み込みたいサウンドが入ってるファイルパス
	bool CreateSoundBuffer(LPDIRECTSOUNDBUFFER *dsb, const char *file);

	//サウンドのメモリ開放
	bool Exit();

	//サウンドの読み込み
	///*file ・・・　読み込みたいサウンドが入ってるファイルパス
	bool LoadSound(const char* file);

	//格納されているサウンドバッファーを呼び出す
	LPDIRECTSOUNDBUFFER GetlpSecondary() { return lpSecondary; }
};

