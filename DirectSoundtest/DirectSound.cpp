#include "DirectSound.h"

#pragma comment(lib,"dsound.lib")
#pragma comment(lib, "winmm.lib")

DirectSound::DirectSound()
{
}


DirectSound::~DirectSound()
{
}

bool
DirectSound::Init(HWND win)
{
	HRESULT ret;

	//COMの初期化
	CoInitialize(nullptr);

	//ダイレクトサウンドの生成
	ret = DirectSoundCreate8(nullptr, &lpDS, nullptr);
	if (FAILED(ret))
	{
		return false;
	}

	ret = lpDS->SetCooperativeLevel(win, DSSCL_EXCLUSIVE | DSSCL_PRIORITY);
	if (FAILED(ret)) {
		return false;
	}

	return true;

}

bool  
DirectSound::CreatePrimaryBuffer()
{
	HRESULT ret;
	WAVEFORMATEX wf;

	// プライマリサウンドバッファの作成
	DSBUFFERDESC dsdesc;
	memset(&dsdesc,0,sizeof(DSBUFFERDESC));
	dsdesc.dwSize = sizeof(DSBUFFERDESC);
	dsdesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
	dsdesc.dwBufferBytes = 0;
	dsdesc.lpwfxFormat = nullptr;
	ret = lpDS->CreateSoundBuffer(&dsdesc, &lpPrimary, nullptr);
	if (FAILED(ret)) {
		return false;
	}

	// プライマリバッファのステータスを決定(基本的にCD規格で)
	wf.cbSize = sizeof(WAVEFORMATEX);
	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = 2;											
	wf.nSamplesPerSec = 44100;									
	wf.wBitsPerSample = 16;										
	wf.nBlockAlign = wf.nChannels * wf.wBitsPerSample / 8;		//8・・・CPUが低くても音が鳴るようにそのbitにダウンサンプリングさせる
	wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;
	ret = lpPrimary->SetFormat(&wf);
	if (FAILED(ret)) {
		return false;
	}

	return true;
}

bool
DirectSound::CreateSoundBuffer(LPDIRECTSOUNDBUFFER *dsb, const char *file)
{
	HRESULT ret;
	MMCKINFO mSrcWaveFile;
	MMCKINFO mSrcWaveFmt;
	MMCKINFO mSrcWaveData;
	LPWAVEFORMATEX wf;

	// WAVファイルをロード
	HMMIO hSrc;
	hSrc = mmioOpenA((LPSTR)file, nullptr, MMIO_ALLOCBUF | MMIO_READ | MMIO_COMPAT);
	if (!hSrc) {
		printf("WAVファイルロードエラー\n");
		return false;
	}

	// 'WAVE'チャンクチェック
	ZeroMemory(&mSrcWaveFile, sizeof(mSrcWaveFile));
	ret = mmioDescend(hSrc, &mSrcWaveFile, nullptr, MMIO_FINDRIFF);
	if (mSrcWaveFile.fccType != mmioFOURCC('W', 'A', 'V', 'E')) {
		printf("WAVEチャンクチェックエラー\n");
		mmioClose(hSrc, 0);
		return false;
	}

	// 'fmt 'チャンクチェック
	ZeroMemory(&mSrcWaveFmt, sizeof(mSrcWaveFmt));
	ret = mmioDescend(hSrc, &mSrcWaveFmt, &mSrcWaveFile, MMIO_FINDCHUNK);
	if (mSrcWaveFmt.ckid != mmioFOURCC('f', 'm', 't', ' ')) {
		printf("fmt チャンクチェックエラー\n");
		mmioClose(hSrc, 0);
		return false;
	}

	// ヘッダサイズの計算
	int iSrcHeaderSize = mSrcWaveFmt.cksize;
	if (iSrcHeaderSize<sizeof(WAVEFORMATEX))
		iSrcHeaderSize = sizeof(WAVEFORMATEX);

	// ヘッダメモリ確保
	wf = (LPWAVEFORMATEX)malloc(iSrcHeaderSize);
	if (!wf) {
		printf("メモリ確保エラー\n");
		mmioClose(hSrc, 0);
		return false;
	}
	ZeroMemory(wf, iSrcHeaderSize);

	// WAVEフォーマットのロード
	ret = mmioRead(hSrc, (char*)wf, mSrcWaveFmt.cksize);
	if (FAILED(ret)) {
		printf("WAVEフォーマットロードエラー\n");
		free(wf);
		mmioClose(hSrc, 0);
		return false;
	}
	printf("チャンネル数       = %d\n", wf->nChannels);
	printf("サンプリングレート = %d\n", wf->nSamplesPerSec);
	printf("ビットレート       = %d\n", wf->wBitsPerSample);


	// fmtチャンクに戻る
	mmioAscend(hSrc, &mSrcWaveFmt, 0);

	// dataチャンクを探す
	while (true) {
		// 検索
		ret = mmioDescend(hSrc, &mSrcWaveData, &mSrcWaveFile, 0);
		if (FAILED(ret)) {
			printf("dataチャンクが見つからない\n");
			free(wf);
			mmioClose(hSrc, 0);
			return false;
		}
		if (mSrcWaveData.ckid == mmioStringToFOURCCA("data", 0))
			break;
		// 次のチャンクへ
		ret = mmioAscend(hSrc, &mSrcWaveData, 0);
	}
	printf("データサイズ       = %d\n", mSrcWaveData.cksize);


	// サウンドバッファの作成
	DSBUFFERDESC dsdesc;
	ZeroMemory(&dsdesc, sizeof(DSBUFFERDESC));
	dsdesc.dwSize = sizeof(DSBUFFERDESC);
	dsdesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_STATIC | DSBCAPS_LOCDEFER;
	dsdesc.dwBufferBytes = mSrcWaveData.cksize;
	dsdesc.lpwfxFormat = wf;
	dsdesc.guid3DAlgorithm = DS3DALG_DEFAULT;
	ret = lpDS->CreateSoundBuffer(&dsdesc, dsb, nullptr);
	if (FAILED(ret)) {
		printf("サウンドバッファの作成エラー\n");
		free(wf);
		mmioClose(hSrc, 0);
		return false;
	}

	// ロック開始
	LPVOID pMem1, pMem2;
	DWORD dwSize1, dwSize2;
	ret = (*dsb)->Lock(0, mSrcWaveData.cksize, &pMem1, &dwSize1, &pMem2, &dwSize2, 0);
	if (FAILED(ret)) {
		printf("ロック失敗\n");
		free(wf);
		mmioClose(hSrc, 0);
		return false;
	}

	// データ書き込み
	mmioRead(hSrc, (char*)pMem1, dwSize1);
	mmioRead(hSrc, (char*)pMem2, dwSize2);

	// ロック解除
	(*dsb)->Unlock(pMem1, dwSize1, pMem2, dwSize2);

	// ヘッダ用メモリを開放
	free(wf);

	// WAVを閉じる
	mmioClose(hSrc, 0);

	return true;
}

bool
DirectSound::Exit(void)
{
	if (lpSecondary) {
		lpSecondary->Release();
		lpSecondary = nullptr;
	}

	if (lpPrimary) {
		lpPrimary->Release();
		lpPrimary = nullptr;
	}

	if (lpDS) {
		lpDS->Release();
		lpDS = nullptr;
	}

	//COMの終了
	CoUninitialize();

	return true;
}

bool 
DirectSound::LoadSound(const char* file) {

	if (!CreateSoundBuffer(&lpSecondary, file)) {
		Exit();
		return false;
	}

}