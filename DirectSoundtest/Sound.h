#pragma once
#include "DirectSound.h"

#define MUSIC_MAX 50 //総曲数

enum SENo {
	None,
	Title,		//タイトル画面
	Main,		//メニュー画面
	Plow,		//耕し
	Walk,		//歩き
	Run,		//走り
	Fish,		//釣り
	Watering,	//水まき
	Favor,		//好感度
};

class Sound :
	public DirectSound
{
private:
	DirectSound dsound[MUSIC_MAX];
public:
	Sound();
	~Sound();

	//サウンドの再生
	void Update();

	//指定されたサウンドの再生
	///No・・・enumに対応する曲の指定番号
	void selectUpdate(SENo No);

	//サウンド初期化
	///win・・・現在使用しているウィンドウクラス
	bool StartUp(HWND win);

};

