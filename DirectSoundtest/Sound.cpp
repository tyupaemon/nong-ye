#include "Sound.h"

Sound::Sound()
{
}


Sound::~Sound()
{
}

bool
Sound::StartUp(HWND win)
{

	for (int i = 0; i < MUSIC_MAX; i++)
	{
		if (!dsound[i].Init(win)) {
			dsound[i].Exit();
			return false;
		}

		//プライマリサウンドバッファ
		if (!dsound[i].CreatePrimaryBuffer()) {
			dsound[i].Exit();
			return false;
		}
	}

	//曲、SE
	dsound[Title].LoadSound("../Sound/BGM/Title.wav");		//タイトル
	dsound[Main].LoadSound("../Sound/BGM/15_years_ago.wav");//メインBGM
	dsound[Plow].LoadSound("../Sound/SE/Plow.wav");			//耕し
	dsound[Walk].LoadSound("../Sound/SE/walk.wav");			//歩き
	dsound[Run].LoadSound("../Sound/SE/Run.wav");			//走り
	dsound[Fish].LoadSound("../Sound/SE/Fishing.wav");		//釣り
	dsound[Watering].LoadSound("../Sound/SE/watering.wav");	//水まき
	dsound[Favor].LoadSound("../Sound/SE/Favor.wav");		//好感度

	return true;

}

void
Sound::Update()
{
	dsound[Title].GetlpSecondary()->Play(0, 0, DSBPLAY_LOOPING);
}

void
Sound::selectUpdate(SENo No)
{
	dsound[No].GetlpSecondary()->Play(0, 0, 0);
}
