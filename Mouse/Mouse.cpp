#include "Mouse.h"

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

Mouse::Mouse()
{
}


Mouse::~Mouse()
{
	ReleaseDInput();
	ReleaseDInpurMouse();
}

bool Mouse::Init(HINSTANCE hInstApp, HWND & hwnd)
{
	_hwnd = hwnd;

	if (!InitDInput(hInstApp, hwnd))
	{
		return false;
	}
	if (!InitDInputMouse(hwnd))
	{
		return false;
	}

	return true;
}

bool Mouse::InitDInput(HINSTANCE hInstApp, HWND & hwnd)
{
	HRESULT res = DirectInput8Create(hInstApp, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&_pDInput, nullptr);
	if (FAILED(res))
	{
		return false;//作成失敗
	}
	return true;
}

bool Mouse::ReleaseDInput()
{
	//DirectInputのデバイスを開放
	if (_pDInput)
	{
		_pDInput->Release();
		_pDInput = nullptr;
	}
	return true;
}

bool Mouse::InitDInputMouse(HWND hwnd)
{
	HRESULT res = S_FALSE;
	if (_pDInput == nullptr)
	{
		return false;
	}

	//マウス用にデバイスオブジェクトを作成
	res = _pDInput->CreateDevice(GUID_SysMouse, &_pDIMouse, nullptr);
	if (FAILED(res))
	{
		//デバイスの作成失敗
		return false;
	}

	//データフォーマットを設定
	res = _pDIMouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(res))
	{
		//データフォーマットに失敗
		return false;
	}

	//モードを設定（バックグラウンド＆非排他モード）
	res = _pDIMouse->SetCooperativeLevel(hwnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if (FAILED(res))
	{
		//モードの設定に失敗
		return false;
	}

	//デバイスの設定
	DIPROPDWORD diprop;
	diprop.diph.dwSize = sizeof(diprop);
	diprop.diph.dwHeaderSize = sizeof(diprop.diph);
	diprop.diph.dwObj = 0;
	diprop.diph.dwHow = DIPH_DEVICE;
	diprop.dwData = DIPROPAXISMODE_REL; //相対値モードで設定（絶対値はDIPROPAXISMODE_ABS）

	res = _pDIMouse->SetProperty(DIPROP_AXISMODE,&diprop.diph);
	if (FAILED(res))
	{
		//デバイスの設定に失敗
		return false;
	}

	//入力制御開始
	_pDIMouse->Acquire();

	return true;
}

bool Mouse::ReleaseDInpurMouse()
{
	//DirectInputのデバイスを開放
	if (_pDIMouse)
	{
		_pDIMouse->Release();
		_pDIMouse = nullptr;
	}
	return true;
}

void Mouse::GetMouseState(HWND hwnd)
{
	if (_pDIMouse == nullptr)
	{
		//オブジェクト生成前に呼ばれたときはここで生成させる
		InitDInputMouse(_hwnd);
	}

	//読み取り前の値を保持します
	_DIMouseState_back = _DIMouseState;

	HRESULT hr = _pDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &_DIMouseState);
	if (hr == DIERR_INPUTLOST)
	{
		_pDIMouse->Acquire();
		hr = _pDIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &_DIMouseState);
	}
}

void Mouse::Update()
{
	//非フォーカス時は更新を無効化
	if (GetFocus() != _hwnd) { return; }
	//マウス状態の更新
	GetMouseState(_hwnd);

	//座標の取得
	GetCursorPos(&_point);//スクリーン座標
	ScreenToClient(_hwnd, &_point);//クライアント座標

	//xyz座標に変換
	_pos.x = _point.x;
	_pos.y = _point.y;
	return;
}

bool Mouse::IsClick_R()
{

	if (!_DIMouseState_back.rgbButtons[1] && _DIMouseState.rgbButtons[1])
	{
		return true;
	}

	return false;
}

bool Mouse::IsClick_L()
{

	if (!_DIMouseState_back.rgbButtons[0] && _DIMouseState.rgbButtons[0])
	{
		return true;
	}

	return false;
}
