#pragma once
#include<DirectXMath.h>
#include <Windows.h>
#include <dinput.h>

using namespace DirectX;

class Mouse
{
private:

	HWND _hwnd;//ウィンドウクラス
	POINT _point;
	XMFLOAT3 _pos;//xyz座標

	LPDIRECTINPUT8 _pDInput = nullptr;//DirrectInputオブジェクト

	LPDIRECTINPUTDEVICE8 _pDIMouse = nullptr;//マウスデバイス
	DIMOUSESTATE _DIMouseState; //マウス状態
	DIMOUSESTATE _DIMouseState_back;//マウス状態１F前

	Mouse();
	Mouse(const Mouse&);
	Mouse& operator = (const Mouse&);
public:
	static Mouse& Instance() {
		static Mouse instance;
		return instance;
	};
	~Mouse();

	//初期化処理
	bool Init(HINSTANCE hInstApp, HWND& hwnd);

	//DirectInput初期化処理
	bool InitDInput(HINSTANCE hInstApp, HWND& hwnd);

	//DirectInputの終了処理
	bool ReleaseDInput();

	//DirectInputのマウスデバイス用の初期化処理
	bool InitDInputMouse(HWND hwnd);

	//DirectInputのマウスデバイス用の終了
	bool ReleaseDInpurMouse();

	//DirectInputのマウスデバイス状態取得処理
	void GetMouseState(HWND hwnd);

	//更新
	void Update();

	//クリック取得
	bool IsClick_R();//右クリック
	bool IsClick_L();//左クリック

	//スクリーン座標取得
	POINT GetPoint() { return _point; };
	XMFLOAT3 GetPos() { return _pos; };
};

