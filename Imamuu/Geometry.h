#pragma once
#include<DirectXMath.h>


struct Vector2	//座標構造体
{
	float x = 0;
	float y = 0;
	Vector2(float inx, float iny)
	{
		x = inx;
		y = iny;
	}
	Vector2()
	{
		x = y = 0;
	}
};

struct  Rect //矩形構造体
{
	float x, y;
	float w, h;
	float Right()
	{
		return x + w/2;
	}
	float Left()
	{
		return x - w / 2;
	}
	float Top()
	{
		return y + h / 2;
	}
	float Bottom()
	{
		return y - h/2;
	}
	bool IsHit(Rect& rc)
	{
		if (Right() < rc.Right() &&
			Left() > rc.Left()&&
			Top() < rc.Top() &&
			Bottom() > rc.Bottom())
		{
			return true;
		}
		else
			return false;
	}
	bool IsHitWide(Rect& rc)
	{
		if (Right() < rc.Right() &&
			Left() > rc.Left())
			return true;
		else
			return false;
	}
	bool IsHitVertical(Rect& rc)
	{
		if (Top() < rc.Top() &&
			Bottom() > rc.Bottom())
			return true;
		else
			return false;
	}
};

class Geometry
{

public:
	Geometry();
	~Geometry();
};

