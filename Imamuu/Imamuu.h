#pragma once
#include"../SimpleObject/CreateSimpleObject.h"
#include"../SimpleObject/Board.h"
#include"../Fish/CreateFish.h"
#include"../Fish//Fish.h"
#include"../FieldObject/FieldObject.h"
#include<vector>

class imamuu
{
private:
	CreateSimpleObject _createSimpleObject;
	std::vector<SimpleObjectData*> _ground;
	std::vector<SimpleObjectData*> _water;


	CreateFish _createFish;
	std::vector<FishData*> _fish;

	FieldObject _fieldObject;


public:
	imamuu();
	~imamuu();
	void Init();
	void Update();
	
};

