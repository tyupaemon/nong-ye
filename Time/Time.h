#pragma once
#include <vector>
#include <chrono>
using namespace std::chrono;

enum class TimeType
{
	NANO_SEC,
	MICRO_SEC,
	MILLI_SEC,
	SEC,
	MIN,
	HOUR,
};

class Time
{

private:

	system_clock::time_point _start;//開始時刻
	system_clock::time_point _end;//終了時刻
public:

	Time();
	~Time();

	//計測開始　　startに時刻を挿入
	void StartCount();

	//計測終了	　endとstartの差を返す
	//返り値　時間（秒）
	long GetCount();

	//指定した型での時間を返す
	long GetCount(TimeType t);
};

