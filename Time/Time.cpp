#include "Time.h"
#include <ctime>


Time::Time()
{
	void StartCount();
}


Time::~Time()
{
}

void Time::StartCount()
{
	//スタート時刻保存
	_start = system_clock::now();
}

long Time::GetCount()
{
	//終了時刻保存
	_end = system_clock::now();

	auto dur = _end - _start;//所要時間

	auto msec = duration_cast<seconds>(dur).count();
	return (long)msec;
}

long Time::GetCount(TimeType t)
{
	//終了時刻保存
	_end = system_clock::now();

	auto dur = _end - _start;//所要時間

	long msec;
	switch (t)
	{
	case TimeType::NANO_SEC:
		msec = (long)duration_cast<nanoseconds>(dur).count();
		break;
	case TimeType::MICRO_SEC:
		msec = (long)duration_cast<microseconds>(dur).count();

		break;
	case TimeType::MILLI_SEC:
		msec = (long)duration_cast<milliseconds>(dur).count();

		break;
	case TimeType::SEC:
		msec = (long)duration_cast<seconds>(dur).count();

		break;
	case TimeType::MIN:
		msec = (long)duration_cast<minutes>(dur).count();

		break;
	case TimeType::HOUR:
		msec = (long)duration_cast<hours>(dur).count();

		break;
	default:
		msec = NULL;

		break;
	}

	return msec;
}
