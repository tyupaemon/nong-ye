#include "MouseClick.h"

XMVECTOR * CalcScreenToWorld(XMVECTOR* pout, int Sx, int Sy, float fZ, int Screen_w, int Screen_h, XMMATRIX * View, XMMATRIX * Prj)
{
	// 各行列の逆行列を算出
	XMMATRIX InvView, InvPrj, VP, InvViewport;
	InvView = XMMatrixInverse(nullptr, *View);
	InvPrj = XMMatrixInverse(nullptr, *Prj);
	VP = XMMatrixIdentity();
	VP.r[0].m128_f32[0] = Screen_w / 2.0f;
	VP.r[1].m128_f32[1] = -Screen_h / 2.0f;
	VP.r[3].m128_f32[0] = Screen_w / 2.0f;
	VP.r[3].m128_f32[1] = Screen_h / 2.0f;
	InvViewport = XMMatrixInverse(nullptr, VP);

	// 逆変換
	XMMATRIX tmp = InvViewport * InvPrj * InvView;
	XMVECTOR val = { (float)Sx, (float)Sy, fZ };
	*pout = XMVector3TransformCoord(val, tmp);

	return pout;
}

XMVECTOR * CalcScreenToXZ(XMVECTOR * pout, int Sx, int Sy, int Screen_w, int Screen_h, XMMATRIX& View, XMMATRIX& Prj)
 {
	XMVECTOR nearpos;
	XMVECTOR farpos;
	XMVECTOR ray;
	CalcScreenToWorld(&nearpos, Sx, Sy, 0.0f, Screen_w, Screen_h, &View, &Prj);
	CalcScreenToWorld(&farpos, Sx, Sy, 1.0f, Screen_w, Screen_h, &View, &Prj);
	ray = farpos - nearpos;
	ray = XMVector3Normalize(ray);
	
	// 床との交差が起きている場合は交点を
	// 起きていない場合は遠くの壁との交点を出力
	if (ray.m128_f32[1] <= 0) {
		// 床交点
		XMVECTOR up = { 0,1,0 };
		float Lray = XMVector3Dot(ray, up).m128_f32[0];
		float LP0 = XMVector3Dot((-nearpos), up).m128_f32[0];
		*pout = nearpos + (LP0 / Lray)*ray;
	}
	else {
		*pout = farpos;
	}

	return pout;
}