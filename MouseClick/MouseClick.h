#pragma once
#include<DirectXMath.h>

using namespace DirectX;

//スクリーン座標をワールド座標に変換
XMVECTOR* CalcScreenToWorld(
	XMVECTOR* pout,
	int Sx,  // スクリーンX座標
	int Sy,  // スクリーンY座標
	float fZ,  // 射影空間でのZ値（0〜1）
	int Screen_w,
	int Screen_h,
	XMMATRIX* View,
	XMMATRIX* Prj
);

// XZ平面とスクリーン座標の交点算出関数
//pout 出力先変数,sx スクリーンX座標,sy　スクリーンY座標
//Screen_w スクリーン幅,Screen_h スクリーン高さ
//View ビュー行列,Prj プロジェクション行列
XMVECTOR* CalcScreenToXZ(
	XMVECTOR* pout,
	int Sx,
	int Sy,
	int Screen_w,
	int Screen_h,
	XMMATRIX& View,
	XMMATRIX& Prj
);