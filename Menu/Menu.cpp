#include "Menu.h"

Menu::Menu()
{
	str[0].SetColor(1, 0, 0, 1);
	str[1].SetColor(0, 0, 0, 1);
	str[2].SetColor(0, 0, 0, 1);
	str[3].SetColor(0, 0, 0, 1);
}				   

Menu::~Menu()
{
}

void
Menu::Update()
{
	str[0].init(_T("メニュー画面"));
	str[0].SetCenter(WINDOW_WIDF / 2, WINDOW_HEIF / 2);
	str[1].init(_T("セーブ"));
	str[1].SetCenter(100, TMP_HIGHT / 2);
	str[2].init(_T("ロード"));
	str[2].SetCenter(300, TMP_HIGHT / 2);
	str[3].init(_T("ゲーム終了"));
	str[3].SetCenter(550, TMP_HIGHT / 2);
	for (int i = 0;i <MENU_MAX_TEXT; i++)
	{
		str[i].Draw();
	}
	
}