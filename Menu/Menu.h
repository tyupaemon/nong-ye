#pragma once

#include "../2DPolygon/DrawString.h"

#define MENU_MAX_TEXT 4
#define TMP_HIGHT 50

class Menu
{
	DrawString str[MENU_MAX_TEXT];
public:
	Menu();
	~Menu();

	void Update();
};

