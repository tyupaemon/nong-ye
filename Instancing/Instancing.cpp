#include "Instancing.h"
#include"../DX12/DirectX12Device.h"
#include"../DX12/common.h"


#include<algorithm>
//Instancing::Instancing(){}
void RGBfromHSV(float& r, float& g, float& b, float h, float s, float v);
Instancing::Instancing()
{
	/*
	std::fill(availables.begin(), availables.end(), -1);
	for (int i = 0; i < INST_MAX; i++)
	{
		availables[i] = i;
	}
	*/
	num = 0;
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };

	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(XMMATRIX)*INST_MAX + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&pcbBuf));
	assert(SUCCEEDED(result));

	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbdescHeap));
	assert(SUCCEEDED(result));

	cbViewDesc.BufferLocation = pcbBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(XMMATRIX)*INST_MAX + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, pcbdescHeap->GetCPUDescriptorHandleForHeapStart());

	D3D12_RANGE range = {};
	result = pcbBuf->Map(0, &range, (void**)&pBuf);


}

Instancing::Instancing(PMXData* _model)
{
	model = _model;
	/*
	std::fill(availables.begin(), availables.end(), -1);
	for (int i = 0; i < INST_MAX; i++)
	{
		availables[i] = i;
	}
	*/
	num = 0;
	HRESULT result;
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };

	D3D12_HEAP_PROPERTIES cbHeapProperties = {};
	D3D12_DESCRIPTOR_HEAP_DESC cbDescriptorHeapDesc = {};
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	cbDescriptorHeapDesc.NumDescriptors = 1;
	cbDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbHeapProperties.VisibleNodeMask = 1;
	cbHeapProperties.CreationNodeMask = 1;
	cbHeapProperties.Type = D3D12_HEAP_TYPE_UPLOAD;

	result = _device->CreateCommittedResource(&cbHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(XMMATRIX)*INST_MAX + 0xff)&~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&pcbBuf));
	assert(SUCCEEDED(result));

	result = _device->CreateDescriptorHeap(&cbDescriptorHeapDesc, IID_PPV_ARGS(&pcbdescHeap));
	assert(SUCCEEDED(result));

	cbViewDesc.BufferLocation = pcbBuf->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = (sizeof(XMMATRIX)*INST_MAX + 0xff)&~0xff;
	_device->CreateConstantBufferView(&cbViewDesc, pcbdescHeap->GetCPUDescriptorHandleForHeapStart());

	D3D12_RANGE range = {};
//	result = pcbBuf->Map(0, &range, (void**)&pBuf);

	result = pcbBuf->Map(0, &range, (void**)&mats[0]);
}


Instancing::~Instancing()
{
	pcbBuf->Unmap(0,nullptr);
	pcbdescHeap->Release();
	pcbBuf->Release();
//	pIDdescHeap->Release();
//	pIDBuf->Release();

}

void Instancing::Draw()
{
	DirectX12Device& dx12dev = DirectX12Device::Instance();
	ID3D12Device* _device = dx12dev.Device();
	ID3D12CommandList* _cmdLists[] = { dx12dev.CmdList() };
	dx12dev.CmdList()->SetDescriptorHeaps(1, (ID3D12DescriptorHeap* const*)&pcbdescHeap);
	dx12dev.CmdList()->SetGraphicsRootDescriptorTable(CBV3, pcbdescHeap->GetGPUDescriptorHandleForHeapStart());

	model->draw(num);
}
/*
void Instancing::Load(std::string _name)
{
	model->Load(_name);
}
*/
void Instancing::Update()
{
	memcpy(pBuf, &mats[0], sizeof(XMMATRIX)*num);
}
int Instancing::AddInstance(InstancingObject* _pObj)
{
	if (num == INST_MAX)
		return -1;

	int ret = num;
	objs[num] = _pObj;
	_pObj->SetMatPtr(&mats[num]);
	//availables[num] = INST_MAX;
	num=min(num+1,objs.size()-1);
	return ret;
}
void Instancing::DelInstance(int _id)
{
	num = max(num - 1, 0);
	objs[_id] = objs[num];
	objs[_id]->SetMatPtr(&mats[_id]);
	//matrixes.erase(_id);
	//availables[num] = _id;

}

void Instancing::SetMatrix(int _id, XMMATRIX& _matrix)
{
	mats[_id] = _matrix;
}

void RGBfromHSV(float& r, float& g, float& b, float h, float s, float v)
{
	r = v;
	g = v;
	b = v;
	if (s > 0.0f) {
		h *= 6.0f;
		int i = (int)h;
		float f = h - (float)i;
		switch (i) {
		default:
		case 0:
			g *= 1 - s * (1 - f);
			b *= 1 - s;
			break;
		case 1:
			r *= 1 - s * f;
			b *= 1 - s;
			break;
		case 2:
			r *= 1 - s;
			b *= 1 - s * (1 - f);
			break;
		case 3:
			r *= 1 - s;
			g *= 1 - s * f;
			break;
		case 4:
			r *= 1 - s * (1 - f);
			g *= 1 - s;
			break;
		case 5:
			g *= 1 - s;
			b *= 1 - s * f;
			break;
		}
	}
}