#pragma once

#include<vector>
#include<map>
#include<array>
#include<DirectXMath.h>
#include<string>
#include"InstancingObject.h"
using namespace DirectX;
const int INST_MAX = 1024;
#include "../PMX/PMXData.h"
struct ID3D12DescriptorHeap;
struct ID3D12Resource;
class InstancingManager;
struct InstanceMatrix
{
	int id;
	XMMATRIX matrix;
};
//インスタンシング描画クラス
//セットされたPMXモデルを登録されたインスタンシングオブジェクト分描画する
//
class Instancing
{
friend InstancingManager;

private:
	PMXData* model;
	ID3D12DescriptorHeap* pcbdescHeap = nullptr;
	ID3D12Resource* pcbBuf = nullptr;
	ID3D12DescriptorHeap* pIDdescHeap = nullptr;
	ID3D12Resource* pIDBuf = nullptr;
	XMMATRIX* pBuf;
	//オブジェクト削除時の並び替え用
	std::array<InstancingObject*, INST_MAX> objs;
	std::array<XMMATRIX, INST_MAX> mats;
	int num;
public:
	int AddInstance(InstancingObject* _pObj);
	void DelInstance(int _id);
	void SetMatrix(int _id, XMMATRIX& _matrix);
	void Draw();
//	void DrawID();
	//バッファデータを登録オブジェクトの座標に更新
	void Update();
	void SetModel(PMXData* _model) { model = _model; }
	Instancing();
	Instancing(PMXData* _model);
	~Instancing();
};

