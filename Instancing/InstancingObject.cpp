#include "InstancingObject.h"
#include "../Instancing/InstancingManager.h"

using namespace DirectX;
InstancingObject::InstancingObject(std::string _name) :RenderableObject(0)
{
	name = _name;
	InstancingManager::Instance().AddInstance(_name, this);
	posMat = XMMatrixTranslation(pos.m128_f32[0], pos.m128_f32[1], pos.m128_f32[2]);
	rotMat = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z);
	SetRot(rot);

	wid = InstancingManager::Instance().GetModelData(name).GetWidth();
	hei = InstancingManager::Instance().GetModelData(name).GetHeight();
}
InstancingObject::InstancingObject() :RenderableObject(0)
{
	posMat = XMMatrixTranslation(pos.m128_f32[0], pos.m128_f32[1], pos.m128_f32[2]);
	rotMat = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z);
}

InstancingObject::~InstancingObject()
{
	if (name.size() > 0)
	{
		InstancingManager::Instance().DelInstance(name, instId);
	}
}
void InstancingObject::Update()
{
//	XMFLOAT3 f = rot;
//	rot.y += 0.01f;
//	SetRot(rot);
}
void InstancingObject::SetPos(XMVECTOR _pos)
{
	pos = _pos;
	posMat = XMMatrixTranslation(pos.m128_f32[0], pos.m128_f32[1], pos.m128_f32[2]);
	*pMat = (rotMat*posMat);
//	InstancingManager::Instance().SetMatrix(name, id,
//		mat);
}
void InstancingObject::SetRot(XMFLOAT3 _rot)
{
	rot = _rot;
	rotMat = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z);
	*pMat = (rotMat*posMat);
//	InstancingManager::Instance().SetMatrix(name, id,
//		mat);
}
