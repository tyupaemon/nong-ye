#pragma once
#include"Instancing.h"
#include"../PMX/PMXData.h"
#include<map>
#include<vector>
#include<string>
#include<DirectXMath.h>
#include"../PMX/PMXMotion.h"
struct ManageInstancing
{
	PMXData model;
	std::vector<Instancing*> instancings;
};

//インスタンシング描画管理クラス
//インスタンシング用PMXデータとインスタンシングクラスをまとめて管理する

class InstancingManager
{
private:
	InstancingManager();
	InstancingManager(const InstancingManager&);
	InstancingManager& operator=(const InstancingManager&) {};

	std::map<std::string, ManageInstancing> manageInstancings;
	std::vector<std::string> instancingNames;
	PMXMotion motion;
public:
	static InstancingManager& Instance()
	{
		static InstancingManager instance;
		return instance;
	}
	int AddInstance(std::string& _name, InstancingObject* _obj);
	void DelInstance(std::string& _name,int _id);
	void SetMatrix(std::string& _name,int _id, XMMATRIX& _matrix);
	const std::vector<std::string>& GetInstancingNames() { return instancingNames; }
	PMXData& GetModelData(std::string& _name) { return manageInstancings[_name].model; };
	void Draw();
	void Update();
	~InstancingManager();

};
