#pragma once
#include"../Goto/RenderableObject.h"
#include<string>
class Produce;
class InstancingObject:public RenderableObject
{
protected:
	int instId;
	std::string name;
	XMMATRIX posMat;
	XMMATRIX rotMat;
	XMMATRIX* pMat;
	InstancingObject();
public:
	void Update();
	InstancingObject(std::string _name);
	void SetPos(XMVECTOR _pos);
	void SetRot(XMFLOAT3 _rot);
	void SetMatPtr(XMMATRIX* _pMat) { pMat = _pMat; }
	void SetInstID(int _id) { instId = _id; }
//	const XMMATRIX& GetMatrix() { return mat; }
	std::string& GetName() { return name; }
	~InstancingObject();
};

