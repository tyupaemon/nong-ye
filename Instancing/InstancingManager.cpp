#include "InstancingManager.h"

#include<codecvt>

#include "../PMX/PMXMotion.h"
#include"../DX12/DirectX12Device.h"

#include <fstream>
#include <iostream>
#include<sstream>

#include<direct.h>
#include<DirectXMath.h>
#include<math.h>
#include<codecvt>

#include"../Instancing/InstancingManager.h"
#include"../Goto/FontManager.h"
#include"../Goto/Save.h"

const std::string FILE_PATH = "Resource/";
const std::string InstConfName = "Goto/InstancingModels.csv";
const std::string InstModels[] =
{
	"�_��/tomato.pmx",
	"�_��/barrel1.pmx",
	"�_��/bench1.pmx",
	"�_��/house1.pmx",
	"�_��/house2.pmx",
	"�_��/water_tower.pmx",
	"�_��/well.pmx"
};
InstancingManager::InstancingManager()
{
	/*
	WIN32_FIND_DATA FindFileData;
	HANDLE  hFind = FindFirstFile(_TEXT("Resource\\�_��\\*.pmx"), &FindFileData);
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			std::string name = "�_��/";
			name += converter.to_bytes(FindFileData.cFileName);
			instancingNames.push_back(name);
			manageInstancings[name] = ManageInstancing();
			manageInstancings[name].model.Load(name);
		} while (FindNextFile(hFind, &FindFileData));
	FindClose(hFind);
	}
	*/
	std::string fName = FILE_PATH;
	fName += InstConfName;
	std::ifstream ifs(fName);
	if (!ifs)
	{
		for (auto& name : InstModels)
		{
			manageInstancings[name] = ManageInstancing();
			manageInstancings[name].model.Load(name);
		}
		return;
	}
	std::string line;
	int num = 0;
	char buf[64] = {};
	unsigned int i;
	while (getline(ifs, line)) {
		if ((line[0] == '#'))
			continue;
		std::istringstream stream(line);
		std::string field;
		while (getline(stream, field, ','))
		{
			if ((field[0] == '#'))
			{
				break;
			}
			sscanf(field.c_str(), "%s", buf);
			instancingNames.push_back(buf);
			manageInstancings[buf] = ManageInstancing();
			manageInstancings[buf].model.Load(buf);
		}
	}
	ifs.close();

}


InstancingManager::~InstancingManager()
{
}

int InstancingManager::AddInstance(std::string& _name, InstancingObject* _obj)
{
	int ret = 0;
	if (manageInstancings.find(_name) == manageInstancings.end())
	{
		manageInstancings[_name] = ManageInstancing();
		manageInstancings[_name].model.Load(_name);
		instancingNames.push_back(_name);
	}
	for (auto& instancing : manageInstancings[_name].instancings)
	{
		if (instancing->num < INST_MAX)
		{
			ret += instancing->AddInstance(_obj);
			_obj->SetInstID(ret);
			return ret;
		}
		ret += INST_MAX;
	}
	manageInstancings[_name].instancings.push_back(new Instancing());
	manageInstancings[_name].instancings[ret / INST_MAX]->SetModel(&manageInstancings[_name].model);
	ret += manageInstancings[_name].instancings[ret / INST_MAX]->AddInstance(_obj);
	_obj->SetInstID(ret);
	return ret;
}
void InstancingManager::DelInstance(std::string& _name, int _id)
{
	auto& a = manageInstancings[_name].instancings[_id / INST_MAX];
	a->DelInstance(_id%INST_MAX);
	a->objs[_id%INST_MAX]->SetInstID(_id);
	auto rot = a->objs[_id%INST_MAX]->GetRot();
	a->objs[_id%INST_MAX]->SetRot(rot);
	//manageInstancings[_name].instancings[_id / INST_MAX].DelInstance(_id%INST_MAX);
}
void InstancingManager::SetMatrix(std::string& _name, int _id,XMMATRIX& _matrix)
{
	manageInstancings[_name].instancings[_id / INST_MAX]->SetMatrix(_id%INST_MAX,_matrix);
}
void InstancingManager::Update()
{
	for (auto &manageInstancing : manageInstancings)
	{
		for (auto& instancing : manageInstancing.second.instancings)
		{
			instancing->Update();
		}
	}
}

void InstancingManager::Draw()
{
	motion.MatrixSet();
	for (auto &manageInstancing : manageInstancings)
	{
		for (auto& instancing : manageInstancing.second.instancings)
		{
			//instancing.DrawID();
			instancing->Draw();
		}
	}
}